CREATE TABLE `sillcaf_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_event` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `module_name` text,
  `data_before` text,
  `data_after` text,
  `ip_source` text,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_logs_users1_idx` (`users_id`),
  CONSTRAINT `fk_logs_users1` FOREIGN KEY (`users_id`) REFERENCES `sillcaf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4933 DEFAULT CHARSET=utf8