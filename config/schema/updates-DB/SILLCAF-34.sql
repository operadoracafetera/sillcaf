CREATE TABLE `sillcaf_infonavy_custom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `info_navy_id` int(11) NOT NULL,
  `custom_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sillcaf_infonavy` (`info_navy_id`) USING BTREE,
  CONSTRAINT `fk_sillcaf_infonavy` FOREIGN KEY (`info_navy_id`) REFERENCES `info_navy` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;