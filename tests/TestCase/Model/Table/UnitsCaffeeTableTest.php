<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UnitsCaffeeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UnitsCaffeeTable Test Case
 */
class UnitsCaffeeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UnitsCaffeeTable
     */
    protected $UnitsCaffee;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.UnitsCaffee',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UnitsCaffee') ? [] : ['className' => UnitsCaffeeTable::class];
        $this->UnitsCaffee = TableRegistry::getTableLocator()->get('UnitsCaffee', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->UnitsCaffee);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
