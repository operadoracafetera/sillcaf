<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SillcafPerfilesHasPermissesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SillcafPerfilesHasPermissesTable Test Case
 */
class SillcafPerfilesHasPermissesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SillcafPerfilesHasPermissesTable
     */
    protected $SillcafPerfilesHasPermisses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SillcafPerfilesHasPermisses',
        'app.SillcafPerfiles',
        'app.SillcafPermisses',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SillcafPerfilesHasPermisses') ? [] : ['className' => SillcafPerfilesHasPermissesTable::class];
        $this->SillcafPerfilesHasPermisses = TableRegistry::getTableLocator()->get('SillcafPerfilesHasPermisses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SillcafPerfilesHasPermisses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
