<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PermissesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PermissesTable Test Case
 */
class PermissesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PermissesTable
     */
    protected $Permisses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Permisses',
        'app.Modules',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Permisses') ? [] : ['className' => PermissesTable::class];
        $this->Permisses = TableRegistry::getTableLocator()->get('Permisses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Permisses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
