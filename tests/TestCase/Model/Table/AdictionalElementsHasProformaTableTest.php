<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdictionalElementsHasProformaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdictionalElementsHasProformaTable Test Case
 */
class AdictionalElementsHasProformaTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AdictionalElementsHasProformaTable
     */
    protected $AdictionalElementsHasProforma;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.AdictionalElementsHasProforma',
        'app.AdictionalElements',
        'app.TallyAdictionalInfo',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AdictionalElementsHasProforma') ? [] : ['className' => AdictionalElementsHasProformaTable::class];
        $this->AdictionalElementsHasProforma = TableRegistry::getTableLocator()->get('AdictionalElementsHasProforma', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->AdictionalElementsHasProforma);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
