<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClosingTnTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClosingTnTable Test Case
 */
class ClosingTnTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClosingTnTable
     */
    protected $ClosingTn;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ClosingTn',
        'app.NavyAgent',
        'app.ArrivalsMotorships',
        'app.Terminals',
        'app.SillcafUsers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ClosingTn') ? [] : ['className' => ClosingTnTable::class];
        $this->ClosingTn = TableRegistry::getTableLocator()->get('ClosingTn', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ClosingTn);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
