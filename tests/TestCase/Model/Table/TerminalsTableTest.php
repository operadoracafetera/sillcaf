<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TerminalsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TerminalsTable Test Case
 */
class TerminalsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TerminalsTable
     */
    protected $Terminals;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Terminals',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Terminals') ? [] : ['className' => TerminalsTable::class];
        $this->Terminals = TableRegistry::getTableLocator()->get('Terminals', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Terminals);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
