<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InfoNavyTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InfoNavyTable Test Case
 */
class InfoNavyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\InfoNavyTable
     */
    protected $InfoNavy;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.InfoNavy',
        'app.NavyAgent',
        'app.ShippingLines',
        'app.Customs',
        'app.ArrivalsMotorships',
        'app.StatusInfoNavy',
        'app.SillcafUsers',
        'app.AdictionalElementsHasPackagingCaffee',
        'app.PackagingCaffee',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InfoNavy') ? [] : ['className' => InfoNavyTable::class];
        $this->InfoNavy = TableRegistry::getTableLocator()->get('InfoNavy', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->InfoNavy);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
