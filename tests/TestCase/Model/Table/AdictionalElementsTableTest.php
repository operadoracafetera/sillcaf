<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdictionalElementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdictionalElementsTable Test Case
 */
class AdictionalElementsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AdictionalElementsTable
     */
    protected $AdictionalElements;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.AdictionalElements',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AdictionalElements') ? [] : ['className' => AdictionalElementsTable::class];
        $this->AdictionalElements = TableRegistry::getTableLocator()->get('AdictionalElements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->AdictionalElements);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
