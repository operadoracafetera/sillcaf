<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClosingCnTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClosingCnTable Test Case
 */
class ClosingCnTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ClosingCnTable
     */
    protected $ClosingCn;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ClosingCn',
        'app.Customs',
        'app.ShippingLines',
        'app.NavyAgent',
        'app.ArrivalsMotorships',
        'app.SillcafUsers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ClosingCn') ? [] : ['className' => ClosingCnTable::class];
        $this->ClosingCn = TableRegistry::getTableLocator()->get('ClosingCn', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ClosingCn);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
