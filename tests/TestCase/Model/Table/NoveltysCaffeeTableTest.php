<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NoveltysCaffeeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NoveltysCaffeeTable Test Case
 */
class NoveltysCaffeeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\NoveltysCaffeeTable
     */
    protected $NoveltysCaffee;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.NoveltysCaffee',
        'app.RemittancesCaffeeHasNoveltysCaffee',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('NoveltysCaffee') ? [] : ['className' => NoveltysCaffeeTable::class];
        $this->NoveltysCaffee = TableRegistry::getTableLocator()->get('NoveltysCaffee', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->NoveltysCaffee);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
