<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SillcafModulesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SillcafModulesTable Test Case
 */
class SillcafModulesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SillcafModulesTable
     */
    protected $SillcafModules;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SillcafModules',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SillcafModules') ? [] : ['className' => SillcafModulesTable::class];
        $this->SillcafModules = TableRegistry::getTableLocator()->get('SillcafModules', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SillcafModules);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
