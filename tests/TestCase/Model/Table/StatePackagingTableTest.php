<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StatePackagingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatePackagingTable Test Case
 */
class StatePackagingTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\StatePackagingTable
     */
    protected $StatePackaging;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.StatePackaging',
        'app.PackagingCaffee',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StatePackaging') ? [] : ['className' => StatePackagingTable::class];
        $this->StatePackaging = TableRegistry::getTableLocator()->get('StatePackaging', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->StatePackaging);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
