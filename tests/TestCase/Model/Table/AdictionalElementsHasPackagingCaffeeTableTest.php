<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdictionalElementsHasPackagingCaffeeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdictionalElementsHasPackagingCaffeeTable Test Case
 */
class AdictionalElementsHasPackagingCaffeeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\AdictionalElementsHasPackagingCaffeeTable
     */
    protected $AdictionalElementsHasPackagingCaffee;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.AdictionalElementsHasPackagingCaffee',
        'app.AdictionalElements',
        'app.InfoNavy',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AdictionalElementsHasPackagingCaffee') ? [] : ['className' => AdictionalElementsHasPackagingCaffeeTable::class];
        $this->AdictionalElementsHasPackagingCaffee = TableRegistry::getTableLocator()->get('AdictionalElementsHasPackagingCaffee', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->AdictionalElementsHasPackagingCaffee);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
