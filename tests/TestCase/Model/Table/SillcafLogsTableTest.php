<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SillcafLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SillcafLogsTable Test Case
 */
class SillcafLogsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SillcafLogsTable
     */
    protected $SillcafLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SillcafLogs',
        'app.SillcafPerfiles',
        'app.SillCafUsers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SillcafLogs') ? [] : ['className' => SillcafLogsTable::class];
        $this->SillcafLogs = TableRegistry::getTableLocator()->get('SillcafLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SillcafLogs);

        parent::tearDown();
    }
}
