<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DetailPackakgingCrossdockingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DetailPackakgingCrossdockingTable Test Case
 */
class DetailPackakgingCrossdockingTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DetailPackakgingCrossdockingTable
     */
    protected $DetailPackakgingCrossdocking;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.DetailPackakgingCrossdocking',
        'app.RemittancesCaffee',
        'app.PackagingCaffee',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DetailPackakgingCrossdocking') ? [] : ['className' => DetailPackakgingCrossdockingTable::class];
        $this->DetailPackakgingCrossdocking = TableRegistry::getTableLocator()->get('DetailPackakgingCrossdocking', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->DetailPackakgingCrossdocking);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
