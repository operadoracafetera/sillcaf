<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SillcafPermissesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SillcafPermissesTable Test Case
 */
class SillcafPermissesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SillcafPermissesTable
     */
    protected $SillcafPermisses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SillcafPermisses',
        'app.SillcafModules',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SillcafPermisses') ? [] : ['className' => SillcafPermissesTable::class];
        $this->SillcafPermisses = TableRegistry::getTableLocator()->get('SillcafPermisses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SillcafPermisses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
