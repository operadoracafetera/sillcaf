<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArrivalsMotorshipsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArrivalsMotorshipsTable Test Case
 */
class ArrivalsMotorshipsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ArrivalsMotorshipsTable
     */
    protected $ArrivalsMotorships;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ArrivalsMotorships',
        'app.Motorships',
        'app.Terminals',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArrivalsMotorships') ? [] : ['className' => ArrivalsMotorshipsTable::class];
        $this->ArrivalsMotorships = TableRegistry::getTableLocator()->get('ArrivalsMotorships', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ArrivalsMotorships);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
