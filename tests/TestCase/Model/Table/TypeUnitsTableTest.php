<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypeUnitsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypeUnitsTable Test Case
 */
class TypeUnitsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TypeUnitsTable
     */
    protected $TypeUnits;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.TypeUnits',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypeUnits') ? [] : ['className' => TypeUnitsTable::class];
        $this->TypeUnits = TableRegistry::getTableLocator()->get('TypeUnits', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->TypeUnits);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
