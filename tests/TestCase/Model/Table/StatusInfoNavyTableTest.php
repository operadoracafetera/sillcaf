<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StatusInfoNavyTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatusInfoNavyTable Test Case
 */
class StatusInfoNavyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\StatusInfoNavyTable
     */
    protected $StatusInfoNavy;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.StatusInfoNavy',
        'app.InfoNavy',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StatusInfoNavy') ? [] : ['className' => StatusInfoNavyTable::class];
        $this->StatusInfoNavy = TableRegistry::getTableLocator()->get('StatusInfoNavy', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->StatusInfoNavy);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
