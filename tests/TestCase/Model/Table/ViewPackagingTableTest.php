<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewPackagingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewPackagingTable Test Case
 */
class ViewPackagingTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewPackagingTable
     */
    protected $ViewPackaging;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ViewPackaging',
        'app.TraceabilityPackagings',
        'app.NavyAgents',
        'app.ShippingLines',
        'app.StatePackagings',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ViewPackaging') ? [] : ['className' => ViewPackagingTable::class];
        $this->ViewPackaging = TableRegistry::getTableLocator()->get('ViewPackaging', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ViewPackaging);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
