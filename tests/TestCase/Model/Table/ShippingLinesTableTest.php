<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ShippingLinesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ShippingLinesTable Test Case
 */
class ShippingLinesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ShippingLinesTable
     */
    protected $ShippingLines;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ShippingLines',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ShippingLines') ? [] : ['className' => ShippingLinesTable::class];
        $this->ShippingLines = TableRegistry::getTableLocator()->get('ShippingLines', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ShippingLines);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
