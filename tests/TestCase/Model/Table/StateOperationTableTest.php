<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StateOperationTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StateOperationTable Test Case
 */
class StateOperationTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\StateOperationTable
     */
    protected $StateOperation;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.StateOperation',
        'app.RemittancesCaffee',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StateOperation') ? [] : ['className' => StateOperationTable::class];
        $this->StateOperation = TableRegistry::getTableLocator()->get('StateOperation', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->StateOperation);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
