<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NavyAgentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NavyAgentTable Test Case
 */
class NavyAgentTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\NavyAgentTable
     */
    protected $NavyAgent;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.NavyAgent',
        'app.InfoNavy',
        'app.ViewPackaging',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('NavyAgent') ? [] : ['className' => NavyAgentTable::class];
        $this->NavyAgent = TableRegistry::getTableLocator()->get('NavyAgent', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->NavyAgent);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
