<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RemittancesCaffeeHasNoveltysCaffeeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RemittancesCaffeeHasNoveltysCaffeeTable Test Case
 */
class RemittancesCaffeeHasNoveltysCaffeeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RemittancesCaffeeHasNoveltysCaffeeTable
     */
    protected $RemittancesCaffeeHasNoveltysCaffee;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.RemittancesCaffeeHasNoveltysCaffee',
        'app.RemittancesCaffee',
        'app.NoveltysCaffee',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RemittancesCaffeeHasNoveltysCaffee') ? [] : ['className' => RemittancesCaffeeHasNoveltysCaffeeTable::class];
        $this->RemittancesCaffeeHasNoveltysCaffee = TableRegistry::getTableLocator()->get('RemittancesCaffeeHasNoveltysCaffee', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->RemittancesCaffeeHasNoveltysCaffee);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
