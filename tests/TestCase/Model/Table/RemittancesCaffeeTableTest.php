<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RemittancesCaffeeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RemittancesCaffeeTable Test Case
 */
class RemittancesCaffeeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RemittancesCaffeeTable
     */
    protected $RemittancesCaffee;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.RemittancesCaffee',
        'app.Guides',
        'app.Clients',
        'app.UnitsCaffee',
        'app.PackingCaffee',
        'app.MarkCaffee',
        'app.Customs',
        'app.CitySource',
        'app.Shippers',
        'app.SlotStore',
        'app.Users',
        'app.StateOperation',
        'app.NoveltyInCaffee',
        'app.TraceabilityDownload',
        'app.TypeUnits',
        'app.Cargolots',
        'app.Bill',
        'app.DetailPackagingCaffee',
        'app.DetailPackakgingCrossdocking',
        'app.DetailsServicesToCaffee',
        'app.RemittancesCaffeeHasFumigationServices',
        'app.RemittancesCaffeeHasNoveltysCaffee',
        'app.SampleRemmitances',
        'app.WeighingDownloadCaffee',
        'app.WeighingEmptyCoffee',
        'app.WeighingReturnCoffee',
        'app.ReturnsCaffees',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RemittancesCaffee') ? [] : ['className' => RemittancesCaffeeTable::class];
        $this->RemittancesCaffee = TableRegistry::getTableLocator()->get('RemittancesCaffee', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->RemittancesCaffee);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
