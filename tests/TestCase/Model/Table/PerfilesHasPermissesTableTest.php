<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PerfilesHasPermissesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PerfilesHasPermissesTable Test Case
 */
class PerfilesHasPermissesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PerfilesHasPermissesTable
     */
    protected $PerfilesHasPermisses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.PerfilesHasPermisses',
        'app.Perfiles',
        'app.Permisses',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PerfilesHasPermisses') ? [] : ['className' => PerfilesHasPermissesTable::class];
        $this->PerfilesHasPermisses = TableRegistry::getTableLocator()->get('PerfilesHasPermisses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->PerfilesHasPermisses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
