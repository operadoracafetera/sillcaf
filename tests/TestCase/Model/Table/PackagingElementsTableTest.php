<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PackagingElementsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PackagingElementsTable Test Case
 */
class PackagingElementsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PackagingElementsTable
     */
    protected $PackagingElements;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.PackagingElements',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PackagingElements') ? [] : ['className' => PackagingElementsTable::class];
        $this->PackagingElements = TableRegistry::getTableLocator()->get('PackagingElements', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->PackagingElements);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
