<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DetailPackagingCaffeeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DetailPackagingCaffeeTable Test Case
 */
class DetailPackagingCaffeeTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DetailPackagingCaffeeTable
     */
    protected $DetailPackagingCaffee;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.DetailPackagingCaffee',
        'app.RemittancesCaffee',
        'app.PackagingCaffee',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DetailPackagingCaffee') ? [] : ['className' => DetailPackagingCaffeeTable::class];
        $this->DetailPackagingCaffee = TableRegistry::getTableLocator()->get('DetailPackagingCaffee', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->DetailPackagingCaffee);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
