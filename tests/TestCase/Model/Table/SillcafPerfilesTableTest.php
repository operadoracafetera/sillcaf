<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SillcafPerfilesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SillcafPerfilesTable Test Case
 */
class SillcafPerfilesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SillcafPerfilesTable
     */
    protected $SillcafPerfiles;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SillcafPerfiles',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SillcafPerfiles') ? [] : ['className' => SillcafPerfilesTable::class];
        $this->SillcafPerfiles = TableRegistry::getTableLocator()->get('SillcafPerfiles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SillcafPerfiles);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
