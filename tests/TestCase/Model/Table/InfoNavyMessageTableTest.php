<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InfoNavyMessageTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InfoNavyMessageTable Test Case
 */
class InfoNavyMessageTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\InfoNavyMessageTable
     */
    protected $InfoNavyMessage;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.InfoNavyMessage',
        'app.InfoNavy',
        'app.SillcafUsers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('InfoNavyMessage') ? [] : ['className' => InfoNavyMessageTable::class];
        $this->InfoNavyMessage = TableRegistry::getTableLocator()->get('InfoNavyMessage', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->InfoNavyMessage);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
