<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\InfoNavyController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\InfoNavyController Test Case
 *
 * @uses \App\Controller\InfoNavyController
 */
class InfoNavyControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.InfoNavy',
        'app.NavyAgent',
        'app.ShippingLines',
        'app.Customs',
        'app.ArrivalsMotorships',
        'app.StatusInfoNavy',
        'app.SillcafUsers',
        'app.AdictionalElementsHasPackagingCaffee',
        'app.PackagingCaffee',
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
