<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\RemittancesCaffeeController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\RemittancesCaffeeController Test Case
 *
 * @uses \App\Controller\RemittancesCaffeeController
 */
class RemittancesCaffeeControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.RemittancesCaffee',
        'app.Guides',
        'app.Clients',
        'app.UnitsCaffee',
        'app.PackingCaffee',
        'app.MarkCaffee',
        'app.Customs',
        'app.CitySource',
        'app.Shippers',
        'app.SlotStore',
        'app.Users',
        'app.StateOperation',
        'app.NoveltyInCaffee',
        'app.TraceabilityDownload',
        'app.TypeUnits',
        'app.Cargolots',
        'app.Bill',
        'app.DetailPackagingCaffee',
        'app.DetailPackakgingCrossdocking',
        'app.DetailsServicesToCaffee',
        'app.RemittancesCaffeeHasFumigationServices',
        'app.RemittancesCaffeeHasNoveltysCaffee',
        'app.SampleRemmitances',
        'app.WeighingDownloadCaffee',
        'app.WeighingEmptyCoffee',
        'app.WeighingReturnCoffee',
        'app.ReturnsCaffees',
        'app.RemittancesCaffeeReturnsCaffees',
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
