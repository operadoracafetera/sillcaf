<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DetailPackagingCaffeeFixture
 */
class DetailPackagingCaffeeFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'detail_packaging_caffee';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'remittances_caffee_id' => ['type' => 'integer', 'length' => 7, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'packaging_caffee_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity_radicated_bag_out' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'updated_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'state' => ['type' => 'string', 'length' => 2, 'null' => false, 'default' => '0', 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'tara_packaging' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'users_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'lot_coffee' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_remittances_caffee_has_packaging_caffee_packaging_caffee_idx' => ['type' => 'index', 'columns' => ['packaging_caffee_id'], 'length' => []],
            'fk_remittances_caffee_has_packaging_caffee_remittances_caff_idx' => ['type' => 'index', 'columns' => ['remittances_caffee_id'], 'length' => []],
            'fk_detail_packaging_caffee_users1_idx' => ['type' => 'index', 'columns' => ['users_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['remittances_caffee_id', 'packaging_caffee_id'], 'length' => []],
            'fk_detail_packaging_caffee_users1' => ['type' => 'foreign', 'columns' => ['users_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_remittances_caffee_has_packaging_caffee_packaging_caffee1' => ['type' => 'foreign', 'columns' => ['packaging_caffee_id'], 'references' => ['packaging_caffee', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_remittances_caffee_has_packaging_caffee_remittances_caffee1' => ['type' => 'foreign', 'columns' => ['remittances_caffee_id'], 'references' => ['remittances_caffee', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'remittances_caffee_id' => 1,
                'packaging_caffee_id' => 1,
                'quantity_radicated_bag_out' => 1,
                'created_date' => '2020-06-25 14:13:40',
                'updated_date' => '2020-06-25 14:13:40',
                'state' => 'Lo',
                'tara_packaging' => 1,
                'users_id' => 1,
                'lot_coffee' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
