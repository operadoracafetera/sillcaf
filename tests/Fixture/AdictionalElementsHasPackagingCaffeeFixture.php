<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AdictionalElementsHasPackagingCaffeeFixture
 */
class AdictionalElementsHasPackagingCaffeeFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'adictional_elements_has_packaging_caffee';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'adictional_elements_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'type_unit' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'info_navy_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'observation' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'sillcaf_user_reg_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_adictional_elements_has_packaging_caffee_adictional_elem_idx' => ['type' => 'index', 'columns' => ['adictional_elements_id'], 'length' => []],
            'fk_adictional_elements_has_packaging_caffee_info_navy1_idx' => ['type' => 'index', 'columns' => ['info_navy_id'], 'length' => []],
            'fk_adictional_elements_has_packaging_caffee_sillcaf_user' => ['type' => 'index', 'columns' => ['sillcaf_user_reg_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['adictional_elements_id', 'info_navy_id'], 'length' => []],
            'fk_adictional_elements_has_packaging_caffee_adictional_elemen1' => ['type' => 'foreign', 'columns' => ['adictional_elements_id'], 'references' => ['adictional_elements', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_adictional_elements_has_packaging_caffee_info_navy1' => ['type' => 'foreign', 'columns' => ['info_navy_id'], 'references' => ['info_navy', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_adictional_elements_has_packaging_caffee_sillcaf_user' => ['type' => 'foreign', 'columns' => ['sillcaf_user_reg_id'], 'references' => ['sillcaf_users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'adictional_elements_id' => 1,
                'quantity' => 1,
                'type_unit' => 'Lorem ipsum dolor sit amet',
                'info_navy_id' => 1,
                'observation' => 'Lorem ipsum dolor sit amet',
                'sillcaf_user_reg_id' => 1,
            ],
        ];
        parent::init();
    }
}
