<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ClosingTnFixture
 */
class ClosingTnFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'closing_tn';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'closing_tn_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'closing_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'arrivals_motorships_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'closing_tn_terminal_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'closing_tn_user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'closing_tn_reg_date' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'current_timestamp()', 'comment' => ''],
        'closing_tn_update_date' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'current_timestamp()', 'comment' => ''],
        '_indexes' => [
            'fk_closing_TN_arrivals_motorships1_idx' => ['type' => 'index', 'columns' => ['arrivals_motorships_id'], 'length' => []],
            'fk_closing_TN_terminal_1dx' => ['type' => 'index', 'columns' => ['closing_tn_terminal_id'], 'length' => []],
            'fk_closing_TN_user1_idx' => ['type' => 'index', 'columns' => ['closing_tn_user_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['closing_tn_id'], 'length' => []],
            'fk_closing_TN_arrivals_motorships1' => ['type' => 'foreign', 'columns' => ['arrivals_motorships_id'], 'references' => ['arrivals_motorships', 'arrivals_motorships_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_closing_TN_arrivals_user1' => ['type' => 'foreign', 'columns' => ['closing_tn_user_id'], 'references' => ['sillcaf_users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_closing_TN_terminal1' => ['type' => 'foreign', 'columns' => ['closing_tn_terminal_id'], 'references' => ['terminals', 'id_terminals'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_general_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'closing_tn_id' => 1,
                'closing_date' => '2020-07-24 15:32:11',
                'arrivals_motorships_id' => 1,
                'closing_tn_terminal_id' => 1,
                'closing_tn_user_id' => 1,
                'closing_tn_reg_date' => 1595622731,
                'closing_tn_update_date' => 1595622731,
            ],
        ];
        parent::init();
    }
}
