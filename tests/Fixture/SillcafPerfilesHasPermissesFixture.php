<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SillcafPerfilesHasPermissesFixture
 */
class SillcafPerfilesHasPermissesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'perfil_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'permisse_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'reg_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'current_timestamp()', 'comment' => ''],
        'active' => ['type' => 'tinyinteger', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_perfiles_has_permisses_idx' => ['type' => 'index', 'columns' => ['perfil_id'], 'length' => []],
            'fk_perfiles_has_permisse_idx' => ['type' => 'index', 'columns' => ['permisse_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_perfiles_has_perfil_id' => ['type' => 'foreign', 'columns' => ['perfil_id'], 'references' => ['sillcaf_perfiles', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_perfiles_has_permisse_id' => ['type' => 'foreign', 'columns' => ['permisse_id'], 'references' => ['sillcaf_permisses', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'perfil_id' => 1,
                'permisse_id' => 1,
                'reg_date' => '2020-06-24 15:24:55',
                'active' => 1,
            ],
        ];
        parent::init();
    }
}
