<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PackagingCaffeeFixture
 */
class PackagingCaffeeFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'packaging_caffee';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'created_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'weight_to_out' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
        'state_packaging_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ready_container_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
        'cause_emptied_ctn_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'info_navy_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'observation' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'packaging_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'cancelled_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'iso_ctn' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'seal_1' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'seal_2' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'users_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'seal_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'seal1_path' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'seal2_path' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'img_ctn' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'checking_ctn_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'traceability_packaging_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'exporter_cod' => ['type' => 'string', 'length' => 4, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'weight_net_cont' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'time_start' => ['type' => 'time', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'time_end' => ['type' => 'time', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'autorization' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'qta_bags_packaging' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'rem_ref' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'emptied_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'seal_3' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'seal_4' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'jetty' => ['type' => 'string', 'length' => 300, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'jornally' => ['type' => 'string', 'length' => 5, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'empty_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'ref_copcsa_gang' => ['type' => 'string', 'length' => 1, 'null' => true, 'default' => '0', 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'consolidate' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'long_ctn' => ['type' => 'string', 'length' => 2, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'msg_spb_navis' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'rx_status_spb_navis' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'rx_date_spb_navis' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'upload_tracking_photos_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        'user_tarja' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_driver' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cooperativa_ctg' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'note_opera' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_packaging_caffee_state_packaging1_idx' => ['type' => 'index', 'columns' => ['state_packaging_id'], 'length' => []],
            'fk_packaging_caffee_ready_container1_idx' => ['type' => 'index', 'columns' => ['ready_container_id'], 'length' => []],
            'fk_packaging_caffee_cause_emptied_ctn1_idx' => ['type' => 'index', 'columns' => ['cause_emptied_ctn_id'], 'length' => []],
            'fk_packaging_caffee_info_navy1_idx' => ['type' => 'index', 'columns' => ['info_navy_id'], 'length' => []],
            'fk_packaging_caffee_users1_idx' => ['type' => 'index', 'columns' => ['users_id'], 'length' => []],
            'checking_ctn_id' => ['type' => 'index', 'columns' => ['checking_ctn_id'], 'length' => []],
            'fk_packaging_caffee_traceability_packaging1_idx' => ['type' => 'index', 'columns' => ['traceability_packaging_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_packaging_caffee_cause_emptied_ctn1' => ['type' => 'foreign', 'columns' => ['cause_emptied_ctn_id'], 'references' => ['cause_emptied_ctn', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_packaging_caffee_info_navy1' => ['type' => 'foreign', 'columns' => ['info_navy_id'], 'references' => ['info_navy', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_packaging_caffee_ready_container1' => ['type' => 'foreign', 'columns' => ['ready_container_id'], 'references' => ['ready_container', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_packaging_caffee_state_packaging1' => ['type' => 'foreign', 'columns' => ['state_packaging_id'], 'references' => ['state_packaging', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_packaging_caffee_traceability_packaging1' => ['type' => 'foreign', 'columns' => ['traceability_packaging_id'], 'references' => ['traceability_packaging', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_packaging_caffee_users1' => ['type' => 'foreign', 'columns' => ['users_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'packaging_caffee_ibfk_1' => ['type' => 'foreign', 'columns' => ['checking_ctn_id'], 'references' => ['checking_ctn', 'id'], 'update' => 'restrict', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'created_date' => '2020-06-25 13:41:44',
                'weight_to_out' => 1,
                'state_packaging_id' => 1,
                'ready_container_id' => 1,
                'active' => 1,
                'cause_emptied_ctn_id' => 1,
                'info_navy_id' => 1,
                'observation' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'packaging_date' => '2020-06-25 13:41:44',
                'cancelled_date' => '2020-06-25 13:41:44',
                'iso_ctn' => 'Lorem ipsum dolor sit amet',
                'seal_1' => 'Lorem ipsum dolor sit amet',
                'seal_2' => 'Lorem ipsum dolor sit amet',
                'users_id' => 1,
                'seal_date' => '2020-06-25 13:41:44',
                'seal1_path' => 'Lorem ipsum dolor sit amet',
                'seal2_path' => 'Lorem ipsum dolor sit amet',
                'img_ctn' => 'Lorem ipsum dolor sit amet',
                'checking_ctn_id' => 1,
                'traceability_packaging_id' => 1,
                'exporter_cod' => 'Lo',
                'weight_net_cont' => 1,
                'time_start' => '13:41:44',
                'time_end' => '13:41:44',
                'autorization' => 1,
                'qta_bags_packaging' => 1,
                'rem_ref' => 1,
                'emptied_date' => '2020-06-25 13:41:44',
                'seal_3' => 'Lorem ipsum dolor sit amet',
                'seal_4' => 'Lorem ipsum dolor sit amet',
                'jetty' => 'Lorem ipsum dolor sit amet',
                'jornally' => 'Lor',
                'empty_date' => '2020-06-25 13:41:44',
                'ref_copcsa_gang' => 'L',
                'consolidate' => 1,
                'long_ctn' => 'Lo',
                'msg_spb_navis' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'rx_status_spb_navis' => 1,
                'rx_date_spb_navis' => '2020-06-25 13:41:44',
                'upload_tracking_photos_date' => '2020-06-25 13:41:44',
                'user_tarja' => 1,
                'user_driver' => 1,
                'cooperativa_ctg' => 'Lorem ip',
                'note_opera' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            ],
        ];
        parent::init();
    }
}
