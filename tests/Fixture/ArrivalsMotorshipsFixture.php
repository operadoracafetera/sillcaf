<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ArrivalsMotorshipsFixture
 */
class ArrivalsMotorshipsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'arrivals_motorships_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'visit_number' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_general_ci', 'comment' => '', 'precision' => null],
        'vissel' => ['type' => 'string', 'length' => 5, 'null' => false, 'default' => null, 'collate' => 'utf8mb4_general_ci', 'comment' => '', 'precision' => null],
        'arrival_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'arrivals_motorships_motorships_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'arrivals_motorships_terminal_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'arrivals_motorships_user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'arrivals_motorships_reg_date' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'current_timestamp()', 'comment' => ''],
        'arrivals_motorships_update_date' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'current_timestamp()', 'comment' => ''],
        'arrivals_motorships_active' => ['type' => 'tinyinteger', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_visit_motorship_motorships1_idx' => ['type' => 'index', 'columns' => ['arrivals_motorships_terminal_id'], 'length' => []],
            'fk_visit_motorship_motorships2_idx' => ['type' => 'index', 'columns' => ['arrivals_motorships_motorships_id'], 'length' => []],
            'fk_visit_motorship_motorships3_idx' => ['type' => 'index', 'columns' => ['arrivals_motorships_user_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['arrivals_motorships_id'], 'length' => []],
            'visit_number_UNIQUE' => ['type' => 'unique', 'columns' => ['visit_number'], 'length' => []],
            'vissel_UNIQUE' => ['type' => 'unique', 'columns' => ['vissel'], 'length' => []],
            'fk_visit_motorship_motorships1' => ['type' => 'foreign', 'columns' => ['arrivals_motorships_motorships_id'], 'references' => ['motorships', 'id_motorship'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_visit_motorship_terminal1' => ['type' => 'foreign', 'columns' => ['arrivals_motorships_terminal_id'], 'references' => ['terminals', 'id_terminals'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_visit_motorship_user1' => ['type' => 'foreign', 'columns' => ['arrivals_motorships_user_id'], 'references' => ['sillcaf_users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_general_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'arrivals_motorships_id' => 1,
                'visit_number' => 'Lorem ipsum dolor sit amet',
                'vissel' => 'Lor',
                'arrival_date' => '2020-06-26 21:39:13',
                'arrivals_motorships_motorships_id' => 1,
                'arrivals_motorships_terminal_id' => 1,
                'arrivals_motorships_user_id' => 1,
                'arrivals_motorships_reg_date' => 1593207553,
                'arrivals_motorships_update_date' => 1593207553,
                'arrivals_motorships_active' => 1,
            ],
        ];
        parent::init();
    }
}
