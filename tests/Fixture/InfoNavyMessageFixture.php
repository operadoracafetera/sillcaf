<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InfoNavyMessageFixture
 */
class InfoNavyMessageFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'info_navy_message';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'info_navy_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'message' => ['type' => 'text', 'length' => null, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'reg_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'current_timestamp()', 'comment' => ''],
        'sillcaf_user_reg_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_info_navy_message_info_navy_idx' => ['type' => 'index', 'columns' => ['info_navy_id'], 'length' => []],
            'fk_info_navy_message_user_reg_id_idx' => ['type' => 'index', 'columns' => ['sillcaf_user_reg_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_info_navy_message_info_navy' => ['type' => 'foreign', 'columns' => ['info_navy_id'], 'references' => ['info_navy', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_info_navy_message_user_reg_id' => ['type' => 'foreign', 'columns' => ['sillcaf_user_reg_id'], 'references' => ['sillcaf_users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'info_navy_id' => 1,
                'message' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'reg_date' => '2020-07-20 13:43:10',
                'sillcaf_user_reg_id' => 1,
            ],
        ];
        parent::init();
    }
}
