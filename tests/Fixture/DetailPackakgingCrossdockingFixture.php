<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DetailPackakgingCrossdockingFixture
 */
class DetailPackakgingCrossdockingFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'detail_packakging_crossdocking';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'remittances_caffee_id' => ['type' => 'integer', 'length' => 7, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'packaging_caffee_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'lot_coffee' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'qta_coffee' => ['type' => 'integer', 'length' => 10, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'reg_date' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'current_timestamp()', 'comment' => ''],
        'status' => ['type' => 'tinyinteger', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_detail_packakging_crossdocking_packaging_caffee_idx' => ['type' => 'index', 'columns' => ['packaging_caffee_id'], 'length' => []],
            'fk_detail_packakging_crossdocking_remittances_caffee_id_idx' => ['type' => 'index', 'columns' => ['remittances_caffee_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_detail_packakging_crossdocking_packaging_caffee_idx_serv1' => ['type' => 'foreign', 'columns' => ['packaging_caffee_id'], 'references' => ['packaging_caffee', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_detail_packakging_crossdocking_remittances_caffee_id_caf1' => ['type' => 'foreign', 'columns' => ['remittances_caffee_id'], 'references' => ['remittances_caffee', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'remittances_caffee_id' => 1,
                'packaging_caffee_id' => 1,
                'lot_coffee' => 'Lorem ip',
                'qta_coffee' => 1,
                'reg_date' => 1594915162,
                'status' => 1,
            ],
        ];
        parent::init();
    }
}
