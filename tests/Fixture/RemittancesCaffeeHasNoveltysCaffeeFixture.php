<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RemittancesCaffeeHasNoveltysCaffeeFixture
 */
class RemittancesCaffeeHasNoveltysCaffeeFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'remittances_caffee_has_noveltys_caffee';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'remittances_caffee_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'noveltys_caffee_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_remittances_cafee_has_noveltys_cafee_noveltys_cafee1_idx' => ['type' => 'index', 'columns' => ['noveltys_caffee_id'], 'length' => []],
            'fk_remittances_cafee_has_noveltys_cafee_remittances_cafee1_idx' => ['type' => 'index', 'columns' => ['remittances_caffee_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['remittances_caffee_id', 'noveltys_caffee_id'], 'length' => []],
            'fk_remittances_cafee_has_noveltys_cafee_noveltys_cafee1' => ['type' => 'foreign', 'columns' => ['noveltys_caffee_id'], 'references' => ['noveltys_caffee', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_remittances_caffee_has_noveltys_cafee_remittances_cafee1' => ['type' => 'foreign', 'columns' => ['remittances_caffee_id'], 'references' => ['remittances_caffee', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'remittances_caffee_id' => 1,
                'noveltys_caffee_id' => 1,
                'created_date' => '2021-02-22 00:06:15',
                'active' => 1,
            ],
        ];
        parent::init();
    }
}
