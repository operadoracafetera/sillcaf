<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AdictionalElementsHasProformaFixture
 */
class AdictionalElementsHasProformaFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'adictional_elements_has_proforma';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'adictional_elements_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tally_adictional_info_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'type_unit' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'observation' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_adictional_elements_has_proforma_adictional_elements_idx' => ['type' => 'index', 'columns' => ['adictional_elements_id'], 'length' => []],
            'fk_adictional_elements_has_proforma_tally_adictional_info_idx' => ['type' => 'index', 'columns' => ['tally_adictional_info_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['adictional_elements_id', 'tally_adictional_info_id'], 'length' => []],
            'fk_adictional_elements_has_proforma_adictional_elements_id1' => ['type' => 'foreign', 'columns' => ['adictional_elements_id'], 'references' => ['adictional_elements', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_adictional_elements_has_proforma_tally_adictional_info_id1' => ['type' => 'foreign', 'columns' => ['tally_adictional_info_id'], 'references' => ['tally_adictional_info', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'adictional_elements_id' => 1,
                'tally_adictional_info_id' => 1,
                'quantity' => 1,
                'type_unit' => 'Lorem ipsum dolor sit amet',
                'observation' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
