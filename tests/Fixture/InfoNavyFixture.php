<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InfoNavyFixture
 */
class InfoNavyFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'info_navy';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'proforma' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'packaging_type' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'motorship_name' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'packaging_mode' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'booking' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'travel_num' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'destiny' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'navy_agent_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'shipping_lines_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'customs_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'observation' => ['type' => 'string', 'length' => 300, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'created_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'elements_adicional' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'long_container' => ['type' => 'string', 'length' => 5, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'type_papper' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'adictional_dry_bags' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'adictional_layer_kraft_20' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'adictional_layer_kraft_40' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'adictional_layer_carton_20' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'adictional_layer_carton_40' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'adictional_num_label' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'adictional_num_stiker' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'adictional_num_manta' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tsl' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'rejillas' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dataloger' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'absortes' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pallets' => ['type' => 'integer', 'length' => 2, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'hc_qta' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tls_desiccant' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'seals' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'arrivals_motorships_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'status_info_navy_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'info_navy_active' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'sillcaf_user_reg_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sillcaf_user_approve_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_info_navy_navy_agent1_idx' => ['type' => 'index', 'columns' => ['navy_agent_id'], 'length' => []],
            'fk_info_navy_shipping_lines1_idx' => ['type' => 'index', 'columns' => ['shipping_lines_id'], 'length' => []],
            'fk_info_navy_customs1_idx' => ['type' => 'index', 'columns' => ['customs_id'], 'length' => []],
            'fk_info_navy_arrivals_motorships1' => ['type' => 'index', 'columns' => ['arrivals_motorships_id'], 'length' => []],
            'fk_info_navy_status1' => ['type' => 'index', 'columns' => ['status_info_navy_id'], 'length' => []],
            'fk_info_navy_user_approve_id' => ['type' => 'index', 'columns' => ['sillcaf_user_approve_id'], 'length' => []],
            'fk_info_navy_user_reg_id' => ['type' => 'index', 'columns' => ['sillcaf_user_reg_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_info_navy_arrivals_motorships1' => ['type' => 'foreign', 'columns' => ['arrivals_motorships_id'], 'references' => ['arrivals_motorships', 'arrivals_motorships_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_info_navy_customs1' => ['type' => 'foreign', 'columns' => ['customs_id'], 'references' => ['customs', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_info_navy_navy_agent1' => ['type' => 'foreign', 'columns' => ['navy_agent_id'], 'references' => ['navy_agent', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_info_navy_shipping_lines1' => ['type' => 'foreign', 'columns' => ['shipping_lines_id'], 'references' => ['shipping_lines', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_info_navy_status1' => ['type' => 'foreign', 'columns' => ['status_info_navy_id'], 'references' => ['status_info_navy', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_info_navy_user_approve_id' => ['type' => 'foreign', 'columns' => ['sillcaf_user_approve_id'], 'references' => ['sillcaf_users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_info_navy_user_reg_id' => ['type' => 'foreign', 'columns' => ['sillcaf_user_reg_id'], 'references' => ['sillcaf_users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'proforma' => 'Lorem ipsum dolor sit amet',
                'packaging_type' => 'Lorem ipsum dolor sit amet',
                'motorship_name' => 'Lorem ipsum dolor sit amet',
                'packaging_mode' => 'Lorem ipsum dolor sit amet',
                'booking' => 'Lorem ipsum dolor sit amet',
                'travel_num' => 'Lorem ipsum dolor sit amet',
                'destiny' => 'Lorem ipsum dolor sit amet',
                'navy_agent_id' => 1,
                'shipping_lines_id' => 1,
                'customs_id' => 1,
                'observation' => 'Lorem ipsum dolor sit amet',
                'created_date' => '2020-07-19 19:41:42',
                'elements_adicional' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
                'long_container' => 'Lor',
                'type_papper' => 'Lorem ipsum dolor sit amet',
                'adictional_dry_bags' => 1,
                'adictional_layer_kraft_20' => 1,
                'adictional_layer_kraft_40' => 1,
                'adictional_layer_carton_20' => 1,
                'adictional_layer_carton_40' => 1,
                'adictional_num_label' => 1,
                'adictional_num_stiker' => 1,
                'adictional_num_manta' => 1,
                'tsl' => 1,
                'rejillas' => 1,
                'dataloger' => 1,
                'absortes' => 1,
                'pallets' => 1,
                'hc_qta' => 1,
                'tls_desiccant' => 1,
                'seals' => 1,
                'arrivals_motorships_id' => 1,
                'status_info_navy_id' => 1,
                'info_navy_active' => 1,
                'sillcaf_user_reg_id' => 1,
                'sillcaf_user_approve_id' => 1,
            ],
        ];
        parent::init();
    }
}
