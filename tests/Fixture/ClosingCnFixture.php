<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ClosingCnFixture
 */
class ClosingCnFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'closing_cn';
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'closing_cn_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'closing_date' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'closing_cn_navy_agent_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'arrivals_motorships_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'closing_cn_user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'closing_cn_reg_date' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'current_timestamp()', 'comment' => ''],
        'closing_cn_update_date' => ['type' => 'timestamp', 'length' => null, 'precision' => null, 'null' => false, 'default' => 'current_timestamp()', 'comment' => ''],
        '_indexes' => [
            'fk_closing_CN_navy_agent1_idx' => ['type' => 'index', 'columns' => ['closing_cn_navy_agent_id'], 'length' => []],
            'fk_closing_CN_arrivals_motorships1_idx' => ['type' => 'index', 'columns' => ['arrivals_motorships_id'], 'length' => []],
            'fk_closing_CN_shippingline1_idx' => ['type' => 'index', 'columns' => ['arrivals_motorships_id'], 'length' => []],
            'fk_closing_CN_user1_idx' => ['type' => 'index', 'columns' => ['closing_cn_user_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['closing_cn_id'], 'length' => []],
            'fk_closing_CN_arrivals_motorships1' => ['type' => 'foreign', 'columns' => ['arrivals_motorships_id'], 'references' => ['arrivals_motorships', 'arrivals_motorships_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_closing_CN_arrivals_user1' => ['type' => 'foreign', 'columns' => ['closing_cn_user_id'], 'references' => ['sillcaf_users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_closing_CN_navy_agents1' => ['type' => 'foreign', 'columns' => ['closing_cn_navy_agent_id'], 'references' => ['navy_agent', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8mb4_general_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'closing_cn_id' => 1,
                'closing_date' => '2020-07-23 16:23:03',
                'closing_cn_navy_agent_id' => 1,
                'arrivals_motorships_id' => 1,
                'closing_cn_user_id' => 1,
                'closing_cn_reg_date' => 1595539383,
                'closing_cn_update_date' => 1595539383,
            ],
        ];
        parent::init();
    }
}
