<?php
include 'serversideConn.php';
class TableData { 
 
 	private $_db;
	
	public function getConnection($database = null) {
		try {			
			$host		= HOST;
			$database	= $database;
			$user		= USER;
			$passwd		= PASSWORD;
			
		    $this->_db = new PDO('mysql:host='.$host.';dbname='.$database, $user, $passwd, array(
				PDO::ATTR_PERSISTENT => true, PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		} catch (PDOException $e) {
		    error_log("Failed to connect to database: ".$e->getMessage());
		}				
	}	
	
	public function get($database, $table, $index_column, $columns, $joins) {

		$this->getConnection($database);

		$sLimit = "";
		if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ) {
			$sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
		}
		
		
		$sOrder = "";
		if ( isset( $_GET['iSortCol_0'] ) ) {
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ) {
				if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ) {
					$sortDir = (strcasecmp($_GET['sSortDir_'.$i], 'ASC') == 0) ? 'ASC' : 'DESC';
					$sOrder .= "".$columns[ intval( $_GET['iSortCol_'.$i] ) ]." ". $sortDir .", ";
				}
			}
			
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" ) {
				$sOrder = "";
			}
		}
		
		

		$sWhere = "";
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$sWhere = "WHERE (";
			for ( $i=0 ; $i<count($columns) ; $i++ ) {
				if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" ) {
					$sWhere .= "".$columns[$i]." LIKE :search OR ";
				}
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		
		

		for ( $i=0 ; $i<count($columns) ; $i++ ) {
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ) {
				if ( $sWhere == "" ) {
					$sWhere = "WHERE ";
				}
				else {
					$sWhere .= " AND ";
				}
				$sWhere .= "".$columns[$i]." LIKE :search".$i." ";
			}
		}
		
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(",", $columns))." FROM ".$table." AS ".$table." ".$joins." ".$sWhere." ".$sOrder." ".$sLimit;
		$statement = $this->_db->prepare($sQuery);
		
		//var_dump($sQuery);exit;
		
		if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
			$statement->bindValue(':search', '%'.$_GET['sSearch'].'%', PDO::PARAM_STR);
		}
		for ( $i=0 ; $i<count($columns) ; $i++ ) {
			if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ) {
				$statement->bindValue(':search'.$i, '%'.$_GET['sSearch_'.$i].'%', PDO::PARAM_STR);
			}
		}


		$statement->execute();
		$rResult = $statement->fetchAll();
		
		
		$iFilteredTotal = current($this->_db->query('SELECT FOUND_ROWS()')->fetch());
		
		
		$sQuery = "SELECT COUNT(`".$index_column."`) FROM `".$table."` AS `".$table."`";
		$iTotal = current($this->_db->query($sQuery)->fetch());
		
		$output = array(
			"sEcho" => intval($_GET['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		$newColumns = array();
		foreach($columns as $column){
			$tmp = explode('.',$column);
			$newColumns[] = $tmp[1];
		}
		
		
		foreach($rResult as $aRow) {
			$row = array();			
			for ( $i = 0; $i < count($newColumns); $i++ ) {
				//var_dump($columns[$i]);
				//var_dump($aRow);
				if ( $newColumns[$i] == "version" ) {
					// Special output formatting for 'version' column
					$row[] = ($aRow[ $newColumns[$i] ]=="0") ? '-' : $aRow[ $newColumns[$i] ];
				}
				else if ( $newColumns[$i] != ' ' ) {
					$row[] = $aRow[ $newColumns[$i] ];
				}
			}
			$output['aaData'][] = $row;
		}
		
		$this->_db = null;
		echo json_encode( $output );
	}
}
header('Pragma: no-cache');
header('Cache-Control: no-store, no-cache, must-revalidate');
$table_data = new TableData();
?>