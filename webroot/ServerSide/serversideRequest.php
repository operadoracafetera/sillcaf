<?php
require 'serverside.php';

if(isset($_GET['model']) && $_GET['model']=='users'){
	//$joins = '';
	$joins = 'INNER JOIN sillcaf_perfiles per ON per.id=sillcaf_users.perfiles_id';
	$table_data->get('siscafe_20200616','sillcaf_users','id',['sillcaf_users.id', 'sillcaf_users.last_name','sillcaf_users.first_name','sillcaf_users.username','sillcaf_users.created_date','sillcaf_perfiles.name','sillcaf_users.active_user'],$joins);
}
else if(isset($_GET['model']) && $_GET['model']=='shippinglines'){
	$joins = '';
	//$joins = 'INNER JOIN perfiles per ON per.id=users.perfiles_id';
	$table_data->get('siscafe_20200616','shipping_lines','id',['shipping_lines.id', 'shipping_lines.business_name','shipping_lines.description','shipping_lines.created_date'],$joins);
}
else if(isset($_GET['model']) && $_GET['model']=='customs'){
	$joins = '';
	//$joins = 'INNER JOIN perfiles per ON per.id=users.perfiles_id';
	$table_data->get('siscafe_20200616','customs','id',['customs.id', 'customs.cia_name','customs.description','customs.created_date'],$joins);
}
else if(isset($_GET['model']) && $_GET['model']=='navyagent'){
	$joins = '';
	//$joins = 'INNER JOIN perfiles per ON per.id=users.perfiles_id';
	$table_data->get('siscafe_20200616','navy_agent','id',['navy_agent.id', 'navy_agent.name','navy_agent.created_date'],$joins);
}
else if(isset($_GET['model']) && $_GET['model']=='sillcaf_perfiles'){
	$joins = '';
	//$joins = 'INNER JOIN perfiles per ON per.id=users.perfiles_id';
	$table_data->get('siscafe_20200616','sillcaf_perfiles','id',['sillcaf_perfiles.id', 'sillcaf_perfiles.perfiles_name','sillcaf_perfiles.perfiles_description','sillcaf_perfiles.perfiles_reg_date','sillcaf_perfiles.perfiles_active'],$joins);
}
else if(isset($_GET['model']) && $_GET['model']=='modules'){
	$joins = '';
	//$joins = 'INNER JOIN perfiles per ON per.id=users.perfiles_id';
	$table_data->get('sillcaf_pro','modules','id',['modules.id', 'modules.name','modules.reg_date'],$joins);
}
else if(isset($_GET['model']) && $_GET['model']=='permisses'){
	//$joins = '';
	$joins = 'INNER JOIN modules module ON module.id=permisses.modules_id';
	$table_data->get('sillcaf_pro','permisses','id',['permisses.id', 'permisses.permisse','module.name','permisses.reg_date','permisses.active'],$joins);
}
else if(isset($_GET['model']) && $_GET['model']=='motorships'){
	//$joins = '';
	$joins = 'INNER JOIN shipping_lines shipping_line ON shipping_line.id=motorships.shippingline_id';
	$table_data->get('siscafe_20200616','motorships','id_motorship',['motorships.id_motorship', 'motorships.name_motorship','shipping_line.business_name','motorships.reg_date_motorship','motorships.active_motorship'],$joins);
}
else if(isset($_GET['model']) && $_GET['model']=='terminals'){
	$joins = '';
	//$joins = 'INNER JOIN perfiles per ON per.id=users.perfiles_id';
	$table_data->get('siscafe_20200616','terminals','id_terminals',['terminals.id_terminals','terminals.name_terminals','terminals.description_terminals','terminals.reg_date_terminals','terminals.active_terminals'],$joins);
}
?>