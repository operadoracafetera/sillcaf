<?php

use Cake\Routing\Router;

echo $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken'));

function getInfoQuantity($packagingCaffee)
{
    $infoRemesa = [];
    $infoRemesa['packedQuantity'] = 0;
    $infoRemesa['crossdockinQuantity'] = 0;
    $infoRemesa['totalQuantity'] = 0;
    $infoRemesa['packedLots'] = "";
    $infoRemesa['crossdockingLots'] = "";
    $infoRemesa['crossdockingLotsToValidate'] = "";
    foreach ($packagingCaffee['detail_packakging_crossdocking'] as $crossdocking) {
        if ($crossdocking['status'] == 1) {
            $infoRemesa['crossdockinQuantity'] = $infoRemesa['crossdockinQuantity'] + $crossdocking['qta_coffee'];
            $infoRemesa['crossdockingLots'] = $infoRemesa['crossdockingLots'] . $crossdocking['lot_coffee'] . " x " . $crossdocking['qta_coffee'] . " | ";
            $infoRemesa['crossdockingLotsToValidate'] = $infoRemesa['crossdockingLotsToValidate'] . $crossdocking['lot_coffee'] . " | ";
        }
    }
    $infoRemesa['totalQuantity'] = $infoRemesa['crossdockinQuantity'];
    $crossdockingLots = explode(" | ", $infoRemesa['crossdockingLotsToValidate']);
    foreach ($packagingCaffee['detail_packaging_caffee'] as $remesa) {
        $infoRemesa['packedQuantity'] = $infoRemesa['packedQuantity'] + $remesa['quantity_radicated_bag_out'];
        $infoRemesa['packedLots'] = $infoRemesa['packedLots'] . "3-" . $remesa['remittances_caffee']['client']['exporter_code'] . "-" . $remesa['remittances_caffee']['lot_caffee'] . " | ";
        $lotToSearch = $remesa['remittances_caffee']['client']['exporter_code'] . "-" . $remesa['remittances_caffee']['lot_caffee'];
        if (!in_array(($lotToSearch), $crossdockingLots)) {
            $infoRemesa['totalQuantity'] = $infoRemesa['totalQuantity'] + $remesa['quantity_radicated_bag_out'];
        } else {
            $key = array_search($lotToSearch, array_column($packagingCaffee['detail_packakging_crossdocking'], 'lot_coffee'));
            $infoCrossdocking = $packagingCaffee['detail_packakging_crossdocking'][$key];
            if ($remesa['remittances_caffee']['download_caffee_date'] < $infoCrossdocking['reg_date']->modify('-5 hours')) {
                $infoRemesa['totalQuantity'] = $infoRemesa['totalQuantity'] + $remesa['quantity_radicated_bag_out'];
            }
        }
    }
    return $infoRemesa;
}

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\InfoNavy[]|\Cake\Collection\CollectionInterface $infoNavy
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h3 class="content-title mb-0 my-auto">
                Búsqueda de contenedores programados
            </h3>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>

<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <?= $this->Form->create() ?>
                <fieldset>
                    <div class="row">
                        <div class="col-xl-3">
                            <?php echo $this->Form->control('initDate', ['label' => 'Fecha Inicio', 'required' => true, 'type' => 'text', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-xl-3">
                            <?php echo $this->Form->control('finalDate', ['label' => 'Fecha Fin', 'required' => true, 'type' => 'text', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-xl-3">
                            <?php echo $this->Form->control('state', ['label' => 'Estado Contenedor', 'type' => 'select', 'required' => true, 'options' => [1 => 'Confirmado', 5 => 'En Proceso', 6 => 'Cancelado'], 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-xl-3">
                            <?= $this->Form->button(__('Buscar'), ['style' => 'margin-top: 28px;', 'class' => "btn btn-primary-gradient btn-block"]) ?>
                        </div>
                    </div>
                </fieldset>
                <?= $this->Form->end() ?>
                <br>
                <table id="PackagingCoffeeDT">
                    <thead>
                        <tr>
                            <th>Proforma</th>
                            <th>Motonave</th>
                            <th>Tipo Embalaje</th>
                            <th>Modo Emabalaje</th>
                            <th>Cantidad</th>
                            <th>Exportador</th>
                            <th>Lotes</th>
                            <th>ISO</th>
                            <!-- <th>Crossdocking</th> -->
                            <th>Instrucciones</th>
                            <th>Booking</th>
                            <th>Booking2</th>
                            <th>Línea</th>
                            <!-- <th>Naviera</th>
                            <th>Estado</th> -->
                            <th class="dt-column-button-right"><?= __('Opciones') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($packagingCaffees as $packagingCaffee) : //debug($packagingCaffee);
                            $infoRemesa = getInfoQuantity($packagingCaffee); ?>
                            <tr>
                                <td><?= $packagingCaffee['info_navy']['proforma'] ?></td>
                                <td><?= $packagingCaffee['info_navy']['arrivals_motorship']['motorship']['name_motorship'] ?></td>
                                <td><?= $packagingCaffee['info_navy']['packaging_type']?></td>
                                <td><?= $packagingCaffee['info_navy']['packaging_mode'] ?></td>
                                <td><?= $infoRemesa['packedQuantity'] . "/" . $infoRemesa['totalQuantity'] ?></td>
                                <td><?=""?></td>
                                <td><?= $infoRemesa['packedLots'] ?></td>
                                <td><?= $packagingCaffee->iso_ctn ?></td>
                                <!-- <td><?//= $infoRemesa['crossdockingLots'] ?></td> -->
                                <td><?=""?></td>
                                <td><?= $packagingCaffee['info_navy']['booking'] ?></td>
                                <td><?= $packagingCaffee['info_navy']['booking2'] ?></td>
                                <!-- <td><?//= $packagingCaffee['info_navy']['navy_agent']['name'] ?></td> -->
                                <td><?= $packagingCaffee['info_navy']['shipping_line']['business_name'] ?></td>
                                <!-- <td><?//= $packagingCaffee['state_packaging']['name'] ?></td> -->
                                <td class="dt-column-button-right">
                                    <?= $this->Html->link(
                                        'Ver',
                                        ['controller' => 'DetailPackagingCaffee', 'action' => 'add', $packagingCaffee->id],
                                        ['class' => 'btn btn-info']
                                    ) ?>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        packagingCoffeeDT = $("#PackagingCoffeeDT").DataTable({
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        location.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            scrollY:        "400px"
        });

        packagingCoffeeDT.buttons().container()
            .appendTo('#table_buttons');

        $('#initdate').appendDtpicker({
            closeOnSelected: true,
            dateFormat: 'DD/MM/YYYY hh:mm',
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });

        $('#finaldate').appendDtpicker({
            closeOnSelected: true,
            dateFormat: 'DD/MM/YYYY hh:mm',
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });

    });
</script>