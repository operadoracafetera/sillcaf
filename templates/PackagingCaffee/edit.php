<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PackagingCaffee $packagingCaffee
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><?= $this->Html->link(__('Proformas'), ['Controller'=>'InfoNavy','action' => 'index'], ['class' => 'side-nav-item']) ?></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"> <?= $this->Html->link(__('/ Detalle proforma'), ['controller' => 'InfoNavy', 'action' => 'view',  $infoNavy['id']], ['class' => 'side-nav-item']) ?> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"> / Editar</span></span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg">
                    <?= $this->Form->create($packagingCaffee) ?>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-8">
                                <legend><?= __('Editar Contenedor') ?></legend>
                                <legend><?= __('Proforma: ').$this->Html->link($infoNavy['proforma'], ['controller' => 'InfoNavy', 'action' => 'view',  $infoNavy['id']]) ?></legend>
                            </div>
                            <div class="col-sm-4"><?= $this->Form->button(__('Guardar'), ['class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        </div>
                        <?php
                        echo $this->Form->control('iso_ctn', ['label' => 'ISO Contenedor', 'options' => ['20ST' => '20ST', '20RE' => '20RE', '40ST' => '40ST', '40HC' => '40HC', '40RH' => '40RH'], 'empty' => true]);
                        ?>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>