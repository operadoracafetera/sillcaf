<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PackagingCaffee $packagingCaffee
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Packaging Caffee'), ['action' => 'edit', $packagingCaffee->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Packaging Caffee'), ['action' => 'delete', $packagingCaffee->id], ['confirm' => __('Are you sure you want to delete # {0}?', $packagingCaffee->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Packaging Caffee'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Packaging Caffee'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="packagingCaffee view content">
            <h3><?= h($packagingCaffee->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Info Navy') ?></th>
                    <td><?= $packagingCaffee->has('info_navy') ? $this->Html->link($packagingCaffee->info_navy->id, ['controller' => 'InfoNavy', 'action' => 'view', $packagingCaffee->info_navy->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Iso Ctn') ?></th>
                    <td><?= h($packagingCaffee->iso_ctn) ?></td>
                </tr>
                <tr>
                    <th><?= __('Seal 1') ?></th>
                    <td><?= h($packagingCaffee->seal_1) ?></td>
                </tr>
                <tr>
                    <th><?= __('Seal 2') ?></th>
                    <td><?= h($packagingCaffee->seal_2) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $packagingCaffee->has('user') ? $this->Html->link($packagingCaffee->user->id, ['controller' => 'Users', 'action' => 'view', $packagingCaffee->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Seal1 Path') ?></th>
                    <td><?= h($packagingCaffee->seal1_path) ?></td>
                </tr>
                <tr>
                    <th><?= __('Seal2 Path') ?></th>
                    <td><?= h($packagingCaffee->seal2_path) ?></td>
                </tr>
                <tr>
                    <th><?= __('Img Ctn') ?></th>
                    <td><?= h($packagingCaffee->img_ctn) ?></td>
                </tr>
                <tr>
                    <th><?= __('Exporter Cod') ?></th>
                    <td><?= h($packagingCaffee->exporter_cod) ?></td>
                </tr>
                <tr>
                    <th><?= __('Seal 3') ?></th>
                    <td><?= h($packagingCaffee->seal_3) ?></td>
                </tr>
                <tr>
                    <th><?= __('Seal 4') ?></th>
                    <td><?= h($packagingCaffee->seal_4) ?></td>
                </tr>
                <tr>
                    <th><?= __('Jetty') ?></th>
                    <td><?= h($packagingCaffee->jetty) ?></td>
                </tr>
                <tr>
                    <th><?= __('Jornally') ?></th>
                    <td><?= h($packagingCaffee->jornally) ?></td>
                </tr>
                <tr>
                    <th><?= __('Ref Copcsa Gang') ?></th>
                    <td><?= h($packagingCaffee->ref_copcsa_gang) ?></td>
                </tr>
                <tr>
                    <th><?= __('Long Ctn') ?></th>
                    <td><?= h($packagingCaffee->long_ctn) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cooperativa Ctg') ?></th>
                    <td><?= h($packagingCaffee->cooperativa_ctg) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('State Packaging Id') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->state_packaging_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Ready Container Id') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->ready_container_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cause Emptied Ctn Id') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->cause_emptied_ctn_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Checking Ctn Id') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->checking_ctn_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Traceability Packaging Id') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->traceability_packaging_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Weight Net Cont') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->weight_net_cont) ?></td>
                </tr>
                <tr>
                    <th><?= __('Autorization') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->autorization) ?></td>
                </tr>
                <tr>
                    <th><?= __('Qta Bags Packaging') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->qta_bags_packaging) ?></td>
                </tr>
                <tr>
                    <th><?= __('Rem Ref') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->rem_ref) ?></td>
                </tr>
                <tr>
                    <th><?= __('User Tarja') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->user_tarja) ?></td>
                </tr>
                <tr>
                    <th><?= __('User Driver') ?></th>
                    <td><?= $this->Number->format($packagingCaffee->user_driver) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created Date') ?></th>
                    <td><?= h($packagingCaffee->created_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Packaging Date') ?></th>
                    <td><?= h($packagingCaffee->packaging_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cancelled Date') ?></th>
                    <td><?= h($packagingCaffee->cancelled_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Seal Date') ?></th>
                    <td><?= h($packagingCaffee->seal_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Time Start') ?></th>
                    <td><?= h($packagingCaffee->time_start) ?></td>
                </tr>
                <tr>
                    <th><?= __('Time End') ?></th>
                    <td><?= h($packagingCaffee->time_end) ?></td>
                </tr>
                <tr>
                    <th><?= __('Emptied Date') ?></th>
                    <td><?= h($packagingCaffee->emptied_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Empty Date') ?></th>
                    <td><?= h($packagingCaffee->empty_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Rx Date Spb Navis') ?></th>
                    <td><?= h($packagingCaffee->rx_date_spb_navis) ?></td>
                </tr>
                <tr>
                    <th><?= __('Upload Tracking Photos Date') ?></th>
                    <td><?= h($packagingCaffee->upload_tracking_photos_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Weight To Out') ?></th>
                    <td><?= $packagingCaffee->weight_to_out ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Active') ?></th>
                    <td><?= $packagingCaffee->active ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Consolidate') ?></th>
                    <td><?= $packagingCaffee->consolidate ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Rx Status Spb Navis') ?></th>
                    <td><?= $packagingCaffee->rx_status_spb_navis ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Observation') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($packagingCaffee->observation)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Msg Spb Navis') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($packagingCaffee->msg_spb_navis)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Note Opera') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($packagingCaffee->note_opera)); ?>
                </blockquote>
            </div>
        </div>
    </div>
</div>
