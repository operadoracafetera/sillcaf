<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPermiss $sillcafPermiss
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sillcafPermiss->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafPermiss->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Sillcaf Permisses'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillcafPermisses form content">
            <?= $this->Form->create($sillcafPermiss) ?>
            <fieldset>
                <legend><?= __('Edit Sillcaf Permiss') ?></legend>
                <?php
                    echo $this->Form->control('permisse');
                    echo $this->Form->control('reg_date');
                    echo $this->Form->control('active');
                    echo $this->Form->control('modules_id', ['options' => $sillcafModules]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
