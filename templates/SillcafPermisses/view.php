<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPermiss $sillcafPermiss
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Sillcaf Permiss'), ['action' => 'edit', $sillcafPermiss->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Sillcaf Permiss'), ['action' => 'delete', $sillcafPermiss->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafPermiss->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Sillcaf Permisses'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Sillcaf Permiss'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillcafPermisses view content">
            <h3><?= h($sillcafPermiss->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Permisse') ?></th>
                    <td><?= h($sillcafPermiss->permisse) ?></td>
                </tr>
                <tr>
                    <th><?= __('Sillcaf Module') ?></th>
                    <td><?= $sillcafPermiss->has('sillcaf_module') ? $this->Html->link($sillcafPermiss->sillcaf_module->name, ['controller' => 'SillcafModules', 'action' => 'view', $sillcafPermiss->sillcaf_module->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($sillcafPermiss->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Active') ?></th>
                    <td><?= $this->Number->format($sillcafPermiss->active) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reg Date') ?></th>
                    <td><?= h($sillcafPermiss->reg_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
