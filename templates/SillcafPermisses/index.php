<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPermiss[]|\Cake\Collection\CollectionInterface $sillcafPermisses
 */
?>
<div class="sillcafPermisses index content">
    <?= $this->Html->link(__('New Sillcaf Permiss'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Sillcaf Permisses') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('permisse') ?></th>
                    <th><?= $this->Paginator->sort('reg_date') ?></th>
                    <th><?= $this->Paginator->sort('active') ?></th>
                    <th><?= $this->Paginator->sort('modules_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sillcafPermisses as $sillcafPermiss): ?>
                <tr>
                    <td><?= $this->Number->format($sillcafPermiss->id) ?></td>
                    <td><?= h($sillcafPermiss->permisse) ?></td>
                    <td><?= h($sillcafPermiss->reg_date) ?></td>
                    <td><?= $this->Number->format($sillcafPermiss->active) ?></td>
                    <td><?= $sillcafPermiss->has('sillcaf_module') ? $this->Html->link($sillcafPermiss->sillcaf_module->name, ['controller' => 'SillcafModules', 'action' => 'view', $sillcafPermiss->sillcaf_module->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $sillcafPermiss->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sillcafPermiss->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sillcafPermiss->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafPermiss->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
