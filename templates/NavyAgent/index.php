<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\NavyAgent[]|\Cake\Collection\CollectionInterface $navyAgent
 */
?>

<div class="breadcrumb-header justify-content-between">
						<div class="my-auto">
							<div class="d-flex">
								<h4 class="content-title mb-0 my-auto"><h3>Navieros</h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
							</div>
						</div>
						<div  class="d-flex my-xl-auto right-content">
							<div id="table_buttons" class="mb-3 mb-xl-0">
								
							</div>
						</div>
</div>
<br>

<div class="navyAgent index content">
    <h3><?= __('Navy Agent') ?></h3>
    <div class="table-responsive">
        <table class="table text-md-" id="tableData">
            <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>created_date</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>

</div>

<script>
        $(document).ready(function(){
			
           var table = $("#tableData").DataTable({
              processing: true,
              serverSide: true,
			  dom: 'Bfitp',
			  //dom: '<Blf<t>ip>',
				buttons: [
					'pageLength','copy', 'excel', 'pdf',
					{
						extend: 'colvis',
						postfixButtons: [ 'colvisRestore' ]
					},
					{
						text: 'Reload',
						action: function ( e, dt, node, config ) {
							dt.ajax.reload();
						}
					}
				],
				lengthMenu: [
					[ 10, 25, 50, -1 ],
					[ '10 rows', '25 rows', '50 rows', 'Show all' ]
				],
			  language: {
					searchPlaceholder: 'Search...',
					sSearch: '',
					lengthMenu: '_MENU_ ',
				},
              sAjaxSource: "http://localhost/sillcaf/ServerSide/serversideRequest.php?model=navyagent",
              columnDefs:[{
                  "data":null,
				  "targets": -1,
				  "defaultContent": "<a id=\"edit\" href=\"#\"><i class=\"side-menu__icon btn btn-sm btn-info far fa-edit\"></i></a><a id=\"view\" href=\"#\"><i class=\"side-menu__icon btn btn-sm btn-primary fas fa-search\"></i></a>"
              }]   
           });
		    
		   table.buttons().container()
			.appendTo( '#table_buttons' );	
			
			$('#tableData tbody').on( 'click', 'a#edit', function () {
			
				var data = table.row( $(this).parents('tr') ).data();
				alert( data[0] +"'s salary is: "+ data[ 2 ] );
			} );
			
        });
</script>
