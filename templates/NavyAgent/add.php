<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\NavyAgent $navyAgent
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Navy Agent'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="navyAgent form content">
            <?= $this->Form->create($navyAgent) ?>
            <fieldset>
                <legend><?= __('Add Navy Agent') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('created_date');
                    echo $this->Form->control('updated_date');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
