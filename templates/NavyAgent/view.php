<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\NavyAgent $navyAgent
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Navy Agent'), ['action' => 'edit', $navyAgent->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Navy Agent'), ['action' => 'delete', $navyAgent->id], ['confirm' => __('Are you sure you want to delete # {0}?', $navyAgent->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Navy Agent'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Navy Agent'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="navyAgent view content">
            <h3><?= h($navyAgent->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($navyAgent->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($navyAgent->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created Date') ?></th>
                    <td><?= h($navyAgent->created_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated Date') ?></th>
                    <td><?= h($navyAgent->updated_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
