<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\NavyAgent $navyAgent
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $navyAgent->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $navyAgent->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Navy Agent'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="navyAgent form content">
            <?= $this->Form->create($navyAgent) ?>
            <fieldset>
                <legend><?= __('Edit Navy Agent') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('created_date');
                    echo $this->Form->control('updated_date');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
