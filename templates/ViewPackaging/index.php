<?php

use Cake\Routing\Router;
use Cake\I18n\FrozenTime;

$now = FrozenTime::now();
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailPackagingCaffee[]|\Cake\Collection\CollectionInterface $detailPackagingCaffee
 */
?>

<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h3 class="content-title mb-0 my-auto">
                <?= $this->Html->link(__('Reportes'), ['controller' => 'InfoNavy', 'action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>


<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <?= $this->Form->create($viewPackagingForm) ?>
                <fieldset>
                    <div class="row">
                        <div class="col-xl-4">
                            <?php echo $this->Form->control('initDate', ['label' => 'Fecha Inicio', 'type' => 'datetime', 'required' => true, 'type' => 'text', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-xl-4">
                            <?php echo $this->Form->control('finalDate', ['label' => 'Fecha Fin', 'type' => 'datetime', 'required' => true, 'type' => 'text', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-xl-4">
                            <?= $this->Form->button(__('Buscar'), ['style' => 'margin-top: 28px;', 'class' => "btn btn-primary-gradient btn-block"]) ?>
                        </div>
                    </div>
                </fieldset>
                <?= $this->Form->end() ?>
                <br>
                <div class="table-responsive">
                    <table class="table text-md-" id="reportTable">
                        <thead>
                            <tr>
                                <th>OIE</th>
                                <th>Proforma</th>
                                <th>CIA</th>
                                <th>Agente Naviero</th>
                                <th>Línea Naviera</th>
                                <th>Motonave</th>
                                <th>Exportador</th>
                                <th>Booking</th>
                                <th>Contenedor</th>
                                <th>Número Viaje</th>
                                <th>Modo Embalaje</th>
                                <th>Tipo Embalaje</th>
                                <th>Lotes</th>
                                <th>Sacos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($viewPackagingList as $viewPackagingItem) : ?>
                                <tr>
                                    <td><?= $viewPackagingItem->oie ?></td>
                                    <td><?= $viewPackagingItem->proforma ?></td>
                                    <td><?= $viewPackagingItem->cia_name ?></td>
                                    <td><?= $viewPackagingItem->agente_naviero ?></td>
                                    <td><?= $viewPackagingItem->business_name ?></td>
                                    <td><?= $viewPackagingItem->motorship_name ?></td>
                                    <td><?= $viewPackagingItem->exportador ?></td>
                                    <td><?= $viewPackagingItem->booking ?></td>
                                    <td><?= $viewPackagingItem->bic_ctn ?></td>
                                    <td><?= $viewPackagingItem->travel_num ?></td>
                                    <td><?= $viewPackagingItem->packaging_mode ?></td>
                                    <td><?= $viewPackagingItem->packaging_type ?></td>
                                    <td><?= $viewPackagingItem->lotes1 ?></td>
                                    <td><?= $viewPackagingItem->total_sacos ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $("#reportTable").DataTable({
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        location.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            }
        });

        table.buttons().container()
            .appendTo('#table_buttons');



        $('#initdate').appendDtpicker({
            closeOnSelected: true,
            dateFormat: 'DD/MM/YYYY hh:mm',
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });


        $('#finaldate').appendDtpicker({
            closeOnSelected: true,
            dateFormat: 'DD/MM/YYYY hh:mm',
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });

    });
</script>