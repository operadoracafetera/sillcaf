<?php

use Cake\Routing\Router;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\InfoNavy $infoNavy
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><?= $this->Html->link(__('Proformas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Registrar</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg">
                    <?= $this->Form->create($infoNavy) ?>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-8">
                                <legend><?= __('Registro de proformas') ?></legend>
                            </div>
                            <div class="col-sm-4"><?= $this->Form->button(__('Guardar'), ['class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        </div>
                        <?php
                        if ($customsId) {
                            echo $this->Form->control('customs', ['options' => $customs, 'disabled' => true, 'value' => $customsId, 'empty' => true, 'label' => 'Agencia de Aduanas', 'class' => 'form-control']);
                            echo $this->Form->control('customs_id', ['type' => 'hidden', 'value' => $customsId, 'empty' => true, 'label' => 'Agencia de Aduanas', 'class' => 'form-control']);
                        } else {
                            echo $this->Form->control('customs_id', ['options' => $customs, 'empty' => true, 'label' => 'Agencia de Aduanas', 'class' => 'form-control']);
                        }
                        echo $this->Form->control('navy_agent_id', ['options' => $navyAgent, 'empty' => true, 'label' => 'Agente Naviero', 'class' => 'form-control']);
                        echo $this->Form->control('shipping_lines_id', ['options' => $shippingLines, 'empty' => true, 'label' => 'Línea Naviera', 'class' => 'form-control']);
                        echo $this->Form->control('arrivals_motorships_id', ['options' => $arrivalsMotorships, 'empty' => true, 'required' => true, 'label' => 'Llegada Motonave', 'class' => 'form-control']);
                        echo $this->Form->control('travel_num', ['type' => 'hidden', 'class' => 'form-control']);
                        echo $this->Form->control('destiny', ['type' => 'hidden', 'class' => 'form-control']);
                        echo $this->Form->control('packaging_type', ['label' => 'Tipo Embalaje', 'empty' => 'Seleccione...', 'required' => true, 'options' => ['FCL' => 'FCL', 'LCL' => 'LCL'], 'class' => 'form-control']);
                        echo $this->Form->control('packaging_mode', ['label' => 'Modo Embalaje', 'empty' => 'Seleccione...', 'required' => true, 'options' => ['SACOS' => 'SACOS', 'GRANEL' => 'GRANEL', 'CACAO' => 'CACAO', 'COMBINADO(SACOS - CAJAS)' => 'COMBINADO(SACOS - CAJAS)', 'COMBINADO(SACOS - BIG BAGS)' => 'COMBINADO(SACOS - BIG BAGS)', 'BIG BAGS' => 'BIG BAGS', 'CAJAS' => 'CAJAS', 'CONSOLIDADO (VARIAS AGENCIAS)' => 'CONSOLIDADO (VARIAS AGENCIAS)'], 'class' => 'form-control']);
                        echo $this->Form->control('additional_customs_id[]', ['options' => $customs, 'label' => 'Agencia de Aduanas', 'class' => 'form-control']);
                        echo $this->Form->control('booking', ['label' => 'Número Booking', 'class' => 'form-control']);
                        echo $this->Form->control('consignee_name', ['label' => 'Consignatario', 'class' => 'form-control']);
                        echo $this->Form->control('observation', ['type' => 'textarea', 'label' => 'Observaciones', 'class' => 'form-control']);
                        ?>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        <?php if ($customsId) {  ?>

            $('#customs').select2({
                placeholder: 'Seleccione..',
                searchInputPlaceholder: 'Search'
            });

        <?php } else { ?>

            $('#customs-id').select2({
                placeholder: 'Seleccione..',
                searchInputPlaceholder: 'Search'
            });

        <?php }  ?>
        $('#arrivals-motorships-id').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });
        $('#shipping-lines-id').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });
        $('#navy-agent-id').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });
        $('#packaging-type').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });
        $('#packaging-mode').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });

        $('#additional-customs-id').select2({
            placeholder: 'Seleccione..',
            multiple: true,
            tags: true,
            tokenSeparators: [',', ' '],
            searchInputPlaceholder: 'Search'
        });

        $('#additional-customs-id').next(".select2-container").hide();
        $('label[for="additional-customs-id"]').hide();

        $('#packaging-mode').on('change', function() {
            if ($(this).val() == 'CONSOLIDADO (VARIAS AGENCIAS)') {
                $('#additional-customs-id').next(".select2-container").show();
                $('label[for="additional-customs-id"]').show();
            } else {
                $('#additional-customs-id').next(".select2-container").hide();
                $('label[for="additional-customs-id"]').hide();
            }

        });

        $('#navy-agent-id').on('change', function() {
            $('#shipping-lines-id').empty();
            $('#arrivals-motorships-id').empty();
            $('#travel-num').val('');
            $('#destiny').val('');
            $.get('<?= Router::url(['action' => 'getInfoByNavyAgent']) ?>', {
                    'navyAgentId': $('#navy-agent-id').val()
                })
                .done(function(data) {
                    resultgetInfoByNavyAgentData = JSON.parse(data);
                    if (resultgetInfoByNavyAgentData['shipping_lines']) {
                        $.each(resultgetInfoByNavyAgentData['shipping_lines'], function(index, value) {
                            $('#shipping-lines-id').append("<option value=" + index + ">  " + value + "  </option>");
                        });
                    }
                    if (resultgetInfoByNavyAgentData['arrivals_motorships']) {
                        $.each(resultgetInfoByNavyAgentData['arrivals_motorships'], function(index, value) {
                            $('#arrivals-motorships-id').append("<option value=" + value['arrivals_motorships_id'] + ">  " + value['arrivals_motorship']['motorship']['name_motorship'] + " - ETA: " + value['arrivals_motorship']['arrival_date'] + " - Viaje: " + value['arrivals_motorship']['vissel'] + "  </option>").trigger('change');
                        });
                    }
                });
        });


        $('#arrivals-motorships-id').on('change', function() {
            $.each(resultgetInfoByNavyAgentData['arrivals_motorships'], function(index, value) {
                if (value['arrivals_motorships_id'] == $('#arrivals-motorships-id').val()) {
                    $('#travel-num').val(value['arrivals_motorship']['vissel']);
                    $('#destiny').val(value['arrivals_motorship']['vissel']);
                }
            });
        });

    });
</script>