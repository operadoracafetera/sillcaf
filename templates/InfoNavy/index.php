<?php

use Cake\Routing\Router;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\InfoNavy[]|\Cake\Collection\CollectionInterface $infoNavy
 */
?>

<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">
                Proformas
            </h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>


<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">Proformas</h4>
                </div>
            </div>
            <div class="card-body">
                <table class="nowrap" style="width:100%" id="tablaData">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Proforma</th>
                            <th>Estado</th>
                            <th>Booking</th>
                            <th>Agente Naviero</th>
                            <th>Motonave</th>
                            <th>Viaje</th>
                            <th>ETA</th>
                            <th>Tipo embalaje</th>
                            <th>Modo embalaje</th>
                            <th># Contenedores</th>
                            <th>Fecha elaboración</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $("#tablaData").DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            order: [
                [0, "desc"]
            ],
            ajax: "<?= Router::url(['controller' => 'InfoNavy', 'action' => 'indexJson']) ?>",
            columnDefs: [{
                    "data": "id",
                    "targets": 0
                },
                {
                    "data": "proforma",
                    "targets": 1
                },
                {
                    "data": "status_info_navy.status_info_navy_name",
                    "targets": 2
                },
                {
                    "data": "booking",
                    "targets": 3
                },
                {
                    "data": "navy_agent.name",
                    "targets": 4
                },
                {
                    "data": "arrivals_motorship.motorship.name_motorship",
                    "defaultContent": "No info",
                    "targets": 5
                },
                {
                    "targets": 6,
                    "data": "arrivals_motorship.vissel",
                },
                {
                    "targets": 7,
                    "data": "arrivals_motorship.arrival_date",
                    "render": function(data) {
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    "data": "packaging_type",
                    "targets": 8
                },
                {
                    "data": "packaging_mode",
                    "targets": 9
                },
                {
                    "targets": 10,
                    "data": "packaging_caffee",
                    "render": function(data) {
                        return data.length;
                    }
                },
                {
                    "targets": 11,
                    "data": "created_date",
                    "render": function(data) {
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    "data": null,
                    "targets": -1,
                    "className": 'dt-column-button-right',
                    "defaultContent": "<a id=\"view\" href=\"#\" class=\"btn btn-info\">Ver</a>"
                }
            ],
            scrollX: true,
            fixedColumns: {
                leftColumns: 1,
                rightColumns: 1
            }
        });

        table.buttons().container()
            .appendTo('#table_buttons');

        $('#tablaData tbody').on('click', 'a#view', function() {
            var data = table.row($(this).parents('tr')).data();
            window.open("<?= Router::url(['controller' => 'InfoNavy', 'action' => 'view']) ?>" + "/" + data['id'], '_self');
        });

        $('#tablaData tbody').on('click', 'a#edit', function() {
            var data = table.row($(this).parents('tr')).data();
            window.open("<?= Router::url(['controller' => 'InfoNavy', 'action' => 'edit']) ?>" + "/" + data['id'], '_self');
        });

    });
</script>