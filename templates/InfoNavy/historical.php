<?php

use Cake\Routing\Router;
use Cake\I18n\FrozenTime;

$now = FrozenTime::now();

?>

<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h3 class="content-title mb-0 my-auto">
                <?= $this->Html->link(__('Reportes'), ['controller' => 'InfoNavy', 'action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>


<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <?= $this->Form->create($historicalInfoNavyForm) ?>
                <fieldset>
                    <div class="row">
                        <div class="col-xl-3">
                            <?php echo $this->Form->control('initDate', ['label' => 'Fecha Inicio', 'required' => true, 'type' => 'text', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-xl-3">
                            <?php echo $this->Form->control('finalDate', ['label' => 'Fecha Fin', 'required' => true, 'type' => 'text', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-xl-3">
                            <?php echo $this->Form->control('status', ['label' => 'Estado', 'options' => $statusInfoNavy, 'empty' => 'Seleccione un estado', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-xl-3">
                            <?php echo $this->Form->control('motorship', ['label' => 'Motonave', 'options' => $motorship, 'empty' => 'Seleccione una motonave', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-xl-3">
                            <?= $this->Form->button(__('Buscar'), ['style' => 'margin-top: 28px;', 'class' => "btn btn-primary-gradient btn-block"]) ?>
                        </div>
                    </div>
                </fieldset>
                <?= $this->Form->end() ?>
                <br>
                <div class="table-responsive">
                    <table class="table text-md-" id="reportTable">
                        <thead>
                            <tr>
                                <th>--</th>
                                <th>Proforma</th>
                                <th>Estado</th>
                                <th>Booking</th>
                                <th>Agente Naviero</th>
                                <th>Motonave</th>
                                <th>Viaje</th>
                                <th>ETA</th>
                                <th>Tipo embalaje</th>
                                <th>Modo embalaje</th>
                                <th># Contenedores</th>
                                <th>Fecha elaboración</th>
                                <th>Fecha confirmación</th>
                                <th>Fecha aprovación</th>
                                <th>Fecha rechazo</th>
                                <th>Fecha cierre</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($historicalInfoNavyList as $item) : ?>
                                <tr>
                                    <td><?= $this->Html->link(__('PDF'), 'http://siscafe.copcsa.com:8080/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=0&proforma=' . $item->id . '&idReport=7&toPdf=true&idPrinter=1', ['class' => "btn btn-primary-gradient"]) ?></td>
                                    <td><?= $item->proforma ?></td>
                                    <td><?= $item->status_info_navy->status_info_navy_name ?></td>
                                    <td><?= $item->booking ?></td>
                                    <td><?= $item->navy_agent->name ?></td>
                                    <td><?= $item->arrivals_motorship->motorship->name_motorship ?></td>
                                    <td><?= $item->arrivals_motorship->vissel ?></td>
                                    <td><?= $item->arrivals_motorship->arrival_date ?></td>
                                    <td><?= $item->packaging_type ?></td>
                                    <td><?= $item->packaging_mode ?></td>
                                    <td><?= count($item->packaging_caffee) ?></td>
                                    <td><?= $item->created_date ?></td>
                                    <td><?= $item->confirm_date ?></td>
                                    <td><?= $item->aprove_date ?></td>
                                    <td><?= $item->reject_date ?></td>
                                    <td><?= $item->closed_date ?></td>
                                    
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $("#reportTable").DataTable({
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        location.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            }
        });

        table.buttons().container()
            .appendTo('#table_buttons');



        $('#initdate').appendDtpicker({
            closeOnSelected: true,
            dateFormat: 'DD/MM/YYYY hh:mm',
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });


        $('#finaldate').appendDtpicker({
            closeOnSelected: true,
            dateFormat: 'DD/MM/YYYY hh:mm',
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });

    });
</script>