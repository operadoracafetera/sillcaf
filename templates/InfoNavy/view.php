<?php

use Cake\I18n\FrozenTime;

function getInfoQuantity($packagingCaffee)
{
    $infoRemesa = [];
    $infoRemesa['packedQuantity'] = 0;
    $infoRemesa['crossdockinQuantity'] = 0;
    $infoRemesa['totalQuantity'] = 0;
    $infoRemesa['packedLots'] = "";
    $infoRemesa['crossdockingLots'] = "";
    $infoRemesa['crossdockingLotsToValidate'] = "";
    foreach ($packagingCaffee['detail_packakging_crossdocking'] as $crossdocking) {
        if ($crossdocking['status'] == 1) {
            $infoRemesa['crossdockinQuantity'] = $infoRemesa['crossdockinQuantity'] + $crossdocking['qta_coffee'];
            $infoRemesa['crossdockingLots'] = $infoRemesa['crossdockingLots'] . $crossdocking['lot_coffee'] . " x " . $crossdocking['qta_coffee'] . " | ";
            $infoRemesa['crossdockingLotsToValidate'] = $infoRemesa['crossdockingLotsToValidate'] . $crossdocking['lot_coffee'] . " | ";
        }
    }
    $infoRemesa['totalQuantity'] = $infoRemesa['crossdockinQuantity'];
    $crossdockingLots = explode(" | ", $infoRemesa['crossdockingLotsToValidate']);
    foreach ($packagingCaffee['detail_packaging_caffee'] as $remesa) {
        $infoRemesa['packedQuantity'] = $infoRemesa['packedQuantity'] + $remesa['quantity_radicated_bag_out'];
        $infoRemesa['packedLots'] = $infoRemesa['packedLots'] . "3-" . $remesa['remittances_caffee']['client']['exporter_code'] . "-" . $remesa['remittances_caffee']['lot_caffee'] . " | ";
        $lotToSearch = $remesa['remittances_caffee']['client']['exporter_code'] . "-" . $remesa['remittances_caffee']['lot_caffee'];
        if (!in_array(($lotToSearch), $crossdockingLots)) {
            $infoRemesa['totalQuantity'] = $infoRemesa['totalQuantity'] + $remesa['quantity_radicated_bag_out'];
        } else {
            $key = array_search($lotToSearch, array_column($packagingCaffee['detail_packakging_crossdocking'], 'lot_coffee'));
            $infoCrossdocking = $packagingCaffee['detail_packakging_crossdocking'][$key];
            if ($remesa['remittances_caffee']['download_caffee_date'] < $infoCrossdocking['reg_date']->modify('-5 hours')) {
                $infoRemesa['totalQuantity'] = $infoRemesa['totalQuantity'] + $remesa['quantity_radicated_bag_out'];
            }
        }
    }
    return $infoRemesa;
}


/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\InfoNavy[]|\Cake\Collection\CollectionInterface $infoNavy
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <?php if ($userLogin['perfiles_id'] != 4) { ?>
                <h4 class="content-title mb-0 my-auto">
                    <?= $this->Html->link(__('Proformas'), ['controller' => 'InfoNavy', 'action' => 'index'], ['class' => 'side-nav-item']) ?>
                </h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Detalle de proforma</span>
            <?php } ?>
        </div>
    </div>
</div>

<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <?php if ($allowEditInfoNavy && $canInfoNavyEditByConsolidated) { ?>
                        <div class="col-sm-2  align-self-end"><br><?= $this->Html->link(__('Editar'), ['action' => 'edit', $infoNavy['id']], ['class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        <div class="col-sm-2  align-self-end"><br><?= $this->Form->PostLink(__('Confirmar'), ['action' => 'confirm', $infoNavy['id']], ['confirm' => 'Está seguro de confirmar la proforma ' . $infoNavy['proforma'], 'class' => "btn btn-primary-gradient btn-block"]) ?></div>
                    <?php } ?>
                    <?php if ($showApproveBtn) { ?>
                        <?php if ($infoNavy['shipping_lines_id'] != 39) { ?>
                            <div class="col-sm-2  align-self-end"><br><?= $this->Form->PostLink(__('Aprobar'), ['action' => 'aprove', $infoNavy['id']], ['confirm' => 'Está seguro de aprobar la proforma ' . $infoNavy['proforma'], 'class' => "btn btn-primary-gradient btn-block"]) ?></div>
                            <div class="col-sm-2  align-self-end"><br><?= $this->Form->PostLink(__('Rechazar'), ['action' => 'reject', $infoNavy['id']], ['confirm' => 'Está seguro de rechazar la proforma ' . $infoNavy['proforma'], 'class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        <?php } else if ($infoNavy['booking2'] != NULL) { ?>
                            <div class="col-sm-2  align-self-end"><br><?= $this->Form->PostLink(__('Aprobar'), ['action' => 'aprove', $infoNavy['id']], ['confirm' => 'Está seguro de aprobar la proforma ' . $infoNavy['proforma'], 'class' => "btn btn-primary-gradient btn-block"]) ?></div>
                            <div class="col-sm-2  align-self-end"><br><?= $this->Form->PostLink(__('Rechazar'), ['action' => 'reject', $infoNavy['id']], ['confirm' => 'Está seguro de rechazar la proforma ' . $infoNavy['proforma'], 'class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        <?php } ?>
                    <?php } ?>
                    <div class="col-sm-2  align-self-end"><br><?= $this->Html->link(__('Descargar PDF'), 'http://siscafe.copcsa.com:8787/SISCAFE-PrintWS/ws/print?remittancesCaffeeId=0&proforma=' . $infoNavy['id'] . '&idReport=7&toPdf=true&idPrinter=1', ['class' => "btn btn-primary-gradient btn-block"]) ?></div>

                </div>
                <div class="row">
                    <?php if ($infoNavy['shipping_lines_id'] == 39 && $infoNavy['booking2'] == NULL && $userLogin['perfiles_id'] == 3) { ?>
                        <div class="col-xl-10">
                            <?= $this->Form->create($packagingCaffee, ['url' => ['controller' => 'InfoNavy', 'action' => 'booking2', $infoNavy['id']]]) ?>
                            <div class="col-xl-3"> <?php echo $this->Form->control('booking2', ['label' => '', 'required' => true, 'placeholder' => 'Booking 2 Navenal', 'class' => 'form-control']); ?></div>
                            <div class="col-xl-3"> <?= $this->Form->button(__('Guardar'), ['class' => "btn btn-warning btn-block"]) ?></div>
                            <?= $this->Form->end() ?>
                        </div>
                    <?php } ?>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-title mb-2">Listado de contenedores por proforma</div>
                        <div class="card-title mb-2">ID sistema: <?= $infoNavy['id'] ?> | Proforma: <?= $this->Html->link($infoNavy['proforma'], ['controller' => 'InfoNavy', 'action' => 'view',  $infoNavy['id']]) ?> | Contenedores: <?= count($infoNavy['packaging_caffee']) ?> | Estado: <?= $infoNavy['status_info_navy']['status_info_navy_name'] ?></div>
                    </div>
                </div>
                <?php if ($allowEditInfoNavy) { ?>
                    <div class="row" style="padding-bottom: 10px;">
                        <div class="col-xl-12">
                            <?= $this->Form->create($packagingCaffee, ['url' => ['controller' => 'PackagingCaffee', 'action' => 'add', $infoNavy['id']]]) ?>
                            <div class="row">
                                <div class="col-xl-3">
                                    <?php
                                    echo $this->Form->control('iso_ctn', ['label' => 'Long Contenedor', 'options' => ['20ST' => '20ST', '20RE' => '20RE', '40ST' => '40ST', '40HC' => '40HC', '40RH' => '40RH'], 'required' => true, 'class' => 'form-control']); ?>
                                </div>
                                <div class="col-xl-3">
                                    <?= $this->Form->button(__('Crear contenedor'), ['style' => 'margin-top: 28px;', 'class' => "btn btn-primary-gradient btn-block"]) ?>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-xl-12 d-flex align-items-end flex-column">
                        <div id="PackagingCoffeeDTB" class="mb-3 mb-xl-0">
                        </div>
                    </div>
                </div>
                <table class="nowrap" style="width:100%" id="PackagingCoffeeDT">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Long</th>
                            <th>Tipo Emabalaje</th>
                            <th>Cantidad</th>
                            <th>Lotes</th>
                            <th>Crossdocking</th>
                            <th class="dt-column-button-right"><?= __('Opciones') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($infoNavy->packaging_caffee as $packagingCaffee) : //debug($packagingCaffee);
                            if ($packagingCaffee->state_packaging_id != 6) {
                                $infoRemesa = getInfoQuantity($packagingCaffee);
                        ?>
                                <tr>
                                    <td><?= $this->Number->format($packagingCaffee->id) ?></td>
                                    <td><?= $packagingCaffee->iso_ctn ?></td>
                                    <td><?= $infoNavy['packaging_type'] ?></td>
                                    <td><?= $infoRemesa['packedQuantity'] . "/" . $infoRemesa['totalQuantity'] ?></td>
                                    <td><?= $infoRemesa['packedLots'] ?></td>
                                    <td><?= $infoRemesa['crossdockingLots'] ?></td>
                                    <td class="dt-column-button-right">
                                        <?= $this->Html->link(
                                            'Ver',
                                            ['controller' => 'DetailPackagingCaffee', 'action' => 'add', $packagingCaffee->id],
                                            ['class' => 'btn btn-info']
                                        ) ?>
                                        <?php if ($allowEditInfoNavy) { ?>
                                            <?= $this->Html->link(
                                                'Editar',
                                                ['controller' => 'PackagingCaffee', 'action' => 'edit', $packagingCaffee->id],
                                                ['class' => 'btn btn-warning']
                                            ) ?>
                                        <?php } ?>
                                        <?php if ($allowCancelInfoNavy && ($packagingCaffee->state_packaging_id != 1 || $packagingCaffee->state_packaging_id != 3)) { ?>
                                            <?= $this->Form->postLink(
                                                'Cancelar',
                                                ['controller' => 'PackagingCaffee', 'action' => 'cancel', $packagingCaffee->id],
                                                ['class' => 'btn btn-danger']
                                            ) ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                        <?php  }
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-title mb-2">Elementos Adicionales de la proforma</div>
                    </div>
                </div>
                <?php if ($infoNavy['status_info_navy_id'] != 3/*$showAddictionalItemsForm || $allowEditInfoNavy*/) { ?>
                    <?= $this->Form->create($adictionalElementsHasPackagingCaffee, ['url' => ['controller' => 'AdictionalElementsHasPackagingCaffee', 'action' => 'add', $infoNavy['id']]]) ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('info_navy_id', ['type' => 'hidden', 'value' => $infoNavy['id']]);
                            echo $this->Form->control('adictional_elements_id', [
                                'label' => 'Elemento adicional',
                                'type' => 'select',
                                'empty' => true,
                                'options' => $adictionalElements, 'class' => 'form-control'
                            ]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('quantity', ['label' => 'Cantidad', 'class' => 'form-control']); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('type_unit', [
                                'label' => 'Unidad',
                                'options' => array('CAPA' => 'CAPA', 'UNIDAD' => 'UNIDAD', 'N/A' => 'N/A'),
                                'empty' => true, 'class' => 'form-control'
                            ]); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo $this->Form->control('observation', ['label' => 'Observación', 'class' => 'form-control']);
                            ?>
                        </div>
                        <div class="col-sm-4"><?= $this->Form->button(__('Guardar Elemento'), ['class' => "btn btn-primary-gradient btn-block", "style" => 'margin-top: 28px;']) ?></div>
                    </div>
                    <?= $this->Form->end() ?>
                <?php } ?>
                <br>
                <div class="row">
                    <div class="col-xl-12 d-flex align-items-end flex-column">
                        <div id="AddictionalElementsDTB" class="mb-3 mb-xl-0">
                        </div>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table text-md-" id="AddictionalElementsDT">
                        <thead>
                            <tr>
                                <th>Elemento</th>
                                <th>Cantidad</th>
                                <th>Tipo de Unidad</th>
                                <th>Observaciones</th>
                                <th class="actions"><?= __('Acciones') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($infoNavy->adictional_elements_has_packaging_caffee as $adictionalElementsHasPackagingCaffee) : ?>
                                <tr>
                                    <td><?= $adictionalElementsHasPackagingCaffee->adictional_element->name ?></td>
                                    <td><?= $this->Number->format($adictionalElementsHasPackagingCaffee->quantity) ?></td>
                                    <td><?= $adictionalElementsHasPackagingCaffee->type_unit ?></td>
                                    <td><?= $adictionalElementsHasPackagingCaffee->observation ?></td>
                                    <td class="actions">
                                        <?php if ($infoNavy['status_info_navy_id'] != 3/*$allowEditInfoNavy*/) { ?>
                                            <?= $this->Html->link(
                                                $this->Html->tag('i', 'Editar', ['class' => 'btn btn-warning']),
                                                ['controller' => 'AdictionalElementsHasPackagingCaffee', 'action' => 'edit', $adictionalElementsHasPackagingCaffee->adictional_elements_id, $infoNavy['id']],
                                                ['escape' => false]
                                            ) ?>
                                            <?= $this->Form->postLink(
                                                $this->Html->tag('i', 'Borrar', ['class' => 'btn btn-danger']),
                                                ['controller' => 'AdictionalElementsHasPackagingCaffee', 'action' => 'delete', $adictionalElementsHasPackagingCaffee->adictional_elements_id, $infoNavy['id']],
                                                ['escape' => false, 'confirm' => __('Está seguro de borrar este elemento?')]
                                            ); ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header pb-1">
                <h3 class="card-title mb-2">Mensajes</h3>
                <p class="tx-12 mb-0 text-muted">Por este medio puedes interactuar con los interesados de la proforma</p>
            </div>
            <div class="product-timeline card-body pt-2 mt-1">
                <ul class="timeline-1 mb-0">
                    <?php foreach ($infoNavyMessages as $message) : ?>
                        <div class="mt-0" style="display:block !important;">
                            <i class="ti-pie-chart bg-primary-gradient text-white product-icon"></i>
                            <p style="margin-left: 40px;margin-bottom:0.0rem !important;" class="font-weight-semibold mb-4 tx-14 "><?= $message['message'] ?></p>
                            <p class="float-right tx-11 text-muted"><?= h($message['reg_date']) ?></p>
                            <p style="margin-left: 40px;" class="mb-0 text-muted tx-12"><?= $message['sillcaf_user']['first_name'] . ' ' . $message['sillcaf_user']['last_name'] ?></p>
                        </div>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="card-body">
                <?= $this->Form->create($infoNavyMessage, ['url' => ['action' => 'message']]) ?>
                <div class="row">
                    <div class="col-sm-10">
                        <?php
                        echo $this->Form->control('message', ['type' => 'textarea', 'placeholder' => 'Escriba el mensaje aquí', 'label' => false, 'class' => 'form-control']);
                        echo $this->Form->control('info_navy_id', ['type' => 'hidden', 'value' => $infoNavy['id']]);
                        ?>
                    </div>
                    <div class="col-sm-2">
                        <?= $this->Form->button(__('Enviar'), ['class' => "btn btn-primary-gradient btn-block"]) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="background-color: white;">
        <div id="detailPackagingCaffee" class="modal-body">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var packagingCoffeeDT = $("#PackagingCoffeeDT").DataTable({
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        location.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            scrollX: true,
            responsive: true
        });

        var addictionalElementsDT = $("#AddictionalElementsDT").DataTable({
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        location.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            }
        });

        packagingCoffeeDT.buttons().container()
            .appendTo('#PackagingCoffeeDTB');

        addictionalElementsDT.buttons().container()
            .appendTo('#AddictionalElementsDTB');

    });
</script>