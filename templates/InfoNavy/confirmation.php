<?php

use Cake\Routing\Router;

echo $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken'));

function getInfoQuantity($packagingCaffee)
{
    $infoRemesa = [];
    $infoRemesa['packedQuantity'] = 0;
    $infoRemesa['crossdockinQuantity'] = 0;
    $infoRemesa['totalQuantity'] = 0;
    $infoRemesa['packedLots'] = "";
    $infoRemesa['packedLotsDate'] = "";
    $infoRemesa['crossdockingLots'] = "";
    $infoRemesa['crossdockingLotsToValidate'] = "";
    $infoRemesa['novelties'] = "";
    $infoRemesa['observations'] = "";
    foreach ($packagingCaffee['detail_packakging_crossdocking'] as $crossdocking) {
        if ($crossdocking['status'] == 1) {
            $infoRemesa['crossdockinQuantity'] = $infoRemesa['crossdockinQuantity'] + $crossdocking['qta_coffee'];
            $infoRemesa['crossdockingLots'] = $infoRemesa['crossdockingLots'] . $crossdocking['lot_coffee'] . " x " . $crossdocking['qta_coffee'] . " | ";
            $infoRemesa['crossdockingLotsToValidate'] = $infoRemesa['crossdockingLotsToValidate'] . $crossdocking['lot_coffee'] . " | ";
        }
    }
    $infoRemesa['totalQuantity'] = $infoRemesa['crossdockinQuantity'];
    $crossdockingLots = explode(" | ", $infoRemesa['crossdockingLotsToValidate']);
    foreach ($packagingCaffee['detail_packaging_caffee'] as $remesa) {
        $infoRemesa['packedQuantity'] = $infoRemesa['packedQuantity'] + $remesa['quantity_radicated_bag_out'];
        $infoRemesa['packedLots'] = $infoRemesa['packedLots'] . "3-" . $remesa['remittances_caffee']['client']['exporter_code'] . "-" . $remesa['remittances_caffee']['lot_caffee'] . " | ";
        $infoRemesa['packedLotsDate'] = $infoRemesa['packedLotsDate'] . $remesa['remittances_caffee']['created_date'] . " | ";
        $lotToSearch = $remesa['remittances_caffee']['client']['exporter_code'] . "-" . $remesa['remittances_caffee']['lot_caffee'];
        if (!in_array(($lotToSearch), $crossdockingLots)) {
            $infoRemesa['totalQuantity'] = $infoRemesa['totalQuantity'] + $remesa['quantity_radicated_bag_out'];
        } else {
            $key = array_search($lotToSearch, array_column($packagingCaffee['detail_packakging_crossdocking'], 'lot_coffee'));
            $infoCrossdocking = $packagingCaffee['detail_packakging_crossdocking'][$key];
            if ($remesa['remittances_caffee']['download_caffee_date'] < $infoCrossdocking['reg_date']->modify('-5 hours')) {
                $infoRemesa['totalQuantity'] = $infoRemesa['totalQuantity'] + $remesa['quantity_radicated_bag_out'];
            }
        }
        if ($remesa['remittances_caffee']['remittances_caffee_has_noveltys_caffee']) {
            foreach ($remesa['remittances_caffee']['remittances_caffee_has_noveltys_caffee'] as $novelties) {
                if ($novelties['active']) {
                    $infoRemesa['novelties'] = $infoRemesa['novelties'] . " - " . $novelties['noveltys_caffee']['name'];
                }
            }
        }
    }
    return $infoRemesa;
}

function getAdditionalElements($elements)
{
    $additionalElements = "";
    foreach ($elements as $element) {
        $additionalElements = $additionalElements . "<p>" . $element['adictional_element']['name'] . " - " . $element['quantity'] . " - " . $element['type_unit'] . "</p>";
    }
    return $additionalElements;
}

function hasCrossdocking($crossdocking)
{
    foreach ($crossdocking as $status) {
        if ($status->status == 1) {
            return 'Hay Crossdocking pendientes por ingresar';
        }
    }
    return 'No hay Crossdocking pendientes';
}

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\InfoNavy[]|\Cake\Collection\CollectionInterface $infoNavy
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h3 class="content-title mb-0 my-auto">
                Búsqueda de contenedores a programar
            </h3>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>

<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-3">
                        <label for="arrival-date">Fecha de embalaje</label>
                        <input id="arrival-date" class='form-control' type="text"></input>
                    </div>
                    <div class="col-xl-3">
                        <button onclick="comfirmSelected()" class="btn btn-success" style="margin-top:27px">Programar</button>
                    </div>

                </div>
                <table id="PackagingCoffeeDT">
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Novedades</th>
                            <th>Motonave</th>
                            <th>Modo Emabalaje</th>
                            <th>Cantidad</th>
                            <th>Exportador</th>
                            <th style="width: 100px">Lotes</th>
                            <!-- <th>Fecha descargado</th> -->
                            <!-- <th style="width: 100px">Crossdocking</th> -->
                            <th>ISO</th>
                            <th>Instrucciones</th>
                            <th>Booking</th>
                            <th>Booking 2</th>
                            <th>Proforma</th>
                            <th>Línea</th>
                            <th style="width: 150px">Observaciones</th>
                            <!-- <th>Naviera</th> -->
                            <th class="dt-column-button-right"><?= __('Opciones') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($infoNavy as $proforma) :
                            if ($proforma->packaging_caffee) {
                                foreach ($proforma->packaging_caffee as $packagingCaffee) :
                                    if ($packagingCaffee['state_packaging_id'] == 2) {
                                        $infoRemesa = getInfoQuantity($packagingCaffee);
                                        $additionalElements = getAdditionalElements($proforma->adictional_elements_has_packaging_caffee); ?>
                                        <tr>
                                            <td><?= $packagingCaffee->id ?></td>
                                            <td><?= $packagingCaffee->id ?></td>
                                            <td><?= $infoRemesa['novelties'] ?></td>
                                            <td><?= $proforma['arrivals_motorship']['motorship']['name_motorship'] ?></td>
                                            <td><?= $proforma['packaging_mode'] ?></td>
                                            <td><?= $infoRemesa['packedQuantity'] . "/" . $infoRemesa['totalQuantity'] ?></td>
                                            <td><?= ""?></td>
                                            <td><?= $infoRemesa['packedLots'] ?></td>
                                            <!-- <td><?//= $infoRemesa['packedLotsDate'] ?></td>
                                            <td><?//= $infoRemesa['crossdockingLots'] ?></td> -->
                                            <td><?= $packagingCaffee->iso_ctn ?></td>
                                            <td><?= $additionalElements ?></td>
                                            <td><?= $proforma['booking'] ?></td>
                                            <td><?= $proforma['booking2'] ?></td>
                                            <td><?= $proforma['proforma'] ?></td>
                                            <td><?= $proforma['shipping_line']['business_name'] ?></td>
                                            <td><?= $proforma['observation'] ?></td>
                                            <!-- <td><?//= $proforma['navy_agent']['name'] ?></td> -->
                                            <td class="dt-column-button-right">
                                                <?= $this->Html->link(
                                                    'Ver',
                                                    ['controller' => 'DetailPackagingCaffee', 'action' => 'add', $packagingCaffee->id],
                                                    ['class' => 'btn btn-info']
                                                ) ?>

                                            </td>
                                        </tr>
                        <?php }
                                endforeach;
                            }
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modalerrorconfirmation" aria-modal="true" style="padding-right: 4px;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-body tx-center pd-y-20 pd-x-20">
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                <i class="icon ion-ios-checkmark-circle-outline tx-100 tx-primary lh-1 mg-t-20 d-inline-block"></i>
                <h4 id="errortext" class="tx-primary tx-semibold mg-b-20"></h4>
                <button aria-label="Close" class="btn ripple btn-primary pd-x-25" data-dismiss="modal" type="button" onclick="location.reload()">Continue</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        packagingCoffeeDT = $("#PackagingCoffeeDT").DataTable({
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        location.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            scrollY:        "400px",
            fixedColumns: {
                leftColumns: 1,
                rightColumns: 1
            },
            columnDefs: [{
                'targets': 0,
                'checkboxes': {
                    'selectRow': true
                }
            }, {
                width: "100%",
                targets: -1
            }],
            'select': {
                'style': 'multi'
            }
        });

        packagingCoffeeDT.buttons().container()
            .appendTo('#table_buttons');

        $('#arrival-date').appendDtpicker({
            closeOnSelected: true,
            dateFormat: 'DD/MM/YYYY hh:mm',
            futureOnly: true,
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });
    });

    function comfirmSelected() {
        var rows_selected = packagingCoffeeDT.column(0).checkboxes.selected();

        var containersId = [];
        $.each(rows_selected, function(index, rowId) {
            // Create a hidden element
            containersId.push(rowId);
        });
        $.ajax({
            type: "POST",
            url: "<?= Router::url(['controller' => 'PackagingCaffee', 'action' => 'confirmation']) ?>",
            data: {
                'packing-date': $('#arrival-date').val(),
                'containers': containersId
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
            },
            success: function(response) {
                var responseJSON = JSON.parse(response);
                var showText = '';
                for (var i in responseJSON) {
                    if (responseJSON[i].includes(";")) {
                        showText = showText + responseJSON[i] + "\n";
                    }
                }
                $('#errortext').html(showText);
                $("#modalerrorconfirmation").modal();
            }
        });
    }
</script>