<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="breadcrumb-header justify-content-between">
	<div class="my-auto">
	<div class="d-flex">
		<h4 class="content-title mb-0 my-auto"><h3><?= $this->Html->link(__('Usuarios'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Adicionar</span>
	</div>
	</div>
	<div  class="d-flex my-xl-auto right-content">
	<div id="table_buttons" class="mb-3 mb-xl-0">
							
	</div>
	</div>
</div>

<div class="row row-sm">

    <div class="column-responsive column-80">
        <div class="users form content">
            <?= $this->Form->create($user) ?>
            <fieldset>
                <div class="row">
				<div class="col-sm-8"><legend><?= __('Registrar nuevo Usuario') ?></legend></div>
				<div class="col-sm-4"><?= $this->Form->button(__('Guardar'),['class'=>"btn btn-primary-gradient btn-block"]) ?></div>
				</div>
                <?php
					echo $this->Form->control('first_name');
					echo $this->Form->control('last_name');
                    echo $this->Form->control('username');
					echo $this->Form->control('password');
                    echo $this->Form->control('perfiles_id', ['options' => $perfiles, 'class'=>'form-control select2']);
					echo $this->Form->control('active_user',['type'=>'checkbox']);
                ?>
            </fieldset>
			
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
		$('.select2').select2({
			placeholder: 'Choose one',
			searchInputPlaceholder: 'Search'
		});
		$('.select2-no-search').select2({
			minimumResultsForSearch: Infinity,
			placeholder: 'Choose one'
		});
	});
</script>