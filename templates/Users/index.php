<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<div class="breadcrumb-header justify-content-between">
						<div class="my-auto">
							<div class="d-flex">
								<h4 class="content-title mb-0 my-auto"><h3>Usuarios</h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
							</div>
						</div>
						<div  class="d-flex my-xl-auto right-content">
							<div id="table_buttons" class="mb-3 mb-xl-0">
								
							</div>
						</div>
</div>


<div class="users index content">
    <div class="table-responsive">
		<div class="row">
                <div class="col">
    </div>
    <div class="col-md-auto">
    </div>
			<div class="col col-lg-2">
			</div>
            </div>
				
        <table class="table text-md-" id="tableData">
            <thead>
                <tr>
                    <th>id</th>
                    <th>apellidos</th>
                    <th>nombres</th>
                    <th>username</th>
					<th>fecha registro</th>
					<th>perfil</th>
					<th>Activo</th>
                    <th>Opciones</th>
                </tr>
            </thead>
        </table>
		</div>
</div>

<script>
        $(document).ready(function(){
			
           var table = $("#tableData").DataTable({
              processing: true,
              serverSide: true,
			  dom: 'Bfitp',
			  //dom: '<Blf<t>ip>',
				buttons: [
					'pageLength','copy', 'excel', 'pdf',
					{
						extend: 'colvis',
						postfixButtons: [ 'colvisRestore' ]
					},
					{
						text: 'Reload',
						action: function ( e, dt, node, config ) {
							dt.ajax.reload();
						}
					}
				],
				lengthMenu: [
					[ 10, 25, 50, -1 ],
					[ '10 rows', '25 rows', '50 rows', 'Show all' ]
				],
			  language: {
					searchPlaceholder: 'Search...',
					sSearch: '',
					lengthMenu: '_MENU_ ',
				},
              sAjaxSource: "../ServerSide/serversideRequest.php?model=users",
              columnDefs:[
			  {
				  "targets": 6,
				   "data": "active",
					"render": function ( data, type, row, meta ) {
						if(row[6] == 1){
							
							return "<span class=\"label text-success d-flex\"><div class=\"dot-label bg-success mr-1\"></div>Active</span>";
						}
						else{
							return "<span class=\"label text-muted d-flex\"><div class=\"dot-label bg-gray-300 mr-1\"></div>Inactive</span>";
						}
					}
				  
			  },
			  {
                  "data":null,
				  "sorteble": false,
				  "targets": -1,
				  "defaultContent": "<a id=\"edit\" href=\"#\"><i class=\"side-menu__icon btn btn-sm btn-info far fa-edit\"></i></a><a id=\"view\" href=\"#\"><i class=\"side-menu__icon btn btn-sm btn-primary fas fa-search\"></i></a>"
              }]   
           });
		    
		   table.buttons().container()
			.appendTo( '#table_buttons' );	
			
			$('#tableData tbody').on( 'click', 'a#edit', function () {
			
				var data = table.row( $(this).parents('tr') ).data();
				window.open("sillcaf/users/edit/"+data[0],'_self');
			} );
			
			$('#tableData tbody').on( 'click', 'a#view', function () {
			
				var data = table.row( $(this).parents('tr') ).data();
				window.open("sillcaf/users/view/"+data[0],'_self');
			} );
			
        });
</script>
