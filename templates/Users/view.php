<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="breadcrumb-header justify-content-between">
						<div class="my-auto">
							<div class="d-flex">
								<h4 class="content-title mb-0 my-auto"><h3><?= $this->Html->link(__('Usuarios'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Vista</span>
							</div>
						</div>
						<div  class="d-flex my-xl-auto right-content">
							<div id="table_buttons" class="mb-3 mb-xl-0">
								
							</div>
						</div>
</div>

<div class="row">
    <div class="column-responsive column-80">
        <div class="users view content">
            
			<div class="row">
				<div class="col-sm-8"><legend><?= __('Detalles Usuario') ?></legend></div>
				<div class="col-sm-4"><?= $this->Html->link(__('Editar'), ['action' => 'edit',$user->id], ['class' => 'btn btn-primary-gradient btn-block'])?></div>
			</div>
            <table>
                <tr>
                    <th><?= __('Username') ?></th>
                    <td><?= h($user->username) ?></td>
                </tr>
                <tr>
                    <th><?= __('Password') ?></th>
                    <td><?= h($user->password) ?></td>
                </tr>
				<tr>
                    <th><?= __('Last Name') ?></th>
                    <td><?= h($user->last_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('First') ?></th>
                    <td><?= h($user->first_name) ?></td>
                </tr>
				<tr>
                    <th><?= __('Status') ?></th>
                    <td>
					<?php if($user->active_user == 1){
						echo "<span class=\"label text-success d-flex\"><div class=\"dot-label bg-success mr-1\"></div>Active</span>";
					}
					else{
						echo "<span class=\"label text-muted d-flex\"><div class=\"dot-label bg-gray-300 mr-1\"></div>Inactive</span>";
					}?>
					</td>
                </tr>
                <tr>
                    <th><?= __('Perfile') ?></th>
                    <td><?= $user->has('perfile') ? $this->Html->link($user->perfile->perfiles_name, ['controller' => 'Perfiles', 'action' => 'view', $user->perfile->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($user->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
