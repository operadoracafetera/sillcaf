<div class="page">
			<div class="container-fluid">
				<div class="row no-gutter">
					<!-- The image half -->
					<div class="col-md-6 col-lg-6 col-xl-7 d-none d-md-flex bg-primary-transparent">
						<div class="row wd-100p mx-auto text-center">
							<div class="col-md-12 col-lg-12 col-xl-12 my-auto mx-auto wd-100p">
								<img src="/sillcaf/assets/img/media/login.png" class="my-auto ht-xl-80p wd-md-100p wd-xl-80p mx-auto" alt="logo">
							</div>
						</div>
					</div>
					<!-- The content half -->
					<div class="col-md-6 col-lg-6 col-xl-5 bg-white">
						<div class="login d-flex align-items-center py-2">
							<!-- Demo content-->
							<div class="container p-0">
								<div class="row">
									<div class="col-md-10 col-lg-10 col-xl-9 mx-auto">
										<div class="card-sigin">
											<div class="mb-5 d-flex"> <a href="index.html"><img src="/sillcaf/assets/img/brand/favicon.png" class="sign-favicon ht-40" alt="logo"></a><h1 class="main-logo1 ml-1 mr-0 my-auto tx-28">Va<span>le</span>x</h1></div>
											<div class="card-sigin">
												<div class="main-signup-header">
													<h2>Bienvenido a SILLCAF</h2>
													<h5 class="font-weight-semibold mb-4">Ingrese los datos acceso para ingresar al sistema</h5>
													 <?= $this->Form->create() ?>
														<div class="form-group">
															<?php echo $this->Form->control('username',['class'=>'form-control','required'=>'required','label'=>'Usuario']);?>
														</div>
														<div class="form-group">
															<?php echo $this->Form->control('password',['required'=>'required','class'=>'form-control']);?>
														</div>
														<?= $this->Form->button(__('Ingresar'),['class'=>['btn btn-main-primary btn-block']]) ?>
													
													<?= $this->Form->end() ?>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div><!-- End -->
						</div>
					</div><!-- End -->
				</div>
			</div>

		</div>