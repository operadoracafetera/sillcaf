<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPerfilesHasPermiss $sillcafPerfilesHasPermiss
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Sillcaf Perfiles Has Permisses'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillcafPerfilesHasPermisses form content">
            <?= $this->Form->create($sillcafPerfilesHasPermiss) ?>
            <fieldset>
                <legend><?= __('Add Sillcaf Perfiles Has Permiss') ?></legend>
                <?php
                    echo $this->Form->control('perfil_id', ['options' => $sillcafPerfiles]);
                    echo $this->Form->control('permisse_id', ['options' => $sillcafPermisses]);
                    echo $this->Form->control('reg_date');
                    echo $this->Form->control('active');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
