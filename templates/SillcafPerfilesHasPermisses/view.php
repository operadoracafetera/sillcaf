<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPerfilesHasPermiss $sillcafPerfilesHasPermiss
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Sillcaf Perfiles Has Permiss'), ['action' => 'edit', $sillcafPerfilesHasPermiss->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Sillcaf Perfiles Has Permiss'), ['action' => 'delete', $sillcafPerfilesHasPermiss->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafPerfilesHasPermiss->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Sillcaf Perfiles Has Permisses'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Sillcaf Perfiles Has Permiss'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillcafPerfilesHasPermisses view content">
            <h3><?= h($sillcafPerfilesHasPermiss->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Sillcaf Perfile') ?></th>
                    <td><?= $sillcafPerfilesHasPermiss->has('sillcaf_perfile') ? $this->Html->link($sillcafPerfilesHasPermiss->sillcaf_perfile->id, ['controller' => 'SillcafPerfiles', 'action' => 'view', $sillcafPerfilesHasPermiss->sillcaf_perfile->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Sillcaf Permiss') ?></th>
                    <td><?= $sillcafPerfilesHasPermiss->has('sillcaf_permiss') ? $this->Html->link($sillcafPerfilesHasPermiss->sillcaf_permiss->id, ['controller' => 'SillcafPermisses', 'action' => 'view', $sillcafPerfilesHasPermiss->sillcaf_permiss->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($sillcafPerfilesHasPermiss->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Active') ?></th>
                    <td><?= $this->Number->format($sillcafPerfilesHasPermiss->active) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reg Date') ?></th>
                    <td><?= h($sillcafPerfilesHasPermiss->reg_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
