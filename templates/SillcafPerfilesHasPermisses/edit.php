<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPerfilesHasPermiss $sillcafPerfilesHasPermiss
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sillcafPerfilesHasPermiss->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafPerfilesHasPermiss->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Sillcaf Perfiles Has Permisses'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillcafPerfilesHasPermisses form content">
            <?= $this->Form->create($sillcafPerfilesHasPermiss) ?>
            <fieldset>
                <legend><?= __('Edit Sillcaf Perfiles Has Permiss') ?></legend>
                <?php
                    echo $this->Form->control('perfil_id', ['options' => $sillcafPerfiles]);
                    echo $this->Form->control('permisse_id', ['options' => $sillcafPermisses]);
                    echo $this->Form->control('reg_date');
                    echo $this->Form->control('active');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
