<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPerfilesHasPermiss[]|\Cake\Collection\CollectionInterface $sillcafPerfilesHasPermisses
 */
?>
<div class="sillcafPerfilesHasPermisses index content">
    <?= $this->Html->link(__('New Sillcaf Perfiles Has Permiss'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Sillcaf Perfiles Has Permisses') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('perfil_id') ?></th>
                    <th><?= $this->Paginator->sort('permisse_id') ?></th>
                    <th><?= $this->Paginator->sort('reg_date') ?></th>
                    <th><?= $this->Paginator->sort('active') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sillcafPerfilesHasPermisses as $sillcafPerfilesHasPermiss): ?>
                <tr>
                    <td><?= $this->Number->format($sillcafPerfilesHasPermiss->id) ?></td>
                    <td><?= $sillcafPerfilesHasPermiss->has('sillcaf_perfile') ? $this->Html->link($sillcafPerfilesHasPermiss->sillcaf_perfile->id, ['controller' => 'SillcafPerfiles', 'action' => 'view', $sillcafPerfilesHasPermiss->sillcaf_perfile->id]) : '' ?></td>
                    <td><?= $sillcafPerfilesHasPermiss->has('sillcaf_permiss') ? $this->Html->link($sillcafPerfilesHasPermiss->sillcaf_permiss->id, ['controller' => 'SillcafPermisses', 'action' => 'view', $sillcafPerfilesHasPermiss->sillcaf_permiss->id]) : '' ?></td>
                    <td><?= h($sillcafPerfilesHasPermiss->reg_date) ?></td>
                    <td><?= $this->Number->format($sillcafPerfilesHasPermiss->active) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $sillcafPerfilesHasPermiss->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sillcafPerfilesHasPermiss->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sillcafPerfilesHasPermiss->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafPerfilesHasPermiss->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
