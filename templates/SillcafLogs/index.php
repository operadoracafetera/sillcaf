<?php

use Cake\Routing\Router;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillCafLog[]|\Cake\Collection\CollectionInterface $sillCafLogs
 */
?>


<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h3 class="content-title mb-0 my-auto">
                Logs de transacciones
            </h3><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>


<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">Transacciones</h4>
                    <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-md-" id="tablaData">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Fecha Evento</th>
                                <th>Modulo</th>
                                <th>Data Antes</th>
                                <th>Data Despues</th>
                                <th>Ip conexion</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $("#tablaData").DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            order: [[ 0, "desc" ]],
            ajax: "<?= Router::url(['controller' => 'SillcafLogs', 'action' => 'indexJson']) ?>",
            columnDefs: [
                {
                    "data": "id",
                    "targets": 0
                },
                {
                    "data": "date_event",
                    "targets": 1,
                    "render":function(data){
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    "data": "module_name",
                    "targets": 2
                },
                {
                    "data": "ip_source",
                    "targets": 5
                },
                {
                    "data": "sillcaf_user",
                    "targets": 6,
                    "render":function(data){
                        return data.username;
                    }
                },
                {
                    "data": "data_before",
                    "targets": 3
                },
                {
                    "data": "data_after",
                    "targets": 4
                }
            ]
        });

        table.buttons().container()
            .appendTo('#table_buttons');

    });
</script>