<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillCafLog $sillCafLog
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Sill Caf Log'), ['action' => 'edit', $sillCafLog->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Sill Caf Log'), ['action' => 'delete', $sillCafLog->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sillCafLog->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Sill Caf Logs'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Sill Caf Log'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillCafLogs view content">
            <h3><?= h($sillCafLog->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Sillcaf Perfile') ?></th>
                    <td><?= $sillCafLog->has('sillcaf_perfile') ? $this->Html->link($sillCafLog->sillcaf_perfile->id, ['controller' => 'SillcafPerfiles', 'action' => 'view', $sillCafLog->sillcaf_perfile->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $sillCafLog->has('user') ? $this->Html->link($sillCafLog->user->id, ['controller' => 'Users', 'action' => 'view', $sillCafLog->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($sillCafLog->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Date Event') ?></th>
                    <td><?= h($sillCafLog->date_event) ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Data') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($sillCafLog->data)); ?>
                </blockquote>
            </div>
        </div>
    </div>
</div>
