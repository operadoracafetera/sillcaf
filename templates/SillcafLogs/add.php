<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillCafLog $sillCafLog
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Sill Caf Logs'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillCafLogs form content">
            <?= $this->Form->create($sillCafLog) ?>
            <fieldset>
                <legend><?= __('Add Sill Caf Log') ?></legend>
                <?php
                    echo $this->Form->control('data');
                    echo $this->Form->control('permisses_id', ['options' => $sillcafPerfiles]);
                    echo $this->Form->control('users_id', ['options' => $users]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
