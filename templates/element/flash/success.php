<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
	
	<div class="modal" id="modaldemo4" >
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content tx-size-sm">
				<div class="modal-body tx-center pd-y-20 pd-x-20">
					<button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button> <i class="icon ion-ios-checkmark-circle-outline tx-100 tx-primary lh-1 mg-t-20 d-inline-block"></i>
					<h4 class="tx-primary tx-semibold mg-b-20"><?= $message ?></h4>
					<button aria-label="Close" class="btn ripple btn-primary pd-x-25" data-dismiss="modal" type="button">Continue</button>
				</div>
			</div>
		</div>
	</div>

<script>
$(document).ready(function(){
    $("#modaldemo4").modal();
});
</script>