<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ClosingCn $closingCn
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><?= $this->Html->link(__('Cierres navieros'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Editar</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="main-content-label mg-b-5"> Registro de </div>
                <div class="col-lg">
                    <?= $this->Form->create($closingCn) ?>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-8">
                                <legend><?= __('Editar cierre naviero') ?></legend>
                            </div>
                            <div class="col-sm-4"><?= $this->Form->button(__('Guardar'), ['class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        </div>
                        <?php 
                        echo $this->Form->control('closing_date', ['label' => 'Fecha de cierre', 'class' => 'form-control', 'type' => 'text']);
                        echo $this->Form->control('closing_cn_navy_agent', ['options' => $navyAgent, 'value' => $closingCn['closing_cn_navy_agent_id'], 'label' => 'Agente naviero', 'class' => 'form-control', 'disabled' => !$allowEdit]);
                        echo $this->Form->control('arrivals_motorships', ['options' => $arrivalsMotorships, 'value' => $closingCn['arrivals_motorships_id'], 'label' => 'Motonave anunciada', 'class' => 'form-control', 'disabled' => !$allowEdit]);
                        echo $this->Form->control('closing_cn_navy_agent_id', ['options' => $navyAgent, 'label' => 'Agente naviero', 'class' => 'form-control', 'type' => 'hidden']);
                        echo $this->Form->control('arrivals_motorships_id', ['options' => $arrivalsMotorships, 'label' => 'Motonave anunciada', 'class' => 'form-control', 'type' => 'hidden']);
                        ?>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#closing-cn-navy-agent').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });
        $('#arrivals-motorships').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });
        $('#closing-date').appendDtpicker({
            closeOnSelected: true,
            futureOnly: true,
            dateFormat: 'DD/MM/YYYY hh:mm',
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });

    });
</script>