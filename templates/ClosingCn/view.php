<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ClosingCn $closingCn
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Closing Cn'), ['action' => 'edit', $closingCn->closing_cn_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Closing Cn'), ['action' => 'delete', $closingCn->closing_cn_id], ['confirm' => __('Are you sure you want to delete # {0}?', $closingCn->closing_cn_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Closing Cn'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Closing Cn'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="closingCn view content">
            <h3><?= h($closingCn->closing_cn_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Custom') ?></th>
                    <td><?= $closingCn->has('custom') ? $this->Html->link($closingCn->custom->id, ['controller' => 'Customs', 'action' => 'view', $closingCn->custom->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Shipping Line') ?></th>
                    <td><?= $closingCn->has('shipping_line') ? $this->Html->link($closingCn->shipping_line->id, ['controller' => 'ShippingLines', 'action' => 'view', $closingCn->shipping_line->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Navy Agent') ?></th>
                    <td><?= $closingCn->has('navy_agent') ? $this->Html->link($closingCn->navy_agent->name, ['controller' => 'NavyAgent', 'action' => 'view', $closingCn->navy_agent->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Arrivals Motorship') ?></th>
                    <td><?= $closingCn->has('arrivals_motorship') ? $this->Html->link($closingCn->arrivals_motorship->arrivals_motorships_id, ['controller' => 'ArrivalsMotorships', 'action' => 'view', $closingCn->arrivals_motorship->arrivals_motorships_id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $closingCn->has('user') ? $this->Html->link($closingCn->user->id, ['controller' => 'Users', 'action' => 'view', $closingCn->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing Cn Id') ?></th>
                    <td><?= $this->Number->format($closingCn->closing_cn_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing Date') ?></th>
                    <td><?= h($closingCn->closing_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing Cn Reg Date') ?></th>
                    <td><?= h($closingCn->closing_cn_reg_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing Cn Update Date') ?></th>
                    <td><?= h($closingCn->closing_cn_update_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
