<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ClosingCn $closingCn
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><?= $this->Html->link(__('Cierres navieros'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Registrar</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg">
                    <?= $this->Form->create($closingCn) ?>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-8">
                                <legend><?= __('Registro de cierre ') ?></legend>
                            </div>
                            <div class="col-sm-4"><?= $this->Form->button(__('Guardar'), ['class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        </div>
                        <?php
                        echo $this->Form->control('closing_date', ['label' => 'Fecha de cierre', 'class' => 'form-control', 'type' => 'text']);
                        echo $this->Form->control('closing_cn_navy_agent_id[]', ['options' => $navyAgent, 'label' => 'Agente naviero', 'class' => 'form-control']);
                        echo $this->Form->control('arrivals_motorships_id', ['options' => $arrivalsMotorships, 'label' => 'Motonave anunciada', 'class' => 'form-control']);
                        ?>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#closing-cn-navy-agent-id').select2({
            placeholder: 'Seleccione..',
            multiple : true,
            tags: true,
            tokenSeparators: [',', ' '],
            searchInputPlaceholder: 'Search'
        });
        $('#arrivals-motorships-id').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });

        $('#closing-date').appendDtpicker({
            closeOnSelected: true,
            futureOnly: true,
            dateFormat:'DD/MM/YYYY hh:mm',
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });

    });
</script>