<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafModule $sillcafModule
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sillcafModule->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafModule->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Sillcaf Modules'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillcafModules form content">
            <?= $this->Form->create($sillcafModule) ?>
            <fieldset>
                <legend><?= __('Edit Sillcaf Module') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('reg_date');
                    echo $this->Form->control('active');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
