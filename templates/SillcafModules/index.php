<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafModule[]|\Cake\Collection\CollectionInterface $sillcafModules
 */
?>
<div class="sillcafModules index content">
    <?= $this->Html->link(__('New Sillcaf Module'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Sillcaf Modules') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('reg_date') ?></th>
                    <th><?= $this->Paginator->sort('active') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sillcafModules as $sillcafModule): ?>
                <tr>
                    <td><?= $this->Number->format($sillcafModule->id) ?></td>
                    <td><?= h($sillcafModule->name) ?></td>
                    <td><?= h($sillcafModule->reg_date) ?></td>
                    <td><?= $this->Number->format($sillcafModule->active) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $sillcafModule->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sillcafModule->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sillcafModule->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafModule->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
