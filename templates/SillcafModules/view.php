<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafModule $sillcafModule
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Sillcaf Module'), ['action' => 'edit', $sillcafModule->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Sillcaf Module'), ['action' => 'delete', $sillcafModule->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafModule->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Sillcaf Modules'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Sillcaf Module'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillcafModules view content">
            <h3><?= h($sillcafModule->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($sillcafModule->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($sillcafModule->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Active') ?></th>
                    <td><?= $this->Number->format($sillcafModule->active) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reg Date') ?></th>
                    <td><?= h($sillcafModule->reg_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
