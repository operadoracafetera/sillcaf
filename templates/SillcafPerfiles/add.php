<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPerfile $sillcafPerfile
 */
?>

    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><h3><?= $this->Html->link(__('Perfiles'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Adicionar</span>
        </div>
        </div>
        <div  class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
                                
        </div>
        </div>
    </div>


<div class="row">

    <div class="column-responsive column-80">
        <div class="sillcafPerfiles form content">
            <?= $this->Form->create($sillcafPerfile) ?>
            <fieldset>
              <div class="row">
                <div class="col-sm-8"><legend><?= __('Registrar nuevo Perfil') ?></legend></div>
                <div class="col-sm-4"><?= $this->Form->button(__('Guardar'),['class'=>"btn btn-primary-gradient btn-block"]) ?></div>
                </div> 
                <?php
                    echo $this->Form->control('perfiles_name');
                    echo $this->Form->control('perfiles_description');
                    echo $this->Form->control('perfiles_reg_date');
                    echo $this->Form->control('perfiles_active',['type'=>'checkbox']);
                ?>
            </fieldset>
            </br>

            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
