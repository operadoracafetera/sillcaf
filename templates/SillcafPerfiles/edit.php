<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPerfile $sillcafPerfile
 */
?>
<style type="text/css">
    #container-profilepermisse {
        height: 300px;
        width: 100%;
        border: 1px solid #ddd;
        background: #f1f1f1;
        overflow-y: scroll;
    }
    #container-profilepermisse-main {height: auto;}
    .container-profilepermisse-form-control {padding:4px; background:#fff;}
    .input.checkbox{margin-bottom: -8px;}    
</style>


    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><h3><?= $this->Html->link(__('Perfil'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Editar</span>
        </div>
        </div>
        <div  class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
                                
        </div>
        </div>
    </div>

<div class="row">

    <div class="column-responsive column-80">

        <div class="w3-bar w3-blue">
          <button class="w3-bar-item btn btn-primary-gradient" onclick="eventOption('tab-1')">Detalle Perfil</button>
          <button class="w3-bar-item btn btn-primary-gradient" onclick="eventOption('tab-2')">Permisos</button>
        </div>

        <div class="sillcafPerfiles form content">
            <?= $this->Form->create($sillcafPerfile) ?> 
            <fieldset>
                <div class="row">
                <div class="col-sm-8"><legend><?= __('Editar Perfil') ?></legend></div>
                <div class="col-sm-4"><?= $this->Form->button(__('Guardar'),['class'=>"btn btn-primary-gradient btn-block"]) ?></div>
                </div> 

                <div id="tab-1" class="w3-container perfil">
                <?php
                    echo $this->Form->control('perfiles_name');
                    echo $this->Form->control('perfiles_description');
                    echo $this->Form->control('perfiles_reg_date');
                    echo $this->Form->control('perfiles_active');
                ?>
                </div>
                                
                <div id="tab-2" class="w3-container perfil" style="display:none">

                    <div id="container-profilepermisse">
                      <div id="container-profilepermisse-main">

                            <?php
                                $arrayAuth = explode(",",$sillcafPerfile['modules']);
                                
                                foreach($modules as $key => $value ){
                                    $this->log(json_encode($value),'debug');
                                    if(in_array(strval($key),$arrayAuth)){
                                        echo $this->Form->control($key, array('label'=>$value['permisse']." - ".$value['sillcaf_module']['name'],'type'=>'checkbox','checked'=>true, "class" => "container-profilepermisse-form-control"));
                                    }
                                    else{
                                        echo $this->Form->control($key, array('label'=>$value['permisse']." - ".$value['sillcaf_module']['name'],'type'=>'checkbox', "class" => "container-profilepermisse-form-control"));
                                    }                    
                                }
                            ?>

                        </div>
                    </div>

                </div>  
                </br>
                </br>

            </fieldset>
            </br>

            <?= $this->Form->end() ?>
        </div>
    </div>
</div>


<script>    
    function eventOption(opctionName) {
          var i;
          var x = document.getElementsByClassName("perfil");
          for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";  
          }
          document.getElementById(opctionName).style.display = "block";  
    }   
</script>
