<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPerfile $sillcafPerfile
 */
?>

    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
        <div class="d-flex">
                <h4 class="content-title mb-0 my-auto"><h3><?= $this->Html->link(__('Perfil'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Vista</span>
        </div>
        </div>
        <div  class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
                                
        </div>
        </div>
    </div>


<div class="row">
    <div class="column-responsive column-80">

        <div class="sillcafPerfiles view content">
            <div class="row">
                <div class="col-sm-8"><legend><?= __('Detalles Modulos') ?></legend></div>
                <div class="col-sm-4"><?= $this->Html->link(__('Editar'), ['action' => 'edit',$sillcafPerfile->id], ['class' => 'btn btn-primary-gradient btn-block'])?></div>
            </div>

                <table>
                    <tr>
                        <th><?= __('Id') ?></th>
                        <td><?= $this->Number->format($sillcafPerfile->id) ?></td>
                    </tr>
                    <tr>
                        <th><?= __('Perfiles Name') ?></th>
                        <td><?= h($sillcafPerfile->perfiles_name) ?></td>
                    </tr>
                    <tr>
                        <th><?= __('Perfiles Description') ?></th>
                        <td><?= h($sillcafPerfile->perfiles_description) ?></td>
                    </tr>
                    <tr>
                        <th><?= __('Perfiles Reg Date') ?></th>
                        <td><?= h($sillcafPerfile->perfiles_reg_date) ?></td>
                    </tr>
                    <tr>
                        <th><?= __('Status') ?></th>
                        <td><?= h(($sillcafPerfile->perfiles_active) ? "ACTIVO":"DESACTIVO") ?></td>
                    </tr>

                </table>

        </div>
    </div>
</div>

