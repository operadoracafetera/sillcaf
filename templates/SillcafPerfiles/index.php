<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafPerfile[]|\Cake\Collection\CollectionInterface $sillcafPerfiles
 */
?>

    <div class="breadcrumb-header justify-content-between">
        <div class="my-auto">
            <div class="d-flex">
                <h4 class="content-title mb-0 my-auto"><h3>Perfiles</h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
            </div>
        </div>
        <div  class="d-flex my-xl-auto right-content">
            <div id="table_buttons" class="mb-3 mb-xl-0">
                
            </div>
        </div>
    </div>
    <br>

        <div class="sillcafModules index content">
        <div class="table-responsive">   
            <table class="table text-md-" id="tableData">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>name</th>
                        <th>description</th>
                        <th>reg_date</th>
                        <th>active</th>
                        <th class="actions"><?= __('Opciones') ?></th>
                    </tr>   
                </thead>
            </table>
        </div>
    </div>


<script>
        $(document).ready(function(){
            
           var table = $("#tableData").DataTable({
              processing: true,
              serverSide: true,
              dom: 'Bfitp',
              //dom: '<Blf<t>ip>',
                buttons: [
                    'pageLength','copy', 'excel', 'pdf',
                    {
                        extend: 'colvis',
                        postfixButtons: [ 'colvisRestore' ]
                    },
                    {
                        text: 'Reload',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                        }
                    }
                ],
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],
              language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ ',
                },
              sAjaxSource: "http://localhost/sillcaf/ServerSide/serversideRequest.php?model=sillcaf_perfiles",
              columnDefs:[{
                  "data":null,
                  "targets": -1,
                  "defaultContent": "<a id=\"edit\" href=\"#\"><i class=\"side-menu__icon far fa-edit\"></i></a><a id=\"view\" href=\"#\"><i class=\"side-menu__icon fas fa-search\"></i></a>"
              }]   
           });
            
           table.buttons().container()
            .appendTo( '#table_buttons' );  
            
            $('#tableData tbody').on( 'click', 'a#edit', function () {
            
                var data = table.row( $(this).parents('tr') ).data();
                window.open("http://localhost/sillcaf/sillcaf-perfiles/edit/"+data[0],'_self');
            } );

            $('#tableData tbody').on( 'click', 'a#view', function () {
            
                var data = table.row( $(this).parents('tr') ).data();
                window.open("http://localhost/sillcaf/sillcaf-perfiles/view/"+data[0],'_self');
            } );
            

        });
</script>

