<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ShippingLine $shippingLine
 */
?>

<div class="breadcrumb-header justify-content-between">
						<div class="my-auto">
							<div class="d-flex">
								<h4 class="content-title mb-0 my-auto"><h3><?= $this->Html->link(__('Lineas Maritimas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Vista</span>
							</div>
						</div>
						<div  class="d-flex my-xl-auto right-content">
							<div id="table_buttons" class="mb-3 mb-xl-0">
								
							</div>
						</div>
</div>

<div class="row">
    <div class="column-responsive column-80">
        <div class="shippingLines view content">
		<div class="row">
				<div class="col-sm-8"><legend><?= __('Detalles Linea Maritima') ?></legend></div>
				<div class="col-sm-4"><?= $this->Html->link(__('Editar'), ['action' => 'edit',$shippingLine->id], ['class' => 'btn btn-primary-gradient btn-block'])?></div>
			</div>
		
            <h3><?= h($shippingLine->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Business Name') ?></th>
                    <td><?= h($shippingLine->business_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Description') ?></th>
                    <td><?= h($shippingLine->description) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($shippingLine->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Codigo Spb') ?></th>
                    <td><?= $this->Number->format($shippingLine->codigo_spb) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created Date') ?></th>
                    <td><?= h($shippingLine->created_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated Date') ?></th>
                    <td><?= h($shippingLine->updated_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
