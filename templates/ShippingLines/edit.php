<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ShippingLine $shippingLine
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $shippingLine->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $shippingLine->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Shipping Lines'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="shippingLines form content">
            <?= $this->Form->create($shippingLine) ?>
            <fieldset>
                <legend><?= __('Edit Shipping Line') ?></legend>
                <?php
                    echo $this->Form->control('business_name');
                    echo $this->Form->control('description');
                    echo $this->Form->control('created_date');
                    echo $this->Form->control('updated_date');
                    echo $this->Form->control('codigo_spb');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
