<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Motorship $motorship
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Motorship'), ['action' => 'edit', $motorship->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Motorship'), ['action' => 'delete', $motorship->id], ['confirm' => __('Are you sure you want to delete # {0}?', $motorship->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Motorships'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Motorship'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="motorships view content">
            <h3><?= h($motorship->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name Motorship') ?></th>
                    <td><?= h($motorship->name_motorship) ?></td>
                </tr>
                <tr>
                    <th><?= __('Description Motorship') ?></th>
                    <td><?= h($motorship->description_motorship) ?></td>
                </tr>
                <tr>
                    <th><?= __('Shipping Line') ?></th>
                    <td><?= $motorship->has('shipping_line') ? $this->Html->link($motorship->shipping_line->id, ['controller' => 'ShippingLines', 'action' => 'view', $motorship->shipping_line->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id Motorship') ?></th>
                    <td><?= $this->Number->format($motorship->id_motorship) ?></td>
                </tr>
                <tr>
                    <th><?= __('Active Motorship') ?></th>
                    <td><?= $this->Number->format($motorship->active_motorship) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reg Date Motorship') ?></th>
                    <td><?= h($motorship->reg_date_motorship) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
