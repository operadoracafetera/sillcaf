<?php

use Cake\Routing\Router;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArrivalsMotorship[]|\Cake\Collection\CollectionInterface $arrivalsMotorships
 */
?>

<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h3 class="content-title mb-0 my-auto">
                Motonaves
            </h3><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>


<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">Motonaves</h4>
                    <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-md-" id="tablaData">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Fecha reg</th>
                                <th>Estado</th>
                                <th>--</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $("#tablaData").DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            order: [[ 0, "desc" ]],
            ajax: "<?= Router::url(['controller' => 'Motorships', 'action' => 'indexJson']) ?>",
            columnDefs: [{
                    "data": "id_motorship",
                    "targets": 0
                },
                {
                    "data": "name_motorship",
                    "targets": 1
                },
                {
                    "data": "description_motorship",
                    "targets": 2
                },
                {
                    "data": "reg_date_motorship",
                    "targets": 3,
                    "render":function(data){
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    "targets": 4,
                    "data": "active_motorship",
                    "render": function(data, type, row, meta) {
                        if (row['active_motorship'] == 1) {
                            return "<span class=\"label text-success d-flex\"><div class=\"dot-label bg-success mr-1\"></div>Active</span>";
                        } else {
                            return "<span class=\"label text-muted d-flex\"><div class=\"dot-label bg-gray-300 mr-1\"></div>Inactive</span>";
                        }
                    }
                },
                {
                    "data": null,
                    "targets": -1,
                    "defaultContent": '<a id="edit" href="#" class="btn btn-warning">Editar</a>'
                }
            ]
        });

        table.buttons().container()
            .appendTo('#table_buttons');

        $('#tablaData tbody').on('click', 'a#edit', function() {
            var data = table.row($(this).parents('tr')).data();
            window.open("<?= Router::url(['controller' => 'Motorships', 'action' => 'edit']) . '/' ?>" + data['id_motorship'], '_self');
        });
    });
</script>