<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArrivalsMotorship $arrivalsMotorship
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><?= $this->Html->link(__('Motonaves'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Registrar</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg">
                <?= $this->Form->create($motorship) ?>
            <fieldset>
                <div class="row">
                <div class="col-sm-8"><legend><?= __('Registrar nueva Motonave') ?></legend></div>
                <div class="col-sm-4"><?= $this->Form->button(__('Guardar'),['class'=>"btn btn-primary-gradient btn-block"]) ?></div>
                </div> 
                <?php
                    echo $this->Form->control('name_motorship',['label' => 'Nombre motonave', 'class' => 'form-control']);
                    echo $this->Form->control('description_motorship',['label' => 'Descripción motonave', 'class' => 'form-control']);
                    echo $this->Form->control('reg_date_motorship',['label' => 'Fecha reg', 'class' => 'form-control']);
                    echo $this->Form->control('active_motorship',['type' => 'checkbox','label' => 'Activo',]);
                ?>
            </fieldset>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
        $('#reg_date_motorship').appendDtpicker({
            closeOnSelected: true,
            dateFormat:'DD/MM/YYYY hh:mm',
            futureOnly: true,
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });
	});
</script>
