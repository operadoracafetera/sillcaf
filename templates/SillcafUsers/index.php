<?php

use Cake\Routing\Router;


/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafUser[]|\Cake\Collection\CollectionInterface $sillcafUsers
 */
?>

<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h3 class="content-title mb-0 my-auto">
                Usuarios
            </h3><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>

<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">Usuarios</h4>
                    <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-md-" id="tablaData">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre Usuario</th>
                                <th>Perfil</th>
                                <th>Fecha Reg</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var table = $("#tablaData").DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            sAjaxSource: "http://localhost/sillcaf/ServerSide/serversideRequest.php?model=users",
            columnDefs: [{
                    "data": "id",
                    "targets": 0
                },
                {
                    "data": "username",
                    "targets": 1
                },
                {
                    "data": "sillcaf_perfiles.perfiles_name",
                    "targets": 2
                },
                {
                    "data": "sillcaf_users.reg_date",
                    "targets": 2
                },
                {
                    "data": "sillcaf_users.last_name",
                    "targets": 3
                },
                {
                    "data": "sillcaf_users.first_name",
                    "targets": 4
                },
                {
                    "data": "sillcaf_users.active_user",
                    "targets": 5
                },
                {
                    "data": null,
                    "targets": -1,
                    "defaultContent": "<a id=\"edit\" href=\"#\"><i class=\"side-menu__icon btn btn-sm btn-info far fa-edit\"></i></a><a id=\"view\" href=\"#\"><i class=\"side-menu__icon btn btn-sm btn-primary far fa-search\"></i></a>"
                }
            ]
        });

        table.buttons().container()
            .appendTo('#table_buttons');

        $('#tablaData tbody').on('click', 'a#edit', function() {
            var data = table.row($(this).parents('tr')).data();
            window.open("<?= Router::url(['controller' => 'ClosingCn', 'action' => 'edit']) . '/' ?>" + data['closing_cn_id'], '_self');
        });

        $('#tablaData tbody').on('click', 'a#ver', function() {
            var data = table.row($(this).parents('tr')).data();
            window.open("<?= Router::url(['controller' => 'ClosingCn', 'action' => 'view']) . '/' ?>" + data['closing_cn_id'], '_self');
        });

    });
</script>
