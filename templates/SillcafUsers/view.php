<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafUser $sillcafUser
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Sillcaf User'), ['action' => 'edit', $sillcafUser->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Sillcaf User'), ['action' => 'delete', $sillcafUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafUser->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Sillcaf Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Sillcaf User'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillcafUsers view content">
            <h3><?= h($sillcafUser->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Username') ?></th>
                    <td><?= h($sillcafUser->username) ?></td>
                </tr>
                <tr>
                    <th><?= __('Password') ?></th>
                    <td><?= h($sillcafUser->password) ?></td>
                </tr>
                <tr>
                    <th><?= __('Last Name') ?></th>
                    <td><?= h($sillcafUser->last_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('First Name') ?></th>
                    <td><?= h($sillcafUser->first_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($sillcafUser->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Perfiles Id') ?></th>
                    <td><?= $this->Number->format($sillcafUser->perfiles_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reg Date') ?></th>
                    <td><?= h($sillcafUser->reg_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Active User') ?></th>
                    <td><?= $sillcafUser->active_user ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
