<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SillcafUser $sillcafUser
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $sillcafUser->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $sillcafUser->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Sillcaf Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="sillcafUsers form content">
            <?= $this->Form->create($sillcafUser) ?>
            <fieldset>
                <legend><?= __('Edit Sillcaf User') ?></legend>
                <?php
                    echo $this->Form->control('username');
                    echo $this->Form->control('password');
                    echo $this->Form->control('reg_date');
                    echo $this->Form->control('perfiles_id', ['options' => $sillcafPerfiles, 'class'=>'form-control select2']);
                    echo $this->Form->control('last_name');
                    echo $this->Form->control('first_name');
                    echo $this->Form->control('active_user');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
