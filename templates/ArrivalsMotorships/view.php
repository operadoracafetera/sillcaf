<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArrivalsMotorship $arrivalsMotorship
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Arrivals Motorship'), ['action' => 'edit', $arrivalsMotorship->arrivals_motorships_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Arrivals Motorship'), ['action' => 'delete', $arrivalsMotorship->arrivals_motorships_id], ['confirm' => __('Are you sure you want to delete # {0}?', $arrivalsMotorship->arrivals_motorships_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Arrivals Motorships'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Arrivals Motorship'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="arrivalsMotorships view content">
            <h3><?= h($arrivalsMotorship->arrivals_motorships_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Visit Number') ?></th>
                    <td><?= h($arrivalsMotorship->visit_number) ?></td>
                </tr>
                <tr>
                    <th><?= __('Vissel') ?></th>
                    <td><?= h($arrivalsMotorship->vissel) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $arrivalsMotorship->has('user') ? $this->Html->link($arrivalsMotorship->user->id, ['controller' => 'Users', 'action' => 'view', $arrivalsMotorship->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Arrivals Motorships Id') ?></th>
                    <td><?= $this->Number->format($arrivalsMotorship->arrivals_motorships_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Arrivals Motorships Motorships Id') ?></th>
                    <td><?= $this->Number->format($arrivalsMotorship->arrivals_motorships_motorships_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Arrivals Motorships Terminal Id') ?></th>
                    <td><?= $this->Number->format($arrivalsMotorship->arrivals_motorships_terminal_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Arrivals Motorships Active') ?></th>
                    <td><?= $this->Number->format($arrivalsMotorship->arrivals_motorships_active) ?></td>
                </tr>
                <tr>
                    <th><?= __('Arrival Date') ?></th>
                    <td><?= h($arrivalsMotorship->arrival_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Arrivals Motorships Reg Date') ?></th>
                    <td><?= h($arrivalsMotorship->arrivals_motorships_reg_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Arrivals Motorships Update Date') ?></th>
                    <td><?= h($arrivalsMotorship->arrivals_motorships_update_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
