<?php

use Cake\Routing\Router;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArrivalsMotorship[]|\Cake\Collection\CollectionInterface $arrivalsMotorships
 */
?>

<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h3 class="content-title mb-0 my-auto">
                Llegadas Motonaves
            </h3><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>


<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">Llegada Motonaves</h4>
                    <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-md-" id="tablaData">
                        <thead>
                            <tr>
                                <th>ID Llegada</th>
                                <th># Visita</th>
                                <th>Vissel</th>
                                <th>Fecha de llegada</th>
                                <th>Motonave</th>
                                <th>Terminal</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $("#tablaData").DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            order: [[ 0, "desc" ]],
            ajax: "<?= Router::url(['controller' => 'ArrivalsMotorships', 'action' => 'indexJson']) ?>",
            columnDefs: [{
                    "data": "arrivals_motorships_id",
                    "targets": 0
                },
                {
                    "data": "visit_number",
                    "targets": 1
                },
                {
                    "data": "vissel",
                    "targets": 2
                },
                {
                    "data": "arrival_date",
                    "targets": 3,
                    "render":function(data){
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    "data": "motorship.name_motorship",
                    "targets": 4
                },
                {
                    "data": "terminal.name_terminals",
                    "targets": 5
                },
                {
                    "targets": 6,
                    "data": "arrivals_motorships_active",
                    "render": function(data, type, row, meta) {
                        if (row['arrivals_motorships_active'] == 1) {
                            return "<span class=\"label text-success d-flex\"><div class=\"dot-label bg-success mr-1\"></div>Active</span>";
                        } else {
                            return "<span class=\"label text-muted d-flex\"><div class=\"dot-label bg-gray-300 mr-1\"></div>Inactive</span>";
                        }
                    }
                },
                {
                    "data": null,
                    "targets": -1,
                    "defaultContent": '<a id="edit" href="#" class="btn btn-warning">Editar</a>'
                }
            ]
        });

        table.buttons().container()
            .appendTo('#table_buttons');

        $('#tablaData tbody').on('click', 'a#edit', function() {
            var data = table.row($(this).parents('tr')).data();
            window.open("<?= Router::url(['controller' => 'ArrivalsMotorships', 'action' => 'edit']) . '/' ?>" + data['arrivals_motorships_id'], '_self');
        });

        $('#tablaData tbody').on('click', 'a#ver', function() {
            var data = table.row($(this).parents('tr')).data();
            window.open("<?= Router::url(['controller' => 'ArrivalsMotorships', 'action' => 'view']) . '/' ?>" + data['arrivals_motorships_id'], '_self');
        });

    });
</script>