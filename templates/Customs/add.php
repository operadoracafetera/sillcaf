<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Custom $custom
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Customs'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="customs form content">
            <?= $this->Form->create($custom) ?>
            <fieldset>
                <legend><?= __('Add Custom') ?></legend>
                <?php
                    echo $this->Form->control('cia_name');
                    echo $this->Form->control('description');
                    echo $this->Form->control('created_date');
                    echo $this->Form->control('updated_date');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
