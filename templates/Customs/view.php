<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Custom $custom
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Custom'), ['action' => 'edit', $custom->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Custom'), ['action' => 'delete', $custom->id], ['confirm' => __('Are you sure you want to delete # {0}?', $custom->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Customs'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Custom'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="customs view content">
            <h3><?= h($custom->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Cia Name') ?></th>
                    <td><?= h($custom->cia_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Description') ?></th>
                    <td><?= h($custom->description) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($custom->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created Date') ?></th>
                    <td><?= h($custom->created_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated Date') ?></th>
                    <td><?= h($custom->updated_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
