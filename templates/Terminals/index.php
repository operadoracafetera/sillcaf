<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Terminal[]|\Cake\Collection\CollectionInterface $terminals
 */
?>
<div class="breadcrumb-header justify-content-between">
						<div class="my-auto">
							<div class="d-flex">
								<h4 class="content-title mb-0 my-auto"><h3>Terminal Portuarios</h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
							</div>
						</div>
						<div  class="d-flex my-xl-auto right-content">
							<div id="table_buttons" class="mb-3 mb-xl-0">
								
							</div>
						</div>
</div>


<div class="users index content">
    <div class="table-responsive">
        <table class="table text-md-" id="tableData">
            <thead>
                <tr>
                    <th>id</th>
                    <th>name_terminals</th>
					<th>description_terminals</th>
					<th>reg_date_terminals</th>
					<th>active_terminals</th>
                    <th>Opciones</th>
                </tr>
            </thead>
        </table>
		</div>
</div>

<script>
        $(document).ready(function(){
			
           var table = $("#tableData").DataTable({
              processing: true,
              serverSide: true,
			  dom: 'Bfitp',
			  //dom: '<Blf<t>ip>',
				buttons: [
					'pageLength','copy', 'excel', 'pdf',
					{
						extend: 'colvis',
						postfixButtons: [ 'colvisRestore' ]
					},
					{
						text: 'Reload',
						action: function ( e, dt, node, config ) {
							dt.ajax.reload();
						}
					}
				],
				lengthMenu: [
					[ 10, 25, 50, -1 ],
					[ '10 rows', '25 rows', '50 rows', 'Show all' ]
				],
			  language: {
					searchPlaceholder: 'Search...',
					sSearch: '',
					lengthMenu: '_MENU_ ',
				},
              sAjaxSource: "ServerSide/serversideRequest.php?model=terminals",
              columnDefs:[
			  {
				  "targets": 4,
				   "data": "active",
					"render": function ( data, type, row, meta ) {
						console.log(row);
						if(row[4] == 1){
							return "<span class=\"label text-success d-flex\"><div class=\"dot-label bg-success mr-1\"></div>Active</span>";
						}
						else{
							return "<span class=\"label text-muted d-flex\"><div class=\"dot-label bg-gray-300 mr-1\"></div>Inactive</span>";
						}
					}
				  
			  },
			  {
                  "data":null,
				  "targets": -1,
				  "defaultContent": "<a id=\"edit\" href=\"#\"><i class=\"side-menu__icon btn btn-sm btn-info far fa-edit\"></i></a><a id=\"view\" href=\"#\"><i class=\"side-menu__icon btn btn-sm btn-primary fas fa-search\"></i></a>"
              }]   
           });
		    
		   table.buttons().container()
			.appendTo( '#table_buttons' );	
			
			$('#tableData tbody').on( 'click', 'a#edit', function () {
			
				var data = table.row( $(this).parents('tr') ).data();
				window.open("terminals/edit/"+data[0],'_self');
			} );
			
			$('#tableData tbody').on( 'click', 'a#view', function () {
				var data = table.row( $(this).parents('tr') ).data();
				window.open("terminals/view/"+data[0],'_self');
			} );
			
        });
</script>

