<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Terminal $terminal
 */
?>

<div class="breadcrumb-header justify-content-between">
						<div class="my-auto">
							<div class="d-flex">
								<h4 class="content-title mb-0 my-auto"><h3><?= $this->Html->link(__('Terminal Portuarios'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h3></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Vista</span>
							</div>
						</div>
						<div  class="d-flex my-xl-auto right-content">
							<div id="table_buttons" class="mb-3 mb-xl-0">
								
							</div>
						</div>
</div>

<div class="row">
    
    <div class="column-responsive column-80">
        <div class="terminals view content">
		<div class="row">
				<div class="col-sm-8"><legend><?= __('Detalles Terminal Portuario') ?></legend></div>
				<div class="col-sm-4"><?= $this->Html->link(__('Editar'), ['action' => 'edit',$terminal->id_terminals], ['class' => 'btn btn-primary-gradient btn-block'])?></div>
			</div>
            <table>
                <tr>
                    <th><?= __('Name Terminals') ?></th>
                    <td><?= h($terminal->name_terminals) ?></td>
                </tr>
                <tr>
                    <th><?= __('Description Terminals') ?></th>
                    <td><?= h($terminal->description_terminals) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id Terminals') ?></th>
                    <td><?= $this->Number->format($terminal->id_terminals) ?></td>
                </tr>
                <tr>
                    <th><?= __('Active Terminals') ?></th>
					<td>
					<?php if($terminal->active_terminals == 1){
						echo "<span class=\"label text-success d-flex\"><div class=\"dot-label bg-success mr-1\"></div>Active</span>";
					}
					else{
						echo "<span class=\"label text-muted d-flex\"><div class=\"dot-label bg-gray-300 mr-1\"></div>Inactive</span>";
					}?>
					</td>
                </tr>
                <tr>
                    <th><?= __('Reg Date Terminals') ?></th>
                    <td><?= h($terminal->reg_date_terminals) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
