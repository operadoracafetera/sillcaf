<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdictionalElementsHasPackagingCaffee[]|\Cake\Collection\CollectionInterface $adictionalElementsHasPackagingCaffee
 */
?>

<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h3 class="content-title mb-0 my-auto">
                <?= $this->Html->link(__('Proformas'), ['controller' => 'InfoNavy', 'action' => 'index'], ['class' => 'side-nav-item']) ?>
            </h3><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Elementos Adicionales</span>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>

<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-8">
                        <legend>Elementos Adicionales de la proforma</legend>
                        <legend>ID sistema: <?= $infoNavy['id'] ?> | Proforma: <?= $infoNavy['proforma'] ?></legend>
                    </div>
                    <?php if ($infoNavy['status_info_navy_id'] == 1) { ?>
                        <div class="col-sm-4"><?= $this->Html->link(__('Nuevo Elemento'), ['action' => 'add', $infoNavy['id'], $infoNavy['proforma']], ['class' => "btn btn-primary-gradient btn-block"]) ?></div>
                    <?php } ?>
                </div>
                <div class="table-responsive">
                    <table class="table text-md-" id="tablaData">
                        <thead>
                            <tr>
                                <th>Elemento</th>
                                <th>Cantidad</th>
                                <th>Tipo de Unidad</th>
                                <th>Observaciones</th>
                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($adictionalElementsHasPackagingCaffee as $adictionalElementsHasPackagingCaffee) : ?>
                                <tr>
                                    <td><?= $adictionalElementsHasPackagingCaffee->adictional_element->name ?></td>
                                    <td><?= $this->Number->format($adictionalElementsHasPackagingCaffee->quantity) ?></td>
                                    <td><?= $adictionalElementsHasPackagingCaffee->type_unit ?></td>
                                    <td><?= $adictionalElementsHasPackagingCaffee->observation ?></td>
                                    <td class="actions">
                                        <?php if ($infoNavy['status_info_navy_id'] == 1) { ?>
                                            <?= $this->Html->link(
                                                $this->Html->tag('i', '', ['class' => 'side-menu__icon btn btn-sm btn-info far fa-edit']),
                                                ['action' => 'edit', $adictionalElementsHasPackagingCaffee->adictional_elements_id, $infoNavy['id'], $infoNavy['proforma']],
                                                ['escape' => false]
                                            ) ?>
                                            <?= $this->Form->postLink(
                                                $this->Html->tag('i', '', ['class' => 'side-menu__icon btn btn-sm btn-info far fa-trash']),
                                                ['action' => 'delete', $adictionalElementsHasPackagingCaffee->adictional_elements_id, $infoNavy['id'], $infoNavy['proforma']],
                                                ['escape' => false, 'confirm' => __('Está seguro de borrar este elemento?')]
                                            ); ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $("#tablaData").DataTable({
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            }
        });

        table.buttons().container()
            .appendTo('#table_buttons');

    });
</script>