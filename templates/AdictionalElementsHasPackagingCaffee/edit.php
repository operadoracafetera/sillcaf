<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdictionalElementsHasPackagingCaffee $adictionalElementsHasPackagingCaffee
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">
                <?= $this->Html->link(__('Proformas'), ['controller' => 'InfoNavy', 'action' => 'index'], ['class' => 'side-nav-item']) ?>
                </h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"> / <?= $this->Html->link(__('Detalle proforma'), ['controller' => 'InfoNavy', 'action' => 'view',  $infoNavy['id']], ['class' => 'side-nav-item']) ?> </span><span class="text-muted mt-1 tx-13 ml-2 mb-0"> / Agregar</span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg">
                    <?= $this->Form->create($adictionalElementsHasPackagingCaffee) ?>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-8">
                                <legend><?= __('Agregar Elemento Adicional para la proforma ').$this->Html->link($infoNavy['proforma'], ['controller' => 'InfoNavy', 'action' => 'view',  $infoNavy['id']]) ?></legend>
                            </div>
                            <div class="col-sm-4"><?= $this->Form->button(__('Guardar'), ['class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        </div>
                        <?php
                        echo $this->Form->control('info_navy_id', ['type' => 'hidden', 'value' => $infoNavy['id']]);
                        echo $this->Form->control('adictional_elements_id', [
                            'label' => 'Elemento adicional',
                            'type' => 'select',
                            'value' => $adictionalElementsHasPackagingCaffee['adictional_elements_id'],
                            'empty' => true,
                            'disabled' => true,
                            'options' => $adictionalElements
                        ]);
                        echo $this->Form->control('quantity', ['label' => 'Cantidad']);
                        echo $this->Form->control('type_unit', [
                            'label' => 'Unidad',
                            'options' => array('CAPA' => 'CAPA', 'UNIDAD' => 'UNIDAD', 'N/A' => 'N/A'),
                            'empty' => true
                        ]);
                        echo $this->Form->control('observation', ['label' => 'Observación', 'type' => 'textarea']);
                        ?>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#adictional-elements').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });
        $('#type-unit').select2({
            placeholder: 'Seleccione..',
            searchInputPlaceholder: 'Search'
        });
    });
</script>