<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdictionalElementsHasPackagingCaffee $adictionalElementsHasPackagingCaffee
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Adictional Elements Has Packaging Caffee'), ['action' => 'edit', $adictionalElementsHasPackagingCaffee->adictional_elements_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Adictional Elements Has Packaging Caffee'), ['action' => 'delete', $adictionalElementsHasPackagingCaffee->adictional_elements_id], ['confirm' => __('Are you sure you want to delete # {0}?', $adictionalElementsHasPackagingCaffee->adictional_elements_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Adictional Elements Has Packaging Caffee'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Adictional Elements Has Packaging Caffee'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="adictionalElementsHasPackagingCaffee view content">
            <h3><?= h($adictionalElementsHasPackagingCaffee->adictional_elements_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Adictional Element') ?></th>
                    <td><?= $adictionalElementsHasPackagingCaffee->has('adictional_element') ? $this->Html->link($adictionalElementsHasPackagingCaffee->adictional_element->name, ['controller' => 'AdictionalElements', 'action' => 'view', $adictionalElementsHasPackagingCaffee->adictional_element->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Type Unit') ?></th>
                    <td><?= h($adictionalElementsHasPackagingCaffee->type_unit) ?></td>
                </tr>
                <tr>
                    <th><?= __('Info Navy') ?></th>
                    <td><?= $adictionalElementsHasPackagingCaffee->has('info_navy') ? $this->Html->link($adictionalElementsHasPackagingCaffee->info_navy->id, ['controller' => 'InfoNavy', 'action' => 'view', $adictionalElementsHasPackagingCaffee->info_navy->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Observation') ?></th>
                    <td><?= h($adictionalElementsHasPackagingCaffee->observation) ?></td>
                </tr>
                <tr>
                    <th><?= __('Quantity') ?></th>
                    <td><?= $this->Number->format($adictionalElementsHasPackagingCaffee->quantity) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
