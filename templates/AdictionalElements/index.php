<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdictionalElement[]|\Cake\Collection\CollectionInterface $adictionalElements
 */
?>
<div class="adictionalElements index content">
    <?= $this->Html->link(__('New Adictional Element'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Adictional Elements') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('code_ext') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('description') ?></th>
                    <th><?= $this->Paginator->sort('created_date') ?></th>
                    <th><?= $this->Paginator->sort('updated_date') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($adictionalElements as $adictionalElement): ?>
                <tr>
                    <td><?= $this->Number->format($adictionalElement->id) ?></td>
                    <td><?= h($adictionalElement->code_ext) ?></td>
                    <td><?= h($adictionalElement->name) ?></td>
                    <td><?= h($adictionalElement->description) ?></td>
                    <td><?= h($adictionalElement->created_date) ?></td>
                    <td><?= h($adictionalElement->updated_date) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $adictionalElement->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $adictionalElement->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $adictionalElement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $adictionalElement->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
