<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdictionalElement $adictionalElement
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Adictional Element'), ['action' => 'edit', $adictionalElement->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Adictional Element'), ['action' => 'delete', $adictionalElement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $adictionalElement->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Adictional Elements'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Adictional Element'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="adictionalElements view content">
            <h3><?= h($adictionalElement->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Code Ext') ?></th>
                    <td><?= h($adictionalElement->code_ext) ?></td>
                </tr>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($adictionalElement->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Description') ?></th>
                    <td><?= h($adictionalElement->description) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($adictionalElement->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created Date') ?></th>
                    <td><?= h($adictionalElement->created_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated Date') ?></th>
                    <td><?= h($adictionalElement->updated_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
