<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AdictionalElement $adictionalElement
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Adictional Elements'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="adictionalElements form content">
            <?= $this->Form->create($adictionalElement) ?>
            <fieldset>
                <legend><?= __('Add Adictional Element') ?></legend>
                <?php
                    echo $this->Form->control('code_ext');
                    echo $this->Form->control('name');
                    echo $this->Form->control('description');
                    echo $this->Form->control('created_date');
                    echo $this->Form->control('updated_date');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
