<?php

use Cake\Routing\Router;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ClosingTn[]|\Cake\Collection\CollectionInterface $closingTn
 */
?>

<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">
                Cierres Terminal
            </h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Listado</span>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>


<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">Cierres Terminal</h4>
                    <i class="mdi mdi-dots-horizontal text-gray"></i>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-md-" id="tablaData">
                        <thead>
                            <tr>
                                <th>ID Cierre</th>
                                <th>Fecha cierre</th>
                                <th>Motonave anunciada</th>
                                <th>Viaje</th>
                                <th>ETA</th>
                                <th>Terminal</th>
                                <th>Fecha de registro</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $("#tablaData").DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfitp',
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            order: [[ 0, "desc" ]],
            ajax: "<?= Router::url(['controller' => 'ClosingTn', 'action' => 'indexJson']) ?>",
            columnDefs: [{
                    "data": "closing_tn_id",
                    "targets": 0
                },
                {
                    "data": "closing_date",
                    "targets": 1,
                    "render":function(data){
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    "data": "arrivals_motorship.motorship.name_motorship",
                    "targets": 2
                },
                {
                    "data": "arrivals_motorship.vissel",
                    "targets": 3
                },
                {
                    "data": "arrivals_motorship.arrival_date",
                    "targets": 4,
                    "render":function(data){
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    "data": "terminal.name_terminals",
                    "targets": 5
                },
                {
                    "data": "closing_tn_reg_date",
                    "targets": 6,
                    "render":function(data){
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    "data": null,
                    "targets": -1,
                    "defaultContent": '<a id="edit" href="#" class="btn btn-warning">Editar</a>'
                }
            ]
        });

        table.buttons().container()
            .appendTo('#table_buttons');

        $('#tablaData tbody').on('click', 'a#edit', function() {
            var data = table.row($(this).parents('tr')).data();
            window.open("<?= Router::url(['controller' => 'ClosingTn', 'action' => 'edit']) . '/' ?>" + data['closing_tn_id'], '_self');
        });

        $('#tablaData tbody').on('click', 'a#delete', function() {
            var data = table.row($(this).parents('tr')).data();
            window.open("<?= Router::url(['controller' => 'ClosingTn', 'action' => 'view']) . '/' ?>" + data['closing_tn_id'], '_self');
        });

    });
</script>