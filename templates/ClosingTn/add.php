<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ClosingTn $closingTn
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><?= $this->Html->link(__('Cierre Terminal'), ['action' => 'index'], ['class' => 'side-nav-item']) ?></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Registrar</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg">
                    <?= $this->Form->create($closingTn) ?>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-8">
                                <legend><?= __('Registro de cierre de terminal') ?></legend>
                            </div>
                            <div class="col-sm-4"><?= $this->Form->button(__('Guardar'), ['class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        </div>
                        <?php
                        echo $this->Form->control('closing_date', ['label' => 'Fecha de cierre', 'class' => 'form-control', 'type' => 'text']);
                        echo $this->Form->control('arrivals_motorships_id', ['options' => $arrivalsMotorships,'label' => 'Motonave Anunciada', 'class' => 'form-control']);
                        echo $this->Form->control('closing_tn_terminal_id', ['options' => $terminals,'label' => 'Terminal', 'class' => 'form-control']);
                        ?>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#closing-tn-terminal-id').select2({
			placeholder: 'Seleccione..',
			searchInputPlaceholder: 'Search'
		});
        $('#arrivals-motorships-id').select2({
			placeholder: 'Seleccione..',
			searchInputPlaceholder: 'Search'
		});
        $('#closing-date').appendDtpicker({
            closeOnSelected: true,
            dateFormat:'DD/MM/YYYY hh:mm',
            futureOnly: true,
            onInit: function(handler) {
                var picker = handler.getPicker();
                $(picker).addClass('main-datetimepicker');
            }
        });
        
    });
</script>