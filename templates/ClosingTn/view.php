<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ClosingTn $closingTn
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Closing Tn'), ['action' => 'edit', $closingTn->closing_tn_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Closing Tn'), ['action' => 'delete', $closingTn->closing_tn_id], ['confirm' => __('Are you sure you want to delete # {0}?', $closingTn->closing_tn_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Closing Tn'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Closing Tn'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="closingTn view content">
            <h3><?= h($closingTn->closing_tn_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Navy Agent') ?></th>
                    <td><?= $closingTn->has('navy_agent') ? $this->Html->link($closingTn->navy_agent->name, ['controller' => 'NavyAgent', 'action' => 'view', $closingTn->navy_agent->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Arrivals Motorship') ?></th>
                    <td><?= $closingTn->has('arrivals_motorship') ? $this->Html->link($closingTn->arrivals_motorship->arrivals_motorships_id, ['controller' => 'ArrivalsMotorships', 'action' => 'view', $closingTn->arrivals_motorship->arrivals_motorships_id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Sillcaf User') ?></th>
                    <td><?= $closingTn->has('sillcaf_user') ? $this->Html->link($closingTn->sillcaf_user->id, ['controller' => 'SillcafUsers', 'action' => 'view', $closingTn->sillcaf_user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing Tn Id') ?></th>
                    <td><?= $this->Number->format($closingTn->closing_tn_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing Tn Terminal Id') ?></th>
                    <td><?= $this->Number->format($closingTn->closing_tn_terminal_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing Date') ?></th>
                    <td><?= h($closingTn->closing_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing Tn Reg Date') ?></th>
                    <td><?= h($closingTn->closing_tn_reg_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Closing Tn Update Date') ?></th>
                    <td><?= h($closingTn->closing_tn_update_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
