<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailPackagingCaffee $detailPackagingCaffee
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Detail Packaging Caffee'), ['action' => 'edit', $detailPackagingCaffee->remittances_caffee_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Detail Packaging Caffee'), ['action' => 'delete', $detailPackagingCaffee->remittances_caffee_id], ['confirm' => __('Are you sure you want to delete # {0}?', $detailPackagingCaffee->remittances_caffee_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Detail Packaging Caffee'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Detail Packaging Caffee'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="detailPackagingCaffee view content">
            <h3><?= h($detailPackagingCaffee->remittances_caffee_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Remittances Caffee') ?></th>
                    <td><?= $detailPackagingCaffee->has('remittances_caffee') ? $this->Html->link($detailPackagingCaffee->remittances_caffee->id, ['controller' => 'RemittancesCaffee', 'action' => 'view', $detailPackagingCaffee->remittances_caffee->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Packaging Caffee') ?></th>
                    <td><?= $detailPackagingCaffee->has('packaging_caffee') ? $this->Html->link($detailPackagingCaffee->packaging_caffee->id, ['controller' => 'PackagingCaffee', 'action' => 'view', $detailPackagingCaffee->packaging_caffee->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('State') ?></th>
                    <td><?= h($detailPackagingCaffee->state) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $detailPackagingCaffee->has('user') ? $this->Html->link($detailPackagingCaffee->user->id, ['controller' => 'Users', 'action' => 'view', $detailPackagingCaffee->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Lot Coffee') ?></th>
                    <td><?= h($detailPackagingCaffee->lot_coffee) ?></td>
                </tr>
                <tr>
                    <th><?= __('Quantity Radicated Bag Out') ?></th>
                    <td><?= $this->Number->format($detailPackagingCaffee->quantity_radicated_bag_out) ?></td>
                </tr>
                <tr>
                    <th><?= __('Tara Packaging') ?></th>
                    <td><?= $this->Number->format($detailPackagingCaffee->tara_packaging) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created Date') ?></th>
                    <td><?= h($detailPackagingCaffee->created_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated Date') ?></th>
                    <td><?= h($detailPackagingCaffee->updated_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
