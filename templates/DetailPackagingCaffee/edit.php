<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailPackagingCaffee $detailPackagingCaffee
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">
                <?= $this->Html->link(__('Proformas'), ['controller' => 'InfoNavy', 'action' => 'index'], ['class' => 'side-nav-item']) ?>
            </h4><span class="text-muted mt-1 tx-13 ml-2 mb-0"> / Contenedores / Remesas / Editar</span>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg">
                    <?= $this->Form->create($detailPackagingCaffee) ?>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-8">
                                <legend><?= __('Editar cantidad de sacos en remesa ')  ?></legend>
                            </div>
                            <div class="col-sm-4"><?= $this->Form->button(__('Guardar'), ['class' => "btn btn-primary-gradient btn-block"]) ?></div>
                        </div>
                        <?php
                        echo $this->Form->control('lote', ['label' => 'Lote', 'disabled' => true, 'value' => $detailPackagingCaffee->remittances_caffee->id, 'class' => 'form-control']);
                        echo $this->Form->control('quantity_radicated_bag_out', ['label' => 'Cantidad']);
                        ?>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>