<?php

use Cake\Routing\Router;
use Cake\I18n\FrozenTime;

echo $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken'));
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailPackagingCaffee[]|\Cake\Collection\CollectionInterface $detailPackagingCaffee
 */
?>

<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <?php if ($userLogin['perfiles_id'] != 4) { ?>
                <h4 class="content-title mb-0 my-auto">
                    <?= $this->Html->link(__('Proformas'), ['controller' => 'InfoNavy', 'action' => 'index'], ['class' => 'side-nav-item']) ?>
                </h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ <?= $this->Html->link(__('Detalle'), ['controller' => 'InfoNavy', 'action' => 'view', $packagingCaffee['info_navy']['id']]) ?> / Remesas</span>
            <?php } ?>
        </div>
    </div>
    <div class="d-flex my-xl-auto right-content">
        <div id="table_buttons" class="mb-3 mb-xl-0">
        </div>
    </div>
</div>


<div class="row row-sm">
    <div class="col-xl-12">
        <div class="card">

            <div class="card-body">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="card-title mb-2">Listado de remesas en contenedor por proforma</div>
                        <div class="card-title mb-2">Proforma: <?= $this->Html->link($packagingCaffee['info_navy']['proforma'], ['controller' => 'InfoNavy', 'action' => 'view',  $packagingCaffee['info_navy']['id']]) ?></div>
                        <div class="card-title mb-2">ID Contenedor: <?= $packagingCaffee['id'] . ' ISO:' . $packagingCaffee['iso_ctn'] ?></div>
                    </div>
                </div>
                <br>
                <?php if ($allowAddDetailPackaging) { ?>
                    <div class="card-title mb-2">Búsqueda de remesas</div>
                    <table class="nowrap" id="RemesasTable">
                        <thead>
                            <tr>
                                <th>Remesa</th>
                                <th>Lote</th>
                                <th>Unidad</th>
                                <th>Sacos disponibles</th>
                                <th>Bloqueos (Novedades)</th>
                                <th class="actions"><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                    </table>
                <?php } ?>
                <br>
                <div class="table-responsive">
                    <table class="table text-md-" id="tablaData">
                        <thead>
                            <tr>
                                <th>Remesa</th>
                                <th>Lote</th>
                                <th>Sacos</th>
                                <th>Fecha creación</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="card-title mb-2">Cross-Docking</div>
                <?php if ($allowAddDetailPackaging) { ?>
                    <div class="col-xl-4">
                        <?= $this->Html->link(__('Crear CrossDocking'), ['controller' => 'DetailPackakgingCrossdocking', 'action' => 'add', $packagingCaffee['id']], ['class' => "btn btn-primary-gradient btn-block"]) ?>
                    </div>
                <?php } ?>
                <div class="table-responsive">
                    <table class="table text-md-" id="crossdockingTable">
                        <thead>
                            <tr>
                                <th>Lote</th>
                                <th>Sacos</th>
                                <th>Fecha creación</th>
                                <th class="actions"><?php echo __('Acciones'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($detailPackagingCrossdocking as $detailPackagingCrossdocking) : ?>
                                <tr>
                                    <td><?= $detailPackagingCrossdocking->lot_coffee ?></td>
                                    <td><?= $detailPackagingCrossdocking->qta_coffee ?></td>
                                    <td><?= h($detailPackagingCrossdocking->reg_date) ?></td>
                                    <td class="actions">
                                        <?php if ($allowAddDetailPackaging) { ?>
                                            <?= $this->Html->link(
                                                $this->Html->tag('i', 'Editar', ['class' => 'btn btn-warning']),
                                                ['controller' => 'DetailPackakgingCrossdocking', 'action' => 'edit', $detailPackagingCrossdocking->id],
                                                ['escape' => false]
                                            ) ?>
                                            <?= $this->Form->postLink(
                                                $this->Html->tag('i', 'Borrar', ['class' => 'btn btn-danger']),
                                                ['controller' => 'DetailPackakgingCrossdocking', 'action' => 'delete',  $detailPackagingCrossdocking->id],
                                                ['escape' => false, 'confirm' => __('Está seguro de borrar este elemento?')]
                                            ); ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var tableDetailPackaging = $("#tablaData").DataTable({
            dom: 'Bfitp',
            searching: false,
            processing: true,
            serverSide: true,
            ajax: "<?= Router::url(['action' => 'addDetailPackagingJson', $packagingCaffee['id']]) ?>",
            buttons: [
                'pageLength', 'copy', 'excel', 'pdf',
                {
                    extend: 'colvis',
                    postfixButtons: ['colvisRestore']
                },
                {
                    text: 'Reload',
                    action: function(e, dt, node, config) {
                        tableDetailPackaging.ajax.reload();
                    }
                }
            ],
            columnDefs: [{
                    "data": "remittances_caffee.id",
                    "targets": 0
                },
                {
                    "data": "remittances_caffee",
                    "targets": 1,
                    "render": function(data) {
                        return data['client']['exporter_code'] + "-" + data['lot_caffee'];
                    }
                },
                {
                    "data": "quantity_radicated_bag_out",
                    "targets": 2
                },
                {
                    "data": "created_date",
                    "targets": 3,
                    "render": function(data) {
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                },
                {
                    "data": null,
                    "targets": -1,
                    "className": 'dt-column-button-right',
                    "render": function(data) {
                        var perfil = <?= $userLogin['perfiles_id']; ?>;
                        var status = <?= $packagingCaffee['info_navy']['status_info_navy_id']; ?>;
                        if (perfil == 2 && status == 1 || status == 5) {
                            return '<a id="deteleDetail" href="#" class="btn btn-danger">Borrar</a>';
                        } else {
                            return '<i>--</i>';
                        }
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, -1],
                ['10 rows', '25 rows', '50 rows', 'Show all']
            ],
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
        });

        $('#tablaData tbody').on('click', 'a#deteleDetail', function() {
            var data = tableDetailPackaging.row($(this).parents('tr')).data();
            var r = confirm("Está seguro de borrar este elemento?");
            if (r == true) {
                $.ajax({
                    type: "POST",
                    url: "<?= Router::url(['action' => 'delete', $packagingCaffee['id']]) ?>" + '/' + data['remittances_caffee_id'],
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                    },
                    success: function(response) {
                        tableDetailPackaging.ajax.reload();
                    }
                });
            }
        });

        tableDetailPackaging.buttons().container()
            .appendTo('#table_buttons');

        var remesasTable = $("#RemesasTable").DataTable({
            processing: true,
            serverSide: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
            ajax: {
                "url": "<?= Router::url(['action' => 'searchRemesaJson']) ?>",
                "data": {
                    'custom_id': <?= $packagingCaffee['info_navy']['customs_id'] ?>
                }
            },
            columnDefs: [{
                    "data": "id",
                    "targets": 0
                },
                {
                    "data": "lot_caffee",
                    "targets": 1
                },
                {
                    "data": "slot_store.name_space",
                    "targets": 2,
                    "render": function(data, type, row, meta) {
                        return row['type_unit']['name'] + " - " + row['units_caffee']['name_unit'];
                    }
                },
                {
                    "data": "quantity_radicated_bag_in",
                    "targets": 3
                },
                {
                    "targets": 4,
                    "data": "novelty_in_caffee",
                    "render": function(data, type, row, meta) {
                        var noveltysCoffee = "";
                        $.each(row['remittances_caffee_has_noveltys_caffee'], function(n, val) {
                            noveltysCoffee += val['noveltys_caffee']['name'] + " - activo: " + val['active'] + " | ";
                        });
                        if (noveltysCoffee === "")
                            return "Sin novedades";
                        else
                            return noveltysCoffee;
                    }
                },
                {
                    "data": null,
                    "targets": -1,
                    "className": 'dt-column-button-right',
                    "defaultContent": '<a id="addRemesa" href="#" class="btn btn-success">Agregar</a>'
                }
            ],
            fixedColumns: {
                leftColumns: 1,
                rightColumns: 1
            }
        });

        $('#RemesasTable tbody').on('click', 'a#addRemesa', function() {
            var data = remesasTable.row($(this).parents('tr')).data();
            $.ajax({
                type: "POST",
                url: "<?= Router::url(['action' => 'add-ajax']) ?>",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
                },
                data: {
                    packaging_caffee_id: <?= $packagingCaffee['id'] ?>,
                    remittances_caffee_id: data['id'],
                    quantity_radicated_bag_out: data['quantity_radicated_bag_in']
                },
                success: function(response) {
                    if (response === 'success') {
                        tableDetailPackaging.ajax.reload();
                    } else {
                        alert(response);
                    }

                }
            });
        });

        var tableCrossdocking = $("#crossdockingTable").DataTable({
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ ',
            },
        });

    });
</script>