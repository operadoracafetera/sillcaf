<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailPackakgingCrossdocking $detailPackakgingCrossdocking
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Detail Packakging Crossdocking'), ['action' => 'edit', $detailPackakgingCrossdocking->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Detail Packakging Crossdocking'), ['action' => 'delete', $detailPackakgingCrossdocking->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detailPackakgingCrossdocking->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Detail Packakging Crossdocking'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Detail Packakging Crossdocking'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="detailPackakgingCrossdocking view content">
            <h3><?= h($detailPackakgingCrossdocking->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Remittances Caffee') ?></th>
                    <td><?= $detailPackakgingCrossdocking->has('remittances_caffee') ? $this->Html->link($detailPackakgingCrossdocking->remittances_caffee->id, ['controller' => 'RemittancesCaffee', 'action' => 'view', $detailPackakgingCrossdocking->remittances_caffee->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Packaging Caffee') ?></th>
                    <td><?= $detailPackakgingCrossdocking->has('packaging_caffee') ? $this->Html->link($detailPackakgingCrossdocking->packaging_caffee->id, ['controller' => 'PackagingCaffee', 'action' => 'view', $detailPackakgingCrossdocking->packaging_caffee->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Lot Coffee') ?></th>
                    <td><?= h($detailPackakgingCrossdocking->lot_coffee) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($detailPackakgingCrossdocking->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Qta Coffee') ?></th>
                    <td><?= $this->Number->format($detailPackakgingCrossdocking->qta_coffee) ?></td>
                </tr>
                <tr>
                    <th><?= __('Status') ?></th>
                    <td><?= $this->Number->format($detailPackakgingCrossdocking->status) ?></td>
                </tr>
                <tr>
                    <th><?= __('Reg Date') ?></th>
                    <td><?= h($detailPackakgingCrossdocking->reg_date) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
