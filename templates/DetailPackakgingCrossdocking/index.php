<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailPackakgingCrossdocking[]|\Cake\Collection\CollectionInterface $detailPackakgingCrossdocking
 */
?>
<div class="detailPackakgingCrossdocking index content">
    <?= $this->Html->link(__('New Detail Packakging Crossdocking'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Detail Packakging Crossdocking') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('remittances_caffee_id') ?></th>
                    <th><?= $this->Paginator->sort('packaging_caffee_id') ?></th>
                    <th><?= $this->Paginator->sort('lot_coffee') ?></th>
                    <th><?= $this->Paginator->sort('qta_coffee') ?></th>
                    <th><?= $this->Paginator->sort('reg_date') ?></th>
                    <th><?= $this->Paginator->sort('status') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($detailPackakgingCrossdocking as $detailPackakgingCrossdocking): ?>
                <tr>
                    <td><?= $this->Number->format($detailPackakgingCrossdocking->id) ?></td>
                    <td><?= $detailPackakgingCrossdocking->has('remittances_caffee') ? $this->Html->link($detailPackakgingCrossdocking->remittances_caffee->id, ['controller' => 'RemittancesCaffee', 'action' => 'view', $detailPackakgingCrossdocking->remittances_caffee->id]) : '' ?></td>
                    <td><?= $detailPackakgingCrossdocking->has('packaging_caffee') ? $this->Html->link($detailPackakgingCrossdocking->packaging_caffee->id, ['controller' => 'PackagingCaffee', 'action' => 'view', $detailPackakgingCrossdocking->packaging_caffee->id]) : '' ?></td>
                    <td><?= h($detailPackakgingCrossdocking->lot_coffee) ?></td>
                    <td><?= $this->Number->format($detailPackakgingCrossdocking->qta_coffee) ?></td>
                    <td><?= h($detailPackakgingCrossdocking->reg_date) ?></td>
                    <td><?= $this->Number->format($detailPackakgingCrossdocking->status) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $detailPackakgingCrossdocking->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $detailPackakgingCrossdocking->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $detailPackakgingCrossdocking->id], ['confirm' => __('Are you sure you want to delete # {0}?', $detailPackakgingCrossdocking->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
