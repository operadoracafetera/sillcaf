<?php

use Cake\Routing\Router;

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DetailPackakgingCrossdocking $detailPackakgingCrossdocking
 */
?>
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto"><?= $this->Html->link(__('Proformas'), ['controller' => 'InfoNavy', 'action' => 'index'], ['class' => 'side-nav-item']) ?></h4><span class="text-muted mt-1 tx-13 ml-2 mb-0">/ Registrar</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xl-12 col-xs-12 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="col-lg">

                    <?= $this->Form->create($detailPackakgingCrossdocking) ?>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="card-title mb-2"><?= __('Registro de CrossDocking') ?></div>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->button(__('Guardar'), ['class' => "btn btn-primary-gradient btn-block"]) ?>
                            </div>
                            <div class="col-sm-6">
                                <div class="card-title mb-2"><?= $this->Html->link('Listado de remesas en contenedor por proforma', ['controller' => 'DetailPackagingCaffee', 'action' => 'add',  $packagingCaffeeId]) ?> </div>
                            </div>
                        </div>
                        <?php
                        echo $this->Form->control('packaging_caffee_id', ['type' => 'hidden', 'value' => $packagingCaffeeId]);
                        echo $this->Form->control('expo_code', ['label' => 'Código Exportador', 'required' => true, 'class' => 'form-control']);
                        echo $this->Form->control('expo_name', ['label' => 'Nombre Exportador', 'disabled' => true, 'class' => 'form-control']);
                        echo $this->Form->control('lot', ['label' => 'Lote', 'type' => 'number','required' => true, 'class' => 'form-control']);
                        echo $this->Form->control('qta_coffee', ['label' => 'Cantidad de sacos', 'required' => true, 'class' => 'form-control']);
                        ?>
                    </fieldset>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        $('#expo-code').keyup(function() {
            $('#expo-name').val('');
            $.ajax({
                type: "GET",
                datatype: "json",
                url: "<?= Router::url(['controller' => 'Clients', 'action' => 'searchExporterByCode']) ?>" + "/" + $(this).val(),
                error: function(msg) {
                    alert("Error networking");
                },
                success: function(data) {
                    var jsonCoffeeSamples = JSON.parse(data);
                    if (jsonCoffeeSamples) {
                        $('#expo-name').val(jsonCoffeeSamples.business_name);
                    } else {
                        $('#expo-name').val('');
                    }
                }
            });
        });

        $(':button').click(function() {
            if (!$('#expo-name').val()) {
                alert('Exportador no válido');
                return false;
            }
        });

    });
</script>