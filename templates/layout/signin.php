<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'SILLCAF';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.css">

    <?= $this->Html->css('milligram.min.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	
	<!-- Favicon -->
		<link rel="icon" href="/assets/img/brand/favicon.png" type="image/x-icon"/>

		<!-- Icons css -->
		<link href="/assets/css/icons.css" rel="stylesheet">

		<!--  Right-sidemenu css -->
		<link href="/assets/plugins/sidebar/sidebar.css" rel="stylesheet">

		<!--  Custom Scroll bar-->
		<link href="/assets/plugins/mscrollbar/jquery.mCustomScrollbar.css" rel="stylesheet"/>

		<!--  Left-Sidebar css -->
		<link rel="stylesheet" href="/assets/css/sidemenu.css">

		<!--- Style css --->
		<link href="/assets/css/style.css" rel="stylesheet">

		<!--- Dark-mode css --->
		<link href="/assets/css/style-dark.css" rel="stylesheet">

		<!---Skinmodes css-->
		<link href="/assets/css/skin-modes.css" rel="stylesheet" />

		<!--- Animations css-->
		<link href="/assets/css/animate.css" rel="stylesheet">
		
		<!-- JQuery min js -->
		<script src="/assets/plugins/jquery/jquery.min.js"></script>

		<!-- Bootstrap Bundle js -->
		<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
		
		<!--- Style css --->
		<link href="/assets/css/style.css" rel="stylesheet">
		<link href="/assets/css/boxed.css" rel="stylesheet">

		<!-- Ionicons js -->
		<script src="/assets/plugins/ionicons/ionicons.js"></script>

		<!-- Moment js -->
		<script src="/assets/plugins/moment/moment.js"></script>

		<!-- eva-icons js -->
		<script src="/assets/js/eva-icons.min.js"></script>

		<!-- Rating js-->
		<script src="/assets/plugins/rating/jquery.rating-stars.js"></script>
		<script src="/assets/plugins/rating/jquery.barrating.js"></script>

		<!-- custom js -->
		<script src="/assets/js/custom.js"></script>
</head>
<body class="main-body bg-light">
    <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
</body>
</html>
