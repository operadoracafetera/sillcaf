<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;
use Cake\I18n\FrozenTime;

class WatchHelper extends Helper{


    public static function watch($data = null)
    {
        $SillCafLogs = TableRegistry::get('SillcafLogs');
        $request = [
            'data_before' =>($data[0] != NULL) ? json_encode($data[0]): NULL,
            'data_after' =>($data[1] != NULL) ? json_encode($data[1]): NULL,
            'module_name' =>$data[2],
            'ip_source' =>$data[3],
            'users_id' =>$data[4],
            'date_event' => FrozenTime::now()
        ];
        $sillCafLog = $SillCafLogs->newEmptyEntity();
        $sillCafLog = $SillCafLogs->patchEntity($sillCafLog, $request);
        $SillCafLogs->save($sillCafLog);
    }  
}
?>