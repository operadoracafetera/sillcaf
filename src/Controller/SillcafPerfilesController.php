<?php
declare(strict_types=1);

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Log\Log;


/**
 * SillcafPerfiles Controller
 *
 * @property \App\Model\Table\SillcafPerfilesTable $SillcafPerfiles
 * @method \App\Model\Entity\SillcafPerfile[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SillcafPerfilesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafPerfiles = $this->paginate($this->SillcafPerfiles);

        $this->set(compact('sillcafPerfiles'));
    }

    /**
     * View method
     *
     * @param string|null $id Sillcaf Perfile id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafPerfile = $this->SillcafPerfiles->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('sillcafPerfile'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafPerfile = $this->SillcafPerfiles->newEmptyEntity();
        if ($this->request->is('post')) {
            $sillcafPerfile = $this->SillcafPerfiles->patchEntity($sillcafPerfile, $this->request->getData());
            if ($this->SillcafPerfiles->save($sillcafPerfile)) {
                $this->Flash->success(__('The sillcaf perfile has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf perfile could not be saved. Please, try again.'));
        }
        $this->set(compact('sillcafPerfile'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sillcaf Perfile id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
            
        $sillcafPerfile = $this->SillcafPerfiles->get($id, [
        ]);

        $this->SillcafPermisses = TableRegistry::get('SillcafPermisses');
        $dataModulesPermises = $this->SillcafPermisses->find('all')
        ->contain(['SillcafModules'])
        ->toArray();

        //$this->log(json_encode($dataModulesPermises),'debug');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $req_data = $this->request->getData();
            $arrayAuth="";
            foreach($dataModulesPermises as $key => $value){
                if($req_data[$key]){
                    $arrayAuth .= $key.",";
                }
                unset($req_data[$key]);
            }
            $arrayAuth = substr($arrayAuth, 0, -1);
            $sillcafPerfile['modules'] = $arrayAuth;
            $sillcafPerfile = $this->SillcafPerfiles->patchEntity($sillcafPerfile, $req_data);

            if ($this->SillcafPerfiles->save($sillcafPerfile)) {
                $this->Flash->success(__('The sillcaf perfile has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf perfile could not be saved. Please, try again.'));
        }
        $this->set(compact('sillcafPerfile'));

        $dataModulesPermises = $this->SillcafPermisses->find('all')->contain(['SillcafModules'])
        ->toArray();
        $this->set('modules',$dataModulesPermises);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sillcaf Perfile id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $sillcafPerfile = $this->SillcafPerfiles->get($id);
        if ($this->SillcafPerfiles->delete($sillcafPerfile)) {
            $this->Flash->success(__('The sillcaf perfile has been deleted.'));
        } else {
            $this->Flash->error(__('The sillcaf perfile could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
