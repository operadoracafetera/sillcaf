<?php
declare(strict_types=1);

namespace App\Controller;

use App\View\Helper\WatchHelper;


/**
 * Motorships Controller
 *
 * @property \App\Model\Table\MotorshipsTable $Motorships
 * @method \App\Model\Entity\Motorship[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MotorshipsController extends AppController
{


    public function isAuthorized($user)
    {
        if (in_array($this->request->getParam('action'), ['add', 'index','edit', 'delete','indexJson'])) {
            if(in_array($user->read('SillcafUsers.perfiles_id'), [1,4])){
                return true;
            }
            else{
                WatchHelper::watch([NULL,'Intento de acceso denegado','Motonaves',$_SERVER['REMOTE_ADDR'],$user->read('SillcafUsers.id')]);
                return false;
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }

        WatchHelper::watch([NULL,NULL,'Index Motonaves',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);
        
        $this->paginate = [
            'contain' => ['ShippingLines'],
        ];
        $motorships = $this->paginate($this->Motorships);

        $this->set(compact('motorships'));
    }


    public function indexJson()
    {
        $requestData = $this->request->getParam('?');

        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }

        WatchHelper::watch([NULL,NULL,'IndexJson Motonaves',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);

        $options = [
            'contain' => []
        ];

        if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => 'id_motorship like "%' . $search . '%"' .
                ' OR name_motorship like "%' . $search . '%"' .
                ' OR description_motorship like "%' . $search . '%"'];
        }

        $orderList = [
            'id_motorship' => 'Motorships.id_motorship',
            'name_motorship' => 'Motorships.name_motorship',
            'description_motorship' => 'Motorships.description_motorship',
            'reg_date_motorship' => 'Motorships.reg_date_motorship',
            'active_motorship' => 'Motorships.active_motorship'
        ];

        $orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
        ];

        $resultData = $this->Motorships->find('all', $options);
        if ($requestData['length'] == -1) {
            $resultData = $resultData->limit(9999);
        }
        else{
            $resultData = $resultData
        ->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
            ->order([$orderBy['column'] => $orderBy['dir']]);
        }

        $this->set([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($resultData->count()),
            "recordsFiltered" => intval($resultData->count()),
            "data"            => $resultData->toList()
        ]);

        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }
        
        $motorship = $this->Motorships->newEmptyEntity();
        if ($this->request->is('post')) {
            $motorship = $this->Motorships->patchEntity($motorship, $this->request->getData());
            $motorship->shippingline_id = 76;
            if ($this->Motorships->save($motorship)) {

                WatchHelper::watch([NULL,$motorship,'Agregar motonave',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);
                
                $this->Flash->success(__('Se registro con exito la Motonave.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pude registrar la motonave.s'));
        }
		
        $this->set(compact('motorship'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Motorship id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }

        $motorship = $this->Motorships->get($id, [
            'contain' => [],
        ]);

        WatchHelper::watch([$motorship,NULL,'Edit Motonaves Form',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $motorship = $this->Motorships->patchEntity($motorship, $this->request->getData());
            if ($this->Motorships->save($motorship)) {

                WatchHelper::watch([NULL,$motorship,'Edit Motonaves Tx',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);
                
                $this->Flash->success(__('Se actualizo con exito el registo.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The motorship could not be saved. Please, try again.'));
        }
        $shippingLines = $this->Motorships->ShippingLines->find('list', ['limit' => 200]);
        $this->set(compact('motorship', 'shippingLines'));
    }

}
