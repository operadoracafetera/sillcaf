<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\Mailer\Mailer;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;
use App\View\Helper\WatchHelper;

/**
 * InfoNavy Controller
 *
 * @property \App\Model\Table\InfoNavyTable $InfoNavy
 * @method \App\Model\Entity\InfoNavy[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InfoNavyController extends AppController
{

    public function isAuthorized($user)
    {
        if ($this->request->getParam('action') === 'add') {
            if (in_array($user->read('SillcafUsers.perfiles_id'), [2])) {
                return true;
            }
        } else if ($this->request->getParam('action') === 'view') {
            if (in_array($user->read('SillcafUsers.perfiles_id'), [2, 3, 4])) {
                $itemId = (int)$this->request->getParam('pass.0');
                $dataInfoNavy = $this->InfoNavy->get($itemId);
                if ($user->read('SillcafUsers.perfiles_id') == 4) {
                    return true;
                } else if ($dataInfoNavy['navy_agent_id'] == $user->read('SillcafUsers.navyagent_user_id')) {
                    return true;
                } else if ($dataInfoNavy['customs_id'] == $user->read('SillcafUsers.customs_user_id')) {
                    return true;
                } else {
                    $this->loadModel('SillcafInfonavyCustom');
                    $resultData = $this->SillcafInfonavyCustom->find('all')->where(['info_navy_id' => $itemId, 'custom_id' => $user->read('SillcafUsers.customs_user_id')]);
                    if ($resultData) {
                        return true;
                    }
                }
            }
        } else if (in_array($this->request->getParam('action'), ['index', 'indexJson'])) {
            if (in_array($user->read('SillcafUsers.perfiles_id'), [1, 2, 3])) {
                return true;
            }
        } else if ($this->request->getParam('action') === 'message') {
            if (in_array($user->read('SillcafUsers.perfiles_id'), [2, 3])) {
                $itemId = (int)$this->request->getData()['info_navy_id'];
                $dataInfoNavy = $this->InfoNavy->get($itemId);
                if ($dataInfoNavy['navy_agent_id'] == $user->read('SillcafUsers.navyagent_user_id')) {
                    return true;
                } else if ($dataInfoNavy['customs_id'] == $user->read('SillcafUsers.customs_user_id')) {
                    return true;
                } else {
                    $this->loadModel('SillcafInfonavyCustom');
                    $resultData = $this->SillcafInfonavyCustom->find('all')->where(['info_navy_id' => $itemId, 'custom_id' => $user->read('SillcafUsers.customs_user_id')]);
                    if ($resultData) {
                        return true;
                    }
                }
            }
        } else if ($this->request->getParam('action') === 'confirmation') {
            if (in_array($user->read('SillcafUsers.perfiles_id'), [4])) {
                return true;
            }
        }
        if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
            $itemId = (int)$this->request->getParam('pass.0');
            $dataInfoNavy = $this->InfoNavy->get($itemId);
            if ($this->InfoNavy->isOwnedBy($itemId, $user->read('SillcafUsers.customs_user_id'))) {
                if (in_array($dataInfoNavy['status_info_navy_id'], [1, 5])) {
                    return true;
                }
            }
        } else if (in_array($this->request->getParam('action'), ['booking2'])) {
            $itemId = (int)$this->request->getParam('pass.0');
            $dataInfoNavy = $this->InfoNavy->get($itemId);
            if ($dataInfoNavy['navy_agent_id'] == 14) {
                if (in_array($dataInfoNavy['status_info_navy_id'], [4])) {
                    if (is_null($dataInfoNavy['booking2'])) {
                        return true;
                    }
                }
            }
        } else if (in_array($this->request->getParam('action'), ['aprove', 'reject'])) {
            $itemId = (int)$this->request->getParam('pass.0');
            $dataInfoNavy = $this->InfoNavy->get($itemId);
            if (in_array($user->read('SillcafUsers.perfiles_id'), [3])) {
                if ($dataInfoNavy['navy_agent_id'] == $user->read('SillcafUsers.navyagent_user_id')) {
                    return true;
                }
            }
        } else if (in_array($this->request->getParam('action'), ['confirm'])) {
            $itemId = (int)$this->request->getParam('pass.0');
            $dataInfoNavy = $this->InfoNavy->get($itemId);
            if (in_array($user->read('SillcafUsers.perfiles_id'), [2])) {
                if ($dataInfoNavy['customs_id'] == $user->read('SillcafUsers.customs_user_id')) {
                    return true;
                } else {
                    $this->loadModel('SillcafInfonavyCustom');
                    $resultData = $this->SillcafInfonavyCustom->find('all')->where(['info_navy_id' => $itemId, 'custom_id' => $user->read('SillcafUsers.customs_user_id')]);
                    if ($resultData) {
                        return true;
                    }
                }
            }
        }

        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }

        WatchHelper::watch([NULL, NULL, 'Index Proformas', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);
    }

    public function indexJson()
    {
        $session = $this->request->getSession();

        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }
        $requestData = $this->request->getParam('?');

        WatchHelper::watch([NULL, NULL, 'IndexJson Proformas', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);

        $options = [
            'contain' => ['NavyAgent', 'ShippingLines', 'Customs', 'ArrivalsMotorships.Motorships', 'StatusInfoNavy', 'SillcafUsers', 'PackagingCaffee', 'SillcafInfonavyCustom'],
        ];

        if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => '(InfoNavy.proforma like "%' . $search . '%"' .
                ' OR InfoNavy.booking like "%' . $search . '%"' .
                ' OR ArrivalsMotorships.vissel like "%' . $search . '%"' .
                ' OR Motorships.name_motorship like "%' . $search . '%"' .
                ' OR ArrivalsMotorships.arrival_date like "%' . $search . '%")'];
        }

        $orderList = [
            'id' => 'InfoNavy.id',
            'proforma' => 'InfoNavy.proforma',
            'status_info_navy.status_info_navy_name' => 'StatusInfoNavy.status_info_navy_name',
            'booking' => 'InfoNavy.booking',
            'navy_agent.name' => 'InfoNavy.NavyAgent.name',
            'arrivals_motorship.motorship.name_motorship' => 'Motorships.name_motorship',
            'arrivals_motorship.vissel' => 'ArrivalsMotorships.vissel',
            'arrivals_motorship.arrival_date' => 'ArrivalsMotorships.arrival_date',
            'packaging_type' => 'InfoNavy.packaging_type',
            'packaging_mode' => 'InfoNavy.packaging_mode',
            'created_date' => 'InfoNavy.created_date',
        ];

        $orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
        ];

        $userInfo = $this->request->getSession()->read('SillcafUsers');
        $whereOps = [];
        //Validación para perfil Agencia de Aduanas
        if ($userInfo['perfiles_id'] == 2) {
            $whereOps = ['InfoNavy.customs_id' => $userInfo['customs_user_id'], 'InfoNavy.status_info_navy_id in (1,4,5)', "InfoNavy.packaging_mode != 'CONSOLIDADO (VARIAS AGENCIAS)'"];
        }
        //Validación para perfil Agente Naviero
        if ($userInfo['perfiles_id'] == 3) {
            $whereOps = ['InfoNavy.navy_agent_id' => $userInfo['navyagent_user_id'], 'InfoNavy.status_info_navy_id in (4,2)', "InfoNavy.packaging_mode != 'CONSOLIDADO (VARIAS AGENCIAS)'"];
        }
        $resultData = $this->InfoNavy->find('all', $options)->where($whereOps);
        Log::debug($resultData->sql());
        if ($requestData['length'] != -1) {
            $resultData = $resultData->limit(intval($requestData['length']));
        }
        $resultData = $resultData->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
            ->order([$orderBy['column'] => $orderBy['dir']]);

        $this->set([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($resultData->count()),
            "recordsFiltered" => intval($resultData->count()),
            "data"            => $resultData->toList()
        ]);

        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }

    public function consolidated()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        WatchHelper::watch([NULL, NULL, 'Index Proformas Consolidadas', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);
    }

    public function consolidatedIndexJson()
    {
        $session = $this->request->getSession();

        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        $requestData = $this->request->getParam('?');

        WatchHelper::watch([NULL, NULL, 'IndexJson Proformas', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);

        $options = [
            'contain' => ['InfoNavy', 'InfoNavy.NavyAgent', 'InfoNavy.ShippingLines', 'InfoNavy.Customs', 'InfoNavy.ArrivalsMotorships.Motorships', 'InfoNavy.StatusInfoNavy', 'InfoNavy.SillcafUsers', 'InfoNavy.PackagingCaffee'],
        ];

        if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => '(InfoNavy.proforma like "%' . $search . '%"' .
                ' OR InfoNavy.booking like "%' . $search . '%"' .
                ' OR ArrivalsMotorships.vissel like "%' . $search . '%"' .
                ' OR Motorships.name_motorship like "%' . $search . '%"' .
                ' OR ArrivalsMotorships.arrival_date like "%' . $search . '%")'];
        }

        $orderList = [
            'info_navy.id' => 'InfoNavy.id',
            'info_navy.proforma' => 'InfoNavy.proforma',
            'info_navy.status_info_navy.status_info_navy_name' => 'StatusInfoNavy.status_info_navy_name',
            'info_navy.booking' => 'InfoNavy.booking',
            'info_navy.navy_agent.name' => 'InfoNavy.NavyAgent.name',
            'info_navy.arrivals_motorship.motorship.name_motorship' => 'Motorships.name_motorship',
            'info_navy.arrivals_motorship.vissel' => 'ArrivalsMotorships.vissel',
            'info_navy.arrivals_motorship.arrival_date' => 'ArrivalsMotorships.arrival_date',
            'info_navy.packaging_type' => 'InfoNavy.packaging_type',
            'info_navy.packaging_mode' => 'InfoNavy.packaging_mode',
        ];

        $orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
        ];

        $userInfo = $this->request->getSession()->read('SillcafUsers');

        $this->loadModel('SillcafInfonavyCustom');
        $resultData = $this->SillcafInfonavyCustom->find('all', $options)->where(['SillcafInfonavyCustom.custom_id' => $userInfo['customs_user_id'], 'InfoNavy.status_info_navy_id in (1,4,5)']);

        if ($requestData['length'] != -1) {
            $resultData = $resultData->limit(intval($requestData['length']));
        }
        $resultData = $resultData->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
            ->order([$orderBy['column'] => $orderBy['dir']]);

        $this->set([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($resultData->count()),
            "recordsFiltered" => intval($resultData->count()),
            "data"            => $resultData->toList()
        ]);

        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }

    /**
     * message method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function message()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        $this->loadModel('InfoNavyMessage');
        $this->loadModel('SillcafUsers');

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }

        $infoNavyMessage = $this->InfoNavyMessage->newEmptyEntity();
        if ($this->request->is('post')) {
            $infoNavyMessage = $this->InfoNavyMessage->patchEntity($infoNavyMessage, $this->request->getData());
            $infoNavyMessage['sillcaf_user_reg_id'] = $this->request->getSession()->read('SillcafUsers.id');
            $infoNavyMessage['reg_date'] = FrozenTime::now();

            if ($this->InfoNavyMessage->save($infoNavyMessage)) {
                $this->Flash->success(__('Mensaje enviado correctamente.'));

                WatchHelper::watch([NULL, $infoNavyMessage, 'Mensaje generado Proformas', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);

                return $this->redirect(['action' => 'view', $infoNavyMessage['info_navy_id']]);
            }
            $this->Flash->error(__('The packaging caffee could not be saved. Please, try again.'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Info Navy id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }
        $infoNavy = $this->InfoNavy->get($id, [
            'contain' => ['NavyAgent', 'ShippingLines', 'Customs', 'ArrivalsMotorships', 'StatusInfoNavy', 'SillcafUsers', 'AdictionalElementsHasPackagingCaffee.AdictionalElements', 'PackagingCaffee.DetailPackagingCaffee.RemittancesCaffee.Clients', 'PackagingCaffee.ReadyContainer', 'PackagingCaffee.DetailPackakgingCrossdocking'],
        ]);

        WatchHelper::watch([NULL, $infoNavy, 'View Proformas', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);

        $this->loadModel('ClosingTn');
        $userLogin = $session->read('SillcafUsers');
        $closingTn = $this->ClosingTn->find('all')->where(['arrivals_motorships_id' => $infoNavy['arrivals_motorships_id']])->first();
        $this->loadModel('InfoNavyMessage');
        $infoNavyMessage = $this->InfoNavyMessage->newEmptyEntity();
        $this->loadModel('PackagingCaffee');
        $packagingCaffee = $this->PackagingCaffee->newEmptyEntity();
        $this->loadModel('AdictionalElementsHasPackagingCaffee');
        $adictionalElementsHasPackagingCaffee = $this->AdictionalElementsHasPackagingCaffee->newEmptyEntity();
        $adictionalElements = $this->AdictionalElementsHasPackagingCaffee->AdictionalElements->find('list', ['limit' => 200]);
        $infoNavyMessages = $this->InfoNavyMessage->find('all')->where(['info_navy_id' => $id])->contain(['SillcafUsers'])->toArray();
        $showApproveBtn = (($infoNavy['status_info_navy_id'] == 4) && $this->canInfoNavyEdit($closingTn, $infoNavy)  &&  (!empty($session->read('SillcafUsers.navyagent_user_id')))) || ($session->read('SillcafUsers.perfiles_id') == 1);
        $allowEditInfoNavy = (($infoNavy['status_info_navy_id'] == 1 || $infoNavy['status_info_navy_id'] == 5) && $this->canInfoNavyEdit($closingTn, $infoNavy)  &&  (!empty($session->read('SillcafUsers.customs_user_id')))) || ($session->read('SillcafUsers.perfiles_id') == 1);
        $allowCancelInfoNavy = ($infoNavy['status_info_navy_id'] == 2 || $infoNavy['status_info_navy_id'] == 4)  &&  ($session->read('SillcafUsers.perfiles_id') == 3 || $session->read('SillcafUsers.perfiles_id') == 4);
        $showAddictionalItemsForm = (($infoNavy['status_info_navy_id'] != 2) && $this->canInfoNavyEdit($closingTn, $infoNavy)  &&  (!empty($session->read('SillcafUsers.navyagent_user_id')))) || ($session->read('SillcafUsers.perfiles_id') == 1);
        $canInfoNavyEditByConsolidated = $this->canInfoNavyEditByConsolidated($infoNavy);
        $this->set(compact('infoNavy', 'userLogin', 'closingTn', 'infoNavyMessage', 'infoNavyMessages', 'showApproveBtn', 'allowEditInfoNavy', 'packagingCaffee', 'adictionalElementsHasPackagingCaffee', 'adictionalElements', 'showAddictionalItemsForm', 'allowCancelInfoNavy', 'canInfoNavyEditByConsolidated'));
    }

    public function booking2($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }
        $this->InfoNavy->updateAll(['booking2' => $this->request->getData()['booking2']], ['id' => $id]);
        WatchHelper::watch([$this->request->getData()['booking2'], $this->InfoNavy->get($id), 'View Proformas', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);
        $this->Flash->success(__('Booking 2 actualizado: ' . $this->request->getData()['booking2']));
        return $this->redirect(['action' => 'view', $id]);
    }

    public function canInfoNavyEdit($closingTn, $infoNavy)
    {
        if ($closingTn['closing_date'] > FrozenTime::now()) {
            return true;
        }
        return false;
    }

    public function canInfoNavyEditByConsolidated($infoNavy)
    {
        if ($infoNavy['packaging_mode'] == 'CONSOLIDADO (VARIAS AGENCIAS)') {
            $this->loadModel('SillcafInfonavyCustom');
            $session = $this->request->getSession();
            $resultData = $this->SillcafInfonavyCustom->find('all')->where(['info_navy_id' => $infoNavy['id'], 'custom_id' => $session->read('SillcafUsers.customs_user_id')]);
            foreach ($resultData as $custom) {
                if ($custom['custom_id'] == $infoNavy['customs_id']) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public function confirmation()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }
        $infoNavy = $this->InfoNavy->find('all', [
            'contain' => ['NavyAgent', 'ShippingLines', 'Customs', 'ArrivalsMotorships.Motorships', 'StatusInfoNavy', 'SillcafUsers', 'AdictionalElementsHasPackagingCaffee.AdictionalElements', 'PackagingCaffee.DetailPackagingCaffee.RemittancesCaffee.RemittancesCaffeeHasNoveltysCaffee.NoveltysCaffee', 'PackagingCaffee.DetailPackagingCaffee.RemittancesCaffee.Clients', 'PackagingCaffee.ReadyContainer', 'PackagingCaffee.DetailPackakgingCrossdocking']

        ])->where(['InfoNavy.status_info_navy_id' => 2])->toArray();

        WatchHelper::watch([NULL, NULL, 'Confirmation Proformas', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);

        $this->set(compact('infoNavy'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['action' => 'index']);
        }
        $customs = $this->InfoNavy->Customs->find('list', [
            'keyField' => 'id',
            'valueField' => 'cia_name'
        ]);

        $this->loadModel('SillcafInfonavyCustom');

        WatchHelper::watch([NULL, NULL, 'Agregar Proformas Form', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);

        $infoNavy = $this->InfoNavy->newEmptyEntity();
        if ($this->request->is('post')) {
            $infoNavy = $this->InfoNavy->patchEntity($infoNavy, $this->request->getData());
            $infoNavy['created_date'] = FrozenTime::now();
            $motorship = $this->InfoNavy->ArrivalsMotorships->get($infoNavy['arrivals_motorships_id'], ['contain' => 'Motorships']);
            $infoNavy['motorship_name'] = $motorship['motorship']['name_motorship'];
            $infoNavy['status_info_navy_id'] = 1;
            $infoNavy['proforma'] = '';
            $infoNavy['sillcaf_user_reg_id'] = $this->request->getSession()->read('SillcafUsers.id');
            if ($this->InfoNavy->save($infoNavy)) {
                $infoNavy['proforma'] = substr($customs->toArray()[$infoNavy['customs_id']], 0, 4) . $infoNavy['id'];
                if ($this->InfoNavy->save($infoNavy)) {
                    if ($infoNavy['packaging_mode'] == 'CONSOLIDADO (VARIAS AGENCIAS)') {
                        $additionalCustom = $this->l->newEmptyEntity();
                        $additionalCustom['info_navy_id'] = $infoNavy['id'];
                        $additionalCustom['custom_id'] = $infoNavy['customs_id'];
                        $this->SillcafInfonavyCustom->save($additionalCustom);
                        $additionalCustomsId = $this->request->getData()['additional_customs_id'];
                        $this->loadModel('SillcafInfonavyCustom');
                        foreach ($additionalCustomsId as $customId) {
                            $additionalCustom = $this->SillcafInfonavyCustom->newEmptyEntity();
                            $additionalCustom['info_navy_id'] = $infoNavy['id'];
                            $additionalCustom['custom_id'] = $customId;
                            $this->SillcafInfonavyCustom->save($additionalCustom);
                        }
                    }
                    WatchHelper::watch([NULL, $infoNavy, 'Agregar nueva Proforma', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);
                    $this->Flash->success(__('Se registró correctamente la proforma ' . $infoNavy['proforma']));
                    return $this->redirect(['action' => 'view', $infoNavy['id']]);
                }
            }
            $this->Flash->error(__('The info navy could not be saved. Please, try again.'));
        }
        $customsId = $session->read('SillcafUsers.customs_user_id');
        $this->loadModel('NavyAgent');
        $navyAgent = $this->NavyAgent->find('list');
        $shippingLines = [];
        $arrivalsMotorships = [];
        $this->set(compact('infoNavy', 'customs', 'arrivalsMotorships', 'shippingLines', 'navyAgent', 'customsId'));
    }

    public function getInfoByNavyAgent()
    {
        $this->autoRender = false;
        $requestData = $this->request->getParam('?');
        $this->loadModel('ClosingCn');
        $options = ['contain' => ['ArrivalsMotorships.Motorships', 'NavyAgent']];
        $resultData['arrivals_motorships'] = $this->ClosingCn->find('all', $options)->where(['ClosingCn.closing_cn_navy_agent_id' => $requestData['navyAgentId'], 'ClosingCn.closing_date >' => FrozenTime::now()]);
        $resultData['shipping_lines'] = $this->InfoNavy->ShippingLines->find('list', [
            'keyField' => 'id',
            'valueField' => 'business_name'
        ])->where(['codigo_spb' => $requestData['navyAgentId']]);
        echo json_encode($resultData);
    }

    public function aprove($id)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }
        $infoNavy = $this->InfoNavy->get($id);
        if ($infoNavy['shipping_lines_id'] == 39 && $infoNavy['booking2'] == null) {
            $this->Flash->error(__('Proforma ' . $id . ' sin booking #2 diligenciado. No se aprobo'));
            return $this->redirect(['action' => 'view', $id]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $infoNavy = $this->InfoNavy->patchEntity($infoNavy, $this->request->getData());
            $infoNavy['sillcaf_user_approve_id'] = $this->request->getSession()->read('SillcafUsers.id');
            $infoNavy['status_info_navy_id'] = 2;
            $infoNavy['aprove_date'] = FrozenTime::now();

            if ($this->InfoNavy->save($infoNavy)) {
                WatchHelper::watch([NULL, $infoNavy, 'Aprobada Proforma', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);
                $this->Flash->success(__('Se ha aprobado la proforma ' . $id));
                $this->emailNotification($infoNavy, " Aprobada");
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The info navy could not be saved. Please, try again.'));
        }
    }

    public function confirm($id)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['action' => 'index']);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $options = ['contain' => ['AdictionalElementsHasPackagingCaffee', 'PackagingCaffee.DetailPackagingCaffee', 'PackagingCaffee.DetailPackakgingCrossdocking']];
            $infoNavy = $this->InfoNavy->find('all', $options)->where(['id' => $id])->first();
            if ((!empty($infoNavy['packaging_caffee'][0]['detail_packaging_caffee']) || !empty($infoNavy['packaging_caffee'][0]['detail_packakging_crossdocking'])) &&
                !empty($infoNavy['adictional_elements_has_packaging_caffee'])
            ) {
                if ($infoNavy['packaging_type'] == 'FCL') {
                    debug('FCL');
                    $infoNavy['sillcaf_user_approve_id'] = $this->request->getSession()->read('SillcafUsers.id');
                    $infoNavy['status_info_navy_id'] = 2;
                } else {
                    debug('LCL');
                    $infoNavy['status_info_navy_id'] = 4;
                }
                $infoNavy['confirm_date'] = FrozenTime::now();

                if ($this->InfoNavy->save($infoNavy)) {
                    WatchHelper::watch([NULL, $infoNavy, 'Confirma Proforma', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);
                    $this->Flash->success(__('Se ha confirmado la proforma ' . $id));
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                $this->Flash->error(__('No se puede confirmar la proforma porque debe tener un contenedor con programación de lote y un elemento adicional'));
            }
            return $this->redirect(['action' => 'view', $id]);
        }
    }

    public function reject($id)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['action' => 'index']);
        }
        $infoNavy = $this->InfoNavy->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $infoNavy = $this->InfoNavy->patchEntity($infoNavy, $this->request->getData());
            $infoNavy['status_info_navy_id'] = 5;
            $infoNavy['reject_date'] = FrozenTime::now();

            if ($this->InfoNavy->save($infoNavy)) {
                WatchHelper::watch([NULL, $infoNavy, 'Rechaza Proforma', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);
                $this->Flash->success(__('Se ha rechazado la proforma ' . $id));
                $this->emailNotification($infoNavy, " Rechazada");
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The info navy could not be saved. Please, try again.'));
        }
    }

    public function emailNotification($infoNavy, $type)
    {
        $this->loadModel('SillcafUsers');
        $ciaUser = $this->SillcafUsers->get($infoNavy->sillcaf_user_reg_id);
        $navyAgentUser = $this->request->getSession()->read('SillcafUsers.email');
        if ($ciaUser->email) {
            $notification = new Mailer('default');
            $notification->setFrom(['asistentetic@operadoracafetera.com' => 'SILLCAF'])
                ->setTo($ciaUser->email);
            if ($navyAgentUser) {
                $notification->addTo($navyAgentUser);
            }
            $notification->setSubject('Proforma ' . $infoNavy->proforma . $type)
                ->deliver('Le informamos que la proforma ' . $infoNavy->proforma . ' ha sido ' . $type . ' por el agente naviero');
        } else {
            $this->Flash->error(__('No se pudo notificar a la CIA porque el usuario no tiene registrado un correo electrónico'));
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Info Navy id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['action' => 'index']);
        }
        $infoNavy = $this->InfoNavy->get($id, [
            'contain' => [],
        ]);
        WatchHelper::watch([$infoNavy, NULL, 'Edit Proforma Form', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $infoNavy = $this->InfoNavy->patchEntity($infoNavy, $this->request->getData());
            if ($this->InfoNavy->save($infoNavy)) {
                WatchHelper::watch([NULL, $infoNavy, 'Edit Proforma Tx', $_SERVER['REMOTE_ADDR'], $session->read('SillcafUsers.id')]);
                $this->Flash->success(__('Se actualizó correctamente el registro.'));

                return $this->redirect(['action' => 'view', $id]);
            }
            $this->Flash->error(__('The info navy could not be saved. Please, try again.'));
        }
        $customs = $this->InfoNavy->Customs->find('list', [
            'keyField' => 'id',
            'valueField' => 'cia_name'
        ]);
        $this->loadModel('ClosingCn');
        $this->loadModel('NavyAgent');
        $navyAgent = $this->NavyAgent->find('list');
        $arrivalsMotorships = [];
        $shippingLines = $this->InfoNavy->ShippingLines->find('list', [
            'keyField' => 'id',
            'valueField' => 'business_name'
        ]);
        $this->loadModel('PackagingCaffee');
        $customsId = $session->read('SillcafUsers.customs_user_id');
        $this->set(compact('infoNavy', 'customs', 'arrivalsMotorships', 'shippingLines', 'navyAgent', 'customsId'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Info Navy id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['action' => 'index']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $infoNavy = $this->InfoNavy->get($id);
        if ($this->InfoNavy->delete($infoNavy)) {
            $this->Flash->success(__('The info navy has been deleted.'));
        } else {
            $this->Flash->error(__('The info navy could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function historical()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        $historicalInfoNavyForm = $this->InfoNavy->newEmptyEntity();
        $historicalInfoNavyList = [];
        if ($this->request->is('post')) {
            $session = $this->request->getSession();
            $initDate = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['initDate']));
            $finalDate = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['finalDate']));
            $status = $this->request->getData()['status'];
            $motorship = $this->request->getData()['motorship'];
            $options = [
                'contain' => ['NavyAgent', 'ShippingLines', 'Customs', 'ArrivalsMotorships.Motorships', 'StatusInfoNavy', 'SillcafUsers', 'PackagingCaffee'],
                'conditions' => 'InfoNavy.created_date between CAST("' . $initDate->i18nFormat('yyyy-MM-dd HH:mm:ss') . '" AS DATETIME) and CAST("' .  $finalDate->i18nFormat('yyyy-MM-dd HH:mm:ss') . '" AS DATETIME)'
            ];
            if ($status) {
                $options['conditions'] = $options['conditions'] . ' AND status_info_navy_id=' . $status;
            }
            if ($motorship) {
                $options['conditions'] = $options['conditions'] . ' AND ArrivalsMotorships.arrivals_motorships_motorships_id=' . $motorship;
            }
            $customs_user_id = $session->read('SillcafUsers.customs_user_id');
            if ($customs_user_id) {
                $options['conditions'] = $options['conditions'] . ' AND customs_id=' . $customs_user_id;
            }
            $historicalInfoNavyList = $this->InfoNavy->find('all', $options);
        }
        $statusInfoNavy = $this->InfoNavy->StatusInfoNavy->find('list', [
            'keyField' => 'id',
            'valueField' => 'status_info_navy_name'
        ]);
        $this->loadModel('Motorships');
        $motorship = $this->Motorships->find('list', [
            'keyField' => 'id_motorship',
            'valueField' => 'name_motorship'
        ]);
        $this->set(compact('historicalInfoNavyForm', 'historicalInfoNavyList', 'statusInfoNavy', 'motorship'));
    }

    /**
     * closeInfoNavyByMotorshipETA method
     *
     * Metodo que cambia el estado de las proformas y contenedores asociados cuando la ETA de la motonave se venza
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function closeInfoNavyByMotorshipEta()
    {
        $this->autoRender = false;
        $infoNavies = $this->InfoNavy->find('all', [
            'contain' => ['ArrivalsMotorships', 'PackagingCaffee.DetailPackagingCaffee.RemittancesCaffee']
        ])->where(['ArrivalsMotorships.arrival_date <' => FrozenTime::now(), 'InfoNavy.status_info_navy_id !=' => 3])->toArray();
        $connection = ConnectionManager::get('default');
        foreach ($infoNavies as $infoNavy) {
            $infoNavy['status_info_navy_id'] = 3;
            $infoNavy['closed_date'] = FrozenTime::now();
            if ($this->InfoNavy->save($infoNavy)) {
                WatchHelper::watch([NULL, $infoNavy, 'Cierre Proforma', $_SERVER['REMOTE_ADDR'], 6]);
                $arrivalMotorship = $infoNavy['arrivals_motorship'];
                $arrivalMotorship['arrivals_motorships_active'] = 0;
                $this->InfoNavy->ArrivalsMotorships->save($arrivalMotorship);
                foreach ($infoNavy['packaging_caffee'] as $packagingCaffees) {
                    if ($packagingCaffees['state_packaging_id'] == 1 || $packagingCaffees['state_packaging_id'] == 2) {
                        $packagingCaffees['state_packaging_id'] = 6;
                        if ($this->InfoNavy->PackagingCaffee->save($packagingCaffees)) {
                            $this->loadModel('DetailPackagingCaffee');
                            foreach ($packagingCaffees['detail_packaging_caffee'] as $detailPackagingCaffee) {
                                $detailPackagingCaffee['state'] = 10;
                                $this->DetailPackagingCaffee->save($detailPackagingCaffee);
                                $total_rad_bag_out = intval($detailPackagingCaffee['remittances_caffee']['total_rad_bag_out'] - $detailPackagingCaffee['quantity_radicated_bag_out']);
                                $connection->update(
                                    'remittances_caffee',
                                    ['state_operation_id' => 2, 'total_rad_bag_out' => $total_rad_bag_out],
                                    ['id' => $detailPackagingCaffee['remittances_caffee']['id']]
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}
