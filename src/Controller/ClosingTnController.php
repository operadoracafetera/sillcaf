<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\Log\Log;
/**
 * ClosingTn Controller
 *
 * @property \App\Model\Table\ClosingTnTable $ClosingTn
 * @method \App\Model\Entity\ClosingTn[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClosingTnController extends AppController
{

    public function isAuthorized($user)
    {
        if ($user->read('SillcafUsers.sillcaf_perfile.id') == 4) {
            return true;
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'indicator']);
        }
        $this->paginate = [
            'contain' => ['ArrivalsMotorships', 'SillcafUsers'],
        ];
        $closingTn = $this->paginate($this->ClosingTn);

        $this->set(compact('closingTn'));
    }

    public function indexJson()
    {
        //$this->log(json_encode($orderBy), 'debug');
        //debug($requestData);
        $requestData = $this->request->getParam('?');

        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'indicator']);
        }

        $options = [
            'contain' => ['ArrivalsMotorships.Motorships', 'SillcafUsers', 'Terminals'],
        ];

        if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => 'closing_date like "%' . $search . '%"' .
                ' OR Motorships.name_motorship like "%' . $search . '%"' .
                ' OR ArrivalsMotorships.vissel like "%' . $search . '%"' .
                ' OR closing_tn_reg_date like "%' . $search . '%"' .
                ' OR Terminals.name_terminals like "%' . $search . '%"'];
        }

        $orderList = [
            'closing_tn_id' => 'ClosingTn.closing_tn_id',
            'closing_date' => 'ClosingTn.closing_date',
            'arrivals_motorship.motorship.name_motorship' => 'Motorships.name_motorship',
            'arrivals_motorship.vissel' => 'ArrivalsMotorships.vissel',
            'terminals.name_terminals' => 'Terminals.name_terminals',
            'closing_tn_reg_date' => 'closing_tn_reg_date'
        ];

        $orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
        ];

        $whereOps = [];

        $userInfo = $this->request->getSession()->read('SillcafUsers');
        if ($userInfo['perfiles_id'] == 3) {
            $whereOps = ['closing_cn_navy_agent_id' => $userInfo['navyagent_user_id']];
        }

        $players = $this->ClosingTn->find('all', $options)->where(['closing_date >=' => FrozenTime::now()]);
        if ($requestData['length'] != -1) {
            $players = $players->limit(intval($requestData['length']));
        }
        $players = $players->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
            ->order([$orderBy['column'] => $orderBy['dir']])->where($whereOps);

        $this->set([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($players->count()),
            "recordsFiltered" => intval($players->count()),
            "data"            => $players->toList()
        ]);

        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }

    /**
     * View method
     *
     * @param string|null $id Closing Tn id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'indicator']);
        }
        $closingTn = $this->ClosingTn->get($id, [
            'contain' => ['ArrivalsMotorships', 'SillcafUsers'],
        ]);

        $this->set(compact('closingTn'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'indicator']);
        }
        $closingTn = $this->ClosingTn->newEmptyEntity();
        if ($this->request->is('post')) {
            $closingTnWithArrivals = $this->ClosingTn->find('all')->where(['arrivals_motorships_id' => $this->request->getData()['arrivals_motorships_id']])->toArray();
            if (count($closingTnWithArrivals) == 0) {
                $arrivalsMotorships = $this->ClosingTn->ArrivalsMotorships->get($this->request->getData()['arrivals_motorships_id']);
                $closing_date = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['closing_date']));
                if ($closing_date < $arrivalsMotorships['arrival_date']) {
                    $closingTn = $this->ClosingTn->patchEntity($closingTn, $this->request->getData());
                    $closingTn['closing_date'] = $closing_date;
                    $closingTn['closing_tn_reg_date'] = FrozenTime::now();
                    $closingTn['closing_tn_update_date'] = FrozenTime::now();
                    $closingTn['closing_tn_user_id'] = $this->request->getSession()->read('SillcafUsers.id');
                    if ($this->ClosingTn->save($closingTn)) {
                        $this->Flash->success(__('El cierre de terminal se ha guardado con éxito'));
                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The closing tn could not be saved. Please, try again.'));
                } else {
                    $this->Flash->error(__('La fecha de cierre no puede ser mayor a la fecha ETA de la motonave'));
                }
            } else {
                $this->Flash->error(__('Ya hay un cierre de terminal registrado para esta motonave'));
            }
        }
        $arrivalsMotorships = $this->ClosingTn->ArrivalsMotorships->find('list', [
            'keyField' => 'arrivals_motorships_id',
            'valueField' => 'fullname'
        ])->where(['arrival_date >' => FrozenTime::now()])->contain(['Motorships']);
        $terminals = $this->ClosingTn->Terminals->find('list', [
            'keyField' => 'id_terminals',
            'valueField' => 'name_terminals'
        ]);
        $this->set(compact('closingTn', 'arrivalsMotorships', 'terminals'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Closing Tn id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'indicator']);
        }
        $closingTn = $this->ClosingTn->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $closingTnWithArrivals = $this->ClosingTn->find('all')->where(['arrivals_motorships_id' => $this->request->getData()['arrivals_motorships_id'], 'closing_tn_id !=' => $id])->toArray();
            if (count($closingTnWithArrivals) == 0) {
                $arrivalsMotorships = $this->ClosingTn->ArrivalsMotorships->get($this->request->getData()['arrivals_motorships_id']);
                $closing_date = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['closing_date']));
                if ($closing_date < $arrivalsMotorships['arrival_date']) {
                    $closingTn = $this->ClosingTn->patchEntity($closingTn, $this->request->getData());
                    $closingTn['closing_date'] = $closing_date;
                    if ($this->ClosingTn->save($closingTn)) {
                        $this->Flash->success(__('El cierre de terminal se ha guardado con éxito'));
                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The closing tn could not be saved. Please, try again.'));
                } else {
                    $this->Flash->error(__('La fecha de cierre no puede ser mayor a la fecha ETA de la motonave'));
                }
            } else {
                $this->Flash->error(__('Ya existe un cierre de terminal registrado para esta motonave'));
            }
        }
        $arrivalsMotorships = $this->ClosingTn->ArrivalsMotorships->find('list', [
            'keyField' => 'arrivals_motorships_id',
            'valueField' => 'fullname'
        ])->where(['arrival_date >' => FrozenTime::now()])->contain(['Motorships']);
        $terminals = $this->ClosingTn->Terminals->find('list', [
            'keyField' => 'id_terminals',
            'valueField' => 'name_terminals'
        ]);
        $this->set(compact('closingTn', 'arrivalsMotorships', 'terminals'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Closing Tn id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'indicator']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $closingTn = $this->ClosingTn->get($id);
        if ($this->ClosingTn->delete($closingTn)) {
            $this->Flash->success(__('The closing tn has been deleted.'));
        } else {
            $this->Flash->error(__('The closing tn could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
