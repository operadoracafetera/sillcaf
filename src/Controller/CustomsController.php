<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Datasource\ConnectionManager;

/**
 * Customs Controller
 *
 * @property \App\Model\Table\CustomsTable $Customs
 * @method \App\Model\Entity\Custom[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        //$customs = $this->paginate($this->Customs);
		
    }

    /**
     * View method
     *
     * @param string|null $id Custom id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        /*$custom = $this->Customs->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('custom'));*/
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $custom = $this->Customs->newEmptyEntity();
        if ($this->request->is('post')) {
            $custom = $this->Customs->patchEntity($custom, $this->request->getData());
            if ($this->Customs->save($custom)) {
                $this->Flash->success(__('The custom has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The custom could not be saved. Please, try again.'));
        }
        $this->set(compact('custom'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Custom id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $custom = $this->Customs->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $custom = $this->Customs->patchEntity($custom, $this->request->getData());
            if ($this->Customs->save($custom)) {
                $this->Flash->success(__('The custom has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The custom could not be saved. Please, try again.'));
        }
        $this->set(compact('custom'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Custom id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $custom = $this->Customs->get($id);
        if ($this->Customs->delete($custom)) {
            $this->Flash->success(__('The custom has been deleted.'));
        } else {
            $this->Flash->error(__('The custom could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
