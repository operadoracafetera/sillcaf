<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\TableRegistry;

/**
 * NavyAgent Controller
 *
 * @property \App\Model\Table\NavyAgentTable $NavyAgent
 * @method \App\Model\Entity\NavyAgent[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class NavyAgentController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
		//debug($this->NavyAgent);exit;
        //$navyAgent = $this->paginate($this->NavyAgent);

        //$this->set(compact('navyAgent'));
    }

    /**
     * View method
     *
     * @param string|null $id Navy Agent id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
		$this->NavyAgent = TableRegistry::get('NavyAgent');
        $navyAgent = $this->NavyAgent->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('navyAgent'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		$this->NavyAgent = TableRegistry::get('NavyAgent');
        $navyAgent = $this->NavyAgent->newEmptyEntity();
        if ($this->request->is('post')) {
            $navyAgent = $this->NavyAgent->patchEntity($navyAgent, $this->request->getData());
            if ($this->NavyAgent->save($navyAgent)) {
                $this->Flash->success(__('The navy agent has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The navy agent could not be saved. Please, try again.'));
        }
        $this->set(compact('navyAgent'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Navy Agent id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$this->NavyAgent = TableRegistry::get('NavyAgent');
        $navyAgent = $this->NavyAgent->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $navyAgent = $this->NavyAgent->patchEntity($navyAgent, $this->request->getData());
            if ($this->NavyAgent->save($navyAgent)) {
                $this->Flash->success(__('The navy agent has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The navy agent could not be saved. Please, try again.'));
        }
        $this->set(compact('navyAgent'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Navy Agent id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
		$this->NavyAgent = TableRegistry::get('NavyAgent');
        $this->request->allowMethod(['post', 'delete']);
        $navyAgent = $this->NavyAgent->get($id);
        if ($this->NavyAgent->delete($navyAgent)) {
            $this->Flash->success(__('The navy agent has been deleted.'));
        } else {
            $this->Flash->error(__('The navy agent could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
