<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\TableRegistry;

/**
 * SillcafLogs Controller
 *
 * @property \App\Model\Table\SillcafLogsTable $SillcafLogs
 * @method \App\Model\Entity\SillcafLog[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SillcafLogsController extends AppController
{

    public function isAuthorized($user)
    {
        if (in_array($this->request->getParam('action'), ['index','delete','indexJson'])) {
            if(in_array($user->read('SillcafUsers.perfiles_id'), [1,4])){
                return true;
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }
        //$sillcafLogs = $this->paginate($this->SillcafLogs);


        //$this->set(compact('sillcafLogs'));
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function indexJson()
    {
        $requestData = $this->request->getParam('?');

        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }

        $this->SillcafLogs = TableRegistry::get('SillcafLogs');

        $options = [
            'contain' => ['SillcafUsers']
        ];

        if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => 'SillcafLogs.date_event like "%' . $search . '%"' .
                ' OR SillcafLogs.module_name like "%' . $search . '%"' .
                ' OR SillcafLogs.id like "%' . $search . '%"' .
                ' OR SillcafLogs.data_after like "%' . $search . '%"' .
                ' OR SillcafLogs.data_before like "%' . $search . '%"' .
                ' OR SillcafLogs.ip_source like "%' . $search . '%"' .
                ' OR SillcafUsers.username like "%' . $search . '%"'];
        }

        $orderList = [
            'id' => 'SillcafLogs.id',
            'date_event' => 'SillcafLogs.date_event',
            'module_name' => 'SillcafLogs.module_name',
            'data_before' => 'SillcafLogs.data_before',
            'data_after' => 'SillcafLogs.data_after',
            'ip_source' => 'SillcafLogs.ip_source',
            'username' => 'SillcafLogs.username'
        ];

        $orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
        ];

        $resultData = $this->SillcafLogs->find('all', $options);
        

        if ($requestData['length'] != -1) {
            $resultData = $resultData->limit(intval($requestData['length']));
        }
        $resultData = $resultData->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
            ->order([$orderBy['column'] => $orderBy['dir']]);


        $this->set([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($resultData->count()),
            "recordsFiltered" => intval($resultData->count()),
            "data"            => $resultData->toList()
        ]);


        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }

    
}
