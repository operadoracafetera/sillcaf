<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Log\Log;
use Cake\I18n\FrozenTime;

/**
 * DetailPackakgingCrossdocking Controller
 *
 * @property \App\Model\Table\DetailPackakgingCrossdockingTable $DetailPackakgingCrossdocking
 * @method \App\Model\Entity\DetailPackakgingCrossdocking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DetailPackakgingCrossdockingController extends AppController
{

    public function isAuthorized($user)
    {
        if (in_array($this->request->getParam('action'), ['add'])) {
            if (in_array($user->read('SillcafUsers.sillcaf_perfile.id'), [2])) {
                $this->loadModel('PackagingCaffee');
                $itemId = (int)$this->request->getParam('pass.0');
                $dataPackagingCaffee = $this->PackagingCaffee->get($itemId, ['contain' => ['InfoNavy']]);
                Log::debug(json_encode($dataPackagingCaffee));
                if ($dataPackagingCaffee['info_navy']['navy_agent_id'] == $user->read('SillcafUsers.navyagent_user_id')) {
                    return true;
                } else if ($dataPackagingCaffee['info_navy']['customs_id'] == $user->read('SillcafUsers.customs_user_id')) {
                    return true;
                } else {
                    $this->loadModel('SillcafInfonavyCustom');
                    $resultData = $this->SillcafInfonavyCustom->find('all')->where(['info_navy_id' => $itemId, 'custom_id' => $user->read('SillcafUsers.customs_user_id')]);
                    if ($resultData) {
                        return true;
                    }
                }
            }
        } else if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
            if (in_array($user->read('SillcafUsers.sillcaf_perfile.id'), [2])) {
                $this->loadModel('PackagingCaffee');
                $itemId = (int)$this->request->getParam('pass.0');
                $idPackagingCaffee = $this->DetailPackakgingCrossdocking->get($itemId)['packaging_caffee_id'];
                $dataPackagingCaffee = $this->PackagingCaffee->get($idPackagingCaffee, ['contain' => ['InfoNavy']]);
                Log::debug(json_encode($dataPackagingCaffee));
                if ($dataPackagingCaffee['info_navy']['navy_agent_id'] == $user->read('SillcafUsers.navyagent_user_id')) {
                    return true;
                } else if ($dataPackagingCaffee['info_navy']['customs_id'] == $user->read('SillcafUsers.customs_user_id')) {
                    return true;
                } else {
                    $this->loadModel('SillcafInfonavyCustom');
                    $resultData = $this->SillcafInfonavyCustom->find('all')->where(['info_navy_id' => $itemId, 'custom_id' => $user->read('SillcafUsers.customs_user_id')]);
                    if ($resultData) {
                        return true;
                    }
                }
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['RemittancesCaffee', 'PackagingCaffee'],
        ];
        $detailPackakgingCrossdocking = $this->paginate($this->DetailPackakgingCrossdocking);

        $this->set(compact('detailPackakgingCrossdocking'));
    }

    /**
     * View method
     *
     * @param string|null $id Detail Packakging Crossdocking id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $detailPackakgingCrossdocking = $this->DetailPackakgingCrossdocking->get($id, [
            'contain' => ['RemittancesCaffee', 'PackagingCaffee'],
        ]);

        $this->set(compact('detailPackakgingCrossdocking'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($packagingCaffeeId)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }

        $detailPackakgingCrossdocking = $this->DetailPackakgingCrossdocking->newEmptyEntity();
        if ($this->request->is('post')) {
            $detailPackakgingCrossdocking = $this->DetailPackakgingCrossdocking->patchEntity($detailPackakgingCrossdocking, $this->request->getData());
            $detailPackakgingCrossdocking['lot_coffee'] = $this->request->getData()['expo_code'] . '-' . $this->request->getData()['lot'];
            $detailPackagingCaffeeForm['reg_date'] = FrozenTime::now();
            if ($this->DetailPackakgingCrossdocking->save($detailPackakgingCrossdocking)) {
                $this->Flash->success(__('Se registró con exito el lote crossdocking ' . $detailPackakgingCrossdocking['lot_coffee']));
                return $this->redirect(['controller' => 'DetailPackagingCaffee', 'action' => 'add', $packagingCaffeeId]);
            }
            $this->Flash->error(__('The detail packakging crossdocking could not be saved. Please, try again.'));
        }
        $this->set(compact('detailPackakgingCrossdocking', 'packagingCaffeeId'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Detail Packakging Crossdocking id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }

        $detailPackakgingCrossdocking = $this->DetailPackakgingCrossdocking->get($id, [
            'contain' => [],
        ]);
        $packagingCaffeeId = $detailPackakgingCrossdocking['packaging_caffee_id'];
        if ($this->request->is(['patch', 'post', 'put'])) {
            $detailPackakgingCrossdocking = $this->DetailPackakgingCrossdocking->patchEntity($detailPackakgingCrossdocking, $this->request->getData());
            $detailPackakgingCrossdocking['lot_coffee'] = $this->request->getData()['expo_code'] . '-' . $this->request->getData()['lot'];
            if ($this->DetailPackakgingCrossdocking->save($detailPackakgingCrossdocking)) {
                $this->Flash->success(__('Se actualizó con exito el registro.'));

                return $this->redirect(['controller' => 'DetailPackagingCaffee', 'action' => 'add', $packagingCaffeeId]);
            }
            $this->Flash->error(__('The detail packakging crossdocking could not be saved. Please, try again.'));
        }
        $detailPackakgingCrossdocking['expo_code'] = explode("-", $detailPackakgingCrossdocking['lot_coffee'])[0];
        $detailPackakgingCrossdocking['lot'] = explode("-", $detailPackakgingCrossdocking['lot_coffee'])[1];
        $this->set(compact('detailPackakgingCrossdocking'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Detail Packakging Crossdocking id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $detailPackakgingCrossdocking = $this->DetailPackakgingCrossdocking->get($id);
        $packagingCaffeeId = $detailPackakgingCrossdocking['packaging_caffee_id'];
        if ($this->DetailPackakgingCrossdocking->delete($detailPackakgingCrossdocking)) {
            $this->Flash->success(__('Se eliminó con exito el registro.'));
        } else {
            $this->Flash->error(__('The detail packakging crossdocking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'DetailPackagingCaffee', 'action' => 'add', $packagingCaffeeId]);
    }
}
