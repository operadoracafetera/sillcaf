<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * SillcafPerfilesHasPermisses Controller
 *
 * @property \App\Model\Table\SillcafPerfilesHasPermissesTable $SillcafPerfilesHasPermisses
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SillcafPerfilesHasPermissesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $this->paginate = [
            'contain' => ['SillcafPerfiles', 'SillcafPermisses'],
        ];
        $sillcafPerfilesHasPermisses = $this->paginate($this->SillcafPerfilesHasPermisses);

        $this->set(compact('sillcafPerfilesHasPermisses'));
    }

    /**
     * View method
     *
     * @param string|null $id Sillcaf Perfiles Has Permiss id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafPerfilesHasPermiss = $this->SillcafPerfilesHasPermisses->get($id, [
            'contain' => ['SillcafPerfiles', 'SillcafPermisses'],
        ]);

        $this->set(compact('sillcafPerfilesHasPermiss'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafPerfilesHasPermiss = $this->SillcafPerfilesHasPermisses->newEmptyEntity();
        if ($this->request->is('post')) {
            $sillcafPerfilesHasPermiss = $this->SillcafPerfilesHasPermisses->patchEntity($sillcafPerfilesHasPermiss, $this->request->getData());
            if ($this->SillcafPerfilesHasPermisses->save($sillcafPerfilesHasPermiss)) {
                $this->Flash->success(__('The sillcaf perfiles has permiss has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf perfiles has permiss could not be saved. Please, try again.'));
        }
        $sillcafPerfiles = $this->SillcafPerfilesHasPermisses->SillcafPerfiles->find('list', ['limit' => 200]);
        $sillcafPermisses = $this->SillcafPerfilesHasPermisses->SillcafPermisses->find('list', ['limit' => 200]);
        $this->set(compact('sillcafPerfilesHasPermiss', 'sillcafPerfiles', 'sillcafPermisses'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sillcaf Perfiles Has Permiss id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafPerfilesHasPermiss = $this->SillcafPerfilesHasPermisses->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sillcafPerfilesHasPermiss = $this->SillcafPerfilesHasPermisses->patchEntity($sillcafPerfilesHasPermiss, $this->request->getData());
            if ($this->SillcafPerfilesHasPermisses->save($sillcafPerfilesHasPermiss)) {
                $this->Flash->success(__('The sillcaf perfiles has permiss has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf perfiles has permiss could not be saved. Please, try again.'));
        }
        $sillcafPerfiles = $this->SillcafPerfilesHasPermisses->SillcafPerfiles->find('list', ['limit' => 200]);
        $sillcafPermisses = $this->SillcafPerfilesHasPermisses->SillcafPermisses->find('list', ['limit' => 200]);
        $this->set(compact('sillcafPerfilesHasPermiss', 'sillcafPerfiles', 'sillcafPermisses'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sillcaf Perfiles Has Permiss id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $sillcafPerfilesHasPermiss = $this->SillcafPerfilesHasPermisses->get($id);
        if ($this->SillcafPerfilesHasPermisses->delete($sillcafPerfilesHasPermiss)) {
            $this->Flash->success(__('The sillcaf perfiles has permiss has been deleted.'));
        } else {
            $this->Flash->error(__('The sillcaf perfiles has permiss could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
