<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * SillcafModules Controller
 *
 * @property \App\Model\Table\SillcafModulesTable $SillcafModules
 * @method \App\Model\Entity\SillcafModule[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SillcafModulesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $sillcafModules = $this->paginate($this->SillcafModules);

        $this->set(compact('sillcafModules'));
    }

    /**
     * View method
     *
     * @param string|null $id Sillcaf Module id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafModule = $this->SillcafModules->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('sillcafModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sillcafModule = $this->SillcafModules->newEmptyEntity();
        if ($this->request->is('post')) {
            $sillcafModule = $this->SillcafModules->patchEntity($sillcafModule, $this->request->getData());
            if ($this->SillcafModules->save($sillcafModule)) {
                $this->Flash->success(__('The sillcaf module has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf module could not be saved. Please, try again.'));
        }
        $this->set(compact('sillcafModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sillcaf Module id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sillcafModule = $this->SillcafModules->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sillcafModule = $this->SillcafModules->patchEntity($sillcafModule, $this->request->getData());
            if ($this->SillcafModules->save($sillcafModule)) {
                $this->Flash->success(__('The sillcaf module has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf module could not be saved. Please, try again.'));
        }
        $this->set(compact('sillcafModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sillcaf Module id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sillcafModule = $this->SillcafModules->get($id);
        if ($this->SillcafModules->delete($sillcafModule)) {
            $this->Flash->success(__('The sillcaf module has been deleted.'));
        } else {
            $this->Flash->error(__('The sillcaf module could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
