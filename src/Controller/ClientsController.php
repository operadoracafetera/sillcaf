<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Datasource\ConnectionManager;

/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 * @method \App\Model\Entity\client[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClientsController extends AppController
{
    /**
     * searchExporterByCode method
     *
     * @param int $code Client Code.
     */
    public function searchExporterByCode($code)
    {
        $this->autoRender = false;
        $client = $this->Clients->find('all')->where(['exporter_code' => $code])->first();
        echo json_encode($client);
    }
}
