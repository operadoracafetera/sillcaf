<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * SillcafPermisses Controller
 *
 * @property \App\Model\Table\SillcafPermissesTable $SillcafPermisses
 * @method \App\Model\Entity\SillcafPermiss[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SillcafPermissesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $this->paginate = [
            'contain' => ['SillcafModules'],
        ];
        $sillcafPermisses = $this->paginate($this->SillcafPermisses);

        $this->set(compact('sillcafPermisses'));
    }

    /**
     * View method
     *
     * @param string|null $id Sillcaf Permiss id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'signin']);
        }
        $sillcafPermiss = $this->SillcafPermisses->get($id, [
            'contain' => ['SillcafModules'],
        ]);

        $this->set(compact('sillcafPermiss'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'signin']);
        }
        $sillcafPermiss = $this->SillcafPermisses->newEmptyEntity();
        if ($this->request->is('post')) {
            $sillcafPermiss = $this->SillcafPermisses->patchEntity($sillcafPermiss, $this->request->getData());
            if ($this->SillcafPermisses->save($sillcafPermiss)) {
                $this->Flash->success(__('The sillcaf permiss has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf permiss could not be saved. Please, try again.'));
        }
        $sillcafModules = $this->SillcafPermisses->SillcafModules->find('list', ['limit' => 200]);
        $this->set(compact('sillcafPermiss', 'sillcafModules'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sillcaf Permiss id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'signin']);
        }
        $sillcafPermiss = $this->SillcafPermisses->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sillcafPermiss = $this->SillcafPermisses->patchEntity($sillcafPermiss, $this->request->getData());
            if ($this->SillcafPermisses->save($sillcafPermiss)) {
                $this->Flash->success(__('The sillcaf permiss has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf permiss could not be saved. Please, try again.'));
        }
        $sillcafModules = $this->SillcafPermisses->SillcafModules->find('list', ['limit' => 200]);
        $this->set(compact('sillcafPermiss', 'sillcafModules'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sillcaf Permiss id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'signin']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $sillcafPermiss = $this->SillcafPermisses->get($id);
        if ($this->SillcafPermisses->delete($sillcafPermiss)) {
            $this->Flash->success(__('The sillcaf permiss has been deleted.'));
        } else {
            $this->Flash->error(__('The sillcaf permiss could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
