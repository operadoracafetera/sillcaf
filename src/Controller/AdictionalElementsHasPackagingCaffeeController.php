<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\Log\Log;

/**
 * AdictionalElementsHasPackagingCaffee Controller
 *
 * @property \App\Model\Table\AdictionalElementsHasPackagingCaffeeTable $AdictionalElementsHasPackagingCaffee
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdictionalElementsHasPackagingCaffeeController extends AppController
{

    public function isAuthorized($user)
    {
        if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
            if (in_array($user->read('SillcafUsers.sillcaf_perfile.id'), [2, 3])) {
                $this->loadModel('InfoNavy');
                $itemId = (int)$this->request->getParam('pass.1');
                $infoNavy = $this->InfoNavy->get($itemId);
                if ($infoNavy['navy_agent_id'] == $user->read('SillcafUsers.navyagent_user_id')) {
                    return true;
                } else if ($infoNavy['customs_id'] == $user->read('SillcafUsers.customs_user_id')) {
                    return true;
                } else {
                    $this->loadModel('SillcafInfonavyCustom');
                    $resultData = $this->SillcafInfonavyCustom->find('all')->where(['info_navy_id' => $itemId, 'custom_id' => $user->read('SillcafUsers.customs_user_id')]);
                    if ($resultData) {
                        return true;
                    }
                }
            }
        } else if (in_array($this->request->getParam('action'), ['add'])) {
            if (in_array($user->read('SillcafUsers.sillcaf_perfile.id'), [2, 3])) {
                $this->loadModel('InfoNavy');
                $itemId = (int)$this->request->getParam('pass.0');
                $infoNavy = $this->InfoNavy->get($itemId);
                if ($infoNavy['navy_agent_id'] == $user->read('SillcafUsers.navyagent_user_id')) {
                    return true;
                } else if ($infoNavy['customs_id'] == $user->read('SillcafUsers.customs_user_id')) {
                    return true;
                } else {
                    $this->loadModel('SillcafInfonavyCustom');
                    $resultData = $this->SillcafInfonavyCustom->find('all')->where(['info_navy_id' => $itemId, 'custom_id' => $user->read('SillcafUsers.customs_user_id')]);
                    if ($resultData) {
                        return true;
                    }
                }
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        $infoNavyId = $this->request->getQuery('infoNavyId');
        $this->loadModel('InfoNavy');
        $infoNavy = $this->InfoNavy->get($infoNavyId);
        $options = [
            'contain' => ['AdictionalElements', 'InfoNavy'],
        ];
        $adictionalElementsHasPackagingCaffee = $this->AdictionalElementsHasPackagingCaffee->find("all", $options)->where(['info_navy_id' => $infoNavyId]);

        $this->set(compact('adictionalElementsHasPackagingCaffee', 'infoNavy'));
    }

    /**
     * View method
     *
     * @param string|null $id Adictional Elements Has Packaging Caffee id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        $adictionalElementsHasPackagingCaffee = $this->AdictionalElementsHasPackagingCaffee->get($id, [
            'contain' => ['AdictionalElements', 'InfoNavy'],
        ]);

        $this->set(compact('adictionalElementsHasPackagingCaffee'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($infoNavyId)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }
        $adictionalElementsHasPackagingCaffee = $this->AdictionalElementsHasPackagingCaffee->newEmptyEntity();
        if ($this->request->is('post')) {
            $adictionalElementsHasPackagingCaffee = $this->AdictionalElementsHasPackagingCaffee->patchEntity($adictionalElementsHasPackagingCaffee, $this->request->getData());
            $adictionalElementsHasPackagingCaffee['adictional_elements_id'] = $this->request->getData()['adictional_elements_id'];
            $adictionalElementsHasPackagingCaffee['info_navy_id'] = $this->request->getData()['info_navy_id'];
            $adictionalElementsHasPackagingCaffee['sillcaf_user_reg_id'] = $this->request->getSession()->read('SillcafUsers.id');
            if ($this->AdictionalElementsHasPackagingCaffee->save($adictionalElementsHasPackagingCaffee)) {
                $this->Flash->success(__('Se ha guardado el elemento adicional con éxito'));

                return $this->redirect(['controller' => 'InfoNavy', 'action' => 'view', $adictionalElementsHasPackagingCaffee['info_navy_id']]);
            }
            $this->Flash->error(__('Se producido un error al guardae el elemento adicional, intente de nuevo'));
        }
        $adictionalElements = $this->AdictionalElementsHasPackagingCaffee->AdictionalElements->find('list', ['limit' => 200]);
        $infoNavy = $this->AdictionalElementsHasPackagingCaffee->InfoNavy->get($infoNavyId);
        $this->set(compact('adictionalElementsHasPackagingCaffee', 'adictionalElements', 'infoNavy'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Adictional Elements Has Packaging Caffee id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($adictional_elements_id, $infoNavyId)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }
        $adictionalElementsHasPackagingCaffee = $this->AdictionalElementsHasPackagingCaffee->find('all')->where(['adictional_elements_id' => $adictional_elements_id, 'info_navy_id' => $infoNavyId])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $adictionalElementsHasPackagingCaffee = $this->AdictionalElementsHasPackagingCaffee->patchEntity($adictionalElementsHasPackagingCaffee, $this->request->getData());
            if ($this->AdictionalElementsHasPackagingCaffee->save($adictionalElementsHasPackagingCaffee)) {
                $this->Flash->success(__('Se actualizó con exito el registro.'));

                return $this->redirect(['controller' => 'InfoNavy', 'action' => 'view', $adictionalElementsHasPackagingCaffee['info_navy_id']]);
            }
            $this->Flash->error(__('The adictional elements has packaging caffee could not be saved. Please, try again.'));
        }
        $adictionalElements = $this->AdictionalElementsHasPackagingCaffee->AdictionalElements->find('list', ['limit' => 200]);
        $infoNavy = $this->AdictionalElementsHasPackagingCaffee->InfoNavy->get($infoNavyId);
        $this->set(compact('adictionalElementsHasPackagingCaffee', 'adictionalElements', 'infoNavy'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Adictional Elements Has Packaging Caffee id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($adictional_elements_id, $infoNavyId)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $adictionalElementsHasPackagingCaffee = $this->AdictionalElementsHasPackagingCaffee->find('all')->where(['adictional_elements_id' => $adictional_elements_id, 'info_navy_id' => $infoNavyId])->first();
        if ($this->AdictionalElementsHasPackagingCaffee->delete($adictionalElementsHasPackagingCaffee)) {
            $this->Flash->success(__('Se elimino con exito el registro.'));
        } else {
            $this->Flash->error(__('The adictional elements has packaging caffee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'InfoNavy', 'action' => 'view', $infoNavyId]);
    }
}
