<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

/**
 * PackagingCaffee Controller
 *
 * @property \App\Model\Table\PackagingCaffeeTable $PackagingCaffee
 * @method \App\Model\Entity\PackagingCaffee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PackagingCaffeeController extends AppController
{

    public function isAuthorized($user)
    {
        if ($user->read('SillcafUsers.sillcaf_perfile.id') == 4) {
            if (in_array($this->request->getParam('action'), ['containers', 'confirmation', 'validateContainerWithNoveltys', 'validateContainerWithCrossdocking'])) {
                return true;
            }
        }
        if ($user->read('SillcafUsers.sillcaf_perfile.id') == 2) {
            if (in_array($this->request->getParam('action'), ['add', 'edit'])) {
                return true;
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }

        $infoNavyId = $this->request->getQuery('infoNavyId');
        $infoNavy = $this->PackagingCaffee->InfoNavy->get($infoNavyId);
        $this->loadModel('ClosingTn');
        $closingTn = $this->ClosingTn->find('all')->where(['arrivals_motorships_id' => $infoNavy['arrivals_motorships_id']])->first();
        $options = [
            'contain' => ['StatePackaging', 'ReadyContainer', 'CauseEmptiedCtn', 'InfoNavy', 'Users', 'CheckingCtn', 'TraceabilityPackaging', 'DetailPackagingCaffee.RemittancesCaffee.Clients'],
        ];
        $packagingCaffee = $this->PackagingCaffee->find("all", $options)->where(['info_navy_id' => $infoNavyId]);
        $this->loadModel('InfoNavyMessage');
        $infoNavyMessage = $this->InfoNavyMessage->newEmptyEntity();
        $infoNavyMessages = $this->InfoNavyMessage->find('all')->where(['info_navy_id' => $infoNavyId])->contain(['SillcafUsers']);
        $this->set(compact('packagingCaffee', 'infoNavy', 'closingTn', 'infoNavyMessage', 'infoNavyMessages'));
    }

    /**
     * View method
     *
     * @param string|null $id Packaging Caffee id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }

        $packagingCaffee = $this->PackagingCaffee->get($id, [
            'contain' => ['StatePackaging', 'ReadyContainer', 'CauseEmptiedCtn', 'InfoNavy', 'Users', 'CheckingCtn', 'TraceabilityPackaging', 'DetailPackagingCaffee'],
        ]);

        $this->set(compact('packagingCaffee'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($infoNavyId)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }

        $packagingCaffee = $this->PackagingCaffee->newEmptyEntity();
        if ($this->request->is('post')) {
            $packagingCaffee = $this->PackagingCaffee->patchEntity($packagingCaffee, $this->request->getData());
            $packagingCaffee['info_navy_id'] = $infoNavyId;
            $packagingCaffee['state_packaging_id'] = 2;
            $packagingCaffee['active'] = 1;
            $packagingCaffee['jetty'] = 'BUN';
            $packagingCaffee['weight_to_out'] = 1;
            $packagingCaffee['created_date'] = FrozenTime::now();
            $packagingCaffee['registered_date'] = FrozenTime::now();

            if ($this->PackagingCaffee->save($packagingCaffee)) {
                $this->Flash->success(__('Se ha creado con éxtio el contenedor ' . $packagingCaffee['id']));
                return $this->redirect(['controller' => 'InfoNavy', 'action' => 'view', $packagingCaffee['info_navy_id']]);
            }
            $this->Flash->error(__('The packaging caffee could not be saved. Please, try again.'));
        }
        $this->set(compact('packagingCaffee', 'infoNavy'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Packaging Caffee id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }

        $packagingCaffee = $this->PackagingCaffee->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $packagingCaffee = $this->PackagingCaffee->patchEntity($packagingCaffee, $this->request->getData());
            if ($this->PackagingCaffee->save($packagingCaffee)) {
                $this->Flash->success(__('Se actualizó correctamente la contenedor con OIE: ' . $packagingCaffee->id));

                return $this->redirect(['controller' => 'InfoNavy', 'action' => 'view', $packagingCaffee['info_navy_id']]);
            }
            $this->Flash->error(__('The packaging caffee could not be saved. Please, try again.'));
        }
        $infoNavy = $this->PackagingCaffee->InfoNavy->get($packagingCaffee['info_navy_id']);
        $this->set(compact('packagingCaffee', 'infoNavy'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Packaging Caffee id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }

        $this->request->allowMethod(['post', 'delete']);
        $packagingCaffee = $this->PackagingCaffee->get($id);
        if ($this->PackagingCaffee->delete($packagingCaffee)) {
            $this->Flash->success(__('The packaging caffee has been deleted.'));
        } else {
            $this->Flash->error(__('The packaging caffee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function confirmation()
    {
        $this->autoRender = false;
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $containers = $this->request->getData()['containers'];
            $packingDate = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['packing-date']));
            $containerSuccess = "Se ha confirmado los contenedores = ";
            $containerError = "Ha ocurrido un error al momento de confirmar los contenedores = ";
            $containerCrossdocking = "Hay crossdocking pendientes con los lotes = ";
            $containerNoveltys = "Hay novedades pendientes = ";
            foreach ($containers as $container) {
                $packagingCaffee = $this->PackagingCaffee->get($container);
                $hasNoveltys = $this->validateContainerWithNoveltys($container);
                if (!$hasNoveltys) {
                    $hasCrossdocking = $this->validateContainerWithCrossdocking($container);
                    if (!$hasCrossdocking) {
                        $connection = ConnectionManager::get('default');
                        $detailPackagingCaffees = $this->PackagingCaffee->DetailPackagingCaffee->find('all')->where(['packaging_caffee_id' => $packagingCaffee['id']]);
                        $this->loadModel('RemittancesCaffee');
                        foreach ($detailPackagingCaffees as $detailPackagingCaffee) {
                            $detailPackagingCaffee['state'] = 1;
                            $this->PackagingCaffee->DetailPackagingCaffee->save($detailPackagingCaffee);
                            $remittancesCaffees = $this->RemittancesCaffee->find('all')->where(['id' => $detailPackagingCaffee['remittances_caffee_id']]);
                            foreach ($remittancesCaffees as $remittanceCaffee) {
                                $connection->update('remittances_caffee', ['state_operation_id' => 3], ['id' => $remittanceCaffee['id']]);
                            }
                        }
                        $packagingCaffee['state_packaging_id'] = 1;
                        $packagingCaffee['created_date'] = $packingDate;
                        if ($this->PackagingCaffee->save($packagingCaffee)) {
                            $containerSuccess = $containerSuccess . $container . ';';
                        } else {
                            $containerError = $containerError . implode(",", $hasCrossdocking) . ";";
                        }
                    } else {
                        $containerCrossdocking = $containerCrossdocking . implode(",", $hasCrossdocking) . ";";
                    }
                } else {
                    $containerNoveltys = $containerNoveltys . implode(",", $hasNoveltys) . ";";
                }
            }
            echo json_encode([$containerSuccess, $containerError, $containerCrossdocking, $containerNoveltys]);
        }
    }

    public function validateContainerWithNoveltys($id)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('select rc.id,rc.lot_caffee, nc.`name` from packaging_caffee pc 
        INNER JOIN detail_packaging_caffee dpc on dpc.packaging_caffee_id = pc.id 
        INNER JOIN remittances_caffee rc on dpc.remittances_caffee_id = rc.id 
        INNER JOIN remittances_caffee_has_noveltys_caffee rchnc on rchnc.remittances_caffee_id = rc.id and rchnc.active = 1 
        INNER JOIN noveltys_caffee nc on rchnc.noveltys_caffee_id = nc.id where pc.id = ' . $id)->fetchAll('assoc');
        $noveltys = [];
        foreach ($results as $item) {
            array_push($noveltys, $item['id'] . '-' . $item['lot_caffee'] . '-' . $item['name']);
        }
        return $noveltys;
    }

    public function validateContainerWithCrossdocking($id)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }
        $connection = ConnectionManager::get('default');
        $results = $connection->execute('select dpc.lot_coffee from packaging_caffee pc
        INNER JOIN detail_packakging_crossdocking dpc on dpc.packaging_caffee_id = pc.id and dpc.status = 1
        where pc.id = ' . $id)->fetchAll('assoc');
        $crossdocking = [];
        foreach ($results as $item) {
            array_push($crossdocking, $item['lot_coffee']);
        }
        return $crossdocking;
    }

    public function containers()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }

        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        $packagingCaffees = [];
        if ($this->request->is('post')) {
            $initDate = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['initDate']));
            $finalDate = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['finalDate']));
            $options['conditions'] = 'PackagingCaffee.jetty="BUN" AND PackagingCaffee.state_packaging_id = ' . $this->request->getData()['state']  . ' AND PackagingCaffee.created_date between CAST("' . $initDate->i18nFormat('yyyy-MM-dd HH:mm:ss') . '" AS DATETIME) and CAST("' .  $finalDate->i18nFormat('yyyy-MM-dd HH:mm:ss') . '" AS DATETIME)';
        } else {
            $options['conditions'] = 'PackagingCaffee.jetty="BUN" AND PackagingCaffee.state_packaging_id in (1,5) ';
        }
        $options['contain'] = ['StatePackaging','InfoNavy.NavyAgent', 'InfoNavy.ShippingLines', 'InfoNavy.ArrivalsMotorships.Motorships', 'DetailPackagingCaffee.RemittancesCaffee.Clients', 'InfoNavy.AdictionalElementsHasPackagingCaffee.AdictionalElements', 'DetailPackakgingCrossdocking'];
        $packagingCaffees = $this->PackagingCaffee->find('all', $options)->toArray();
        $this->set(compact('packagingCaffees'));
    }

    public function cancel($id)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        $packagingCaffee = $this->PackagingCaffee->get($id, [
            'contain' => ['DetailPackagingCaffee.RemittancesCaffee', 'DetailPackakgingCrossdocking'],
        ]);

        $packagingCaffee['state_packaging_id'] = 6;
        if ($this->PackagingCaffee->save($packagingCaffee)) {
            $this->loadModel('DetailPackagingCaffee');
            $this->loadModel('DetailPackakgingCrossdocking');
            $connection = ConnectionManager::get('default');
            foreach ($packagingCaffee['detail_packaging_caffee'] as $detailPackagingCaffee) {
                $detailPackagingCaffee['state'] = 10;
                $this->DetailPackagingCaffee->save($detailPackagingCaffee);
                $total_rad_bag_out = intval($detailPackagingCaffee['remittances_caffee']['total_rad_bag_out'] - $detailPackagingCaffee['quantity_radicated_bag_out']);
                $connection->update(
                    'remittances_caffee',
                    ['state_operation_id' => 2, 'total_rad_bag_out' => $total_rad_bag_out],
                    ['id' => $detailPackagingCaffee['remittances_caffee']['id']]
                );
            }
            foreach ($packagingCaffee['detail_packakging_crossdocking'] as $detailPackagingCrossdocking) {
                $detailPackagingCrossdocking['status'] = 3;
                $this->DetailPackakgingCrossdocking->save($detailPackagingCrossdocking);
            }
            return $this->redirect(['controller' => 'InfoNavy', 'action' => 'view', $packagingCaffee['info_navy_id']]);
        }
    }
}
