<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\ShippingLinesTable $ShippingLines
 * @method \App\Model\Entity\ShippingLine[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ShippingLinesController extends AppController
{

	/**
     * index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
	public function index(){
		
	}

    /**
     * View method
     *
     * @param string|null $id Shipping Line id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
		$this->ShippingLines = TableRegistry::get('ShippingLines');
        $shippingLine = $this->ShippingLines->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('shippingLine'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		$this->ShippingLines = TableRegistry::get('ShippingLines');
        $shippingLine = $this->ShippingLines->newEmptyEntity();
        if ($this->request->is('post')) {
            $shippingLine = $this->ShippingLines->patchEntity($shippingLine, $this->request->getData());
			
            if ($this->ShippingLines->save($shippingLine)) {
                $this->Flash->success(__('The shipping line has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The shipping line could not be saved. Please, try again.'));
			$this->Logs->save($shippingLine, $this->request->getData());
        }
        $this->set(compact('shippingLine'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Shipping Line id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$this->ShippingLines = TableRegistry::get('ShippingLines');
        $shippingLine = $this->ShippingLines->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $shippingLine = $this->ShippingLines->patchEntity($shippingLine, $this->request->getData());
            if ($this->ShippingLines->save($shippingLine)) {
                $this->Flash->success(__('The shipping line has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The shipping line could not be saved. Please, try again.'));
        }
        $this->set(compact('shippingLine'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Shipping Line id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
		$this->ShippingLines = TableRegistry::get('ShippingLines');
        $this->request->allowMethod(['post', 'delete']);
        $shippingLine = $this->ShippingLines->get($id);
        if ($this->ShippingLines->delete($shippingLine)) {
            $this->Flash->success(__('The shipping line has been deleted.'));
        } else {
            $this->Flash->error(__('The shipping line could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
