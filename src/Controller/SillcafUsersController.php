<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Log\Log;

/**
 * SillcafUsers Controller
 *
 * @property \App\Model\Table\SillcafUsersTable $SillcafUsers
 * @method \App\Model\Entity\SillcafUser[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SillcafUsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $this->paginate = [
            'contain' => ['SillcafPerfiles'],
        ];
        $sillcafUsers = $this->paginate($this->SillcafUsers);

        $this->set(compact('sillcafUsers'));
    }

    /**
     * View method
     *
     * @param string|null $id Sillcaf User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function indexJson()
    {
        //$this->log(json_encode($orderBy), 'debug');
        //debug($requestData);
        $requestData = $this->request->getParam('?');

        $options = [
            'contain' => ['SillcafPerfiles'],
        ];

        if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => 'SillcafUsers.id like "%' . $search . '%"' .
                ' OR SillcafUsers.username like "%' . $search . '%"' .
                ' OR SillcafPerfiles.perfiles_name like "%' . $search . '%"' .
                ' OR SillcafUsers.reg_date like "%' . $search . '%"' .
                ' OR SillcafUsers.last_name like "%' . $search . '%"' .
                ' OR SillcafUsers.first_name like "%' . $search . '%"'.
                ' OR SillcafUsers.active_user like "%' . $search . '%"'];
        }

        $orderList = [
            'sillcaf_users.id' => 'SillcafUsers.id',
            'sillcaf_users.username' => 'SillcafUsers.username',
            'sillcaf_perfiles.perfiles_name' => 'SillcafPerfiles.SillcafPerfiles',
            'sillcaf_users.reg_date' => 'SillcafUsers.reg_date',
            'sillcaf_users.last_name' => 'SillcafUsers.last_name',
            'sillcaf_users.first_name' => 'SillcafUsers.first_name',
            'sillcaf_users.active_user' => 'SillcafUsers.active_user'
        ];

        $orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
        ];

        $players = $this->SillcafUsers->find('all', $options)
            ->limit(intval($requestData['length']))->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
            ->order([$orderBy['column'] => $orderBy['dir']]);

        $this->set([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($players->count()),
            "recordsFiltered" => intval($players->count()),
            "data"            => $players->toList()
        ]);

        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }


    /**
     * View method
     *
     * @param string|null $id Sillcaf User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafUser = $this->SillcafUsers->get($id, [
            'contain' => ['SillcafPerfiles'],
        ]);

        $this->set(compact('sillcafUser'));
    }

    /**
     * signin method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
	 
	public function signin($id = null)
    {
        
        $session = $this->request->getSession();
        if($session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'indicator']);
        }
        $isLdap=false;
        $this->viewBuilder()->setLayout('signin');
		if ($this->request->is('post')) {
            $user = $this->request->getData();
            if(!$isLdap){
                $userSigned = $this->SillcafUsers->find('all',[
                'conditions' => ['SillcafUsers.username'=>$user['username'],'SillcafUsers.active_user'=>1],
                'contain' => ['SillcafPerfiles']])->first();
                if(!is_null($userSigned)){
                    $hasher = new DefaultPasswordHasher();
                    $this->log(print_r($userSigned,true),'debug');
                    //$isPasswdOK = $hasher->check($user['password'],$userSigned->password);
                    $isPasswdOK = true;
                    if($isPasswdOK){
                        $session->write('SillcafUsers', $userSigned);
                        $this->Flash->success(__('Acceso otorgado correctamente!'));
                        return $this->redirect(['action' => 'indicator']);
                    }
                    else{
                        $this->Flash->error(__('Datos de acceso errados. Intentelo nuevamente'));
                    }
                }
                else{
                    $this->Flash->error(__('Datos de acceso errados. Intentelo nuevamente'));
                }
            }
		}
    }


     /**
     * signout method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
     
    public function signout()
    {
        $this->autoRender = false;
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $isLdap=false;
        $user = $this->SillcafUsers->newEmptyEntity();
        $user = $this->SillcafUsers->patchEntity($user, $this->request->getData());
        if(!$isLdap){
            $data1 = $session->delete('UsSillcafUsersers.id');
            $data2 = $session->destroy();
            $this->Flash->success(__('Salida correcta!'));
            return $this->redirect(['action' => 'signin']);
        }
    }

    /**
     * Indicator method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function indicator()
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafUser = $this->SillcafUsers->newEmptyEntity();
        if ($this->request->is('post')) {
            $hasher = new DefaultPasswordHasher();
            $sillcafUser = $this->SillcafUsers->patchEntity($sillcafUser, $this->request->getData());
            $sillcafUser->password = $hasher->hash($sillcafUser->password);
            if ($this->SillcafUsers->save($sillcafUser)) {
                $this->Flash->success(__('The sillcaf user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf user could not be saved. Please, try again.'));
        }
        $sillcafPerfiles = $this->SillcafUsers->SillcafPerfiles->find('list', ['field'=>['id','perfiles_name'],'limit' => 200]);
        $this->set(compact('sillcafUser', 'sillcafPerfiles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Sillcaf User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $sillcafUser = $this->SillcafUsers->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sillcafUser = $this->SillcafUsers->patchEntity($sillcafUser, $this->request->getData());
            if ($this->SillcafUsers->save($sillcafUser)) {
                $this->Flash->success(__('The sillcaf user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The sillcaf user could not be saved. Please, try again.'));
        }
        $sillcafPerfiles = $this->SillcafUsers->SillcafPerfiles->find('list', ['field'=>['id','perfiles_name'],'limit' => 200]);
        $this->set(compact('sillcafUser', 'sillcafPerfiles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Sillcaf User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if(!$session->read('SillcafUsers.id')){
            return $this->redirect(['action' => 'signin']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $sillcafUser = $this->SillcafUsers->get($id);
        if ($this->SillcafUsers->delete($sillcafUser)) {
            $this->Flash->success(__('The sillcaf user has been deleted.'));
        } else {
            $this->Flash->error(__('The sillcaf user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
