<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * AdictionalElements Controller
 *
 * @property \App\Model\Table\AdictionalElementsTable $AdictionalElements
 * @method \App\Model\Entity\AdictionalElement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdictionalElementsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $adictionalElements = $this->paginate($this->AdictionalElements);

        $this->set(compact('adictionalElements'));
    }

    /**
     * View method
     *
     * @param string|null $id Adictional Element id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $adictionalElement = $this->AdictionalElements->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('adictionalElement'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $adictionalElement = $this->AdictionalElements->newEmptyEntity();
        if ($this->request->is('post')) {
            $adictionalElement = $this->AdictionalElements->patchEntity($adictionalElement, $this->request->getData());
            if ($this->AdictionalElements->save($adictionalElement)) {
                $this->Flash->success(__('The adictional element has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The adictional element could not be saved. Please, try again.'));
        }
        $this->set(compact('adictionalElement'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Adictional Element id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $adictionalElement = $this->AdictionalElements->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $adictionalElement = $this->AdictionalElements->patchEntity($adictionalElement, $this->request->getData());
            if ($this->AdictionalElements->save($adictionalElement)) {
                $this->Flash->success(__('The adictional element has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The adictional element could not be saved. Please, try again.'));
        }
        $this->set(compact('adictionalElement'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Adictional Element id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $adictionalElement = $this->AdictionalElements->get($id);
        if ($this->AdictionalElements->delete($adictionalElement)) {
            $this->Flash->success(__('The adictional element has been deleted.'));
        } else {
            $this->Flash->error(__('The adictional element could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
