<?php

declare(strict_types=1);


namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\Log\Log;

/**
 * ViewPackaging Controller
 *
 * @property \App\Model\Table\ViewPackagingTable $ViewPackaging
 * @method \App\Model\Entity\ViewPackaging[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ViewPackagingController extends AppController
{

    public function isAuthorized($user)
    {
        if (in_array($user->read('SillcafUsers.sillcaf_perfile.id'), [2,3,4])) {
            
            if (in_array($this->request->getParam('action'), ['index'])) {
                return true;
            }
        }
        
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'SillcafUsers','action' => 'indicator']);
        }

        $viewPackagingForm = $this->ViewPackaging->newEmptyEntity();
        $viewPackagingList = [];
        if ($this->request->is('post')) {
            $session = $this->request->getSession();
            $initDate = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['initDate']));
            $finalDate = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['finalDate']));
            $options = ['conditions' => 'jetty="BUN" AND packaging_date between CAST("' . $initDate->i18nFormat('yyyy-MM-dd HH:mm:ss') . '" AS DATETIME) and CAST("' .  $finalDate->i18nFormat('yyyy-MM-dd HH:mm:ss') . '" AS DATETIME)'];
            $customs_user_id = $session->read('SillcafUsers.customs_user_id');
            if ($customs_user_id) {
                $options['conditions'] = $options['conditions'] . ' AND customs_id=' . $customs_user_id;
            }
            $navyagent_user_id  = $session->read('SillcafUsers.navyagent_user_id');
            if ($navyagent_user_id) {
                $options['conditions'] = $options['conditions'] . ' AND navy_agent_id=' . $navyagent_user_id;
            }
            $shippingline_user_id   = $session->read('SillcafUsers.shippingline_user_id');
            if ($shippingline_user_id) {
                $options['conditions'] = $options['conditions'] . ' AND shipping_lines_id=' . $shippingline_user_id;
            }
            $viewPackagingList = $this->ViewPackaging->find('all', $options);
        }
        $this->set(compact('viewPackagingForm', 'viewPackagingList'));
    }
}
