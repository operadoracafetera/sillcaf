<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use App\View\Helper\WatchHelper;

/**
 * ArrivalsMotorships Controller
 *
 * @property \App\Model\Table\ArrivalsMotorshipsTable $ArrivalsMotorships
 * @method \App\Model\Entity\ArrivalsMotorship[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArrivalsMotorshipsController extends AppController
{

    public function isAuthorized($user)
    {
        if (in_array($this->request->getParam('action'), ['add', 'index','edit', 'delete','indexJson'])) {
            if(in_array($user->read('SillcafUsers.perfiles_id'), [1,4])){
                return true;
            }
            else{
                WatchHelper::watch([NULL,'Intento de acceso denegado','Llegada Motonave',$_SERVER['REMOTE_ADDR'],$user->read('SillcafUsers.id')]);
                return false;
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }
        
        WatchHelper::watch([NULL,NULL,'Index Llegada Motonave',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);

        $this->paginate = [
            'contain' => ['Motorships', 'Terminals'],
        ];
        $arrivalsMotorships = $this->paginate($this->ArrivalsMotorships);

        $dataInput = ['ArrivalsMotorships.index',$arrivalsMotorships,'22',$session->read('SillcafUsers.id')];

        $this->WatchHelper;

        $this->set(compact('arrivalsMotorships'));
    }

    public function indexJson()
    {
        $requestData = $this->request->getParam('?');

        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }

        WatchHelper::watch([NULL,NULL,'IndexJson Llegada Motonave',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);

        $options = [
            'contain' => ['Motorships', 'Terminals']
        ];

        if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => 'visit_number like "%' . $search . '%"' .
                ' OR vissel like "%' . $search . '%"' .
                ' OR arrival_date like "%' . $search . '%"' .
                ' OR Motorships.name_motorship like "%' . $search . '%"' .
                ' OR Terminals.name_terminals like "%' . $search . '%"'];
        }

        $orderList = [
            'arrivals_motorships_id' => 'ArrivalsMotorships.arrivals_motorships_id',
            'visit_number' => 'ArrivalsMotorships.visit_number',
            'vissel' => 'ArrivalsMotorships.vissel',
            'arrival_date' => 'ArrivalsMotorships.arrival_date',
            'motorship.name_motorship' => 'Motorships.name_motorship',
            'terminal.name_terminals' => 'Terminals.name_terminals',
            'arrivals_motorships_active' => 'ArrivalsMotorships.arrivals_motorships_active'
        ];

        $orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
        ];

        $resultData = $this->ArrivalsMotorships->find('all', $options)->where(['arrival_date >=' => FrozenTime::now()]);
        if ($requestData['length'] != -1) {
            $resultData = $resultData->limit(intval($requestData['length']));
        }
        $resultData = $resultData->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
            ->order([$orderBy['column'] => $orderBy['dir']]);

        $this->set([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($resultData->count()),
            "recordsFiltered" => intval($resultData->count()),
            "data"            => $resultData->toList()
        ]);

        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }

    /**
     * View method
     *
     * @param string|null $id Arrivals Motorship id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }

        $arrivalsMotorship = $this->ArrivalsMotorships->get($id, [
            'contain' => ['Motorships', 'Terminals'],
        ]);

        $this->set(compact('arrivalsMotorship'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }

        WatchHelper::watch([NULL,NULL,'Agregar Llegada motonave Form',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);

        $arrivalsMotorship = $this->ArrivalsMotorships->newEmptyEntity();
        if ($this->request->is('post')) {
            $motorshipFound = $this->ArrivalsMotorships->find('all')->where(['arrivals_motorships_motorships_id' => $this->request->getData()['arrivals_motorships_motorships_id'], 'arrival_date >' => FrozenTime::now()])->toArray();
            if (count($motorshipFound) == 0) {
                $arrivalsMotorship = $this->ArrivalsMotorships->patchEntity($arrivalsMotorship, $this->request->getData());
                $arrivalsMotorship['arrival_date'] = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['arrival_date']));
                $arrivalsMotorship['arrivals_motorships_user_id'] = $this->request->getSession()->read('SillcafUsers.id');
                $arrivalsMotorship['arrivals_motorships_reg_date'] = FrozenTime::now();
                $arrivalsMotorship['arrivals_motorships_update_date'] = FrozenTime::now();
                if ($this->ArrivalsMotorships->save($arrivalsMotorship)) {

                    WatchHelper::watch([NULL,$arrivalsMotorship,'Agregar Llegada Motonave Tx',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);
                    $this->Flash->success(__('Se ha guardado el anuncio con éxito'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The arrivals motorship could not be saved. Please, try again.'));
            } else {
                $this->Flash->error(__('Ya existe un anuncio para esta motonave'));
            }
        }
        $motorships = $this->ArrivalsMotorships->Motorships->find('list', [
            'keyField' => 'id_motorship',
            'valueField' => 'name_motorship'
        ])->toArray();
        $terminals = $this->ArrivalsMotorships->Terminals->find('list', [
            'keyField' => 'id_terminals',
            'valueField' => 'name_terminals'
        ]);
        $this->set(compact('arrivalsMotorship', 'motorships', 'terminals'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Arrivals Motorship id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        $arrivalsMotorship = $this->ArrivalsMotorships->get($id, [
            'contain' => [],
        ]);

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }

        WatchHelper::watch([$arrivalsMotorship,NULL,'Edit ArrivalsMotorships Form',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $motorshipFound = $this->ArrivalsMotorships->find('all')->where(['arrivals_motorships_motorships_id' => $this->request->getData()['arrivals_motorships_motorships_id'], 'arrival_date >' => FrozenTime::now(), 'arrivals_motorships_id !=' => $id])->toArray();
            if (count($motorshipFound) == 0) {
                $arrivalsMotorship = $this->ArrivalsMotorships->patchEntity($arrivalsMotorship, $this->request->getData());
                $arrivalsMotorship['arrival_date'] = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['arrival_date']));
                $arrivalsMotorship['arrivals_motorships_user_id'] = $this->request->getSession()->read('SillcafUsers.id');
                $arrivalsMotorship['arrivals_motorships_update_date'] = FrozenTime::now();
                if ($this->ArrivalsMotorships->save($arrivalsMotorship)) {

                    WatchHelper::watch([NULL,$arrivalsMotorship,'Editar Llegada Motonave Tx',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);
                    $this->Flash->success(__('Se ha guardado el anuncio con éxito'));

                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The arrivals motorship could not be saved. Please, try again.'));
            } else {
                $this->Flash->error(__('Ya existe un anuncio para esta motonave'));
            }
        }
        $motorships = $this->ArrivalsMotorships->Motorships->find('list', [
            'keyField' => 'id_motorship',
            'valueField' => 'name_motorship'
        ]);
        $terminals = $this->ArrivalsMotorships->Terminals->find('list', [
            'keyField' => 'id_terminals',
            'valueField' => 'name_terminals'
        ]);
        $this->set(compact('arrivalsMotorship', 'motorships', 'terminals'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Arrivals Motorship id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller'=>'sillcaf-users','action' => 'indicator']);
        }

        $this->request->allowMethod(['post', 'delete']);
        $arrivalsMotorship = $this->ArrivalsMotorships->get($id);
        if ($this->ArrivalsMotorships->delete($arrivalsMotorship)) {
            $this->Flash->success(__('The arrivals motorship has been deleted.'));
        } else {
            $this->Flash->error(__('The arrivals motorship could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
