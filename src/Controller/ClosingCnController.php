<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\I18n\Time;
use Cake\Log\Log;
use App\View\Helper\WatchHelper;

/**
 * ClosingCn Controller
 *
 * @property \App\Model\Table\ClosingCnTable $ClosingCn
 * @method \App\Model\Entity\ClosingCn[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClosingCnController extends AppController
{

    public function isAuthorized($user)
    {
        if ($user->read('SillcafUsers.sillcaf_perfile.id') == 4) {
            if (in_array($this->request->getParam('action'), ['add','edit','index','indexJson'])) {
                return true;
            }
        }
        else if ($user->read('SillcafUsers.sillcaf_perfile.id') == 3) {
            if (in_array($this->request->getParam('action'), ['edit'])) {
                $itemId = (int)$this->request->getParam('pass.0');
                if ($this->ClosingCn->isOwnedBy($itemId, $user->read('SillcafUsers.navyagent_user_id'))) {
                    return true;
                }
            }
            else if (in_array($this->request->getParam('action'), ['index','indexJson'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }

        WatchHelper::watch([NULL,NULL,'Index Cierre naviero',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);
        $this->paginate = [
            'contain' => ['NavyAgent', 'ArrivalsMotorships'],
        ];
        $closingCn = $this->paginate($this->ClosingCn);

        $this->set(compact('closingCn'));
    }

    public function indexJson()
    {
        //$this->log(json_encode($orderBy), 'debug');
        //debug($requestData);
        $requestData = $this->request->getParam('?');

        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }

        WatchHelper::watch([NULL,NULL,'IndexJson Cierre naviero',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);

        $userInfo = $this->request->getSession()->read('SillcafUsers');

        $options = [
            'contain' => ['NavyAgent', 'ArrivalsMotorships.Motorships'],
        ];

        if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            $search = $requestData['search']['value'];
            $options += ['conditions' => 'closing_date like "%' . $search . '%"' .
                ' OR NavyAgent.name like "%' . $search . '%"' .
                ' OR ArrivalsMotorships.arrival_date like "%' . $search . '%"' .
                ' OR ArrivalsMotorships.vissel like "%' . $search . '%"' .
                ' OR Motorships.name_motorship like "%' . $search . '%"'];
        }

        $orderList = [
            'closing_cn_id' => 'ClosingCn.closing_cn_id',
            'closing_date' => 'ClosingCn.closing_date',
            'navy_agent.name' => 'NavyAgent.name',
            'arrivals_motorship.motorship.name_motorship' => 'Motorships.name_motorship',
            'arrivals_motorship.vissel' => 'ArrivalsMotorships.vissel',
            'arrivals_motorship.arrival_date' => 'ArrivalsMotorships.arrival_date'
        ];

        $orderBy = [
            'column' => $orderList[$requestData['columns'][$requestData['order'][0]['column']]['data']],
            'dir' => $requestData['order'][0]['dir']
        ];

        $whereOps = [];

        if ($userInfo['perfiles_id'] == 3) {
            $whereOps = ['closing_cn_navy_agent_id' => $userInfo['navyagent_user_id']];
        }

        $players = $this->ClosingCn->find('all', $options)->where(['closing_date >=' => FrozenTime::now()]);
        if ($requestData['length'] != -1) {
            $players = $players->limit(intval($requestData['length']));
        }
        $players = $players->page((intval($requestData['start']) + intval($requestData['length'])) / intval($requestData['length']))
            ->order([$orderBy['column'] => $orderBy['dir']])->where($whereOps);

        $this->set([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($players->count()),
            "recordsFiltered" => intval($players->count()),
            "data"            => $players->toList()
        ]);

        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }

    /**
     * View method
     *
     * @param string|null $id Closing Cn id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }
        $closingCn = $this->ClosingCn->get($id, [
            'contain' => ['NavyAgent', 'ArrivalsMotorships'],
        ]);

        $this->set(compact('closingCn'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }
        WatchHelper::watch([NULL,NULL,'agregar Cierre naviero Form',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);
        $closingCn = $this->ClosingCn->newEmptyEntity();
        $saveNavies = "";
        $noSaveNavies = "";
        $navyAgent = $this->ClosingCn->NavyAgent->find('list', ['limit' => 200])->toArray();
        if ($this->request->is('post')) {
            $closing_date = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['closing_date']));
            $arrivalMotorship = $this->ClosingCn->ArrivalsMotorships->get($this->request->getData()['arrivals_motorships_id']);
            if ($closing_date < $arrivalMotorship['arrival_date']) {
                $naviesId = $this->request->getData()['closing_cn_navy_agent_id'];
                $closingCns = [];
                foreach ($naviesId as $navyId) {
                    $closingTnFound = $this->ClosingCn->find('all')->where(['arrivals_motorships_id' => $this->request->getData()['arrivals_motorships_id'], 'closing_cn_navy_agent_id' => intVal($navyId)])->toArray();
                    if (count($closingTnFound) == 0) {
                        $closingCnByCustom = $this->ClosingCn->newEmptyEntity();
                        $closingCnByCustom = $this->ClosingCn->patchEntity($closingCn, $this->request->getData());
                        $closingCnByCustom['closing_cn_navy_agent_id'] = intVal($navyId);
                        $closingCnByCustom['closing_date'] = date_format($closing_date, "Y-m-d H:i:s");
                        $closingCnByCustom['closing_cn_reg_date'] = date_format(FrozenTime::now(), "Y-m-d H:i:s");
                        $closingCnByCustom['closing_cn_update_date'] = date_format(FrozenTime::now(), "Y-m-d H:i:s");
                        $closingCnByCustom['closing_cn_user_id'] = $this->request->getSession()->read('SillcafUsers.id');
                        array_push($closingCns, (array) json_decode(json_encode($closingCnByCustom)));
                        $saveNavies = $saveNavies . " " . $navyAgent[intVal($navyId)] . ",";
                    } else {
                        $noSaveNavies = $noSaveNavies . " " . $navyAgent[intVal($navyId)] . ",";
                    }
                }
                $entities = $this->ClosingCn->newEntities($closingCns);
                Log::debug(json_encode($entities));
                if (!empty($entities)) {
                    if ($this->ClosingCn->saveMany($entities)) {
                        WatchHelper::watch([NULL,$entities,'Agregar Cierre naviero Tx',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);
                        $this->Flash->success(__('Se registraron los cierres para las siguientes navieras ' . $saveNavies . (($noSaveNavies) ? " y no se registraron para las navieras " . $noSaveNavies . " porque ya cuentan con un cierre" : "")));
                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->error(__('Ha ocurrido un error al momento de guardar los cierres físicos'));
                    }
                } else {
                    $this->Flash->error(__('Ha ocurrido un error al momento de guardar los cierres físicos'. (($noSaveNavies) ? " y no se registraron para las navieras " . $noSaveNavies . " porque ya cuentan con un cierre" : "")));
                }
            } else {
                $this->Flash->error(__('La fecha de cierre no puede ser mayor a la fecha ETA de la motonave'));
            }
        }
        $arrivalsMotorships = $this->ClosingCn->ArrivalsMotorships->find('list', [
            'keyField' => 'arrivals_motorships_id',
            'valueField' => 'fullname'
        ])->where(['arrival_date >' => FrozenTime::now()])->contain(['Motorships'])->toArray();

        $this->set(compact('closingCn', 'navyAgent', 'arrivalsMotorships'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Closing Cn id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }
        $userInfo = $this->request->getSession()->read('SillcafUsers');

        $closingCn = $this->ClosingCn->get($id, [
            'contain' => [],
        ]);

        WatchHelper::watch([$closingCn,NULL,'Editar Cierre naviero Form',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $closingTnFound = $this->ClosingCn->find('all')->where(['arrivals_motorships_id' => $this->request->getData()['arrivals_motorships_id'], 'closing_cn_navy_agent_id' => $this->request->getData()['closing_cn_navy_agent_id'], 'closing_cn_id !=' => $id])->toArray();
            if (count($closingTnFound) == 0) {
                $closing_date = FrozenTime::parse(str_replace('/', '-', $this->request->getData()['closing_date']));
                $arrivalMotorship = $this->ClosingCn->ArrivalsMotorships->get($this->request->getData()['arrivals_motorships_id']);
                if ($closing_date < $arrivalMotorship['arrival_date']) {
                    $closingCn = $this->ClosingCn->patchEntity($closingCn, $this->request->getData());
                    $closingCn['closing_date'] = $closing_date;
                    $closingCn['closing_cn_update_date'] = FrozenTime::now();
                    if ($this->ClosingCn->save($closingCn)) {
                        WatchHelper::watch([NULL,$closingCn,'Editar Cierre naviero Form',$_SERVER['REMOTE_ADDR'],$session->read('SillcafUsers.id')]);
                        $this->Flash->success(__('El cierre físico se ha guardado con éxito'));
                        return $this->redirect(['action' => 'index']);
                    }
                    $this->Flash->error(__('The closing cn could not be saved. Please, try again.'));
                } else {
                    $this->Flash->error(__('La fecha de cierre no puede ser mayor a la fecha ETA de la motonave'));
                }
            } else {
                $this->Flash->error(__('Ya existe un cierre físico registrado para esta naviera'));
            }
        }
        $navyAgent = $this->ClosingCn->NavyAgent->find('list', ['limit' => 200])->toArray();

        $arrivalsMotorships = $this->ClosingCn->ArrivalsMotorships->find('list', [
            'keyField' => 'arrivals_motorships_id',
            'valueField' => 'fullname'
        ])->where(['arrival_date >' => FrozenTime::now()])->contain(['Motorships'])->toArray();
        
        $allowEdit = false;
        if ($session->read('SillcafUsers.sillcaf_perfile.id') == 4 || $session->read('SillcafUsers.sillcaf_perfile.id') == 1) {
            $allowEdit = true;
        }

        $this->set(compact('closingCn', 'navyAgent', 'arrivalsMotorships', 'allowEdit'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Closing Cn id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'SillcafUsers', 'action' => 'indicator']);
        }
        $this->request->allowMethod(['post', 'delete']);
        $closingCn = $this->ClosingCn->get($id);
        if ($this->ClosingCn->delete($closingCn)) {
            $this->Flash->success(__('The closing cn has been deleted.'));
        } else {
            $this->Flash->error(__('The closing cn could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
