<?php

declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\FrozenTime;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;

/**
 * DetailPackagingCaffee Controller
 *
 * @property \App\Model\Table\DetailPackagingCaffeeTable $DetailPackagingCaffee
 * @method \App\Model\Entity\DetailPackagingCaffee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DetailPackagingCaffeeController extends AppController
{


    public function isAuthorized($user)
    {
        if (in_array($this->request->getParam('action'), ['add', 'edit', 'delete'])) {
            if (in_array($user->read('SillcafUsers.sillcaf_perfile.id'), [2, 3])) {
                $this->loadModel('PackagingCaffee');
                $itemId = (int)$this->request->getParam('pass.0');
                $dataPackagingCaffee = $this->PackagingCaffee->get($itemId, ['contain' => ['InfoNavy']]);
                if ($dataPackagingCaffee['info_navy']['navy_agent_id'] == $user->read('SillcafUsers.navyagent_user_id')) {
                    return true;
                } else if ($dataPackagingCaffee['info_navy']['customs_id'] == $user->read('SillcafUsers.customs_user_id')) {
                    return true;
                } else {
                    $this->loadModel('SillcafInfonavyCustom');
                    $resultData = $this->SillcafInfonavyCustom->find('all')->where(['info_navy_id' => $itemId, 'custom_id' => $user->read('SillcafUsers.customs_user_id')]);
                    if ($resultData) {
                        return true;
                    }
                }
            } else if (in_array($user->read('SillcafUsers.sillcaf_perfile.id'), [4])) {
                return true;
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }
        $packagingCaffeeId = $this->request->getQuery('packagingCaffeeId');
        $options = [
            'contain' => ['RemittancesCaffee.Clients', 'PackagingCaffee', 'Users'],
        ];
        $detailPackagingCaffee = $this->DetailPackagingCaffee->find('all', $options)->where(['packaging_caffee_id' => $packagingCaffeeId]);
        $detailPackagingCaffeeForm = $this->DetailPackagingCaffee->newEmptyEntity();
        $this->set(compact('detailPackagingCaffee', 'detailPackagingCaffeeForm'));
    }

    public function searchRemesaJson()
    {
        $requestData = $this->request->getParam('?');

        $options = [
            'contain' => ['Clients', 'SlotStore', 'UnitsCaffee', 'TypeUnits', 'RemittancesCaffeeHasNoveltysCaffee.NoveltysCaffee']
        ];
        $search = $requestData['search']['value'];
        if (isset($requestData['search']) && $requestData['search']['value'] != '') {
            if (preg_match('/\d-\d/', $search)) {
                list($codExpo, $lot) = explode('-', $search);
                $options += ['conditions' => 'RemittancesCaffee.jetty="1" AND RemittancesCaffee.state_operation_id not in (9,4,5) AND RemittancesCaffee.lot_caffee like "%' . $lot .
                    '%" AND Clients.exporter_code like "%' . $codExpo .
                    '%" AND RemittancesCaffee.custom_id = ' . $requestData['custom_id']];
            }
        }
        if (isset($options['conditions'])) {
            $this->loadModel('RemittancesCaffee');
            Log::debug(json_encode($options));
            $resultData = $this->RemittancesCaffee->find('all', $options)->limit(10);
            $this->set([
                "draw"            => intval($requestData['draw']),
                "recordsTotal"    => intval($resultData->count()),
                "recordsFiltered" => intval($resultData->count()),
                "data"            => $resultData->toList()
            ]);
        } else {
            $this->set([
                "draw"            => intval($requestData['draw']),
                "recordsTotal"    => 0,
                "recordsFiltered" => 0,
                "data"            => []
            ]);
        }


        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }

    /**
     * View method
     *
     * @param string|null $id Detail Packaging Caffee id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $detailPackagingCaffee = $this->DetailPackagingCaffee->get($id, [
            'contain' => ['RemittancesCaffee', 'PackagingCaffee', 'Users'],
        ]);

        $this->set(compact('detailPackagingCaffee'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($packagingCaffeeId = null)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }

        $detailPackagingCaffeeForm = $this->DetailPackagingCaffee->newEmptyEntity();
        $this->loadModel('PackagingCaffee');
        $userLogin = $session->read('SillcafUsers');
        $packagingCaffee = $this->PackagingCaffee->get($packagingCaffeeId, ['contain' => ['InfoNavy']]);
        $this->loadModel('DetailPackakgingCrossdocking');
        $detailPackagingCrossdocking = $this->DetailPackakgingCrossdocking->find('all')->where(['packaging_caffee_id' => $packagingCaffeeId]);
        $allowAddDetailPackaging = (($packagingCaffee['info_navy']['status_info_navy_id'] == 1 || $packagingCaffee['info_navy']['status_info_navy_id'] == 5) && (!empty($session->read('SillcafUsers.customs_user_id')))) || $session->read('SillcafUsers.perfiles_id') == 1;
        $this->set(compact('detailPackagingCrossdocking', 'detailPackagingCaffeeForm', 'userLogin', 'packagingCaffee', 'allowAddDetailPackaging'));
    }

    public function addDetailPackagingJson($packagingCaffeeId)
    {
        $requestData = $this->request->getParam('?');

        $options = [
            'contain' => ['RemittancesCaffee', 'RemittancesCaffee.Clients', 'PackagingCaffee', 'Users'],
        ];

        $resultData = $this->DetailPackagingCaffee->find('all', $options)->where(['packaging_caffee_id' => $packagingCaffeeId]);
        if ($requestData['length'] != -1) {
            $resultData = $resultData->limit(intval($requestData['length']));
        }

        $this->set([
            "draw"            => intval($requestData['draw']),
            "recordsTotal"    => intval($resultData->count()),
            "recordsFiltered" => intval($resultData->count()),
            "data"            => $resultData->toList()
        ]);

        Log::debug(json_encode($resultData->toList()));

        $this->viewBuilder()->setOption('serialize', ['draw', 'recordsTotal', 'recordsFiltered', 'data']);
    }

    public function addAjax()
    {
        $this->autoRender = false;
        $detailPackagingCaffeeForm = $this->DetailPackagingCaffee->newEmptyEntity();
        if ($this->request->is('post')) {
            $resultData = $this->DetailPackagingCaffee->find('all')->where(['remittances_caffee_id' => intval($this->request->getData()['remittances_caffee_id']), 'state != 10'])->toArray();
            if (count($resultData) == 0) {
                $detailPackagingCaffeeForm = $this->DetailPackagingCaffee->patchEntity($detailPackagingCaffeeForm, $this->request->getData());
                $detailPackagingCaffeeForm['packaging_caffee_id'] = intval($this->request->getData()['packaging_caffee_id']);
                $detailPackagingCaffeeForm['remittances_caffee_id'] = intval($this->request->getData()['remittances_caffee_id']);
                $detailPackagingCaffeeForm['created_date'] = FrozenTime::now();
                $detailPackagingCaffeeForm['updated_date'] = FrozenTime::now();
                $detailPackagingCaffeeForm['state'] = '1';
                $detailPackagingCaffeeForm['tara_packaging'] = 0;
                if ($this->DetailPackagingCaffee->save($detailPackagingCaffeeForm)) {
                    $remittanceCoffee = $this->DetailPackagingCaffee->RemittancesCaffee->get($detailPackagingCaffeeForm['remittances_caffee_id']);
                    $remittanceCoffee['total_rad_bag_out'] = $remittanceCoffee['total_rad_bag_out'] + $detailPackagingCaffeeForm['quantity_radicated_bag_out'];
                    $connection = ConnectionManager::get('default');
                    $connection->update('remittances_caffee', ['total_rad_bag_out' => $remittanceCoffee['total_rad_bag_out']], ['id' => $detailPackagingCaffeeForm['remittances_caffee_id']]);
                    echo "success";
                } else {
                    echo "error";
                }
            } else {
                echo "La remesa ya fue agregada en este u otro contenedor";
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Detail Packaging Caffee id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($packagingCaffeeId, $remittancesCaffeeId)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }

        $options = [
            'contain' => ['RemittancesCaffee'],
        ];
        $detailPackagingCaffee = $this->DetailPackagingCaffee->find('all', $options)->where(['packaging_caffee_id' => $packagingCaffeeId, 'remittances_caffee_id' => $remittancesCaffeeId])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $detailPackagingCaffee = $this->DetailPackagingCaffee->patchEntity($detailPackagingCaffee, $this->request->getData());
            if ($this->DetailPackagingCaffee->save($detailPackagingCaffee)) {
                $this->Flash->success(__('The detail packaging caffee has been saved.'));

                return $this->redirect(['action' => 'add', $packagingCaffeeId]);
            }
            $this->Flash->error(__('The detail packaging caffee could not be saved. Please, try again.'));
        }
        $this->set(compact('detailPackagingCaffee'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Detail Packaging Caffee id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($packagingCaffeeId, $remittancesCaffeeId)
    {
        $session = $this->request->getSession();
        if (!$session->read('SillcafUsers.id')) {
            return $this->redirect(['controller' => 'sillcafUsers', 'action' => 'signin']);
        }

        if (!$this->isAuthorized($session)) {
            return $this->redirect(['controller' => 'sillcaf-users', 'action' => 'indicator']);
        }

        $this->request->allowMethod(['post', 'delete']);
        $detailPackagingCaffee = $this->DetailPackagingCaffee->find('all')->where(['packaging_caffee_id' => $packagingCaffeeId, 'remittances_caffee_id' => $remittancesCaffeeId])->first();
        if ($this->DetailPackagingCaffee->delete($detailPackagingCaffee)) {
            $this->Flash->success(__('The detail packaging caffee has been deleted.'));
        } else {
            $this->Flash->error(__('The detail packaging caffee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'add', $packagingCaffeeId]);
    }
}
