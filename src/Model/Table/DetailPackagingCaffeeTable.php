<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DetailPackagingCaffee Model
 *
 * @property \App\Model\Table\RemittancesCaffeeTable&\Cake\ORM\Association\BelongsTo $RemittancesCaffee
 * @property \App\Model\Table\PackagingCaffeeTable&\Cake\ORM\Association\BelongsTo $PackagingCaffee
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\DetailPackagingCaffee newEmptyEntity()
 * @method \App\Model\Entity\DetailPackagingCaffee newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee get($primaryKey, $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DetailPackagingCaffee[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class DetailPackagingCaffeeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('detail_packaging_caffee');
        $this->setDisplayField('remittances_caffee_id');
        $this->setPrimaryKey(['remittances_caffee_id', 'packaging_caffee_id']);

        $this->belongsTo('RemittancesCaffee', [
            'foreignKey' => 'remittances_caffee_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('PackagingCaffee', [
            'foreignKey' => 'packaging_caffee_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'users_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('quantity_radicated_bag_out')
            ->requirePresence('quantity_radicated_bag_out', 'create')
            ->notEmptyString('quantity_radicated_bag_out');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmptyDateTime('created_date');

        $validator
            ->dateTime('updated_date')
            ->requirePresence('updated_date', 'create')
            ->notEmptyDateTime('updated_date');

        $validator
            ->scalar('state')
            ->maxLength('state', 2)
            ->notEmptyString('state');

        $validator
            ->integer('tara_packaging')
            ->notEmptyString('tara_packaging');

        $validator
            ->scalar('lot_coffee')
            ->maxLength('lot_coffee', 45)
            ->allowEmptyString('lot_coffee');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['remittances_caffee_id'], 'RemittancesCaffee'));
        $rules->add($rules->existsIn(['packaging_caffee_id'], 'PackagingCaffee'));
        $rules->add($rules->existsIn(['users_id'], 'Users'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
