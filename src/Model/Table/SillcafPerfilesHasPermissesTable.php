<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SillcafPerfilesHasPermisses Model
 *
 * @property \App\Model\Table\SillcafPerfilesTable&\Cake\ORM\Association\BelongsTo $SillcafPerfiles
 * @property \App\Model\Table\SillcafPermissesTable&\Cake\ORM\Association\BelongsTo $SillcafPermisses
 *
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss newEmptyEntity()
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss get($primaryKey, $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafPerfilesHasPermiss[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SillcafPerfilesHasPermissesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sillcaf_perfiles_has_permisses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('SillcafPerfiles', [
            'foreignKey' => 'perfil_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SillcafPermisses', [
            'foreignKey' => 'permisse_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->dateTime('reg_date')
            ->notEmptyDateTime('reg_date');

        $validator
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['perfil_id'], 'SillcafPerfiles'));
        $rules->add($rules->existsIn(['permisse_id'], 'SillcafPermisses'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
