<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StatusInfoNavy Model
 *
 * @property \App\Model\Table\InfoNavyTable&\Cake\ORM\Association\HasMany $InfoNavy
 *
 * @method \App\Model\Entity\StatusInfoNavy newEmptyEntity()
 * @method \App\Model\Entity\StatusInfoNavy newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\StatusInfoNavy[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StatusInfoNavy get($primaryKey, $options = [])
 * @method \App\Model\Entity\StatusInfoNavy findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\StatusInfoNavy patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StatusInfoNavy[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\StatusInfoNavy|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StatusInfoNavy saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StatusInfoNavy[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StatusInfoNavy[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\StatusInfoNavy[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StatusInfoNavy[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class StatusInfoNavyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('status_info_navy');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('InfoNavy', [
            'foreignKey' => 'status_info_navy_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('status_info_navy_name')
            ->maxLength('status_info_navy_name', 100)
            ->requirePresence('status_info_navy_name', 'create')
            ->notEmptyString('status_info_navy_name');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmptyDateTime('created_date');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
