<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ViewPackaging Model
 *
 * @property \App\Model\Table\TraceabilityPackagingsTable&\Cake\ORM\Association\BelongsTo $TraceabilityPackagings
 * @property \App\Model\Table\NavyAgentsTable&\Cake\ORM\Association\BelongsTo $NavyAgents
 * @property \App\Model\Table\ShippingLinesTable&\Cake\ORM\Association\BelongsTo $ShippingLines
 * @property \App\Model\Table\StatePackagingsTable&\Cake\ORM\Association\BelongsTo $StatePackagings
 *
 * @method \App\Model\Entity\ViewPackaging newEmptyEntity()
 * @method \App\Model\Entity\ViewPackaging newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ViewPackaging[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ViewPackaging get($primaryKey, $options = [])
 * @method \App\Model\Entity\ViewPackaging findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ViewPackaging patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ViewPackaging[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ViewPackaging|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ViewPackaging saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ViewPackaging[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ViewPackaging[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ViewPackaging[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ViewPackaging[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ViewPackagingTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('view_packaging');

        $this->belongsTo('TraceabilityPackagings', [
            'foreignKey' => 'traceability_packaging_id',
        ]);
        $this->belongsTo('NavyAgents', [
            'foreignKey' => 'navy_agent_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ShippingLines', [
            'foreignKey' => 'shipping_lines_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('StatePackagings', [
            'foreignKey' => 'state_packaging_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('oie')
            ->notEmptyString('oie');

        $validator
            ->scalar('jetty')
            ->maxLength('jetty', 300)
            ->allowEmptyString('jetty');

        $validator
            ->dateTime('upload_tracking_photos_date')
            ->allowEmptyDateTime('upload_tracking_photos_date');

        $validator
            ->dateTime('empty_date')
            ->allowEmptyDateTime('empty_date');

        $validator
            ->scalar('seal1_path')
            ->maxLength('seal1_path', 100)
            ->allowEmptyString('seal1_path');

        $validator
            ->scalar('seal2_path')
            ->maxLength('seal2_path', 100)
            ->allowEmptyString('seal2_path');

        $validator
            ->scalar('img_ctn')
            ->maxLength('img_ctn', 100)
            ->allowEmptyString('img_ctn');

        $validator
            ->scalar('estado_embalaje')
            ->maxLength('estado_embalaje', 45)
            ->requirePresence('estado_embalaje', 'create')
            ->notEmptyString('estado_embalaje');

        $validator
            ->scalar('bic_ctn')
            ->maxLength('bic_ctn', 11)
            ->requirePresence('bic_ctn', 'create')
            ->notEmptyString('bic_ctn');

        $validator
            ->scalar('lotes1')
            ->maxLength('lotes1', 16777215)
            ->allowEmptyString('lotes1');

        $validator
            ->decimal('total_sacos')
            ->allowEmptyString('total_sacos');

        $validator
            ->scalar('lotes')
            ->maxLength('lotes', 16777215)
            ->allowEmptyString('lotes');

        $validator
            ->scalar('cargolot')
            ->maxLength('cargolot', 16777215)
            ->allowEmptyString('cargolot');

        $validator
            ->scalar('booking')
            ->maxLength('booking', 45)
            ->requirePresence('booking', 'create')
            ->notEmptyString('booking');

        $validator
            ->scalar('proforma')
            ->maxLength('proforma', 100)
            ->requirePresence('proforma', 'create')
            ->notEmptyString('proforma');

        $validator
            ->scalar('travel_num')
            ->maxLength('travel_num', 45)
            ->requirePresence('travel_num', 'create')
            ->notEmptyString('travel_num');

        $validator
            ->scalar('cia_name')
            ->maxLength('cia_name', 100)
            ->requirePresence('cia_name', 'create')
            ->notEmptyString('cia_name');

        $validator
            ->scalar('business_name')
            ->maxLength('business_name', 200)
            ->requirePresence('business_name', 'create')
            ->notEmptyString('business_name');

        $validator
            ->scalar('exportador')
            ->maxLength('exportador', 300)
            ->allowEmptyString('exportador');

        $validator
            ->scalar('expotador_code')
            ->maxLength('expotador_code', 10)
            ->allowEmptyString('expotador_code');

        $validator
            ->nonNegativeInteger('expo_id2')
            ->allowEmptyString('expo_id2');

        $validator
            ->scalar('expo_id1')
            ->maxLength('expo_id1', 4)
            ->allowEmptyString('expo_id1');

        $validator
            ->scalar('agente_naviero')
            ->maxLength('agente_naviero', 100)
            ->requirePresence('agente_naviero', 'create')
            ->notEmptyString('agente_naviero');

        $validator
            ->scalar('packaging_mode')
            ->maxLength('packaging_mode', 45)
            ->requirePresence('packaging_mode', 'create')
            ->notEmptyString('packaging_mode');

        $validator
            ->scalar('packaging_type')
            ->maxLength('packaging_type', 45)
            ->requirePresence('packaging_type', 'create')
            ->notEmptyString('packaging_type');

        $validator
            ->scalar('seal_1')
            ->maxLength('seal_1', 200)
            ->allowEmptyString('seal_1');

        $validator
            ->scalar('seal_2')
            ->maxLength('seal_2', 200)
            ->allowEmptyString('seal_2');

        $validator
            ->dateTime('packaging_date')
            ->allowEmptyDateTime('packaging_date');

        $validator
            ->scalar('iso_ctn')
            ->maxLength('iso_ctn', 45)
            ->allowEmptyString('iso_ctn');

        $validator
            ->numeric('total_cafe_empaque')
            ->allowEmptyString('total_cafe_empaque');

        $validator
            ->allowEmptyString('total_pallets_remesa');

        $validator
            ->allowEmptyString('total_pallets_embalaje');

        $validator
            ->numeric('tare_estibas_descargue')
            ->allowEmptyString('tare_estibas_descargue');

        $validator
            ->numeric('tare_empaque')
            ->allowEmptyString('tare_empaque');

        $validator
            ->scalar('elementos_adicionales2')
            ->maxLength('elementos_adicionales2', 16777215)
            ->allowEmptyString('elementos_adicionales2');

        $validator
            ->scalar('elementos_adicionales')
            ->maxLength('elementos_adicionales', 244)
            ->allowEmptyString('elementos_adicionales');

        $validator
            ->scalar('tipo_papel')
            ->maxLength('tipo_papel', 100)
            ->allowEmptyString('tipo_papel');

        $validator
            ->integer('dry_bags')
            ->allowEmptyString('dry_bags');

        $validator
            ->integer('capas_adiconales_kraft20')
            ->allowEmptyString('capas_adiconales_kraft20');

        $validator
            ->integer('capas_adiconales_kraft40')
            ->allowEmptyString('capas_adiconales_kraft40');

        $validator
            ->integer('capas_adiconales_carton20')
            ->allowEmptyString('capas_adiconales_carton20');

        $validator
            ->integer('capas_adiconales_carton40')
            ->allowEmptyString('capas_adiconales_carton40');

        $validator
            ->integer('instalacion_etiqueta')
            ->allowEmptyString('instalacion_etiqueta');

        $validator
            ->integer('calcomanias')
            ->allowEmptyString('calcomanias');

        $validator
            ->integer('manta_termica')
            ->allowEmptyString('manta_termica');

        $validator
            ->scalar('motorship_name')
            ->maxLength('motorship_name', 200)
            ->requirePresence('motorship_name', 'create')
            ->notEmptyString('motorship_name');

        $validator
            ->scalar('cooperativa')
            ->maxLength('cooperativa', 10)
            ->allowEmptyString('cooperativa');

        $validator
            ->scalar('observation')
            ->allowEmptyString('observation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['traceability_packaging_id'], 'TraceabilityPackagings'));
        $rules->add($rules->existsIn(['navy_agent_id'], 'NavyAgents'));
        $rules->add($rules->existsIn(['shipping_lines_id'], 'ShippingLines'));
        $rules->add($rules->existsIn(['state_packaging_id'], 'StatePackagings'));

        return $rules;
    }
}
