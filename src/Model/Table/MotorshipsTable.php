<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Motorships Model
 *
 * @property \App\Model\Table\ShippingLinesTable&\Cake\ORM\Association\BelongsTo $ShippingLines
 *
 * @method \App\Model\Entity\Motorship newEmptyEntity()
 * @method \App\Model\Entity\Motorship newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Motorship[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Motorship get($primaryKey, $options = [])
 * @method \App\Model\Entity\Motorship findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Motorship patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Motorship[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Motorship|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Motorship saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Motorship[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Motorship[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Motorship[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Motorship[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class MotorshipsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('motorships');
        $this->setDisplayField('id_motorship');
        $this->setPrimaryKey('id_motorship');

        $this->belongsTo('ShippingLines', [
            'foreignKey' => 'shippingline_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id_motorship')
            ->allowEmptyString('id_motorship', null, 'create');

        $validator
            ->scalar('name_motorship')
            ->maxLength('name_motorship', 200)
            ->requirePresence('name_motorship', 'create')
            ->notEmptyString('name_motorship');

        $validator
            ->scalar('description_motorship')
            ->maxLength('description_motorship', 200)
            ->requirePresence('description_motorship', 'create')
            ->notEmptyString('description_motorship');

        $validator
            ->dateTime('reg_date_motorship')
            ->notEmptyDateTime('reg_date_motorship');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['shippingline_id'], 'ShippingLines'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
