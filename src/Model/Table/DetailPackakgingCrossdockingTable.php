<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DetailPackakgingCrossdocking Model
 *
 * @property \App\Model\Table\RemittancesCaffeeTable&\Cake\ORM\Association\BelongsTo $RemittancesCaffee
 * @property \App\Model\Table\PackagingCaffeeTable&\Cake\ORM\Association\BelongsTo $PackagingCaffee
 *
 * @method \App\Model\Entity\DetailPackakgingCrossdocking newEmptyEntity()
 * @method \App\Model\Entity\DetailPackakgingCrossdocking newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking get($primaryKey, $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\DetailPackakgingCrossdocking[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class DetailPackakgingCrossdockingTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('detail_packakging_crossdocking');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('RemittancesCaffee', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->belongsTo('PackagingCaffee', [
            'foreignKey' => 'packaging_caffee_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('lot_coffee')
            ->add('lot_coffee', [
                'length' => [
                    'rule' => ['minLength', 5],
                    'message' => 'El lote cafetero se compone de 5 digitos.',
                ]
            ])
            ->add('lot_coffee', 'validFormat', [
                'rule' => ['custom', '/\d-\d/'],
                'message' => 'Lote no está en formato requerido'])
            ->allowEmptyString('lot_coffee');

        $validator
            ->integer('qta_coffee')
            ->allowEmptyString('qta_coffee');

        $validator
            ->dateTime('reg_date')
            ->notEmptyDateTime('reg_date');

        $validator
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['remittances_caffee_id'], 'RemittancesCaffee'));
        $rules->add($rules->existsIn(['packaging_caffee_id'], 'PackagingCaffee'));

        return $rules;
    }
}
