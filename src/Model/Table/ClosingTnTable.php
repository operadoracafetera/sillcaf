<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClosingTn Model
 *
 * @property \App\Model\Table\ArrivalsMotorshipsTable&\Cake\ORM\Association\BelongsTo $ArrivalsMotorships
 * @property \App\Model\Table\TerminalsTable&\Cake\ORM\Association\BelongsTo $Terminals
 * @property \App\Model\Table\SillcafUsersTable&\Cake\ORM\Association\BelongsTo $SillcafUsers
 *
 * @method \App\Model\Entity\ClosingTn newEmptyEntity()
 * @method \App\Model\Entity\ClosingTn newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ClosingTn[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClosingTn get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClosingTn findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ClosingTn patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClosingTn[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClosingTn|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClosingTn saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClosingTn[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ClosingTn[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ClosingTn[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ClosingTn[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ClosingTnTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('closing_tn');
        $this->setDisplayField('closing_tn_id');
        $this->setPrimaryKey('closing_tn_id');

        $this->belongsTo('ArrivalsMotorships', [
            'foreignKey' => 'arrivals_motorships_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Terminals', [
            'foreignKey' => 'closing_tn_terminal_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SillcafUsers', [
            'foreignKey' => 'closing_tn_user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('closing_tn_id')
            ->allowEmptyString('closing_tn_id', null, 'create');

        $validator
            ->dateTime('closing_date')
            ->requirePresence('closing_date', 'create')
            ->notEmptyDateTime('closing_date');

        $validator
            ->dateTime('closing_tn_reg_date')
            ->notEmptyDateTime('closing_tn_reg_date');

        $validator
            ->dateTime('closing_tn_update_date')
            ->notEmptyDateTime('closing_tn_update_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['arrivals_motorships_id'], 'ArrivalsMotorships'));
        $rules->add($rules->existsIn(['closing_tn_terminal_id'], 'Terminals'));
        $rules->add($rules->existsIn(['closing_tn_user_id'], 'SillcafUsers'));

        return $rules;
    }
}
