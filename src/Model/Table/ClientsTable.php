<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clients Model
 *
 * @property \App\Model\Table\CoffeeSampleTable&\Cake\ORM\Association\HasMany $CoffeeSample
 * @property \App\Model\Table\RemittancesCaffeeTable&\Cake\ORM\Association\HasMany $RemittancesCaffee
 * @property \App\Model\Table\ReturnsCaffeesTable&\Cake\ORM\Association\HasMany $ReturnsCaffees
 *
 * @method \App\Model\Entity\Client newEmptyEntity()
 * @method \App\Model\Entity\Client newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Client[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Client get($primaryKey, $options = [])
 * @method \App\Model\Entity\Client findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Client patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Client[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Client|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Client saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ClientsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('clients');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('CoffeeSample', [
            'foreignKey' => 'client_id',
        ]);
        $this->hasMany('RemittancesCaffee', [
            'foreignKey' => 'client_id',
        ]);
        $this->hasMany('ReturnsCaffees', [
            'foreignKey' => 'client_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('business_name')
            ->maxLength('business_name', 300)
            ->requirePresence('business_name', 'create')
            ->notEmptyString('business_name');

        $validator
            ->scalar('emails')
            ->maxLength('emails', 500)
            ->requirePresence('emails', 'create')
            ->notEmptyString('emails');

        $validator
            ->scalar('contact_name')
            ->maxLength('contact_name', 500)
            ->allowEmptyString('contact_name');

        $validator
            ->scalar('exporter_code')
            ->maxLength('exporter_code', 10)
            ->allowEmptyString('exporter_code');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 100)
            ->requirePresence('phone', 'create')
            ->notEmptyString('phone');

        $validator
            ->scalar('city_location')
            ->maxLength('city_location', 45)
            ->requirePresence('city_location', 'create')
            ->notEmptyString('city_location');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmptyDateTime('created_date');

        $validator
            ->dateTime('updated_date')
            ->requirePresence('updated_date', 'create')
            ->notEmptyDateTime('updated_date');

        $validator
            ->integer('price_download_bag')
            ->allowEmptyString('price_download_bag');

        $validator
            ->boolean('notify_email')
            ->allowEmptyString('notify_email');

        $validator
            ->scalar('nit')
            ->maxLength('nit', 200)
            ->allowEmptyString('nit');

        $validator
            ->integer('price_bag_stm')
            ->allowEmptyString('price_bag_stm');

        return $validator;
    }
}
