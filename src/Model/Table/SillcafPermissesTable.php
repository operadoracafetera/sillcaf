<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SillcafPermisses Model
 *
 * @property \App\Model\Table\SillcafModulesTable&\Cake\ORM\Association\BelongsTo $SillcafModules
 *
 * @method \App\Model\Entity\SillcafPermiss newEmptyEntity()
 * @method \App\Model\Entity\SillcafPermiss newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPermiss[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPermiss get($primaryKey, $options = [])
 * @method \App\Model\Entity\SillcafPermiss findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\SillcafPermiss patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPermiss[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPermiss|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafPermiss saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafPermiss[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafPermiss[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafPermiss[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafPermiss[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SillcafPermissesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sillcaf_permisses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('SillcafModules', [
            'foreignKey' => 'modules_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('permisse')
            ->maxLength('permisse', 250)
            ->allowEmptyString('permisse');

        $validator
            ->dateTime('reg_date')
            ->notEmptyDateTime('reg_date');

        $validator
            ->requirePresence('active', 'create')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['modules_id'], 'SillcafModules'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
