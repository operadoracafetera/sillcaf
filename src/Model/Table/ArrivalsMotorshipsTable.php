<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArrivalsMotorships Model
 *
 * @property \App\Model\Table\MotorshipsTable&\Cake\ORM\Association\BelongsTo $Motorships
 * @property \App\Model\Table\TerminalsTable&\Cake\ORM\Association\BelongsTo $Terminals
 *
 * @method \App\Model\Entity\ArrivalsMotorship newEmptyEntity()
 * @method \App\Model\Entity\ArrivalsMotorship newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ArrivalsMotorship[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ArrivalsMotorshipsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('arrivals_motorships');
        $this->setDisplayField('arrivals_motorships_id');
        $this->setPrimaryKey('arrivals_motorships_id');

        $this->belongsTo('Motorships', [
            'foreignKey' => 'arrivals_motorships_motorships_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Terminals', [
            'foreignKey' => 'arrivals_motorships_terminal_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SillcafUsers', [
            'foreignKey' => 'arrivals_motorships_user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('arrivals_motorships_id')
            ->allowEmptyString('arrivals_motorships_id', null, 'create');

        $validator
            ->scalar('visit_number')
            ->maxLength('visit_number', 45)
            ->requirePresence('visit_number', 'create')
            ->notEmptyString('visit_number')
            ->add('visit_number', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('vissel')
            ->maxLength('vissel', 20)
            ->requirePresence('vissel', 'create')
            ->notEmptyString('vissel')
            ->add('vissel', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->dateTime('arrival_date')
            ->requirePresence('arrival_date', 'create')
            ->notEmptyDateTime('arrival_date');

        $validator
            ->dateTime('arrivals_motorships_reg_date')
            ->notEmptyDateTime('arrivals_motorships_reg_date');

        $validator
            ->dateTime('arrivals_motorships_update_date')
            ->notEmptyDateTime('arrivals_motorships_update_date');

        $validator
            ->notEmptyString('arrivals_motorships_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['visit_number']));
        $rules->add($rules->isUnique(['vissel']));
        $rules->add($rules->existsIn(['arrivals_motorships_motorships_id'], 'Motorships'));
        $rules->add($rules->existsIn(['arrivals_motorships_terminal_id'], 'Terminals'));
        $rules->add($rules->existsIn(['arrivals_motorships_user_id'], 'SillcafUsers'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
