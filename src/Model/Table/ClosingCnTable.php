<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ClosingCn Model
 *
 * @property \App\Model\Table\NavyAgentTable&\Cake\ORM\Association\BelongsTo $NavyAgent
 * @property \App\Model\Table\ArrivalsMotorshipsTable&\Cake\ORM\Association\BelongsTo $ArrivalsMotorships
 * @property \App\Model\Table\SillcafUsersTable&\Cake\ORM\Association\BelongsTo $SillcafUsers
 *
 * @method \App\Model\Entity\ClosingCn newEmptyEntity()
 * @method \App\Model\Entity\ClosingCn newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\ClosingCn[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ClosingCn get($primaryKey, $options = [])
 * @method \App\Model\Entity\ClosingCn findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\ClosingCn patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ClosingCn[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\ClosingCn|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClosingCn saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ClosingCn[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ClosingCn[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\ClosingCn[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\ClosingCn[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class ClosingCnTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('closing_cn');
        $this->setDisplayField('closing_cn_id');
        $this->setPrimaryKey('closing_cn_id');

        $this->belongsTo('NavyAgent', [
            'foreignKey' => 'closing_cn_navy_agent_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ArrivalsMotorships', [
            'foreignKey' => 'arrivals_motorships_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SillcafUsers', [
            'foreignKey' => 'closing_cn_user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('closing_cn_id')
            ->allowEmptyString('closing_cn_id', null, 'create');

        $validator
            ->dateTime('closing_date')
            ->requirePresence('closing_date', 'create')
            ->notEmptyDateTime('closing_date');

        $validator
            ->dateTime('closing_cn_reg_date')
            ->notEmptyDateTime('closing_cn_reg_date');

        $validator
            ->dateTime('closing_cn_update_date')
            ->notEmptyDateTime('closing_cn_update_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['closing_cn_navy_agent_id'], 'NavyAgent'));
        $rules->add($rules->existsIn(['arrivals_motorships_id'], 'ArrivalsMotorships'));
        $rules->add($rules->existsIn(['closing_cn_user_id'], 'SillcafUsers'));

        return $rules;
    }

    public function isOwnedBy($articleId, $userId)
    {
        return $this->exists(['closing_cn_id is' => $articleId, 'closing_cn_navy_agent_id is' => $userId]);
    }
}
