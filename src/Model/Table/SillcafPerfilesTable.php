<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SillcafPerfiles Model
 *
 * @method \App\Model\Entity\SillcafPerfile newEmptyEntity()
 * @method \App\Model\Entity\SillcafPerfile newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPerfile[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPerfile get($primaryKey, $options = [])
 * @method \App\Model\Entity\SillcafPerfile findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\SillcafPerfile patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPerfile[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafPerfile|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafPerfile saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafPerfile[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafPerfile[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafPerfile[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafPerfile[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SillcafPerfilesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sillcaf_perfiles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('perfiles_name')
            ->maxLength('perfiles_name', 45)
            ->allowEmptyFile('perfiles_name');

        $validator
            ->scalar('perfiles_description')
            ->maxLength('perfiles_description', 45)
            ->requirePresence('perfiles_description', 'create')
            ->notEmptyFile('perfiles_description');

        $validator
            ->dateTime('perfiles_reg_date')
            ->notEmptyDateTime('perfiles_reg_date');

        $validator
            ->boolean('perfiles_active')
            ->notEmptyFile('perfiles_active');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
