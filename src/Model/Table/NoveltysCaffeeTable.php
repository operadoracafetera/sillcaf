<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NoveltysCaffee Model
 *
 * @property \App\Model\Table\RemittancesCaffeeHasNoveltysCaffeeTable&\Cake\ORM\Association\HasMany $RemittancesCaffeeHasNoveltysCaffee
 *
 * @method \App\Model\Entity\NoveltysCaffee newEmptyEntity()
 * @method \App\Model\Entity\NoveltysCaffee newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\NoveltysCaffee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NoveltysCaffee get($primaryKey, $options = [])
 * @method \App\Model\Entity\NoveltysCaffee findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\NoveltysCaffee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NoveltysCaffee[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\NoveltysCaffee|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NoveltysCaffee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NoveltysCaffee[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\NoveltysCaffee[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\NoveltysCaffee[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\NoveltysCaffee[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class NoveltysCaffeeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('noveltys_caffee');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('RemittancesCaffeeHasNoveltysCaffee', [
            'foreignKey' => 'noveltys_caffee_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 200)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('description')
            ->maxLength('description', 100)
            ->allowEmptyString('description');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmptyDateTime('created_date');

        $validator
            ->dateTime('updated_date')
            ->requirePresence('updated_date', 'create')
            ->notEmptyDateTime('updated_date');

        return $validator;
    }
}
