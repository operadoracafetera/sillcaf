<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Customs Model
 *
 * @property \App\Model\Table\RemittancesCaffeeTable&\Cake\ORM\Association\HasMany $RemittancesCaffee
 *
 * @method \App\Model\Entity\Custom newEmptyEntity()
 * @method \App\Model\Entity\Custom newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Custom[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Custom get($primaryKey, $options = [])
 * @method \App\Model\Entity\Custom findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Custom patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Custom[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Custom|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Custom saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Custom[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Custom[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Custom[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Custom[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class CustomsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('customs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        /*$this->hasMany('RemittancesCaffee', [
            'foreignKey' => 'custom_id',
        ]);*/
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('cia_name')
            ->maxLength('cia_name', 100)
            ->requirePresence('cia_name', 'create')
            ->notEmptyString('cia_name');

        $validator
            ->scalar('description')
            ->maxLength('description', 45)
            ->allowEmptyString('description');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmptyDateTime('created_date');

        $validator
            ->dateTime('updated_date')
            ->requirePresence('updated_date', 'create')
            ->notEmptyDateTime('updated_date');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
