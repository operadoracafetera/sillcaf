<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RemittancesCaffee Model
 *
 * @property \App\Model\Table\GuidesTable&\Cake\ORM\Association\BelongsTo $Guides
 * @property \App\Model\Table\ClientsTable&\Cake\ORM\Association\BelongsTo $Clients
 * @property \App\Model\Table\UnitsCaffeeTable&\Cake\ORM\Association\BelongsTo $UnitsCaffee
 * @property \App\Model\Table\PackingCaffeeTable&\Cake\ORM\Association\BelongsTo $PackingCaffee
 * @property \App\Model\Table\MarkCaffeeTable&\Cake\ORM\Association\BelongsTo $MarkCaffee
 * @property \App\Model\Table\CustomsTable&\Cake\ORM\Association\BelongsTo $Customs
 * @property \App\Model\Table\CitySourceTable&\Cake\ORM\Association\BelongsTo $CitySource
 * @property \App\Model\Table\ShippersTable&\Cake\ORM\Association\BelongsTo $Shippers
 * @property \App\Model\Table\SlotStoreTable&\Cake\ORM\Association\BelongsTo $SlotStore
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\StateOperationTable&\Cake\ORM\Association\BelongsTo $StateOperation
 * @property \App\Model\Table\NoveltyInCaffeeTable&\Cake\ORM\Association\BelongsTo $NoveltyInCaffee
 * @property \App\Model\Table\TraceabilityDownloadTable&\Cake\ORM\Association\BelongsTo $TraceabilityDownload
 * @property \App\Model\Table\TypeUnitsTable&\Cake\ORM\Association\BelongsTo $TypeUnits
 * @property \App\Model\Table\CargolotsTable&\Cake\ORM\Association\BelongsTo $Cargolots
 * @property \App\Model\Table\BillTable&\Cake\ORM\Association\BelongsTo $Bill
 * @property \App\Model\Table\DetailPackagingCaffeeTable&\Cake\ORM\Association\HasMany $DetailPackagingCaffee
 * @property \App\Model\Table\DetailPackakgingCrossdockingTable&\Cake\ORM\Association\HasMany $DetailPackakgingCrossdocking
 * @property \App\Model\Table\DetailsServicesToCaffeeTable&\Cake\ORM\Association\HasMany $DetailsServicesToCaffee
 * @property \App\Model\Table\RemittancesCaffeeHasFumigationServicesTable&\Cake\ORM\Association\HasMany $RemittancesCaffeeHasFumigationServices
 * @property \App\Model\Table\RemittancesCaffeeHasNoveltysCaffeeTable&\Cake\ORM\Association\HasMany $RemittancesCaffeeHasNoveltysCaffee
 * @property \App\Model\Table\SampleRemmitancesTable&\Cake\ORM\Association\HasMany $SampleRemmitances
 * @property \App\Model\Table\WeighingDownloadCaffeeTable&\Cake\ORM\Association\HasMany $WeighingDownloadCaffee
 * @property \App\Model\Table\WeighingEmptyCoffeeTable&\Cake\ORM\Association\HasMany $WeighingEmptyCoffee
 * @property \App\Model\Table\WeighingReturnCoffeeTable&\Cake\ORM\Association\HasMany $WeighingReturnCoffee
 * @property \App\Model\Table\ReturnsCaffeesTable&\Cake\ORM\Association\BelongsToMany $ReturnsCaffees
 *
 * @method \App\Model\Entity\RemittancesCaffee newEmptyEntity()
 * @method \App\Model\Entity\RemittancesCaffee newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RemittancesCaffee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RemittancesCaffee get($primaryKey, $options = [])
 * @method \App\Model\Entity\RemittancesCaffee findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RemittancesCaffee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RemittancesCaffee[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RemittancesCaffee|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RemittancesCaffee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RemittancesCaffee[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RemittancesCaffee[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RemittancesCaffee[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RemittancesCaffee[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class RemittancesCaffeeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('remittances_caffee');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Guides', [
            'foreignKey' => 'guide_id',
        ]);
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('UnitsCaffee', [
            'foreignKey' => 'units_cafee_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('PackingCaffee', [
            'foreignKey' => 'packing_cafee_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('MarkCaffee', [
            'foreignKey' => 'mark_cafee_id',
        ]);
        $this->belongsTo('Customs', [
            'foreignKey' => 'custom_id',
        ]);
        $this->belongsTo('CitySource', [
            'foreignKey' => 'city_source_id',
        ]);
        $this->belongsTo('Shippers', [
            'foreignKey' => 'shippers_id',
        ]);
        $this->belongsTo('SlotStore', [
            'foreignKey' => 'slot_store_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'staff_sample_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'staff_wt_in_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'staff_wt_out_id',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'staff_driver_id',
        ]);
        $this->belongsTo('StateOperation', [
            'foreignKey' => 'state_operation_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('NoveltyInCaffee', [
            'foreignKey' => 'novelty_in_caffee_id',
        ]);
        $this->belongsTo('TraceabilityDownload', [
            'foreignKey' => 'traceability_download_id',
        ]);
        $this->belongsTo('TypeUnits', [
            'foreignKey' => 'type_units_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Cargolots', [
            'foreignKey' => 'cargolot_id',
        ]);
        $this->belongsTo('Bill', [
            'foreignKey' => 'bill_id',
        ]);
        $this->hasMany('DetailPackagingCaffee', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->hasMany('DetailPackakgingCrossdocking', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->hasMany('DetailsServicesToCaffee', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->hasMany('RemittancesCaffeeHasFumigationServices', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->hasMany('RemittancesCaffeeHasNoveltysCaffee', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->hasMany('SampleRemmitances', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->hasMany('WeighingDownloadCaffee', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->hasMany('WeighingEmptyCoffee', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->hasMany('WeighingReturnCoffee', [
            'foreignKey' => 'remittances_caffee_id',
        ]);
        $this->belongsToMany('ReturnsCaffees', [
            'foreignKey' => 'remittances_caffee_id',
            'targetForeignKey' => 'returns_caffee_id',
            'joinTable' => 'remittances_caffee_returns_caffees',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('lot_caffee')
            ->maxLength('lot_caffee', 10)
            ->requirePresence('lot_caffee', 'create')
            ->notEmptyString('lot_caffee');

        $validator
            ->integer('quantity_bag_in_store')
            ->allowEmptyString('quantity_bag_in_store');

        $validator
            ->integer('quantity_bag_out_store')
            ->allowEmptyString('quantity_bag_out_store');

        $validator
            ->nonNegativeInteger('quantity_in_pallet_caffee')
            ->allowEmptyString('quantity_in_pallet_caffee');

        $validator
            ->nonNegativeInteger('quantity_out_pallet_caffee')
            ->allowEmptyString('quantity_out_pallet_caffee');

        $validator
            ->integer('quantity_radicated_bag_in')
            ->allowEmptyString('quantity_radicated_bag_in');

        $validator
            ->integer('quantity_radicated_bag_out')
            ->allowEmptyString('quantity_radicated_bag_out');

        $validator
            ->numeric('total_weight_net_nominal')
            ->allowEmptyString('total_weight_net_nominal');

        $validator
            ->numeric('total_weight_net_real')
            ->allowEmptyString('total_weight_net_real');

        $validator
            ->numeric('tare_download')
            ->allowEmptyString('tare_download');

        $validator
            ->numeric('tare_packaging')
            ->allowEmptyString('tare_packaging');

        $validator
            ->nonNegativeInteger('source_location')
            ->allowEmptyString('source_location');

        $validator
            ->scalar('nota_entrega')
            ->maxLength('nota_entrega', 100)
            ->allowEmptyString('nota_entrega');

        $validator
            ->scalar('vehicle_plate')
            ->maxLength('vehicle_plate', 10)
            ->allowEmptyString('vehicle_plate');

        $validator
            ->dateTime('download_caffee_date')
            ->allowEmptyDateTime('download_caffee_date');

        $validator
            ->dateTime('packaging_caffee_date')
            ->allowEmptyDateTime('packaging_caffee_date');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmptyDateTime('created_date');

        $validator
            ->dateTime('updated_dated')
            ->allowEmptyDateTime('updated_dated');

        $validator
            ->boolean('is_active')
            ->notEmptyString('is_active');

        $validator
            ->scalar('observation')
            ->allowEmptyString('observation');

        $validator
            ->scalar('details_weight')
            ->allowEmptyString('details_weight');

        $validator
            ->integer('total_tare')
            ->allowEmptyString('total_tare');

        $validator
            ->numeric('weight_pallet_packaging_total')
            ->allowEmptyString('weight_pallet_packaging_total');

        $validator
            ->integer('total_rad_bag_out')
            ->allowEmptyString('total_rad_bag_out');

        $validator
            ->scalar('ref_packaging')
            ->maxLength('ref_packaging', 100)
            ->allowEmptyString('ref_packaging');

        $validator
            ->scalar('seals')
            ->maxLength('seals', 200)
            ->allowEmptyString('seals');

        $validator
            ->nonNegativeInteger('user_register')
            ->requirePresence('user_register', 'create')
            ->notEmptyString('user_register');

        $validator
            ->scalar('jetty')
            ->maxLength('jetty', 100)
            ->requirePresence('jetty', 'create')
            ->notEmptyString('jetty');

        $validator
            ->scalar('ref_driver')
            ->maxLength('ref_driver', 100)
            ->allowEmptyString('ref_driver');

        $validator
            ->scalar('microlot')
            ->maxLength('microlot', 1)
            ->allowEmptyString('microlot');

        $validator
            ->scalar('type_packaging_coffee')
            ->maxLength('type_packaging_coffee', 1)
            ->allowEmptyString('type_packaging_coffee');

        $validator
            ->scalar('coffee_to_fumigation')
            ->maxLength('coffee_to_fumigation', 1)
            ->allowEmptyString('coffee_to_fumigation');

        $validator
            ->scalar('material')
            ->maxLength('material', 100)
            ->allowEmptyString('material');

        $validator
            ->integer('valor_descargue')
            ->allowEmptyString('valor_descargue');

        $validator
            ->date('date_expedition_guide')
            ->allowEmptyDate('date_expedition_guide');

        $validator
            ->scalar('ref_copcsa_gang')
            ->maxLength('ref_copcsa_gang', 1)
            ->allowEmptyString('ref_copcsa_gang');

        $validator
            ->scalar('document_number_sapmigo_fnc')
            ->maxLength('document_number_sapmigo_fnc', 100)
            ->allowEmptyString('document_number_sapmigo_fnc');

        $validator
            ->scalar('sync_msg_sapmigo_fnc')
            ->allowEmptyString('sync_msg_sapmigo_fnc');

        $validator
            ->dateTime('sync_date_sapmigo_fnc')
            ->allowEmptyDateTime('sync_date_sapmigo_fnc');

        $validator
            ->scalar('sync_data_sapmigo_fnc')
            ->allowEmptyString('sync_data_sapmigo_fnc');

        $validator
            ->scalar('msg_spb_navis')
            ->allowEmptyString('msg_spb_navis');

        $validator
            ->boolean('rx_status_spb_navis')
            ->allowEmptyString('rx_status_spb_navis');

        $validator
            ->dateTime('rx_date_spb_navis')
            ->allowEmptyDateTime('rx_date_spb_navis');

        $validator
            ->boolean('coffee_with_packging_anormal')
            ->notEmptyString('coffee_with_packging_anormal');

        $validator
            ->numeric('tare_packing_coffee_anormal')
            ->notEmptyString('tare_packing_coffee_anormal');

        $validator
            ->scalar('cooperativa_cta')
            ->maxLength('cooperativa_cta', 200)
            ->allowEmptyString('cooperativa_cta');

        $validator
            ->time('ini_ctg_download')
            ->allowEmptyTime('ini_ctg_download');

        $validator
            ->time('end_ctg_download')
            ->allowEmptyTime('end_ctg_download');

        $validator
            ->scalar('machine_download_ctg')
            ->maxLength('machine_download_ctg', 10)
            ->allowEmptyString('machine_download_ctg');

        $validator
            ->scalar('plague_control_ctg')
            ->maxLength('plague_control_ctg', 2)
            ->allowEmptyString('plague_control_ctg');

        $validator
            ->scalar('sample_req_ctg')
            ->maxLength('sample_req_ctg', 2)
            ->allowEmptyString('sample_req_ctg');

        $validator
            ->integer('ref_bulk_upload')
            ->allowEmptyString('ref_bulk_upload');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['guide_id'], 'Guides'));
        $rules->add($rules->existsIn(['client_id'], 'Clients'));
        $rules->add($rules->existsIn(['units_cafee_id'], 'UnitsCaffee'));
        $rules->add($rules->existsIn(['packing_cafee_id'], 'PackingCaffee'));
        $rules->add($rules->existsIn(['mark_cafee_id'], 'MarkCaffee'));
        $rules->add($rules->existsIn(['custom_id'], 'Customs'));
        $rules->add($rules->existsIn(['city_source_id'], 'CitySource'));
        $rules->add($rules->existsIn(['shippers_id'], 'Shippers'));
        $rules->add($rules->existsIn(['slot_store_id'], 'SlotStore'));
        $rules->add($rules->existsIn(['staff_sample_id'], 'Users'));
        $rules->add($rules->existsIn(['staff_wt_in_id'], 'Users'));
        $rules->add($rules->existsIn(['staff_wt_out_id'], 'Users'));
        $rules->add($rules->existsIn(['staff_driver_id'], 'Users'));
        $rules->add($rules->existsIn(['state_operation_id'], 'StateOperation'));
        $rules->add($rules->existsIn(['novelty_in_caffee_id'], 'NoveltyInCaffee'));
        $rules->add($rules->existsIn(['traceability_download_id'], 'TraceabilityDownload'));
        $rules->add($rules->existsIn(['type_units_id'], 'TypeUnits'));
        $rules->add($rules->existsIn(['cargolot_id'], 'Cargolots'));
        $rules->add($rules->existsIn(['bill_id'], 'Bill'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
