<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RemittancesCaffeeHasNoveltysCaffee Model
 *
 * @property \App\Model\Table\RemittancesCaffeeTable&\Cake\ORM\Association\BelongsTo $RemittancesCaffee
 * @property \App\Model\Table\NoveltysCaffeeTable&\Cake\ORM\Association\BelongsTo $NoveltysCaffee
 *
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee newEmptyEntity()
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee get($primaryKey, $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class RemittancesCaffeeHasNoveltysCaffeeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('remittances_caffee_has_noveltys_caffee');
        $this->setDisplayField('remittances_caffee_id');
        $this->setPrimaryKey(['remittances_caffee_id', 'noveltys_caffee_id']);

        $this->belongsTo('RemittancesCaffee', [
            'foreignKey' => 'remittances_caffee_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('NoveltysCaffee', [
            'foreignKey' => 'noveltys_caffee_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmptyDateTime('created_date');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['remittances_caffee_id'], 'RemittancesCaffee'));
        $rules->add($rules->existsIn(['noveltys_caffee_id'], 'NoveltysCaffee'));

        return $rules;
    }
}
