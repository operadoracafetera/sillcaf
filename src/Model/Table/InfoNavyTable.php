<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InfoNavy Model
 *
 * @property \App\Model\Table\NavyAgentTable&\Cake\ORM\Association\BelongsTo $NavyAgent
 * @property \App\Model\Table\ShippingLinesTable&\Cake\ORM\Association\BelongsTo $ShippingLines
 * @property \App\Model\Table\CustomsTable&\Cake\ORM\Association\BelongsTo $Customs
 * @property \App\Model\Table\ArrivalsMotorshipsTable&\Cake\ORM\Association\BelongsTo $ArrivalsMotorships
 * @property \App\Model\Table\StatusInfoNavyTable&\Cake\ORM\Association\BelongsTo $StatusInfoNavy
 * @property \App\Model\Table\SillcafUsersTable&\Cake\ORM\Association\BelongsTo $SillcafUsers
 * @property \App\Model\Table\SillcafUsersTable&\Cake\ORM\Association\BelongsTo $SillcafUsers
 * @property \App\Model\Table\AdictionalElementsHasPackagingCaffeeTable&\Cake\ORM\Association\HasMany $AdictionalElementsHasPackagingCaffee
 * @property \App\Model\Table\PackagingCaffeeTable&\Cake\ORM\Association\HasMany $PackagingCaffee
 *
 * @method \App\Model\Entity\InfoNavy newEmptyEntity()
 * @method \App\Model\Entity\InfoNavy newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\InfoNavy[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InfoNavy get($primaryKey, $options = [])
 * @method \App\Model\Entity\InfoNavy findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\InfoNavy patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InfoNavy[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\InfoNavy|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InfoNavy saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InfoNavy[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\InfoNavy[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\InfoNavy[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\InfoNavy[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class InfoNavyTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('info_navy');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('NavyAgent', [
            'foreignKey' => 'navy_agent_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ShippingLines', [
            'foreignKey' => 'shipping_lines_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Customs', [
            'foreignKey' => 'customs_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ArrivalsMotorships', [
            'foreignKey' => 'arrivals_motorships_id',
        ]);
        $this->belongsTo('StatusInfoNavy', [
            'foreignKey' => 'status_info_navy_id',
        ]);
        $this->belongsTo('SillcafUsers', [
            'foreignKey' => 'sillcaf_user_reg_id',
        ]);
        $this->belongsTo('SillcafUsers', [
            'foreignKey' => 'sillcaf_user_approve_id',
        ]);
        $this->hasMany('AdictionalElementsHasPackagingCaffee', [
            'foreignKey' => 'info_navy_id',
        ]);
        $this->hasMany('PackagingCaffee', [
            'foreignKey' => 'info_navy_id',
        ]);
        $this->hasMany('SillcafInfonavyCustom', [
            'foreignKey' => 'info_navy_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('proforma')
            ->maxLength('proforma', 100)
            ->requirePresence('proforma', 'create')
            ->notEmptyString('proforma');

        $validator
            ->scalar('packaging_type')
            ->maxLength('packaging_type', 45)
            ->requirePresence('packaging_type', 'create')
            ->notEmptyString('packaging_type');

        $validator
            ->scalar('motorship_name')
            ->maxLength('motorship_name', 200)
            ->requirePresence('motorship_name', 'create')
            ->notEmptyString('motorship_name');

        $validator
            ->scalar('packaging_mode')
            ->maxLength('packaging_mode', 45)
            ->requirePresence('packaging_mode', 'create')
            ->notEmptyString('packaging_mode');

        $validator
            ->scalar('booking')
            ->maxLength('booking', 45)
            ->requirePresence('booking', 'create')
            ->notEmptyString('booking');

        $validator
            ->scalar('travel_num')
            ->maxLength('travel_num', 45)
            ->requirePresence('travel_num', 'create')
            ->notEmptyString('travel_num');

        $validator
            ->scalar('destiny')
            ->maxLength('destiny', 100)
            ->requirePresence('destiny', 'create')
            ->notEmptyString('destiny');

        $validator
            ->scalar('observation')
            ->maxLength('observation', 300)
            ->allowEmptyString('observation');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmptyDateTime('created_date');

        $validator
            ->scalar('elements_adicional')
            ->allowEmptyString('elements_adicional');

        $validator
            ->scalar('long_container')
            ->maxLength('long_container', 5)
            ->allowEmptyString('long_container');

        $validator
            ->scalar('type_papper')
            ->maxLength('type_papper', 100)
            ->allowEmptyString('type_papper');

        $validator
            ->integer('adictional_dry_bags')
            ->allowEmptyString('adictional_dry_bags');

        $validator
            ->integer('adictional_layer_kraft_20')
            ->allowEmptyString('adictional_layer_kraft_20');

        $validator
            ->integer('adictional_layer_kraft_40')
            ->allowEmptyString('adictional_layer_kraft_40');

        $validator
            ->integer('adictional_layer_carton_20')
            ->allowEmptyString('adictional_layer_carton_20');

        $validator
            ->integer('adictional_layer_carton_40')
            ->allowEmptyString('adictional_layer_carton_40');

        $validator
            ->integer('adictional_num_label')
            ->allowEmptyString('adictional_num_label');

        $validator
            ->integer('adictional_num_stiker')
            ->allowEmptyString('adictional_num_stiker');

        $validator
            ->integer('adictional_num_manta')
            ->allowEmptyString('adictional_num_manta');

        $validator
            ->integer('tsl')
            ->allowEmptyString('tsl');

        $validator
            ->integer('rejillas')
            ->allowEmptyString('rejillas');

        $validator
            ->integer('dataloger')
            ->allowEmptyString('dataloger');

        $validator
            ->integer('absortes')
            ->allowEmptyString('absortes');

        $validator
            ->integer('pallets')
            ->allowEmptyString('pallets');

        $validator
            ->integer('hc_qta')
            ->allowEmptyString('hc_qta');

        $validator
            ->integer('tls_desiccant')
            ->allowEmptyString('tls_desiccant');

        $validator
            ->integer('seals')
            ->allowEmptyString('seals');

        $validator
            ->boolean('info_navy_active')
            ->allowEmptyString('info_navy_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['navy_agent_id'], 'NavyAgent'));
        $rules->add($rules->existsIn(['shipping_lines_id'], 'ShippingLines'));
        $rules->add($rules->existsIn(['customs_id'], 'Customs'));
        $rules->add($rules->existsIn(['arrivals_motorships_id'], 'ArrivalsMotorships'));
        $rules->add($rules->existsIn(['status_info_navy_id'], 'StatusInfoNavy'));
        $rules->add($rules->existsIn(['sillcaf_user_reg_id'], 'SillcafUsers'));
        $rules->add($rules->existsIn(['sillcaf_user_approve_id'], 'SillcafUsers'));

        return $rules;
    }

    public function isOwnedBy($articleId, $userId)
    {
        return $this->exists(['id is' => $articleId, 'customs_id is' => $userId]);
    }
}
