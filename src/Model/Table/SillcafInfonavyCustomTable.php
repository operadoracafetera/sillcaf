<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SillcafInfonavyCustom Model
 *
 * @property \App\Model\Table\InfoNavyTable&\Cake\ORM\Association\BelongsTo $InfoNavy
 * @property \App\Model\Table\CustomsTable&\Cake\ORM\Association\BelongsTo $Customs
 *
 * @method \App\Model\Entity\SillcafInfonavyCustom newEmptyEntity()
 * @method \App\Model\Entity\SillcafInfonavyCustom newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom get($primaryKey, $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafInfonavyCustom[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SillcafInfonavyCustomTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sillcaf_infonavy_custom');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('InfoNavy', [
            'foreignKey' => 'info_navy_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Customs', [
            'foreignKey' => 'custom_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['info_navy_id'], 'InfoNavy'));
        $rules->add($rules->existsIn(['custom_id'], 'Customs'));

        return $rules;
    }
}
