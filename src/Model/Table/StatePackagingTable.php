<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StatePackaging Model
 *
 * @property \App\Model\Table\PackagingCaffeeTable&\Cake\ORM\Association\HasMany $PackagingCaffee
 *
 * @method \App\Model\Entity\StatePackaging newEmptyEntity()
 * @method \App\Model\Entity\StatePackaging newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\StatePackaging[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StatePackaging get($primaryKey, $options = [])
 * @method \App\Model\Entity\StatePackaging findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\StatePackaging patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StatePackaging[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\StatePackaging|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StatePackaging saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StatePackaging[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StatePackaging[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\StatePackaging[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\StatePackaging[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class StatePackagingTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('state_packaging');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('PackagingCaffee', [
            'foreignKey' => 'state_packaging_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->dateTime('create_date')
            ->requirePresence('create_date', 'create')
            ->notEmptyDateTime('create_date');

        $validator
            ->dateTime('update_date')
            ->requirePresence('update_date', 'create')
            ->notEmptyDateTime('update_date');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
