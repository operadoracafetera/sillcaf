<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Terminals Model
 *
 * @method \App\Model\Entity\Terminal newEmptyEntity()
 * @method \App\Model\Entity\Terminal newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Terminal[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Terminal get($primaryKey, $options = [])
 * @method \App\Model\Entity\Terminal findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Terminal patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Terminal[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Terminal|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Terminal saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Terminal[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Terminal[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Terminal[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Terminal[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class TerminalsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('terminals');
        $this->setDisplayField('id_terminals');
        $this->setPrimaryKey('id_terminals');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id_terminals')
            ->allowEmptyString('id_terminals', null, 'create');

        $validator
            ->scalar('name_terminals')
            ->maxLength('name_terminals', 200)
            ->requirePresence('name_terminals', 'create')
            ->notEmptyString('name_terminals');

        $validator
            ->scalar('description_terminals')
            ->maxLength('description_terminals', 200)
            ->requirePresence('description_terminals', 'create')
            ->notEmptyString('description_terminals');

        $validator
            ->dateTime('reg_date_terminals')
            ->notEmptyDateTime('reg_date_terminals');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
