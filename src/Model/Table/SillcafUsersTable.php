<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SillcafUsers Model
 *
 * @property \App\Model\Table\SillcafPerfilesTable&\Cake\ORM\Association\BelongsTo $SillcafPerfiles
 *
 * @method \App\Model\Entity\SillcafUser newEmptyEntity()
 * @method \App\Model\Entity\SillcafUser newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SillcafUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\SillcafUser findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\SillcafUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafUser[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\SillcafUser|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafUser saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SillcafUser[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafUser[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafUser[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\SillcafUser[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class SillcafUsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sillcaf_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('SillcafPerfiles', [
            'foreignKey' => 'perfiles_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 45)
            ->allowEmptyString('username');

        $validator
            ->scalar('password')
            ->maxLength('password', 200)
            ->allowEmptyString('password');

        $validator
            ->dateTime('reg_date')
            ->notEmptyDateTime('reg_date');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 200)
            ->allowEmptyString('last_name');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 200)
            ->allowEmptyString('first_name');

        $validator
            ->scalar('email')
            ->maxLength('email', 200)
            ->allowEmptyString('email');
            
        $validator
            ->boolean('active_user')
            ->allowEmptyString('active_user');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->existsIn(['perfiles_id'], 'SillcafPerfiles'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
