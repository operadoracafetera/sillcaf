<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InfoNavyMessage Model
 *
 * @property \App\Model\Table\InfoNavyTable&\Cake\ORM\Association\BelongsTo $InfoNavy
 * @property \App\Model\Table\SillcafUsersTable&\Cake\ORM\Association\BelongsTo $SillcafUsers
 *
 * @method \App\Model\Entity\InfoNavyMessage newEmptyEntity()
 * @method \App\Model\Entity\InfoNavyMessage newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\InfoNavyMessage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InfoNavyMessage get($primaryKey, $options = [])
 * @method \App\Model\Entity\InfoNavyMessage findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\InfoNavyMessage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InfoNavyMessage[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\InfoNavyMessage|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InfoNavyMessage saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InfoNavyMessage[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\InfoNavyMessage[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\InfoNavyMessage[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\InfoNavyMessage[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class InfoNavyMessageTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('info_navy_message');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('InfoNavy', [
            'foreignKey' => 'info_navy_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SillcafUsers', [
            'foreignKey' => 'sillcaf_user_reg_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('message')
            ->requirePresence('message', 'create')
            ->notEmptyString('message');

        $validator
            ->dateTime('reg_date')
            ->notEmptyDateTime('reg_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['info_navy_id'], 'InfoNavy'));
        $rules->add($rules->existsIn(['sillcaf_user_reg_id'], 'SillcafUsers'));

        return $rules;
    }
}
