<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PackagingCaffee Model
 *
 * @property \App\Model\Table\StatePackagingTable&\Cake\ORM\Association\BelongsTo $StatePackaging
 * @property \App\Model\Table\ReadyContainerTable&\Cake\ORM\Association\BelongsTo $ReadyContainer
 * @property \App\Model\Table\CauseEmptiedCtnTable&\Cake\ORM\Association\BelongsTo $CauseEmptiedCtn
 * @property \App\Model\Table\InfoNavyTable&\Cake\ORM\Association\BelongsTo $InfoNavy
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CheckingCtnTable&\Cake\ORM\Association\BelongsTo $CheckingCtn
 * @property \App\Model\Table\TraceabilityPackagingTable&\Cake\ORM\Association\BelongsTo $TraceabilityPackaging
 * @property \App\Model\Table\DetailPackagingCaffeeTable&\Cake\ORM\Association\HasMany $DetailPackagingCaffee
 *
 * @method \App\Model\Entity\PackagingCaffee newEmptyEntity()
 * @method \App\Model\Entity\PackagingCaffee newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\PackagingCaffee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PackagingCaffee get($primaryKey, $options = [])
 * @method \App\Model\Entity\PackagingCaffee findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\PackagingCaffee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PackagingCaffee[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\PackagingCaffee|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PackagingCaffee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PackagingCaffee[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\PackagingCaffee[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\PackagingCaffee[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\PackagingCaffee[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class PackagingCaffeeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('packaging_caffee');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('StatePackaging', [
            'foreignKey' => 'state_packaging_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ReadyContainer', [
            'foreignKey' => 'ready_container_id',
        ]);
        $this->belongsTo('CauseEmptiedCtn', [
            'foreignKey' => 'cause_emptied_ctn_id',
        ]);
        $this->belongsTo('InfoNavy', [
            'foreignKey' => 'info_navy_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'users_id',
        ]);
        $this->belongsTo('CheckingCtn', [
            'foreignKey' => 'checking_ctn_id',
        ]);
        $this->belongsTo('TraceabilityPackaging', [
            'foreignKey' => 'traceability_packaging_id',
        ]);
        $this->belongsTo('ReadyContainer', [
            'foreignKey' => 'ready_container_id',
        ]);
        $this->hasMany('DetailPackagingCaffee', [
            'foreignKey' => 'packaging_caffee_id',
        ]);
        $this->hasMany('DetailPackakgingCrossdocking', [
            'foreignKey' => 'packaging_caffee_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->dateTime('created_date')
            ->requirePresence('created_date', 'create')
            ->notEmptyDateTime('created_date');

        $validator
            ->boolean('weight_to_out')
            ->notEmptyString('weight_to_out');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        $validator
            ->scalar('observation')
            ->allowEmptyString('observation');

        $validator
            ->dateTime('packaging_date')
            ->allowEmptyDateTime('packaging_date');

        $validator
            ->dateTime('cancelled_date')
            ->allowEmptyDateTime('cancelled_date');

        $validator
            ->scalar('iso_ctn')
            ->maxLength('iso_ctn', 45)
            ->allowEmptyString('iso_ctn');

        $validator
            ->scalar('seal_1')
            ->maxLength('seal_1', 200)
            ->allowEmptyString('seal_1');

        $validator
            ->scalar('seal_2')
            ->maxLength('seal_2', 200)
            ->allowEmptyString('seal_2');

        $validator
            ->dateTime('seal_date')
            ->allowEmptyDateTime('seal_date');

        $validator
            ->scalar('seal1_path')
            ->maxLength('seal1_path', 100)
            ->allowEmptyString('seal1_path');

        $validator
            ->scalar('seal2_path')
            ->maxLength('seal2_path', 100)
            ->allowEmptyString('seal2_path');

        $validator
            ->scalar('img_ctn')
            ->maxLength('img_ctn', 100)
            ->allowEmptyString('img_ctn');

        $validator
            ->scalar('exporter_cod')
            ->maxLength('exporter_cod', 4)
            ->allowEmptyString('exporter_cod');

        $validator
            ->integer('weight_net_cont')
            ->allowEmptyString('weight_net_cont');

        $validator
            ->time('time_start')
            ->allowEmptyTime('time_start');

        $validator
            ->time('time_end')
            ->allowEmptyTime('time_end');

        $validator
            ->integer('autorization')
            ->allowEmptyString('autorization');

        $validator
            ->integer('qta_bags_packaging')
            ->allowEmptyString('qta_bags_packaging');

        $validator
            ->integer('rem_ref')
            ->allowEmptyString('rem_ref');

        $validator
            ->dateTime('emptied_date')
            ->allowEmptyDateTime('emptied_date');

        $validator
            ->scalar('seal_3')
            ->maxLength('seal_3', 200)
            ->allowEmptyString('seal_3');

        $validator
            ->scalar('seal_4')
            ->maxLength('seal_4', 200)
            ->allowEmptyString('seal_4');

        $validator
            ->scalar('jetty')
            ->maxLength('jetty', 300)
            ->allowEmptyString('jetty');

        $validator
            ->scalar('jornally')
            ->maxLength('jornally', 5)
            ->allowEmptyString('jornally');

        $validator
            ->dateTime('empty_date')
            ->allowEmptyDateTime('empty_date');

        $validator
            ->scalar('ref_copcsa_gang')
            ->maxLength('ref_copcsa_gang', 1)
            ->allowEmptyString('ref_copcsa_gang');

        $validator
            ->boolean('consolidate')
            ->allowEmptyString('consolidate');

        $validator
            ->scalar('long_ctn')
            ->maxLength('long_ctn', 2)
            ->allowEmptyString('long_ctn');

        $validator
            ->scalar('msg_spb_navis')
            ->allowEmptyString('msg_spb_navis');

        $validator
            ->boolean('rx_status_spb_navis')
            ->allowEmptyString('rx_status_spb_navis');

        $validator
            ->dateTime('rx_date_spb_navis')
            ->allowEmptyDateTime('rx_date_spb_navis');

        $validator
            ->dateTime('upload_tracking_photos_date')
            ->allowEmptyDateTime('upload_tracking_photos_date');

        $validator
            ->integer('user_tarja')
            ->allowEmptyString('user_tarja');

        $validator
            ->integer('user_driver')
            ->allowEmptyString('user_driver');

        $validator
            ->scalar('cooperativa_ctg')
            ->maxLength('cooperativa_ctg', 10)
            ->allowEmptyString('cooperativa_ctg');

        $validator
            ->scalar('note_opera')
            ->allowEmptyString('note_opera');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['state_packaging_id'], 'StatePackaging'));
        $rules->add($rules->existsIn(['ready_container_id'], 'ReadyContainer'));
        $rules->add($rules->existsIn(['cause_emptied_ctn_id'], 'CauseEmptiedCtn'));
        $rules->add($rules->existsIn(['info_navy_id'], 'InfoNavy'));
        $rules->add($rules->existsIn(['users_id'], 'Users'));
        $rules->add($rules->existsIn(['checking_ctn_id'], 'CheckingCtn'));
        $rules->add($rules->existsIn(['traceability_packaging_id'], 'TraceabilityPackaging'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName(): string
    {
        return 'siscafe_pro';
    }
}
