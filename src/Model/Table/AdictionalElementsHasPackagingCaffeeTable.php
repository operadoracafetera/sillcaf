<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AdictionalElementsHasPackagingCaffee Model
 *
 * @property \App\Model\Table\AdictionalElementsTable&\Cake\ORM\Association\BelongsTo $AdictionalElements
 * @property \App\Model\Table\InfoNavyTable&\Cake\ORM\Association\BelongsTo $InfoNavy
 *
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee newEmptyEntity()
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee get($primaryKey, $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\AdictionalElementsHasPackagingCaffee[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class AdictionalElementsHasPackagingCaffeeTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('adictional_elements_has_packaging_caffee');
        $this->setDisplayField('adictional_elements_id');
        $this->setPrimaryKey(['adictional_elements_id', 'info_navy_id']);

        $this->belongsTo('AdictionalElements', [
            'foreignKey' => 'adictional_elements_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('InfoNavy', [
            'foreignKey' => 'info_navy_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('SillcafUsers', [
            'foreignKey' => 'sillcaf_user_reg_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('quantity')
            ->requirePresence('quantity', 'create')
            ->notEmptyString('quantity');

        $validator
            ->scalar('type_unit')
            ->maxLength('type_unit', 45)
            ->requirePresence('type_unit', 'create')
            ->notEmptyString('type_unit');

        $validator
            ->scalar('observation')
            ->maxLength('observation', 200)
            ->allowEmptyString('observation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['adictional_elements_id'], 'AdictionalElements'));
        $rules->add($rules->existsIn(['info_navy_id'], 'InfoNavy'));
        $rules->add($rules->existsIn(['sillcaf_user_reg_id'], 'SillcafUsers'));

        return $rules;
    }
}
