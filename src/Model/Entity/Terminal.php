<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Terminal Entity
 *
 * @property int $id_terminals
 * @property string $name_terminals
 * @property string $description_terminals
 * @property \Cake\I18n\FrozenTime $reg_date_terminals
 * @property int $active_terminals
 */
class Terminal extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name_terminals' => true,
        'description_terminals' => true,
        'reg_date_terminals' => true,
        'active_terminals' => true,
    ];
}
