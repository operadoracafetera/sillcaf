<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SillcafPerfilesHasPermiss Entity
 *
 * @property int $id
 * @property int $perfil_id
 * @property int $permisse_id
 * @property \Cake\I18n\FrozenTime $reg_date
 * @property int $active
 *
 * @property \App\Model\Entity\SillcafPerfile $sillcaf_perfile
 * @property \App\Model\Entity\SillcafPermiss $sillcaf_permiss
 */
class SillcafPerfilesHasPermiss extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'perfil_id' => true,
        'permisse_id' => true,
        'reg_date' => true,
        'active' => true,
        'sillcaf_perfile' => true,
        'sillcaf_permiss' => true,
    ];
}
