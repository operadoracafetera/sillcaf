<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InfoNavyMessage Entity
 *
 * @property int $id
 * @property int $info_navy_id
 * @property string $message
 * @property \Cake\I18n\FrozenTime $reg_date
 * @property int $sillcaf_user_reg_id
 *
 * @property \App\Model\Entity\InfoNavy $info_navy
 * @property \App\Model\Entity\SillcafUser $sillcaf_user
 */
class InfoNavyMessage extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'info_navy_id' => true,
        'message' => true,
        'reg_date' => true,
        'sillcaf_user_reg_id' => true,
        'info_navy' => true,
        'sillcaf_user' => true,
    ];
}
