<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DetailPackakgingCrossdocking Entity
 *
 * @property int $id
 * @property int|null $remittances_caffee_id
 * @property int|null $packaging_caffee_id
 * @property string|null $lot_coffee
 * @property int|null $qta_coffee
 * @property \Cake\I18n\FrozenTime $reg_date
 * @property int|null $status
 *
 * @property \App\Model\Entity\RemittancesCaffee $remittances_caffee
 * @property \App\Model\Entity\PackagingCaffee $packaging_caffee
 */
class DetailPackakgingCrossdocking extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'remittances_caffee_id' => true,
        'packaging_caffee_id' => true,
        'lot_coffee' => true,
        'qta_coffee' => true,
        'reg_date' => true,
        'status' => true,
        'remittances_caffee' => true,
        'packaging_caffee' => true,
    ];
}
