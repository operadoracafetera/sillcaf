<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InfoNavy Entity
 *
 * @property int $id
 * @property string $proforma
 * @property string $packaging_type
 * @property string $motorship_name
 * @property string $packaging_mode
 * @property string $booking
 * @property string $travel_num
 * @property string $destiny
 * @property int $navy_agent_id
 * @property int $shipping_lines_id
 * @property int $customs_id
 * @property string|null $observation
 * @property \Cake\I18n\FrozenTime $created_date
 * @property string|null $elements_adicional
 * @property string|null $long_container
 * @property string|null $type_papper
 * @property int|null $adictional_dry_bags
 * @property int|null $adictional_layer_kraft_20
 * @property int|null $adictional_layer_kraft_40
 * @property int|null $adictional_layer_carton_20
 * @property int|null $adictional_layer_carton_40
 * @property int|null $adictional_num_label
 * @property int|null $adictional_num_stiker
 * @property int|null $adictional_num_manta
 * @property int|null $tsl
 * @property int|null $rejillas
 * @property int|null $dataloger
 * @property int|null $absortes
 * @property int|null $pallets
 * @property int|null $hc_qta
 * @property int|null $tls_desiccant
 * @property int|null $seals
 * @property int|null $arrivals_motorships_id
 * @property int|null $status_info_navy_id
 * @property bool|null $info_navy_active
 * @property int|null $sillcaf_user_reg_id
 * @property int|null $sillcaf_user_approve_id
 *
 * @property \App\Model\Entity\NavyAgent $navy_agent
 * @property \App\Model\Entity\ShippingLine $shipping_line
 * @property \App\Model\Entity\Custom $custom
 * @property \App\Model\Entity\ArrivalsMotorship $arrivals_motorship
 * @property \App\Model\Entity\StatusInfoNavy $status_info_navy
 * @property \App\Model\Entity\SillcafUser $sillcaf_user
 * @property \App\Model\Entity\AdictionalElementsHasPackagingCaffee[] $adictional_elements_has_packaging_caffee
 * @property \App\Model\Entity\PackagingCaffee[] $packaging_caffee
 */
class InfoNavy extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'proforma' => true,
        'packaging_type' => true,
        'motorship_name' => true,
        'packaging_mode' => true,
        'booking' => true,
        'travel_num' => true,
        'destiny' => true,
        'navy_agent_id' => true,
        'shipping_lines_id' => true,
        'customs_id' => true,
        'observation' => true,
        'created_date' => true,
        'elements_adicional' => true,
        'long_container' => true,
        'type_papper' => true,
        'adictional_dry_bags' => true,
        'adictional_layer_kraft_20' => true,
        'adictional_layer_kraft_40' => true,
        'adictional_layer_carton_20' => true,
        'adictional_layer_carton_40' => true,
        'adictional_num_label' => true,
        'adictional_num_stiker' => true,
        'adictional_num_manta' => true,
        'tsl' => true,
        'rejillas' => true,
        'dataloger' => true,
        'absortes' => true,
        'pallets' => true,
        'hc_qta' => true,
        'tls_desiccant' => true,
        'seals' => true,
        'arrivals_motorships_id' => true,
        'status_info_navy_id' => true,
        'info_navy_active' => true,
        'sillcaf_user_reg_id' => true,
        'sillcaf_user_approve_id' => true,
        'navy_agent' => true,
        'shipping_line' => true,
        'custom' => true,
        'consignee_name' => true,
        'arrivals_motorship' => true,
        'status_info_navy' => true,
        'sillcaf_user' => true,
        'adictional_elements_has_packaging_caffee' => true,
        'packaging_caffee' => true,
    ];
}
