<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AdictionalElementsHasPackagingCaffee Entity
 *
 * @property int $adictional_elements_id
 * @property int $quantity
 * @property string $type_unit
 * @property int $info_navy_id
 * @property string|null $observation
 * @property int|null $sillcaf_user_reg_id
 *
 * @property \App\Model\Entity\AdictionalElement $adictional_element
 * @property \App\Model\Entity\InfoNavy $info_navy
 */
class AdictionalElementsHasPackagingCaffee extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'quantity' => true,
        'type_unit' => true,
        'observation' => true,
        'sillcaf_user_reg_id' => true,
        'adictional_element' => true,
        'info_navy' => true,
    ];
}
