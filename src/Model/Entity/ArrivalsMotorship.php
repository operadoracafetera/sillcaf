<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArrivalsMotorship Entity
 *
 * @property int $arrivals_motorships_id
 * @property string $visit_number
 * @property string $vissel
 * @property \Cake\I18n\FrozenTime $arrival_date
 * @property int $arrivals_motorships_motorships_id
 * @property int $arrivals_motorships_terminal_id
 * @property int $arrivals_motorships_user_id
 * @property \Cake\I18n\FrozenTime $arrivals_motorships_reg_date
 * @property \Cake\I18n\FrozenTime $arrivals_motorships_update_date
 * @property int $arrivals_motorships_active
 *
 * @property \App\Model\Entity\Motorship $motorship
 * @property \App\Model\Entity\Terminal $terminal
 * @property \App\Model\Entity\User $user
 */
class ArrivalsMotorship extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'visit_number' => true,
        'vissel' => true,
        'arrival_date' => true,
        'arrivals_motorships_motorships_id' => true,
        'arrivals_motorships_terminal_id' => true,
        'arrivals_motorships_user_id' => true,
        'arrivals_motorships_reg_date' => true,
        'arrivals_motorships_update_date' => true,
        'arrivals_motorships_active' => true,
        'motorship' => true,
        'terminal' => true,
        'user' => true,
    ];

    
    protected function _getFullname()
    {
        return $this->_fields['motorship']['name_motorship'] . ' - ETA:' . $this->_fields['arrival_date']. ' - Viaje:' . $this->_fields['vissel'];
    }
}
