<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SillcafPermiss Entity
 *
 * @property int $id
 * @property string|null $permisse
 * @property \Cake\I18n\FrozenTime $reg_date
 * @property int $active
 * @property int $modules_id
 *
 * @property \App\Model\Entity\SillcafModule $sillcaf_module
 */
class SillcafPermiss extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'permisse' => true,
        'reg_date' => true,
        'active' => true,
        'modules_id' => true,
        'sillcaf_module' => true,
    ];
}
