<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SillcafUser Entity
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password
 * @property \Cake\I18n\FrozenTime $reg_date
 * @property int $perfiles_id
 * @property string|null $last_name
 * @property string|null $first_name
 * @property bool|null $active_user
 *
 * @property \App\Model\Entity\SillcafPerfile $sillcaf_perfile
 */
class SillcafUser extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'reg_date' => true,
        'perfiles_id' => true,
        'last_name' => true,
        'first_name' => true,
        'active_user' => true,
        'sillcaf_perfile' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    protected function _getFullname()
{
    return $this->_fields['first_name'] . ' ' . $this->_fields['last_name'];
}
}
