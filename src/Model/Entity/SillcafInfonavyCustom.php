<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SillcafInfonavyCustom Entity
 *
 * @property int $id
 * @property int $info_navy_id
 * @property int $custom_id
 *
 * @property \App\Model\Entity\InfoNavy $info_navy
 * @property \App\Model\Entity\Custom $custom
 */
class SillcafInfonavyCustom extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'info_navy_id' => true,
        'custom_id' => true,
        'info_navy' => true,
        'custom' => true,
    ];
}
