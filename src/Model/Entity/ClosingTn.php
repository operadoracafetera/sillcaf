<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClosingTn Entity
 *
 * @property int $closing_tn_id
 * @property \Cake\I18n\FrozenTime $closing_date
 * @property int $arrivals_motorships_id
 * @property int $closing_tn_terminal_id
 * @property int $closing_tn_user_id
 * @property \Cake\I18n\FrozenTime $closing_tn_reg_date
 * @property \Cake\I18n\FrozenTime $closing_tn_update_date
 *
 * @property \App\Model\Entity\NavyAgent $navy_agent
 * @property \App\Model\Entity\ArrivalsMotorship $arrivals_motorship
 * @property \App\Model\Entity\Terminal $terminal
 * @property \App\Model\Entity\SillcafUser $sillcaf_user
 */
class ClosingTn extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'closing_date' => true,
        'arrivals_motorships_id' => true,
        'closing_tn_terminal_id' => true,
        'closing_tn_user_id' => true,
        'closing_tn_reg_date' => true,
        'closing_tn_update_date' => true,
        'navy_agent' => true,
        'arrivals_motorship' => true,
        'terminal' => true,
        'sillcaf_user' => true,
    ];
}
