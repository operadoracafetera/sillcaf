<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClosingCn Entity
 *
 * @property int $closing_cn_id
 * @property \Cake\I18n\FrozenTime $closing_date
 * @property int $closing_cn_navy_agent_id
 * @property int $arrivals_motorships_id
 * @property int $closing_cn_user_id
 * @property \Cake\I18n\FrozenTime $closing_cn_reg_date
 * @property \Cake\I18n\FrozenTime $closing_cn_update_date
 *
 * @property \App\Model\Entity\Custom $custom
 * @property \App\Model\Entity\ShippingLine $shipping_line
 * @property \App\Model\Entity\NavyAgent $navy_agent
 * @property \App\Model\Entity\ArrivalsMotorship $arrivals_motorship
 * @property \App\Model\Entity\SillcafUser $sillcaf_user
 */
class ClosingCn extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'closing_date' => true,
        'closing_cn_navy_agent_id' => true,
        'arrivals_motorships_id' => true,
        'closing_cn_user_id' => true,
        'closing_cn_reg_date' => true,
        'closing_cn_update_date' => true,
        'custom' => true,
        'shipping_line' => true,
        'navy_agent' => true,
        'arrivals_motorship' => true,
        'sillcaf_user' => true,
    ];

    protected function _getFullname()
    {
        return $this->_fields['arrivals_motorship']['motorship']['name_motorship'] . ' - ETA:' . $this->_fields['arrivals_motorship']['arrival_date']. ' - Viaje:' . $this->_fields['arrivals_motorship']['vissel'];
    }
}
