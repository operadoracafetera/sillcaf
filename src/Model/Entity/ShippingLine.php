<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ShippingLine Entity
 *
 * @property int $id
 * @property string $business_name
 * @property string|null $description
 * @property \Cake\I18n\FrozenTime $created_date
 * @property \Cake\I18n\FrozenTime $updated_date
 * @property int|null $codigo_spb
 */
class ShippingLine extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'business_name' => true,
        'description' => true,
        'created_date' => true,
        'updated_date' => true,
        'codigo_spb' => true,
    ];
}
