<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Client Entity
 *
 * @property int $id
 * @property string $business_name
 * @property string $emails
 * @property string|null $contact_name
 * @property string|null $exporter_code
 * @property string $phone
 * @property string $city_location
 * @property \Cake\I18n\FrozenTime $created_date
 * @property \Cake\I18n\FrozenTime $updated_date
 * @property int|null $price_download_bag
 * @property bool|null $notify_email
 * @property string|null $nit
 * @property int|null $price_bag_stm
 *
 * @property \App\Model\Entity\CoffeeSample[] $coffee_sample
 * @property \App\Model\Entity\RemittancesCaffee[] $remittances_caffee
 * @property \App\Model\Entity\ReturnsCaffee[] $returns_caffees
 */
class Client extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'business_name' => true,
        'emails' => true,
        'contact_name' => true,
        'exporter_code' => true,
        'phone' => true,
        'city_location' => true,
        'created_date' => true,
        'updated_date' => true,
        'price_download_bag' => true,
        'notify_email' => true,
        'nit' => true,
        'price_bag_stm' => true,
        'coffee_sample' => true,
        'remittances_caffee' => true,
        'returns_caffees' => true,
    ];
}
