<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DetailPackagingCaffee Entity
 *
 * @property int $remittances_caffee_id
 * @property int $packaging_caffee_id
 * @property int $quantity_radicated_bag_out
 * @property \Cake\I18n\FrozenTime $created_date
 * @property \Cake\I18n\FrozenTime $updated_date
 * @property string $state
 * @property int $tara_packaging
 * @property int|null $users_id
 * @property string|null $lot_coffee
 *
 * @property \App\Model\Entity\RemittancesCaffee $remittances_caffee
 * @property \App\Model\Entity\PackagingCaffee $packaging_caffee
 * @property \App\Model\Entity\User $user
 */
class DetailPackagingCaffee extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'quantity_radicated_bag_out' => true,
        'created_date' => true,
        'updated_date' => true,
        'state' => true,
        'tara_packaging' => true,
        'users_id' => true,
        'lot_coffee' => true,
        'remittances_caffee' => true,
        'packaging_caffee' => true,
        'user' => true,
    ];
}
