<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SillcafLog Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $date_event
 * @property string|null $module_name
 * @property string|null $data_before
 * @property string|null $data_after
 * @property string|null $ip_source
 * @property int $users_id
 *
 * @property \App\Model\Entity\SillcafPerfile $sillcaf_perfile
 * @property \App\Model\Entity\SillcafUser $sill_caf_user
 */
class SillcafLog extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'date_event' => true,
        'module_name' => true,
        'data_before' => true,
        'data_after' => true,
        'ip_source' => true,
        'users_id' => true,
        'sillcaf_perfile' => true,
        'sill_caf_user' => true,
    ];
}
