<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RemittancesCaffee Entity
 *
 * @property int $id
 * @property string $lot_caffee
 * @property int|null $quantity_bag_in_store
 * @property int|null $quantity_bag_out_store
 * @property int|null $quantity_in_pallet_caffee
 * @property int|null $quantity_out_pallet_caffee
 * @property int|null $quantity_radicated_bag_in
 * @property int|null $quantity_radicated_bag_out
 * @property float|null $total_weight_net_nominal
 * @property float|null $total_weight_net_real
 * @property float|null $tare_download
 * @property float|null $tare_packaging
 * @property int|null $source_location
 * @property string|null $nota_entrega
 * @property string|null $vehicle_plate
 * @property \Cake\I18n\FrozenTime|null $download_caffee_date
 * @property \Cake\I18n\FrozenTime|null $packaging_caffee_date
 * @property \Cake\I18n\FrozenTime $created_date
 * @property \Cake\I18n\FrozenTime|null $updated_dated
 * @property bool $is_active
 * @property string|null $guide_id
 * @property int $client_id
 * @property int $units_cafee_id
 * @property int $packing_cafee_id
 * @property int|null $mark_cafee_id
 * @property int|null $custom_id
 * @property int|null $city_source_id
 * @property int|null $shippers_id
 * @property int|null $slot_store_id
 * @property int|null $staff_sample_id
 * @property int|null $staff_wt_in_id
 * @property int|null $staff_wt_out_id
 * @property int|null $staff_driver_id
 * @property string|null $observation
 * @property string|null $details_weight
 * @property int|null $total_tare
 * @property float|null $weight_pallet_packaging_total
 * @property int $state_operation_id
 * @property int|null $novelty_in_caffee_id
 * @property int|null $traceability_download_id
 * @property int $type_units_id
 * @property int|null $total_rad_bag_out
 * @property string|null $ref_packaging
 * @property string|null $seals
 * @property int $user_register
 * @property string $jetty
 * @property string|null $cargolot_id
 * @property string|null $ref_driver
 * @property string|null $microlot
 * @property string|null $type_packaging_coffee
 * @property string|null $coffee_to_fumigation
 * @property string|null $material
 * @property int|null $valor_descargue
 * @property \Cake\I18n\FrozenDate|null $date_expedition_guide
 * @property string|null $ref_copcsa_gang
 * @property string|null $document_number_sapmigo_fnc
 * @property string|null $sync_msg_sapmigo_fnc
 * @property \Cake\I18n\FrozenTime|null $sync_date_sapmigo_fnc
 * @property string|null $sync_data_sapmigo_fnc
 * @property string|null $msg_spb_navis
 * @property bool|null $rx_status_spb_navis
 * @property \Cake\I18n\FrozenTime|null $rx_date_spb_navis
 * @property bool $coffee_with_packging_anormal
 * @property float $tare_packing_coffee_anormal
 * @property string|null $cooperativa_cta
 * @property \Cake\I18n\FrozenTime|null $ini_ctg_download
 * @property \Cake\I18n\FrozenTime|null $end_ctg_download
 * @property string|null $machine_download_ctg
 * @property string|null $plague_control_ctg
 * @property string|null $sample_req_ctg
 * @property int|null $ref_bulk_upload
 * @property int|null $bill_id
 *
 * @property \App\Model\Entity\Guide $guide
 * @property \App\Model\Entity\Client $client
 * @property \App\Model\Entity\UnitsCaffee $units_caffee
 * @property \App\Model\Entity\PackingCaffee $packing_caffee
 * @property \App\Model\Entity\MarkCaffee $mark_caffee
 * @property \App\Model\Entity\Custom $custom
 * @property \App\Model\Entity\CitySource $city_source
 * @property \App\Model\Entity\Shipper $shipper
 * @property \App\Model\Entity\SlotStore $slot_store
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\StateOperation $state_operation
 * @property \App\Model\Entity\NoveltyInCaffee $novelty_in_caffee
 * @property \App\Model\Entity\TraceabilityDownload $traceability_download
 * @property \App\Model\Entity\TypeUnit $type_unit
 * @property \App\Model\Entity\Cargolot $cargolot
 * @property \App\Model\Entity\Bill $bill
 * @property \App\Model\Entity\DetailPackagingCaffee[] $detail_packaging_caffee
 * @property \App\Model\Entity\DetailPackakgingCrossdocking[] $detail_packakging_crossdocking
 * @property \App\Model\Entity\DetailsServicesToCaffee[] $details_services_to_caffee
 * @property \App\Model\Entity\RemittancesCaffeeHasFumigationService[] $remittances_caffee_has_fumigation_services
 * @property \App\Model\Entity\RemittancesCaffeeHasNoveltysCaffee[] $remittances_caffee_has_noveltys_caffee
 * @property \App\Model\Entity\SampleRemmitance[] $sample_remmitances
 * @property \App\Model\Entity\WeighingDownloadCaffee[] $weighing_download_caffee
 * @property \App\Model\Entity\WeighingEmptyCoffee[] $weighing_empty_coffee
 * @property \App\Model\Entity\WeighingReturnCoffee[] $weighing_return_coffee
 * @property \App\Model\Entity\ReturnsCaffee[] $returns_caffees
 */
class RemittancesCaffee extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'lot_caffee' => true,
        'quantity_bag_in_store' => true,
        'quantity_bag_out_store' => true,
        'quantity_in_pallet_caffee' => true,
        'quantity_out_pallet_caffee' => true,
        'quantity_radicated_bag_in' => true,
        'quantity_radicated_bag_out' => true,
        'total_weight_net_nominal' => true,
        'total_weight_net_real' => true,
        'tare_download' => true,
        'tare_packaging' => true,
        'source_location' => true,
        'nota_entrega' => true,
        'vehicle_plate' => true,
        'download_caffee_date' => true,
        'packaging_caffee_date' => true,
        'created_date' => true,
        'updated_dated' => true,
        'is_active' => true,
        'guide_id' => true,
        'client_id' => true,
        'units_cafee_id' => true,
        'packing_cafee_id' => true,
        'mark_cafee_id' => true,
        'custom_id' => true,
        'city_source_id' => true,
        'shippers_id' => true,
        'slot_store_id' => true,
        'staff_sample_id' => true,
        'staff_wt_in_id' => true,
        'staff_wt_out_id' => true,
        'staff_driver_id' => true,
        'observation' => true,
        'details_weight' => true,
        'total_tare' => true,
        'weight_pallet_packaging_total' => true,
        'state_operation_id' => true,
        'novelty_in_caffee_id' => true,
        'traceability_download_id' => true,
        'type_units_id' => true,
        'total_rad_bag_out' => true,
        'ref_packaging' => true,
        'seals' => true,
        'user_register' => true,
        'jetty' => true,
        'cargolot_id' => true,
        'ref_driver' => true,
        'microlot' => true,
        'type_packaging_coffee' => true,
        'coffee_to_fumigation' => true,
        'material' => true,
        'valor_descargue' => true,
        'date_expedition_guide' => true,
        'ref_copcsa_gang' => true,
        'document_number_sapmigo_fnc' => true,
        'sync_msg_sapmigo_fnc' => true,
        'sync_date_sapmigo_fnc' => true,
        'sync_data_sapmigo_fnc' => true,
        'msg_spb_navis' => true,
        'rx_status_spb_navis' => true,
        'rx_date_spb_navis' => true,
        'coffee_with_packging_anormal' => true,
        'tare_packing_coffee_anormal' => true,
        'cooperativa_cta' => true,
        'ini_ctg_download' => true,
        'end_ctg_download' => true,
        'machine_download_ctg' => true,
        'plague_control_ctg' => true,
        'sample_req_ctg' => true,
        'ref_bulk_upload' => true,
        'bill_id' => true,
        'guide' => true,
        'client' => true,
        'units_caffee' => true,
        'packing_caffee' => true,
        'mark_caffee' => true,
        'custom' => true,
        'city_source' => true,
        'shipper' => true,
        'slot_store' => true,
        'user' => true,
        'state_operation' => true,
        'novelty_in_caffee' => true,
        'traceability_download' => true,
        'type_unit' => true,
        'cargolot' => true,
        'bill' => true,
        'detail_packaging_caffee' => true,
        'detail_packakging_crossdocking' => true,
        'details_services_to_caffee' => true,
        'remittances_caffee_has_fumigation_services' => true,
        'remittances_caffee_has_noveltys_caffee' => true,
        'sample_remmitances' => true,
        'weighing_download_caffee' => true,
        'weighing_empty_coffee' => true,
        'weighing_return_coffee' => true,
        'returns_caffees' => true,
    ];
}
