<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ViewPackaging Entity
 *
 * @property int $oie
 * @property string|null $jetty
 * @property \Cake\I18n\FrozenTime|null $upload_tracking_photos_date
 * @property int|null $traceability_packaging_id
 * @property \Cake\I18n\FrozenTime|null $empty_date
 * @property string|null $seal1_path
 * @property string|null $seal2_path
 * @property string|null $img_ctn
 * @property int $navy_agent_id
 * @property int $shipping_lines_id
 * @property string $estado_embalaje
 * @property string $bic_ctn
 * @property string|null $lotes1
 * @property int $state_packaging_id
 * @property string|null $total_sacos
 * @property string|null $lotes
 * @property string|null $cargolot
 * @property string $booking
 * @property string $proforma
 * @property string $travel_num
 * @property string $cia_name
 * @property string $business_name
 * @property string|null $exportador
 * @property string|null $expotador_code
 * @property int|null $expo_id2
 * @property string|null $expo_id1
 * @property string $agente_naviero
 * @property string $packaging_mode
 * @property string $packaging_type
 * @property string|null $seal_1
 * @property string|null $seal_2
 * @property \Cake\I18n\FrozenTime|null $packaging_date
 * @property string|null $iso_ctn
 * @property float|null $total_cafe_empaque
 * @property int|null $total_pallets_remesa
 * @property int|null $total_pallets_embalaje
 * @property float|null $tare_estibas_descargue
 * @property float|null $tare_empaque
 * @property string|null $elementos_adicionales2
 * @property string|null $elementos_adicionales
 * @property string|null $tipo_papel
 * @property int|null $dry_bags
 * @property int|null $capas_adiconales_kraft20
 * @property int|null $capas_adiconales_kraft40
 * @property int|null $capas_adiconales_carton20
 * @property int|null $capas_adiconales_carton40
 * @property int|null $instalacion_etiqueta
 * @property int|null $calcomanias
 * @property int|null $manta_termica
 * @property string $motorship_name
 * @property string|null $cooperativa
 * @property string|null $observation
 *
 * @property \App\Model\Entity\TraceabilityPackaging $traceability_packaging
 * @property \App\Model\Entity\NavyAgent $navy_agent
 * @property \App\Model\Entity\ShippingLine $shipping_line
 * @property \App\Model\Entity\StatePackaging $state_packaging
 */
class ViewPackaging extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'oie' => true,
        'jetty' => true,
        'upload_tracking_photos_date' => true,
        'traceability_packaging_id' => true,
        'empty_date' => true,
        'seal1_path' => true,
        'seal2_path' => true,
        'img_ctn' => true,
        'navy_agent_id' => true,
        'shipping_lines_id' => true,
        'estado_embalaje' => true,
        'bic_ctn' => true,
        'lotes1' => true,
        'state_packaging_id' => true,
        'total_sacos' => true,
        'lotes' => true,
        'cargolot' => true,
        'booking' => true,
        'proforma' => true,
        'travel_num' => true,
        'cia_name' => true,
        'business_name' => true,
        'exportador' => true,
        'expotador_code' => true,
        'expo_id2' => true,
        'expo_id1' => true,
        'agente_naviero' => true,
        'packaging_mode' => true,
        'packaging_type' => true,
        'seal_1' => true,
        'seal_2' => true,
        'packaging_date' => true,
        'iso_ctn' => true,
        'total_cafe_empaque' => true,
        'total_pallets_remesa' => true,
        'total_pallets_embalaje' => true,
        'tare_estibas_descargue' => true,
        'tare_empaque' => true,
        'elementos_adicionales2' => true,
        'elementos_adicionales' => true,
        'tipo_papel' => true,
        'dry_bags' => true,
        'capas_adiconales_kraft20' => true,
        'capas_adiconales_kraft40' => true,
        'capas_adiconales_carton20' => true,
        'capas_adiconales_carton40' => true,
        'instalacion_etiqueta' => true,
        'calcomanias' => true,
        'manta_termica' => true,
        'motorship_name' => true,
        'cooperativa' => true,
        'observation' => true,
        'traceability_packaging' => true,
        'navy_agent' => true,
        'shipping_line' => true,
        'state_packaging' => true,
    ];
}
