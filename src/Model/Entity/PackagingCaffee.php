<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PackagingCaffee Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created_date
 * @property bool $weight_to_out
 * @property int $state_packaging_id
 * @property int|null $ready_container_id
 * @property bool $active
 * @property int|null $cause_emptied_ctn_id
 * @property int $info_navy_id
 * @property string|null $observation
 * @property \Cake\I18n\FrozenTime|null $packaging_date
 * @property \Cake\I18n\FrozenTime|null $cancelled_date
 * @property string|null $iso_ctn
 * @property string|null $seal_1
 * @property string|null $seal_2
 * @property int|null $users_id
 * @property \Cake\I18n\FrozenTime|null $seal_date
 * @property string|null $seal1_path
 * @property string|null $seal2_path
 * @property string|null $img_ctn
 * @property int|null $checking_ctn_id
 * @property int|null $traceability_packaging_id
 * @property string|null $exporter_cod
 * @property int|null $weight_net_cont
 * @property \Cake\I18n\FrozenTime|null $time_start
 * @property \Cake\I18n\FrozenTime|null $time_end
 * @property int|null $autorization
 * @property int|null $qta_bags_packaging
 * @property int|null $rem_ref
 * @property \Cake\I18n\FrozenTime|null $emptied_date
 * @property string|null $seal_3
 * @property string|null $seal_4
 * @property string|null $jetty
 * @property string|null $jornally
 * @property \Cake\I18n\FrozenTime|null $empty_date
 * @property string|null $ref_copcsa_gang
 * @property bool|null $consolidate
 * @property string|null $long_ctn
 * @property string|null $msg_spb_navis
 * @property bool|null $rx_status_spb_navis
 * @property \Cake\I18n\FrozenTime|null $rx_date_spb_navis
 * @property \Cake\I18n\FrozenTime|null $upload_tracking_photos_date
 * @property int|null $user_tarja
 * @property int|null $user_driver
 * @property string|null $cooperativa_ctg
 * @property string|null $note_opera
 *
 * @property \App\Model\Entity\StatePackaging $state_packaging
 * @property \App\Model\Entity\ReadyContainer $ready_container
 * @property \App\Model\Entity\CauseEmptiedCtn $cause_emptied_ctn
 * @property \App\Model\Entity\InfoNavy $info_navy
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\CheckingCtn $checking_ctn
 * @property \App\Model\Entity\TraceabilityPackaging $traceability_packaging
 * @property \App\Model\Entity\DetailPackagingCaffee[] $detail_packaging_caffee
 */
class PackagingCaffee extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created_date' => true,
        'weight_to_out' => true,
        'state_packaging_id' => true,
        'ready_container_id' => true,
        'active' => true,
        'cause_emptied_ctn_id' => true,
        'info_navy_id' => true,
        'observation' => true,
        'packaging_date' => true,
        'cancelled_date' => true,
        'iso_ctn' => true,
        'seal_1' => true,
        'seal_2' => true,
        'users_id' => true,
        'seal_date' => true,
        'seal1_path' => true,
        'seal2_path' => true,
        'img_ctn' => true,
        'checking_ctn_id' => true,
        'traceability_packaging_id' => true,
        'exporter_cod' => true,
        'weight_net_cont' => true,
        'time_start' => true,
        'time_end' => true,
        'autorization' => true,
        'qta_bags_packaging' => true,
        'rem_ref' => true,
        'emptied_date' => true,
        'seal_3' => true,
        'seal_4' => true,
        'jetty' => true,
        'jornally' => true,
        'empty_date' => true,
        'ref_copcsa_gang' => true,
        'consolidate' => true,
        'long_ctn' => true,
        'msg_spb_navis' => true,
        'rx_status_spb_navis' => true,
        'rx_date_spb_navis' => true,
        'upload_tracking_photos_date' => true,
        'user_tarja' => true,
        'user_driver' => true,
        'cooperativa_ctg' => true,
        'note_opera' => true,
        'state_packaging' => true,
        'ready_container' => true,
        'cause_emptied_ctn' => true,
        'info_navy' => true,
        'user' => true,
        'checking_ctn' => true,
        'traceability_packaging' => true,
        'detail_packaging_caffee' => true,
    ];
}
