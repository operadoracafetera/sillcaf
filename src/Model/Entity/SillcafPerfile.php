<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SillcafPerfile Entity
 *
 * @property int $id
 * @property string|null $perfiles_name
 * @property string $perfiles_description
 * @property \Cake\I18n\FrozenTime $perfiles_reg_date
 * @property bool $perfiles_active
 */
class SillcafPerfile extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'perfiles_name' => true,
        'perfiles_description' => true,
        'perfiles_reg_date' => true,
        'perfiles_active' => true,
    ];
}
