<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Motorship Entity
 *
 * @property int $id_motorship
 * @property string $name_motorship
 * @property string $description_motorship
 * @property int $shippingline_id
 * @property \Cake\I18n\FrozenTime $reg_date_motorship
 * @property int $active_motorship
 *
 * @property \App\Model\Entity\ShippingLine $shipping_line
 */
class Motorship extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name_motorship' => true,
        'description_motorship' => true,
        'shippingline_id' => true,
        'reg_date_motorship' => true,
        'active_motorship' => true,
        'shipping_line' => true,
    ];
}
